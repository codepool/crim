/*
 Navicat Premium Data Transfer

 Source Server         : localhost3306
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : zyzy-im

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 11/01/2024 15:07:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for im_classify
-- ----------------------------
DROP TABLE IF EXISTS `im_classify`;
CREATE TABLE `im_classify`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1：好友，2：群组',
  `sort_key` tinyint(2) NULL DEFAULT NULL COMMENT '排序id',
  `classify_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '归类名称',
  `created_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '归类' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of im_classify
-- ----------------------------
INSERT INTO `im_classify` VALUES (1, 3, 1, 0, '航班动态', '2023-12-19 23:09:38');
INSERT INTO `im_classify` VALUES (2, 3, 1, 1, '机组讨论', '2023-12-19 23:10:03');
INSERT INTO `im_classify` VALUES (3, 3, 0, 2, '大学同学', '2023-12-19 23:11:30');
INSERT INTO `im_classify` VALUES (4, 3, 0, 3, '信息技术部', '2023-12-19 23:11:41');
INSERT INTO `im_classify` VALUES (5, 3, 0, 4, '黑名单', '2023-12-19 23:11:52');
INSERT INTO `im_classify` VALUES (6, 3, 0, 5, '陌生人', '2023-12-19 23:12:02');
INSERT INTO `im_classify` VALUES (7, 1, 1, 0, '航班动态', '2023-12-26 14:27:55');
INSERT INTO `im_classify` VALUES (9, 3, 1, 6, 'IT部门', '2023-12-26 15:10:35');
INSERT INTO `im_classify` VALUES (10, 3, 0, 0, '公司领导', '2023-12-26 15:11:03');

-- ----------------------------
-- Table structure for im_friend
-- ----------------------------
DROP TABLE IF EXISTS `im_friend`;
CREATE TABLE `im_friend`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `friend_id` bigint(20) NOT NULL COMMENT '好友id',
  `friend_nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '好友昵称',
  `friend_head_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '好友头像',
  `created_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `remark_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '好友备注',
  `classify_id` bigint(20) NULL DEFAULT 0 COMMENT '归类ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE,
  INDEX `idx_friend_id`(`friend_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '好友' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of im_friend
-- ----------------------------
INSERT INTO `im_friend` VALUES (1, 2, 1, '周先生', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '2023-09-26 16:41:34', NULL, 0);
INSERT INTO `im_friend` VALUES (2, 1, 2, '张三', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231008/1696743035532.jpg', '2023-09-26 16:41:34', NULL, 0);
INSERT INTO `im_friend` VALUES (3, 1, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '2023-10-08 14:00:45', NULL, 0);
INSERT INTO `im_friend` VALUES (4, 3, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '2023-10-08 14:00:45', NULL, 0);
INSERT INTO `im_friend` VALUES (5, 4, 2, '张三', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231008/1696743035532.jpg', '2023-10-08 22:04:57', NULL, 0);
INSERT INTO `im_friend` VALUES (6, 2, 4, '哈哈', '', '2023-10-08 22:04:57', NULL, 0);
INSERT INTO `im_friend` VALUES (7, 3, 2, '张三', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743035532.jpg', '2023-10-18 06:14:00', NULL, 0);
INSERT INTO `im_friend` VALUES (8, 2, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '2023-10-18 06:14:00', NULL, 0);
INSERT INTO `im_friend` VALUES (9, 6, 5, 'psl', '', '2023-11-01 17:10:23', NULL, 0);
INSERT INTO `im_friend` VALUES (10, 5, 6, 'psl2', '', '2023-11-01 17:10:23', NULL, 0);
INSERT INTO `im_friend` VALUES (11, 3, 5, 'psl', '', '2023-11-14 15:03:13', NULL, 10);
INSERT INTO `im_friend` VALUES (12, 5, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '2023-11-14 15:03:13', NULL, 0);
INSERT INTO `im_friend` VALUES (13, 8, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '2023-12-08 14:23:53', NULL, 0);
INSERT INTO `im_friend` VALUES (14, 1, 8, '王麻子', '', '2023-12-08 14:23:53', NULL, 0);
INSERT INTO `im_friend` VALUES (15, 8, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '2023-12-08 14:24:03', NULL, 0);
INSERT INTO `im_friend` VALUES (16, 3, 8, '王麻子', '', '2023-12-08 14:24:03', NULL, 0);
INSERT INTO `im_friend` VALUES (17, 8, 6, 'psl2', '', '2023-12-08 14:24:12', NULL, 0);
INSERT INTO `im_friend` VALUES (18, 6, 8, '王麻子', '', '2023-12-08 14:24:12', NULL, 0);
INSERT INTO `im_friend` VALUES (19, 8, 5, 'psl', '', '2023-12-08 14:24:13', NULL, 0);
INSERT INTO `im_friend` VALUES (20, 5, 8, '王麻子', '', '2023-12-08 14:24:13', NULL, 0);
INSERT INTO `im_friend` VALUES (21, 3, 6, 'psl2', '', '2023-12-19 23:12:14', NULL, 4);
INSERT INTO `im_friend` VALUES (22, 6, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '2023-12-19 23:12:14', NULL, 0);
INSERT INTO `im_friend` VALUES (25, 21, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (26, 1, 21, 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (27, 21, 2, '张三', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743034631.jpg', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (28, 2, 21, 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (29, 21, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (30, 3, 21, 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (31, 21, 4, '哈哈', '', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (32, 4, 21, 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (33, 21, 5, 'psl', '', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (34, 5, 21, 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (35, 21, 6, 'psl2', '', '2024-01-08 17:44:33', NULL, 0);
INSERT INTO `im_friend` VALUES (36, 6, 21, 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:34', NULL, 0);
INSERT INTO `im_friend` VALUES (37, 21, 7, '李四', '', '2024-01-08 17:44:34', NULL, 0);
INSERT INTO `im_friend` VALUES (38, 7, 21, 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', '2024-01-08 17:44:34', NULL, 0);

-- ----------------------------
-- Table structure for im_group
-- ----------------------------
DROP TABLE IF EXISTS `im_group`;
CREATE TABLE `im_group`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '群名字',
  `owner_id` bigint(20) NOT NULL COMMENT '群主id',
  `head_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '群头像',
  `head_image_thumb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '群头像缩略图',
  `notice` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '群公告',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '群备注',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否已删除',
  `created_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '群' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of im_group
-- ----------------------------
INSERT INTO `im_group` VALUES (1, '测试群聊', 1, 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743821160.png', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743821169.png', '这是群公告哟！这是群公告哟！这是群公告哟！这是群公告哟！', '备注啊', 0, '2023-10-08 13:59:11');
INSERT INTO `im_group` VALUES (2, '测试群二', 2, '', '', '今天大家都要打开哟', '', 0, '2023-12-08 14:27:33');
INSERT INTO `im_group` VALUES (3, '干饭群', 1, '', '', '这是群公告哟！', '', 0, '2023-12-08 14:28:53');
INSERT INTO `im_group` VALUES (4, '开发群一', 1, '', '', '这是群公告哟！', '', 0, '2023-12-08 14:29:19');
INSERT INTO `im_group` VALUES (5, 'zhouyz创建的群聊', 1, 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '', '', 0, '2023-12-13 16:15:46');
INSERT INTO `im_group` VALUES (6, '测试群', 3, 'https://im.zyzy.team/minio_api/zyzy-im/image/20231213/1702455390346.jpg', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231213/1702455390919.jpg', '这才是群公告哟', '', 0, '2023-12-13 16:16:33');
INSERT INTO `im_group` VALUES (7, 'G3686航班', 3, '', '', '入群者，请修改备注为：部门+姓名！', '这是航班讨论群', 0, '2023-12-19 23:10:52');

-- ----------------------------
-- Table structure for im_group_member
-- ----------------------------
DROP TABLE IF EXISTS `im_group_member`;
CREATE TABLE `im_group_member`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group_id` bigint(20) NOT NULL COMMENT '群id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `alias_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '组内显示名称',
  `head_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户头像',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
  `quit` tinyint(1) NULL DEFAULT 0 COMMENT '是否已退出',
  `created_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `classify_id` bigint(20) NULL DEFAULT 0 COMMENT '归类ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_group_id`(`group_id`) USING BTREE,
  INDEX `idx_user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '群成员' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of im_group_member
-- ----------------------------
INSERT INTO `im_group_member` VALUES (1, 1, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '测试群聊', 0, '2023-10-08 13:59:11', 0);
INSERT INTO `im_group_member` VALUES (2, 1, 2, '张三', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743035532.jpg', '测试群聊', 0, '2023-10-08 13:59:25', 0);
INSERT INTO `im_group_member` VALUES (3, 1, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '测试群聊', 0, '2023-10-08 14:00:54', 0);
INSERT INTO `im_group_member` VALUES (4, 1, 5, 'psl', '', '测试群聊', 0, '2023-11-14 16:04:54', 0);
INSERT INTO `im_group_member` VALUES (5, 2, 2, '张三', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743035532.jpg', '测试群二', 0, '2023-12-08 14:27:33', 0);
INSERT INTO `im_group_member` VALUES (6, 3, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '干饭群', 0, '2023-12-19 23:21:28', 0);
INSERT INTO `im_group_member` VALUES (7, 3, 2, '张三', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743035532.jpg', '干饭群', 0, '2023-12-19 23:21:28', 0);
INSERT INTO `im_group_member` VALUES (8, 3, 8, '王麻子', '', '干饭群', 0, '2023-12-08 14:29:06', 0);
INSERT INTO `im_group_member` VALUES (9, 3, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '干饭', 0, '2023-12-08 14:29:06', 9);
INSERT INTO `im_group_member` VALUES (10, 4, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '开发群一', 0, '2023-12-08 14:29:19', 0);
INSERT INTO `im_group_member` VALUES (11, 4, 2, '张三', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231008/1696743035532.jpg', '开发群一', 0, '2023-12-08 14:29:42', 0);
INSERT INTO `im_group_member` VALUES (12, 4, 3, '开发者', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '开发群一', 0, '2023-12-08 14:29:42', 0);
INSERT INTO `im_group_member` VALUES (13, 5, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', 'zhouyz创建的群聊', 0, '2023-12-13 16:15:46', 0);
INSERT INTO `im_group_member` VALUES (14, 5, 3, '开发者', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', 'zhouyz创建的群聊', 0, '2023-12-13 16:15:54', 0);
INSERT INTO `im_group_member` VALUES (15, 5, 2, '张三', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231008/1696743035532.jpg', 'zhouyz创建的群聊', 0, '2023-12-13 16:15:54', 0);
INSERT INTO `im_group_member` VALUES (16, 5, 8, '王麻子', '', 'zhouyz创建的群聊', 0, '2023-12-13 16:15:54', 0);
INSERT INTO `im_group_member` VALUES (17, 6, 3, 'zhouy', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '这是群备注哟，不是群公告', 0, '2023-12-13 16:16:33', 0);
INSERT INTO `im_group_member` VALUES (18, 6, 1, '周先生', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', 'admin创建的群聊', 0, '2023-12-13 16:16:41', 0);
INSERT INTO `im_group_member` VALUES (19, 6, 2, '张三', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231008/1696743035532.jpg', 'admin创建的群聊', 0, '2023-12-13 16:16:41', 0);
INSERT INTO `im_group_member` VALUES (20, 6, 5, 'psl', '', 'admin创建的群聊', 0, '2023-12-13 16:16:41', 0);
INSERT INTO `im_group_member` VALUES (21, 6, 8, '王麻子', '', 'admin创建的群聊', 0, '2023-12-13 16:16:41', 0);
INSERT INTO `im_group_member` VALUES (22, 7, 3, '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '这是航班讨论群', 0, '2023-12-19 23:10:52', 1);
INSERT INTO `im_group_member` VALUES (23, 7, 1, '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', 'G3686航班', 0, '2023-12-19 23:10:52', 0);
INSERT INTO `im_group_member` VALUES (24, 7, 5, 'psl', '', 'G3686航班', 0, '2023-12-19 23:10:52', 0);
INSERT INTO `im_group_member` VALUES (25, 7, 8, '王麻子', '', 'G3686航班', 0, '2023-12-19 23:10:52', 0);

-- ----------------------------
-- Table structure for im_group_message
-- ----------------------------
DROP TABLE IF EXISTS `im_group_message`;
CREATE TABLE `im_group_message`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group_id` bigint(20) NOT NULL COMMENT '群id',
  `send_id` bigint(20) NOT NULL COMMENT '发送用户id',
  `send_nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '发送用户昵称',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '发送内容',
  `at_user_ids` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '被@的用户id列表，逗号分隔',
  `type` tinyint(1) NOT NULL COMMENT '消息类型 0:文字 1:图片 2:文件 3:语音 10:系统提示',
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '状态 0:正常  2:撤回',
  `send_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '发送时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 167 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '群消息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of im_group_message
-- ----------------------------
INSERT INTO `im_group_message` VALUES (1, 1, 3, '', '54544554', NULL, 0, 0, '2023-10-08 14:01:14');
INSERT INTO `im_group_message` VALUES (2, 1, 2, '', '#呲牙;', NULL, 0, 0, '2023-10-08 16:46:15');
INSERT INTO `im_group_message` VALUES (3, 1, 1, '', '说', NULL, 0, 0, '2023-11-30 17:08:13');
INSERT INTO `im_group_message` VALUES (143, 3, 3, '开发者', '{\"name\":\"voice_record_temp_3_2_.wav\",\"duration\":2,\"url\":\"https://im.zyzy.team/minio_api/zyzy-im/file/20231214/1702548346074.wav\"}', NULL, 3, 0, '2023-12-14 18:05:46');
INSERT INTO `im_group_message` VALUES (144, 7, 3, '周游舟', '明天航班有重要客人，大家提前准备', NULL, 0, 0, '2023-12-19 23:14:42');
INSERT INTO `im_group_message` VALUES (149, 7, 3, '周游舟', '{\"name\":\"voice_record_temp_7_3_.wav\",\"duration\":3,\"url\":\"https://im.zyzy.team/minio_api/zyzy-im/file/20231221/1703132370378.wav\"}', NULL, 3, 0, '2023-12-21 12:19:31');
INSERT INTO `im_group_message` VALUES (150, 7, 3, '周游舟', '不好意思，刚才我是测试网络情况？', NULL, 0, 0, '2023-12-21 12:28:56');
INSERT INTO `im_group_message` VALUES (161, 7, 8, '王麻子', '有那些人还在哟', NULL, 0, 0, '2023-12-26 16:02:52');
INSERT INTO `im_group_message` VALUES (162, 7, 3, '周游舟', '我在我在', NULL, 0, 0, '2023-12-26 16:04:29');
INSERT INTO `im_group_message` VALUES (163, 1, 3, '开发者', '#呲牙;', NULL, 0, 0, '2024-01-02 15:04:34');
INSERT INTO `im_group_message` VALUES (164, 7, 3, '周游舟', '#月亮;', NULL, 0, 0, '2024-01-02 15:08:30');


-- ----------------------------
-- Table structure for im_private_message
-- ----------------------------
DROP TABLE IF EXISTS `im_private_message`;
CREATE TABLE `im_private_message`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `send_id` bigint(20) NOT NULL COMMENT '发送用户id',
  `recv_id` bigint(20) NOT NULL COMMENT '接收用户id',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '发送内容',
  `type` tinyint(1) NOT NULL COMMENT '消息类型 0:文字 1:图片 2:文件 3:语音 10:系统提示',
  `status` tinyint(1) NOT NULL COMMENT '状态 0:未读 1:已读 2:撤回',
  `send_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '发送时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_send_recv_id`(`send_id`, `recv_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 725 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '私聊消息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of im_private_message
-- ----------------------------

INSERT INTO `im_private_message` VALUES (7, 1, 2, '767', 0, 3, '2023-10-08 12:53:45');
INSERT INTO `im_private_message` VALUES (8, 1, 2, '#呲牙;', 0, 3, '2023-10-08 12:53:57');


-- ----------------------------
-- Table structure for im_user
-- ----------------------------
DROP TABLE IF EXISTS `im_user`;
CREATE TABLE `im_user`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户昵称',
  `head_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户头像',
  `head_image_thumb` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '用户头像缩略图',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码(明文)',
  `sex` tinyint(1) NULL DEFAULT 0 COMMENT '性别 0:男 1:女',
  `type` smallint(6) NULL DEFAULT 1 COMMENT '用户类型 1:普通用户 2:店铺用户 3:系统用户 4:管理用户',
  `signature` varchar(1024) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '个性签名',
  `last_login_time` datetime(0) NULL DEFAULT NULL COMMENT '最后登录时间',
  `created_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `online_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '在线说明',
  `phone` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `sso_type` tinyint(1) NULL DEFAULT 0 COMMENT '单点登录类型， 0:HR系统 1:企业门户 2：ERP',
  `sso_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单点登录用户名',
  `sso_key` varchar(230) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '单点登录KEY',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_user_name`(`user_name`) USING BTREE,
  INDEX `idx_nick_name`(`nick_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of im_user
-- ----------------------------
INSERT INTO `im_user` VALUES (1, 'zhouyz', '周先生', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '$2a$10$0O0hwsSIr/RdYAVq6/LJve5dxuzS7HEeRezX7asj8Ab6cFG5szi5e', 0, 1, '个性签名，就如同我的灵魂，独一无二，无法复制。', NULL, '2023-09-26 16:40:30', '上班中', '13896190000', 0, NULL, NULL);
INSERT INTO `im_user` VALUES (2, 'zhangsan', '张三', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743034631.jpg', 'http://minio.sszhgyl.com/file/zyzy-im/image/20231008/1696743035532.jpg', '$2a$10$a.IkvbN6YQRdTGkTeslyKOrL8.gQy6ODkw/q/qPjqIvQXLgHXjEle', 0, 1, '这是张三的签名愿我是阳光，明媚而不忧伤', NULL, '2023-09-26 16:40:44', NULL, NULL, 0, NULL, NULL);
INSERT INTO `im_user` VALUES (3, 'admin', '周游舟', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', 'https://im.zyzy.team/minio_api/zyzy-im/image/20231226/1703574153118.jpg', '$2a$10$PjZn22R9n7G/61tpztEwoOebvreJ37cInh08j63XGguhzYYIPZecG', 0, 1, '三惑五劫,静夜思归,浮生若梦,恍若出尘,唯爱永恒。', NULL, '2023-10-08 13:59:57', '忙碌', '13896190000', 0, NULL, NULL);
INSERT INTO `im_user` VALUES (4, 'hr', '哈哈', '', '', '$2a$10$ong2yODpNwAG7j5DtEYCQepEWKc5/Qv3QYx9g.0HSxYvhM/CNjQ..', 0, 1, '我不敢太勇敢太执着太骄傲，我怕失去', NULL, '2023-10-08 22:04:29', NULL, NULL, 0, NULL, NULL);
INSERT INTO `im_user` VALUES (5, 'psl', 'psl', '', '', '$2a$10$mQ/ON1WEI.i2Rrd1mkxOfuWpOYfUI06uX/cWljmx.U.mnR6lZ1qqS', 0, 1, '品味独特，不拘一格！', NULL, '2023-11-01 16:39:19', NULL, NULL, 0, NULL, NULL);
INSERT INTO `im_user` VALUES (6, 'psl2', 'psl2', '', '', '$2a$10$aPr7I9dPUwuwmRGuCDSSuuPVTtnEbKyUqghMCr.qbhuf/U1xgd4zi', 0, 1, '你是太阳如此耀眼你似星星如此难寻', NULL, '2023-11-01 17:10:08', NULL, NULL, 0, NULL, NULL);
INSERT INTO `im_user` VALUES (7, 'lisi', '李四', '', '', '$2a$10$2AoX.hFdB21ahI/JOVWJKepfwiDCQeHwTIe3iUlpquayx6g/mMRpm', 0, 1, '不见面不等于不思念，不联络只是因为有了CRIM。', NULL, '2023-11-09 14:46:14', NULL, NULL, 0, NULL, NULL);
INSERT INTO `im_user` VALUES (8, 'wangmz', '王麻子', '', '', '$2a$10$l/SyVN0CYQ0Ty6C3M1.Dn.K7KxaMrdQrd5qJaFDBTtgYzItGfPOFG', 0, 1, '用自己的方式，诠释生命的精彩。', NULL, '2023-12-08 14:23:02', NULL, NULL, 0, NULL, NULL);
INSERT INTO `im_user` VALUES (20, 'mail', '邮件系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '$2a$10$0O0hwsSIr/RdYAVq6/LJve5dxuzS7HEeRezX7asj8Ab6cFG5szi5e', 0, 2, '系统信息，请不要回复。', NULL, '2023-09-26 16:40:30', '运行中', '13896190000', 0, NULL, NULL);
INSERT INTO `im_user` VALUES (21, 'oa', 'OA系统', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421875131.png', 'http://im.zyzy.team/minio_api/zyzy-im/image/20231201/1701421877066.png', '$2a$10$0O0hwsSIr/RdYAVq6/LJve5dxuzS7HEeRezX7asj8Ab6cFG5szi5e', 0, 2, '系统信息，请不要回复。', NULL, '2023-09-26 16:40:30', '运行中', '13896190000', 0, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
