# 企业级IM免费解决方案

![GPL3协议](https://img.shields.io/badge/license-GPL3-red)
[![star](https://gitee.com/zyzyteam/crim/badge/star.svg)](https://gitee.com/zyzyteam/crim) 
[![star](https://img.shields.io/github/stars/zyzyteam/crim.svg?style=flat&logo=GitHub)](https://github.com/zyzyteam/crim) 
CRIM是一个仿QQ2010年左右的聊天工具。后端采用springboot+netty实现，前端使用vue，移动端使用uniapp，PC端采用C#，支持私聊、群聊、离线消息、发送图片、文件、语音、emoji表情、视频聊天、视频会议等功能。 
后端可参考盒子IM：https://gitee.com/bluexsx/box-im 
视频会议参考：https://github.com/jitsi/jitsi-me


#### 近期更新
发布1.0版本:

- 服务端说明：为了尊重盒子IM的劳动成果，本代码暂不单独发布与盒子IM相关的代码，本项目应该是兼容盒子IM1.0后的版本，只要URL保持兼容。盒子IM项目地址：https://gitee.com/bluexsx/box-im
- 需要做相应修改的是终端类型需要添加一个PC端，代码如下：
public enum IMTerminalType {

    /**
     * web
     */
    WEB(0, "web"),
    /**
     * app
     */
    APP(1, "app"),
    /**
     * pc
     */
    PC(2,"pc");
- 视频会议功能：同样本项目不单独发布Jitsi Meet相关代码，如需相关功能请参考：https://github.com/jitsi/jitsi-meet
- 软件下载地址：https://www.zyzy.team/soft/CRIM.exe
- 本地调试适配：https://www.zyzy.team/soft/crim_local.exe
- 盒子服务适配：https://www.zyzy.team/soft/crim_box.exe
- CRIM服务适配：https://www.zyzy.team/soft/crim_zyzy.exe


#### 直接体验
- CRIM服务适配：https://www.zyzy.team/soft/crim_zyzy.exe
- 盒子服务适配：https://www.zyzy.team/soft/crim_box.exe
- 功能文档地址：
- 账号：admin 密码：123456
- 也可以自行注册账号


#### 项目结构
|  模块  |     功能 |
|-------------|------------|
| Team.Zyzy.Im | 核心业务功能部分 |
| Team.Zyzy.Im.App   | 软件更新、启动部分|
| Team.Zyzy.Im.Business   | 业务逻辑及数据库处理部分|
| Team.Zyzy.tool   | 工具包，主要是为了快速处理图片  |


#### 界面截图
![输入图片说明](https://gitee.com/zyzyteam/crim/raw/master/doc/CRIM.png)
欢迎进群与小伙们一起交流， **申请加群前请务必先star哦** 

#### 点下star吧
如果项目对您有帮助，请点亮右上方的star，支持一下作者吧！

#### 说明几点

1. 本系统允许用于商业用途，且不收费（自愿投币）。**但切记不要用于任何非法用途** ，本软件作者不会为此承担任何责任
2. 基于本系统二次开发后再次开源的项目，请注明引用出处，以避免引发不必要的误会


#### 参与贡献

1.  Master 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 其他

1.  作者博客 https://www.cnblogs.com/zhouyz
2.  作者官网 https://www.zyzy.team/

