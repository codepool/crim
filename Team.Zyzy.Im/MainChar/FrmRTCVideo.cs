﻿using Microsoft.MixedReality.WebRTC;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using Team.Zyzy.Im.Control.Model;

namespace Team.Zyzy.Im
{
    public partial class FrmRTCVideo : FormQQCommon
    {
        public FrmRTCVideo()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            //comboBoxVideo.DataSource = ManagedConductor.GetVideoDevices();
            this.Load += FrmRTCVideo_Load;
            this.FormClosed += FrmRTCVideo_FormClosed;
        }

        private void FrmRTCVideo_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        public bool IsMaster { get; set; }

        public PrivateMessage FriendMessage { get; set; }
        public FriendVOItem FriendInfo { get; set; }

        private void FrmRTCVideo_Load(object sender, EventArgs e)
        {
            pictureBoxLocal.Left = pnlContent.Width - pictureBoxLocal.Width + 6;
            try
            {
                WebRTCServer.Instance.FriendId = this.FriendInfo.ID;
                WebRTCServer.Instance.OnInitSuccess -= Instance_OnInitSuccess;
                WebRTCServer.Instance.OnInitSuccess += Instance_OnInitSuccess;
                WebRTCServer.Instance.RemoteAudioAdded -= Instance_RemoteAudioAdded;
                WebRTCServer.Instance.RemoteAudioAdded += Instance_RemoteAudioAdded;
                WebRTCServer.Instance.LocalAVideoFrameReady -= Instance_LocalAVideoFrameReady;
                WebRTCServer.Instance.LocalAVideoFrameReady += Instance_LocalAVideoFrameReady;
                WebRTCServer.Instance.RemoteAVideoFrameReady -= Instance_RemoteAVideoFrameReady;
                WebRTCServer.Instance.RemoteAVideoFrameReady += Instance_RemoteAVideoFrameReady;
                if (FriendInfo != null)
                {
                    this.Text = "你正在与【" + FriendInfo.DisplayName + "】视频";
                }
                if (IsMaster)
                {
                    btnCall.Location = btnClose.Location;
                    btnClose.Visible = false;
                }
                btnCall.Visible = false;
            }catch(Exception ex)
            {
                FrmAutoCloseMessageBox.Show(ex.Message, MessageBoxIcon.Error);
            }
        }

        private void Instance_OnInitSuccess(object sender, EventArgs e)
        {
            if (!IsMaster)
            {
                WebRTCServer.Instance.OnReceiveMessage(this.FriendMessage);
            }
            else
            {
                //WebRTCServer.Instance.Call();
                btnCall.Visible = true;
            }
        }
        private void Instance_RemoteAudioAdded(RemoteAudioTrack track)
        {
        }
        private void Instance_LocalAVideoFrameReady(I420AVideoFrame frame)
        {
            try
            {
                displayRemortFrame(pictureBoxLocal, frame);
            }
            catch (Exception ex)
            {
            }
        }
        private void Instance_RemoteAVideoFrameReady(I420AVideoFrame frame)
        {
            try
            {
                displayRemortFrame(pictureBoxRemote, frame);
            }
            catch (Exception ex)
            {

            }
        }


        #region 视频显示

        /// <summary>
        /// 计算YUV420视频帧占用的内存大小
        /// </summary>
        /// <param name="strideY">Y通道宽度</param>
        /// <param name="width">视频宽度</param>
        /// <param name="height">视频高度</param>
        /// <returns></returns>
        private int GetMemeryCount(int strideY, int width, int height)
        {
            int number = width / strideY;
            if (number < 1) number = 1;
            return strideY * height / number;
        }

        private void displayRemortFrame(PictureBox pb, I420AVideoFrame obj)
        {
            var lenY = GetMemeryCount(obj.strideY, (int)obj.width, (int)obj.height);
            var lenU = GetMemeryCount(obj.strideU, (int)obj.width, (int)obj.height);
            var lenV = GetMemeryCount(obj.strideV, (int)obj.width, (int)obj.height);
            var lenA = 0;
            if (obj.strideA > 0) lenA = GetMemeryCount(obj.strideA, (int)obj.width, (int)obj.height);

            OpenCvSharp.Mat yuvImg = new OpenCvSharp.Mat();
            yuvImg.Create((int)(obj.height * 3 / 2), (int)obj.width, OpenCvSharp.MatType.CV_8UC1);

            unsafe
            {
                Buffer.MemoryCopy(obj.dataY.ToPointer(), yuvImg.Data.ToPointer(), lenY, lenY);
                Buffer.MemoryCopy(obj.dataU.ToPointer(), (byte*)yuvImg.Data.ToPointer() + lenY, lenU, lenU);
                Buffer.MemoryCopy(obj.dataV.ToPointer(), (byte*)yuvImg.Data.ToPointer() + lenY + lenU, lenV, lenV);
                if (lenA > 0) Buffer.MemoryCopy(obj.dataA.ToPointer(), (byte*)yuvImg.Data.ToPointer() + lenY + lenU + lenV, lenA, lenA);
            }
            var target = new OpenCvSharp.Mat();
            OpenCvSharp.Cv2.CvtColor(yuvImg, target, OpenCvSharp.ColorConversionCodes.YUV2BGRA_I420);
            Action action = () =>
            {
                pb.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(target);
                target.Dispose();
            };
            Invoke(action);
        }

        #endregion
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            if (WebRTCServer.Instance != null)
            {
                if (WebRTCServer.Instance.IsConnected)
                {
                    WebRTCServer.Instance.SendHandup();
                }
                else
                {
                    WebRTCServer.Instance.SendCancel();
                }
            }
            WebRTCServer.CloseInstance();
        }
        protected override void WndProc(ref Message msg)
        {
            const int WM_SYSCOMMAND = 0x0112;
            const int SC_CLOSE = 0xF060;
            if (msg.Msg == WM_SYSCOMMAND && ((int)msg.WParam == SC_CLOSE))
            {
                base.WndProc(ref msg);

                if (WebRTCServer.Instance != null)
                {
                    if (WebRTCServer.Instance.IsConnected)
                    {
                        WebRTCServer.Instance.SendHandup();
                    }
                    else
                    {
                        WebRTCServer.Instance.SendCancel();
                    }
                }
                WebRTCServer.CloseInstance();
                return;
            }
            else
            {
                base.WndProc(ref msg);
            }
        }
        private void btnCall_Click(object sender, EventArgs e)
        {
            btnCall.Visible = false;
            btnClose.Visible = true;
            WebRTCServer.Instance.Call();
        }

        private void pictureBoxLocal_DoubleClick(object sender, EventArgs e)
        {
            pictureBoxRemote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            pictureBoxRemote.Location = pictureBoxLocal.Location;
            pictureBoxRemote.Size = pictureBoxLocal.Size;
            pictureBoxRemote.BringToFront();

            pictureBoxLocal.SendToBack();
            pictureBoxLocal.Dock = System.Windows.Forms.DockStyle.Fill;
            pictureBoxLocal.BackColor = Color.White;
        }

        private void pictureBoxRemote_DoubleClick(object sender, EventArgs e)
        {
            pictureBoxLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            pictureBoxLocal.Location = pictureBoxRemote.Location;
            pictureBoxLocal.Size = pictureBoxRemote.Size;
            pictureBoxLocal.BringToFront();

            pictureBoxRemote.SendToBack();
            pictureBoxRemote.Dock = System.Windows.Forms.DockStyle.Fill;
            pictureBoxRemote.BackColor = Color.White;

        }

    }
}

