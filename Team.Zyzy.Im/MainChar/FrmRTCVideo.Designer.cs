﻿
namespace Team.Zyzy.Im
{
    partial class FrmRTCVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRTCVideo));
            this.pictureBoxLocal = new System.Windows.Forms.PictureBox();
            this.pnlContent = new System.Windows.Forms.Panel();
            this.btnCall = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnClose = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.pictureBoxRemote = new System.Windows.Forms.PictureBox();
            this.qqFrmRound.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocal)).BeginInit();
            this.pnlContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRemote)).BeginInit();
            this.SuspendLayout();
            // 
            // qqFrmRound
            // 
            this.qqFrmRound.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqFrmRound.BackgroundImage")));
            this.qqFrmRound.Controls.Add(this.pnlContent);
            this.qqFrmRound.Location = new System.Drawing.Point(5, 5);
            this.qqFrmRound.Size = new System.Drawing.Size(1256, 898);
            this.qqFrmRound.Tag = "正在与";
            this.qqFrmRound.Text = "正在与";
            // 
            // pictureBoxLocal
            // 
            this.pictureBoxLocal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxLocal.BackColor = System.Drawing.Color.Gray;
            this.pictureBoxLocal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBoxLocal.Image = global::Team.Zyzy.Im.Properties.Resources.VideoWaitToAnswer;
            this.pictureBoxLocal.Location = new System.Drawing.Point(1105, 0);
            this.pictureBoxLocal.Margin = new System.Windows.Forms.Padding(6);
            this.pictureBoxLocal.Name = "pictureBoxLocal";
            this.pictureBoxLocal.Size = new System.Drawing.Size(145, 145);
            this.pictureBoxLocal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLocal.TabIndex = 3;
            this.pictureBoxLocal.TabStop = false;
            this.pictureBoxLocal.DoubleClick += new System.EventHandler(this.pictureBoxLocal_DoubleClick);
            // 
            // pnlContent
            // 
            this.pnlContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlContent.Controls.Add(this.btnCall);
            this.pnlContent.Controls.Add(this.btnClose);
            this.pnlContent.Controls.Add(this.pictureBoxLocal);
            this.pnlContent.Controls.Add(this.pictureBoxRemote);
            this.pnlContent.Location = new System.Drawing.Point(3, 60);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(1250, 835);
            this.pnlContent.TabIndex = 10;
            this.pnlContent.Text = "groupBox1";
            // 
            // btnCall
            // 
            this.btnCall.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCall.BackColor = System.Drawing.Color.Transparent;
            this.btnCall.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCall.BackgroundImage")));
            this.btnCall.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCall.Caption = "呼叫";
            this.btnCall.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCall.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnCall.Location = new System.Drawing.Point(900, 741);
            this.btnCall.Margin = new System.Windows.Forms.Padding(6);
            this.btnCall.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnCall.MouseDownImage")));
            this.btnCall.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnCall.MouseMoveImage")));
            this.btnCall.Name = "btnCall";
            this.btnCall.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCall.NormalImage")));
            this.btnCall.Size = new System.Drawing.Size(170, 88);
            this.btnCall.TabIndex = 5;
            this.btnCall.ToolTip = null;
            this.btnCall.Click += new System.EventHandler(this.btnCall_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Caption = "挂断";
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnClose.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnClose.Location = new System.Drawing.Point(1074, 741);
            this.btnClose.Margin = new System.Windows.Forms.Padding(6);
            this.btnClose.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseDownImage")));
            this.btnClose.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseMoveImage")));
            this.btnClose.Name = "btnClose";
            this.btnClose.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnClose.NormalImage")));
            this.btnClose.Size = new System.Drawing.Size(170, 88);
            this.btnClose.TabIndex = 4;
            this.btnClose.ToolTip = null;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBoxRemote
            // 
            this.pictureBoxRemote.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBoxRemote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxRemote.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxRemote.Margin = new System.Windows.Forms.Padding(6);
            this.pictureBoxRemote.Name = "pictureBoxRemote";
            this.pictureBoxRemote.Size = new System.Drawing.Size(1250, 835);
            this.pictureBoxRemote.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxRemote.TabIndex = 3;
            this.pictureBoxRemote.TabStop = false;
            this.pictureBoxRemote.DoubleClick += new System.EventHandler(this.pictureBoxRemote_DoubleClick);
            // 
            // FrmRTCVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1266, 908);
            this.Name = "FrmRTCVideo";
            this.Text = "正在与";
            this.qqFrmRound.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLocal)).EndInit();
            this.pnlContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRemote)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBoxLocal;
        private System.Windows.Forms.Panel pnlContent;
        private System.Windows.Forms.PictureBox pictureBoxRemote;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnClose;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnCall;
    }
}