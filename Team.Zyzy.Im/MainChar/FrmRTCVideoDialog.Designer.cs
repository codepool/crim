﻿
namespace Team.Zyzy.Im
{
    partial class FrmRTCVideoDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRTCVideoDialog));
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.labFriendName = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.btnRejected = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnAccept = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.timFail = new System.Windows.Forms.Timer(this.components);
            this.labInfo = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqPanelFixSize1.SuspendLayout();
            this.qqPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.Controls.Add(this.qqPanel1);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(780, 430);
            this.qqPanelFixSize1.Text = "视频聊天请求";
            // 
            // qqPanel1
            // 
            this.qqPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qqPanel1.Controls.Add(this.labInfo);
            this.qqPanel1.Controls.Add(this.labFriendName);
            this.qqPanel1.Controls.Add(this.qqLabel1);
            this.qqPanel1.Controls.Add(this.btnRejected);
            this.qqPanel1.Controls.Add(this.btnAccept);
            this.qqPanel1.Location = new System.Drawing.Point(3, 60);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(777, 370);
            this.qqPanel1.TabIndex = 6;
            // 
            // labFriendName
            // 
            this.labFriendName.AutoSize = true;
            this.labFriendName.BackColor = System.Drawing.Color.Transparent;
            this.labFriendName.Font = new System.Drawing.Font("宋体", 13.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labFriendName.Location = new System.Drawing.Point(258, 52);
            this.labFriendName.Name = "labFriendName";
            this.labFriendName.Size = new System.Drawing.Size(131, 37);
            this.labFriendName.TabIndex = 4;
            this.labFriendName.Text = "王麻子";
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Font = new System.Drawing.Font("宋体", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel1.Location = new System.Drawing.Point(50, 52);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(202, 37);
            this.qqLabel1.TabIndex = 3;
            this.qqLabel1.Text = "请求好友：";
            // 
            // btnRejected
            // 
            this.btnRejected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRejected.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRejected.BackgroundImage")));
            this.btnRejected.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRejected.Caption = "拒绝";
            this.btnRejected.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnRejected.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnRejected.Location = new System.Drawing.Point(424, 155);
            this.btnRejected.Margin = new System.Windows.Forms.Padding(6);
            this.btnRejected.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnRejected.MouseDownImage")));
            this.btnRejected.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnRejected.MouseMoveImage")));
            this.btnRejected.Name = "btnRejected";
            this.btnRejected.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnRejected.NormalImage")));
            this.btnRejected.Size = new System.Drawing.Size(170, 88);
            this.btnRejected.TabIndex = 1;
            this.btnRejected.ToolTip = null;
            this.btnRejected.Click += new System.EventHandler(this.btnRejected_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.BackgroundImage")));
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.Caption = "接受";
            this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnAccept.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnAccept.Location = new System.Drawing.Point(115, 155);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(6);
            this.btnAccept.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseDownImage")));
            this.btnAccept.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseMoveImage")));
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.NormalImage")));
            this.btnAccept.Size = new System.Drawing.Size(170, 88);
            this.btnAccept.TabIndex = 0;
            this.btnAccept.ToolTip = null;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // timFail
            // 
            this.timFail.Interval = 1000;
            this.timFail.Tick += new System.EventHandler(this.timFail_Tick);
            // 
            // labInfo
            // 
            this.labInfo.AutoSize = true;
            this.labInfo.BackColor = System.Drawing.Color.Transparent;
            this.labInfo.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.labInfo.Location = new System.Drawing.Point(50, 103);
            this.labInfo.Name = "labInfo";
            this.labInfo.Size = new System.Drawing.Size(621, 28);
            this.labInfo.TabIndex = 5;
            this.labInfo.Text = "你正在与其他好友通话，接收将关闭当前通话？";
            // 
            // FrmRTCVideoDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Name = "FrmRTCVideoDialog";
            this.Text = "视频聊天请求";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.qqPanel1.ResumeLayout(false);
            this.qqPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labFriendName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnRejected;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnAccept;
        private System.Windows.Forms.Timer timFail;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labInfo;
    }
}