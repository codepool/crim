﻿
namespace Team.Zyzy.Im
{
    partial class FrmVoiceRecord
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVoiceRecord));
            this.btnStart = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnStop = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnSend = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labTime = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.qqLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqPanelFixSize1.SuspendLayout();
            this.qqPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackColor = System.Drawing.SystemColors.Control;
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.Controls.Add(this.qqLabel2);
            this.qqPanelFixSize1.Controls.Add(this.qqPanel1);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(696, 393);
            this.qqPanelFixSize1.Text = "开始录音";
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStart.BackgroundImage")));
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStart.Caption = "开始";
            this.btnStart.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnStart.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnStart.Location = new System.Drawing.Point(29, 155);
            this.btnStart.Margin = new System.Windows.Forms.Padding(6);
            this.btnStart.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnStart.MouseDownImage")));
            this.btnStart.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnStart.MouseMoveImage")));
            this.btnStart.Name = "btnStart";
            this.btnStart.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnStart.NormalImage")));
            this.btnStart.Size = new System.Drawing.Size(170, 88);
            this.btnStart.TabIndex = 0;
            this.btnStart.ToolTip = null;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStop.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnStop.BackgroundImage")));
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Caption = "停止";
            this.btnStop.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnStop.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnStop.Location = new System.Drawing.Point(260, 155);
            this.btnStop.Margin = new System.Windows.Forms.Padding(6);
            this.btnStop.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnStop.MouseDownImage")));
            this.btnStop.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnStop.MouseMoveImage")));
            this.btnStop.Name = "btnStop";
            this.btnStop.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnStop.NormalImage")));
            this.btnStop.Size = new System.Drawing.Size(170, 88);
            this.btnStop.TabIndex = 1;
            this.btnStop.ToolTip = null;
            this.btnStop.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSend.BackgroundImage")));
            this.btnSend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSend.Caption = "发送";
            this.btnSend.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnSend.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnSend.Location = new System.Drawing.Point(491, 155);
            this.btnSend.Margin = new System.Windows.Forms.Padding(6);
            this.btnSend.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnSend.MouseDownImage")));
            this.btnSend.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnSend.MouseMoveImage")));
            this.btnSend.Name = "btnSend";
            this.btnSend.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnSend.NormalImage")));
            this.btnSend.Size = new System.Drawing.Size(170, 88);
            this.btnSend.TabIndex = 2;
            this.btnSend.ToolTip = null;
            this.btnSend.Click += new System.EventHandler(this.btnStopAndSend_Click);
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Font = new System.Drawing.Font("宋体", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel1.Location = new System.Drawing.Point(50, 52);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(128, 37);
            this.qqLabel1.TabIndex = 3;
            this.qqLabel1.Text = "时长：";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labTime
            // 
            this.labTime.AutoSize = true;
            this.labTime.BackColor = System.Drawing.Color.Transparent;
            this.labTime.Font = new System.Drawing.Font("宋体", 13.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTime.Location = new System.Drawing.Point(184, 52);
            this.labTime.Name = "labTime";
            this.labTime.Size = new System.Drawing.Size(57, 37);
            this.labTime.TabIndex = 4;
            this.labTime.Text = "0s";
            // 
            // qqPanel1
            // 
            this.qqPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qqPanel1.Controls.Add(this.labTime);
            this.qqPanel1.Controls.Add(this.qqLabel1);
            this.qqPanel1.Controls.Add(this.btnSend);
            this.qqPanel1.Controls.Add(this.btnStop);
            this.qqPanel1.Controls.Add(this.btnStart);
            this.qqPanel1.Location = new System.Drawing.Point(2, 70);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(691, 320);
            this.qqPanel1.TabIndex = 5;
            // 
            // qqLabel2
            // 
            this.qqLabel2.AutoSize = true;
            this.qqLabel2.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel2.Location = new System.Drawing.Point(54, 12);
            this.qqLabel2.Name = "qqLabel2";
            this.qqLabel2.Size = new System.Drawing.Size(124, 28);
            this.qqLabel2.TabIndex = 5;
            this.qqLabel2.Text = "开始录音";
            // 
            // FrmVoiceRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 413);
            this.Name = "FrmVoiceRecord";
            this.Text = "开始录音";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.qqPanelFixSize1.PerformLayout();
            this.qqPanel1.ResumeLayout(false);
            this.qqPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnSend;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnStop;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnStart;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private System.Windows.Forms.Timer timer1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labTime;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel2;
    }
}