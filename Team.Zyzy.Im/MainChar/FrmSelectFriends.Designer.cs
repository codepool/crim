﻿
namespace Team.Zyzy.Im
{
    partial class FrmSelectFriends
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSelectFriends));
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.scFriends = new YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.qlbFriend = new YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lvSelectedFriends = new System.Windows.Forms.ListView();
            this.btnAccept = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.ilFriends = new System.Windows.Forms.ImageList(this.components);
            this.btnCancel = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.qqPanelFixSize1.SuspendLayout();
            this.qqPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scFriends)).BeginInit();
            this.scFriends.Panel1.SuspendLayout();
            this.scFriends.Panel2.SuspendLayout();
            this.scFriends.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.Controls.Add(this.qqPanel1);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(967, 843);
            this.qqPanelFixSize1.Text = "选择好友";
            // 
            // qqPanel1
            // 
            this.qqPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qqPanel1.BackColor = System.Drawing.Color.White;
            this.qqPanel1.Controls.Add(this.scFriends);
            this.qqPanel1.Controls.Add(this.btnCancel);
            this.qqPanel1.Controls.Add(this.btnAccept);
            this.qqPanel1.Location = new System.Drawing.Point(3, 60);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(961, 783);
            this.qqPanel1.TabIndex = 6;
            // 
            // scFriends
            // 
            this.scFriends.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.scFriends.Location = new System.Drawing.Point(3, 3);
            this.scFriends.Name = "scFriends";
            // 
            // scFriends.Panel1
            // 
            this.scFriends.Panel1.Controls.Add(this.groupBox1);
            // 
            // scFriends.Panel2
            // 
            this.scFriends.Panel2.Controls.Add(this.groupBox2);
            this.scFriends.Size = new System.Drawing.Size(955, 668);
            this.scFriends.SplitterDistance = 350;
            this.scFriends.TabIndex = 9;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.qlbFriend);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 668);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "单击选择好友";
            // 
            // qlbFriend
            // 
            this.qlbFriend.AllowDragItem = false;
            this.qlbFriend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qlbFriend.FlatStyle = YiWangYi.MsgQQ.WinUI.QQListBar.QFlatStyle.None;
            this.qlbFriend.GroupHeaderBackColor = System.Drawing.Color.Gray;
            this.qlbFriend.GroupTextColor = System.Drawing.Color.Black;
            this.qlbFriend.HeightV = 634;
            this.qlbFriend.IsShowBackgroundImage = true;
            this.qlbFriend.ItemStyle = YiWangYi.MsgQQ.WinUI.QQListBar.CbItemStyle.PushButton;
            this.qlbFriend.LeftX = 0;
            this.qlbFriend.Location = new System.Drawing.Point(3, 31);
            this.qlbFriend.Name = "qlbFriend";
            this.qlbFriend.Size = new System.Drawing.Size(344, 634);
            this.qlbFriend.TabIndex = 8;
            this.qlbFriend.TopY = 0;
            this.qlbFriend.View = YiWangYi.MsgQQ.WinUI.QQListBar.CbViewStyle.LargeIcon;
            this.qlbFriend.WidthH = 344;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.lvSelectedFriends);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(601, 668);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "单击取消选择好友";
            // 
            // lvSelectedFriends
            // 
            this.lvSelectedFriends.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvSelectedFriends.HideSelection = false;
            this.lvSelectedFriends.LargeImageList = this.ilFriends;
            this.lvSelectedFriends.Location = new System.Drawing.Point(3, 31);
            this.lvSelectedFriends.Name = "lvSelectedFriends";
            this.lvSelectedFriends.Size = new System.Drawing.Size(595, 634);
            this.lvSelectedFriends.TabIndex = 0;
            this.lvSelectedFriends.UseCompatibleStateImageBehavior = false;
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.BackColor = System.Drawing.Color.Transparent;
            this.btnAccept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.BackgroundImage")));
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.Caption = "确定";
            this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnAccept.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnAccept.Location = new System.Drawing.Point(518, 680);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(6);
            this.btnAccept.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseDownImage")));
            this.btnAccept.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseMoveImage")));
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.NormalImage")));
            this.btnAccept.Size = new System.Drawing.Size(170, 88);
            this.btnAccept.TabIndex = 0;
            this.btnAccept.ToolTip = null;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // ilFriends
            // 
            this.ilFriends.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.ilFriends.ImageSize = new System.Drawing.Size(16, 16);
            this.ilFriends.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Caption = "取消";
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnCancel.Location = new System.Drawing.Point(766, 680);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(6);
            this.btnCancel.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseDownImage")));
            this.btnCancel.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseMoveImage")));
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.Size = new System.Drawing.Size(170, 88);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.ToolTip = null;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FrmSelectFriends
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 863);
            this.ImeMode = System.Windows.Forms.ImeMode.HangulFull;
            this.Name = "FrmSelectFriends";
            this.Text = "选择好友";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.qqPanel1.ResumeLayout(false);
            this.scFriends.Panel1.ResumeLayout(false);
            this.scFriends.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scFriends)).EndInit();
            this.scFriends.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnAccept;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer scFriends;
        private System.Windows.Forms.GroupBox groupBox1;
        private YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar qlbFriend;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView lvSelectedFriends;
        private System.Windows.Forms.ImageList ilFriends;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnCancel;
    }
}