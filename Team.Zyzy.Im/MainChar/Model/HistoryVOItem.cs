﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;

namespace Team.Zyzy.Im.Control.Model
{
    public class HistoryVOItem : CbItem
    {
        public HistoryVOItem()
            : base()
        {
        }

        public HistoryVOItem(string name)
            : base(name)
        {
        }

        public HistoryVOItem(ImHistoryMessage info)
: base(info.opposite_name)
        {
            this.HistoryVO = info;
            this.ID = info.opposite_id;
            this.NickName = null;
            //this.Name = info.opposite_name;
            this.DisplayName = info.opposite_name;
            this.ShortRemark = "";
            this.PersonSignaure = info.GetContentText();
            this.UpdateTime = info.send_time;
            this.ShowUpdateTime = true;
            if(info.source_type == (int)MessageSourceType.Friend)
            {
                this.CurrentState = CurrentState.OutLine;
                this.HeadImage = HeadImageHelp.FriendHeadImage( info.opposite_id);
            }
            else
            {
                this.CurrentState = CurrentState.None;
                this.HeadImage = HeadImageHelp.GroupHeadImage(info.opposite_id);
            }           
            this.IsShowEmail = false;
        }

        public ClassificationT<HistoryVOItem> ClassificationInfo
        {
            get
            {
                return (Parent as ClassificationT<HistoryVOItem>);
            }
            set
            {
                this.Parent = value;
            }
        }

        public ImHistoryMessage HistoryVO
        {
            get
            {
                return base.TagObject as ImHistoryMessage;
            }
            set
            {
                base.TagObject = value;
            }
        }
    }
}