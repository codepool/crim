﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;

namespace Team.Zyzy.Im.Control.Model
{
    public class GroupMemberVOItem : CbItem
    {
        public GroupMemberVOItem()
    : base()
        {
        }

        public GroupMemberVOItem(string name)
            : base(name)
        {
        }

        public GroupMemberVOItem(GroupMemberVO info)
     : base(info.aliasName)
        {
            this.GroupMemberVO = info;

            this.TagObject = info;
            this.ID = info.userId;
            this.NickName = info.aliasName;
            this.DisplayName = info.aliasName;
            this.ShortRemark = info.remark;
            this.PersonSignaure = "";
            this.CurrentState = info.online ? CurrentState.OnLine : CurrentState.OutLine;
            this.HeadImage = HeadImageHelp.FriendHeadImage(info.headImage, info.userId);
            this.IsShowEmail = false;
        }

        public ClassificationT<GroupMemberVOItem> ClassificationInfo
        {
            get
            {
                return (Parent as ClassificationT<GroupMemberVOItem>);
            }
            set
            {
                this.Parent = value;
            }
        }

        public GroupMemberVO GroupMemberVO
        {
            get
            {
                return base.TagObject as GroupMemberVO;
            }
            set
            {
                base.TagObject = value;
            }
        }
    }
}