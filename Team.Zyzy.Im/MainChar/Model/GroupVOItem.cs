﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;

namespace Team.Zyzy.Im.Control.Model
{
    public class GroupVOItem : CbItem
    {
        public GroupVOItem()
    : base()
        {
        }

        public GroupVOItem(string name)
            : base(name)
        {
        }

        public GroupVOItem(GroupVO info)
     : base(info.aliasName)
        {
            this.GroupVO = info;
            this.ID = info.id;
            this.NickName = info.aliasName;
            //this.Name = info.name;
            this.DisplayName = info.name;
            //this.ShortRemark = info.remark;
            this.PersonSignaure = String.IsNullOrWhiteSpace(info.remark) ? "" : "["+info.remark+"]";
            this.CurrentState = CurrentState.None;
            this.HeadImage = HeadImageHelp.GroupHeadImage(info.headImageThumb, info.id);
            this.IsShowEmail = false;
            this.IsManager = false;
        }

        public ClassificationT<GroupVOItem> ClassificationInfo
        {
            get
            {
                return (Parent as ClassificationT<GroupVOItem>);
            }
            set
            {
                this.Parent = value;
            }
        }

        public GroupVO GroupVO
        {
            get
            {
                return base.TagObject as GroupVO;
            }
            set
            {
                base.TagObject = value;
            }
        }
    }
}