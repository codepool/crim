﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;

namespace Team.Zyzy.Im.Control.Model
{
    public class FriendVOItem : CbItem
    {
        public FriendVOItem()
            : base()
        {
        }

        public FriendVOItem(string name)
            : base(name)
        {
        }

        public FriendVOItem(FriendVO info)
    : base(info.nickName)
        {
            this.FriendVO = info;
            this.ID = info.id;
            this.ShortRemark = string.IsNullOrWhiteSpace(info.onlineRemark) ? "在线中" : info.onlineRemark;
            this.DisplayName =string.IsNullOrWhiteSpace(info.remarkName) ? info.nickName : info.nickName + "["+ info.remarkName + "]";
            this.NickName = null;
            this.PersonSignaure =string.IsNullOrWhiteSpace(info.signature) ? "这个家伙很懒，没有签名！" : info.signature;
            this.HeadImage = HeadImageHelp.FriendHeadImage(info.headImage, info.id);
            //this.CurrentState = info.online ? CurrentState.OnLine : CurrentState.OutLine;
            this.IsShowEmail = true;
            this.TerminalType = TerminalType.None;
        }

        public ClassificationT<FriendVOItem> ClassificationInfo
        {
            get
            {
                return (Parent as ClassificationT<FriendVOItem>);
            }
            set
            {
                this.Parent = value;
            }
        }

        public FriendVO FriendVO
        {
            get
            {
                return base.TagObject as FriendVO;
            }
            set
            {
                base.TagObject = value;
            }
        }
    }
}