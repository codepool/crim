﻿
namespace Team.Zyzy.Im.Control
{
    partial class UCFriendItemInfo
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UCFriendItemInfo));
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.btnAddExist = new System.Windows.Forms.Button();
            this.btnAdd = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.labOnLine = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.labNickName = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.picHeadImg = new System.Windows.Forms.PictureBox();
            this.qqPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImg)).BeginInit();
            this.SuspendLayout();
            // 
            // qqPanel1
            // 
            this.qqPanel1.Controls.Add(this.btnAddExist);
            this.qqPanel1.Controls.Add(this.btnAdd);
            this.qqPanel1.Controls.Add(this.labOnLine);
            this.qqPanel1.Controls.Add(this.labNickName);
            this.qqPanel1.Controls.Add(this.picHeadImg);
            this.qqPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qqPanel1.Location = new System.Drawing.Point(0, 0);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(886, 91);
            this.qqPanel1.TabIndex = 0;
            // 
            // btnAddExist
            // 
            this.btnAddExist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddExist.Enabled = false;
            this.btnAddExist.Location = new System.Drawing.Point(734, 8);
            this.btnAddExist.Name = "btnAddExist";
            this.btnAddExist.Size = new System.Drawing.Size(135, 75);
            this.btnAddExist.TabIndex = 9;
            this.btnAddExist.Text = "已添加";
            this.btnAddExist.UseVisualStyleBackColor = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnAdd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.BackgroundImage")));
            this.btnAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAdd.Caption = "已添加";
            this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnAdd.Location = new System.Drawing.Point(734, 8);
            this.btnAdd.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.MouseDownImage")));
            this.btnAdd.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.MouseMoveImage")));
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnAdd.NormalImage")));
            this.btnAdd.Size = new System.Drawing.Size(135, 75);
            this.btnAdd.TabIndex = 8;
            this.btnAdd.ToolTip = null;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // labOnLine
            // 
            this.labOnLine.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labOnLine.AutoSize = true;
            this.labOnLine.BackColor = System.Drawing.Color.Transparent;
            this.labOnLine.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labOnLine.ForeColor = System.Drawing.Color.Silver;
            this.labOnLine.Location = new System.Drawing.Point(581, 21);
            this.labOnLine.Name = "labOnLine";
            this.labOnLine.Size = new System.Drawing.Size(116, 48);
            this.labOnLine.TabIndex = 7;
            this.labOnLine.Text = "在线";
            // 
            // labNickName
            // 
            this.labNickName.AutoSize = true;
            this.labNickName.BackColor = System.Drawing.Color.Transparent;
            this.labNickName.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labNickName.Location = new System.Drawing.Point(109, 21);
            this.labNickName.Name = "labNickName";
            this.labNickName.Size = new System.Drawing.Size(212, 48);
            this.labNickName.TabIndex = 6;
            this.labNickName.Text = "新射天狼";
            // 
            // picHeadImg
            // 
            this.picHeadImg.Image = ((System.Drawing.Image)(resources.GetObject("picHeadImg.Image")));
            this.picHeadImg.Location = new System.Drawing.Point(6, 5);
            this.picHeadImg.Margin = new System.Windows.Forms.Padding(6);
            this.picHeadImg.Name = "picHeadImg";
            this.picHeadImg.Size = new System.Drawing.Size(80, 80);
            this.picHeadImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHeadImg.TabIndex = 5;
            this.picHeadImg.TabStop = false;
            // 
            // UCFriendItemInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.qqPanel1);
            this.Name = "UCFriendItemInfo";
            this.Size = new System.Drawing.Size(886, 91);
            this.qqPanel1.ResumeLayout(false);
            this.qqPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private System.Windows.Forms.PictureBox picHeadImg;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labNickName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labOnLine;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnAdd;
        private System.Windows.Forms.Button btnAddExist;
    }
}
