﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Team.Zyzy.Im.Control
{
    public partial class UCFriendItemInfo : UserControl
    {
        public UCFriendItemInfo()
        {
            InitializeComponent();
            //btnAddExist.Location = btnAdd.Location;
            //btnAddExist.Size = btnAdd.Size;
            //btnAddExist.Font = btnAdd.Font;
        }

        public PictureBox HeadPicture
        {
            get
            {
                return picHeadImg;
            }
        }

        public string HeadImgUrl
        {
            set
            {
                if(value!=null && value.Length>5)
                picHeadImg.Load(value);
            }
        }
        public Image HeadImg
        {
            set
            {
                picHeadImg.Image = value;
            }
        }

        public string NickName
        {
            get
            {
                return labNickName.Text;
            }
            set
            {
                labNickName.Text = value;
            }
        }

        private bool _online = true;
        public bool OnLine
        {
            get
            {
                return _online;
            }
            set
            {
                if (value != _online)
                {
                    _online = value;
                    if (_online)
                    {
                        labOnLine.Text = "[在线]";
                        labOnLine.ForeColor = Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
                    }
                    else
                    {
                        labOnLine.Text = "[离线]";
                        labOnLine.ForeColor = Color.Silver;
                    }
                }
            }
        }

        private bool _haveAdd = true;
        public bool HaveAdd
        {
            get
            {
                return _haveAdd;
            }
            set
            {
                if (value != _haveAdd)
                {
                    _haveAdd = value;
                    if (_haveAdd)
                    {
                        btnAdd.Caption = "已添加";
                        btnAdd.Visible = false;
                        btnAddExist.Visible = true;
                        //btnAddExist.Location = btnAdd.Location;
                        //btnAddExist.Size = btnAdd.Size;
                        //btnAddExist.Font = btnAdd.Font;
                    }
                    else
                    {
                        btnAdd.Caption = "添加";
                        btnAdd.Visible = true;
                        btnAddExist.Visible = false;
                    }
                }
            }
        }

        public event EventHandler OnAddClick;

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (_haveAdd)
            {
                return;
            }
            if (OnAddClick != null)
            {
                OnAddClick(this.Tag, e);
            }
        }
    }
}
