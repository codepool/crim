﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.mainService;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.ContractPanel;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.Model.Chart;

namespace Team.Zyzy.Im
{
    public partial class FrmRenameGroup : FormQQFixSize
    {
        private GroupVO group = null;
        public GroupVO GroupVO
        {
            get
            {
                return group;
            }
            set
            {
                if (group != value)
                {
                    group = value;
                    txtGroupRemarkName.Text = group.name;
                }
            }
        }
        public QQContractListBase ContractListBar;
        public FrmRenameGroup()
        {
            InitializeComponent();
            this.Load += FrmClassifyManage_Load;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.TopMost = true;
        }

        private void FrmClassifyManage_Load(object sender, EventArgs e)
        {
 
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            string url = "/group/remark?groupId=" + group.id + "&remark=" + txtGroupRemarkName.Text.Trim();
            HttpHelp.HttpApi<Group>(url, null, "PUT");
            LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
