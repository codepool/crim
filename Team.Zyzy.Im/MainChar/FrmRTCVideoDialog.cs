﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;

namespace Team.Zyzy.Im
{
    public partial class FrmRTCVideoDialog : FormQQFixSize
    {
        private static List<FrmRTCVideoDialog> _instaces = new List<FrmRTCVideoDialog>();
        protected FrmRTCVideoDialog()
        {
            InitializeComponent();
            labInfo.Visible = false;
            this.Load += FrmRTCVideoDialog_Load;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.TopMost = true;
        }

        public static FrmRTCVideoDialog GetInstance()
        {
            FrmRTCVideoDialog instance = new FrmRTCVideoDialog();
            _instaces.Add(instance);
            return instance;
        }

        public static void CloseInstance(long friendId)
        {
            FrmRTCVideoDialog frd = null;
            foreach (FrmRTCVideoDialog item in _instaces)
            {
                if (item.FriendInfo.ID == friendId)
                {
                    frd = item;
                    break;
                }
            }
            if (frd != null)
            {
                _instaces.Remove(frd);
                frd.Close();
            }
        }

        private void FrmRTCVideoDialog_Load(object sender, EventArgs e)
        {
            if (WebRTCServer.Instance != null && WebRTCServer.Instance.IsConnected)
            {
                labInfo.Visible = true;
            }
            else
            {
                labInfo.Visible = false;
            }
            VoiceRecordHelp.PlayCall();
        }

        private FriendVOItem _friendInfo = new FriendVOItem();
        public FriendVOItem FriendInfo
        {
            get
            {
                return _friendInfo;
            }
            set
            {
                _friendInfo = value;
                if (_friendInfo != null)
                {
                    labFriendName.Text = _friendInfo.Name;
                    this.Text = labFriendName.Text + "请求视频聊天";
                }
            }
        }
        public PrivateMessage FriendMessage { get; set; }
        private int timeFailCount = 0;
        public void SetNewChart()
        {
            timeFailCount = 0;
            this.timFail.Enabled = true;
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            VoiceRecordHelp.StopPlayCall();
            if (WebRTCServer.Instance!=null && WebRTCServer.Instance.IsConnected)
            {                
                //已经在对话，先挂断
                WebRTCServer.Instance.SendHandup();
            }
            WebRTCServer.CreateInstance(false);
            if (WebRTCServer.Instance.Form == null)
            {
                WebRTCServer.Instance.ID = FriendMessage.recvId;
                WebRTCServer.Instance.FriendId = FriendMessage.sendId;
                WebRTCServer.Instance.Form = new FrmRTCVideo();
                WebRTCServer.Instance.Form.IsMaster = WebRTCServer.Instance.Master;
                WebRTCServer.Instance.Form.FriendInfo = _friendInfo;
                WebRTCServer.Instance.Form.FriendMessage = this.FriendMessage;
                WebRTCServer.Instance.Form.Show();
            }
            this.Close();
        }

        private void btnRejected_Click(object sender, EventArgs e)
        {
            VoiceRecordHelp.StopPlayCall();
            try
            {
                string url = "/webrtc/private/reject?uid=" + this.FriendInfo.ID;
                HttpHelp.HttpApi<long?>(url, null, "POST");
            }
            catch { }
            this.Close();
        }

        private void timFail_Tick(object sender, EventArgs e)
        {
            timeFailCount++;
            if (timeFailCount > 30)
            {
                sendFail("对方未接听");
            }
        }
        private void sendFail(string reason)
        {
            try
            {
                string url = "/webrtc/private/failed?uid=" + this.FriendInfo.ID + "&reason=" + reason;
                HttpHelp.HttpApi<long?>(url, null, "POST");
            }
            catch { }
            timeFailCount = 0;
            timFail.Enabled = false;
            this.Close();
        }
    }
}
