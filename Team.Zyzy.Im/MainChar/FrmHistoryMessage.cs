﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.Business.vo;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.mainService;

namespace Team.Zyzy.Im
{
    public partial class FrmHistoryMessage : FormQQCommon
    {
        public FrmHistoryMessage()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Load += FrmMessage_Load;
        }
        private void FrmMessage_Load(object sender, EventArgs e)
        {
            this.ChangeSkin();
            if (qtcContent.SelectedIndex == 0)
            {
                qtcContent_SelectedIndexChanged(sender, e);
            }
            qlbFriend.ItemClick += QlbFriend_ItemClick;
            qlbGroup.ItemClick += QlbGroup_ItemClick;
            qlbHistory.ItemClick += QlbHistory_ItemClick;
            qlbSystem.ItemClick += QlbSystem_ItemClick;
        }


        private FrmMain _frmMain = null;
        public FrmMain FrmMain
        {
            get
            {
                if (_frmMain == null)
                {
                    _frmMain = ShareInfo.FormQQMain as FrmMain;
                }
                return _frmMain;
            }
        }

        #region 好友信息
        private bool _isInitFriend = false;
        private void initFriend()
        {
            this.cbFriend.Location = new Point(0, 2);
            this.cbFriend.Width = this.spcFriend.Panel2.Width - this.spcFriend.Panel2.Padding.Left-2 ;
            this.cbFriend.Height = this.spcFriend.Panel2.Height - this.spcFriend.Panel2.Padding.Top - this.cbFriend.Margin.Top-this.ptFriend.Height - this.ptFriend.Margin.Top;
            this.ptFriend.Left = this.cbFriend.Width - this.ptFriend.Width;
            this.ptFriend.Top = this.cbFriend.Height+ this.ptFriend.Margin.Top;
            this.spcFriend.Panel1.BackColor = Color.White;
            this.ptFriend.FirstPageClick += delegate
            {
                ptFriend.CurrentPage = 1;
                getFriendHistoryMessage();
            };
            this.ptFriend.PreviousPageClick += delegate
            {
                ptFriend.CurrentPage = ptFriend.CurrentPage - 1;
                getFriendHistoryMessage();
            };
            this.ptFriend.NextPageClick += delegate
            {
                ptFriend.CurrentPage = ptFriend.CurrentPage + 1;
                getFriendHistoryMessage();
            };
            this.ptFriend.LastPageClick += delegate
            {
                ptFriend.CurrentPage = ptFriend.CurrentPage + 1;
                getFriendHistoryMessage();
            };

            spcFriend.SplitterDistance = 150;
            LoadChartBarsService.GetInstance().InitChartFriends(this.qlbFriend.Groups);
        }

        private FriendVO _currentFriendVO = null;
        private void QlbFriend_ItemClick(YiWangYi.MsgQQ.WinUI.QQListBar.CbItem sender, MouseEventArgs e)
        {
            _currentFriendVO = sender.TagObject as FriendVO;
            this.ptFriend.CurrentPage = 1;
            this.ptFriend.SumPage = 2;
            getFriendHistoryMessage();
        }

        private void getFriendHistoryMessage()
        {
            cbFriend.ClearMessage();
            HistoryMessagePageVO page = new HistoryMessagePageVO();
            page.friendId = _currentFriendVO.id;
            page.page = ptFriend.CurrentPage;
            page.size = 10;
            try
            {
                Task.Run(async () =>
                {
                    Action action = () =>
                    {
                        List<PrivateMessage> messages = pullHistoryPrivateMessage(page);
                        messages.Sort();
                        foreach (PrivateMessage item in messages)
                        {
                            ChartMessageHelp.DisplaySingle(cbFriend, item, _currentFriendVO.id);
                        }
                        if(messages!=null && messages.Count== page.size)
                        {
                            ptFriend.SumPage = ptFriend.SumPage + 1;
                        }
                    };
                    Invoke(action);
                });
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        public void ShowFriendHistory(long friendId)
        {
            this.Show();
            this.Shown += delegate {
                foreach (CbGroup group in qlbFriend.Groups)
                {
                    for (int i = 0; i < group.SbItemsBox.Items.Count; i++)
                    {
                        CbItem item = group.SbItemsBox.Items[i];
                        if (item.ID == friendId)
                        {
                            group.IsExpand = true;
                            item.Selected = true;
                            QlbFriend_ItemClick(item, null);
                            break;
                        }
                    }
                }
                
            };
        }

 
        private List<PrivateMessage> pullHistoryPrivateMessage(HistoryMessagePageVO page)
        {
            string url = "/message/private/history";
            List<PrivateMessage> messages = HttpHelp.HttpApi<List<PrivateMessage>>(url, page, "GET");
            return messages;
        }
        #endregion

        #region 群组信息
        private bool _isInitGroup = false;
        private void QlbGroup_ItemClick(CbItem sender, MouseEventArgs e)
        {
            _currentGroupVO = sender.TagObject as GroupVO;
            this.ptGroup.CurrentPage = 1;
            this.ptGroup.SumPage = 2;
            getGroupHistoryMessage();
        }
        private GroupVO _currentGroupVO = null;
        private void initGroup()
        {
            this.cbGroup.Location = new Point(0, 2);
            this.cbGroup.Width = this.spcGroup.Panel2.Width - this.spcGroup.Panel2.Padding.Left - 2;
            this.cbGroup.Height = this.spcGroup.Panel2.Height - this.spcGroup.Panel2.Padding.Top - this.cbGroup.Margin.Top - this.ptGroup.Height - this.ptGroup.Margin.Top;
            this.ptGroup.Left = this.cbGroup.Width - this.ptGroup.Width;
            this.ptGroup.Top = this.cbGroup.Height + this.ptGroup.Margin.Top;
            this.spcGroup.Panel1.BackColor = Color.White;
            this.ptGroup.FirstPageClick += delegate
            {
                ptGroup.CurrentPage = 1;
                getGroupHistoryMessage();
            };
            this.ptGroup.PreviousPageClick += delegate
            {
                ptGroup.CurrentPage = ptGroup.CurrentPage - 1;
                getGroupHistoryMessage();
            };
            this.ptGroup.NextPageClick += delegate
            {
                ptGroup.CurrentPage = ptGroup.CurrentPage + 1;
                getGroupHistoryMessage();
            };
            this.ptGroup.LastPageClick += delegate
            {
                ptGroup.CurrentPage = ptGroup.CurrentPage + 1;
                getGroupHistoryMessage();
            };

            spcGroup.SplitterDistance = 150;

            if (this.qlbGroup.Groups.Count > 0)
            {
                return;
            }
            spcGroup.SplitterDistance = 150;
            LoadChartBarsService.GetInstance().InitChartGroups(this.qlbGroup.Groups);
        }

        private void getGroupHistoryMessage()
        {
            cbGroup.ClearMessage();
            HistoryMessagePageVO page = new HistoryMessagePageVO();
            page.groupId = _currentGroupVO.id;
            page.page = ptGroup.CurrentPage;
            page.size = 10;
            try
            {
                Task.Run(async () =>
                {
                    Action action = () =>
                    {
                        List<GroupMessage> messages = pullHistoryGroupMessage(page);
                        messages.Sort();
                        foreach (GroupMessage item in messages)
                        {
                            ChartMessageHelp.DisplaySingle(cbGroup, item, _currentGroupVO.id);
                        }
                        if (messages != null && messages.Count == page.size)
                        {
                            ptGroup.SumPage = ptGroup.SumPage + 1;
                        }
                    };
                    Invoke(action);
                });
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }
        public void ShowGroupHistory(long groupId)
        {
            qtcContent.SelectedIndex = 1;
            this.Show();
            qtcContent_SelectedIndexChanged(this, null);
            this.Shown += delegate {
                foreach (CbGroup group in qlbGroup.Groups)
                {
                    for (int i = 0; i < group.SbItemsBox.Items.Count; i++)
                    {
                        CbItem item = group.SbItemsBox.Items[i];
                        if (item.ID == groupId)
                        {
                            group.IsExpand = true;
                            item.Selected = true;
                            QlbGroup_ItemClick(item, null);
                            break;
                        }
                    }
                }

            };
        }

        private List<GroupMessage> pullHistoryGroupMessage(HistoryMessagePageVO page)
        {
            string url = "/message/group/history";
            List<GroupMessage> messages = HttpHelp.HttpApi<List<GroupMessage>>(url, page, "GET");
            return messages;
        }
        #endregion

        #region 历史信息
        private bool _isInitHistory = false;
        private ImHistoryMessage _currentHistoryVO = null;
        private void QlbHistory_ItemClick(CbItem sender, MouseEventArgs e)
        {
            _currentHistoryVO = sender.TagObject as ImHistoryMessage;
            this.ptHistory.CurrentPage = 1;
            this.ptHistory.SumPage = 2;
            getHistoryHistoryMessage();
        }

        private void getHistoryHistoryMessage()
        {
            cbHistory.ClearMessage();
            HistoryMessagePageVO page = new HistoryMessagePageVO();
            page.groupId = _currentHistoryVO.opposite_id;
            page.friendId = _currentHistoryVO.opposite_id;
            page.page = ptHistory.CurrentPage;
            page.size = 10;
            if (_currentHistoryVO.source_type == (int)MessageSourceType.Friend)
            {
                try
                {
                    Task.Run(async () =>
                    {
                        Action action = () =>
                        {
                            List<PrivateMessage> messages = pullHistoryPrivateMessage(page);
                            messages.Sort();
                            foreach (PrivateMessage item in messages)
                            {
                                ChartMessageHelp.DisplaySingle(cbHistory, item, page.friendId);
                            }
                            if (messages != null && messages.Count == page.size)
                            {
                                ptHistory.SumPage = ptHistory.SumPage + 1;
                            }
                        };
                        Invoke(action);
                    });
                }
                catch (Exception ex)
                {
                    FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
                }
            }
            else if (_currentHistoryVO.source_type == (int)MessageSourceType.Group)
            {
                try
                {
                    Task.Run(async () =>
                    {
                        Action action = () =>
                        {
                            List<GroupMessage> messages = pullHistoryGroupMessage(page);
                            messages.Sort();
                            foreach (GroupMessage item in messages)
                            {
                                ChartMessageHelp.DisplaySingle(cbHistory, item, RequestApiInfo.SendId);
                            }
                            if (messages != null && messages.Count == page.size)
                            {
                                ptHistory.SumPage = ptHistory.SumPage + 1;
                            }
                        };
                        Invoke(action);
                    });
                }
                catch (Exception ex)
                {
                    FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
                }
            }
        }
        private void initHistory()
        {
            this.cbHistory.Location = new Point(0, 2);
            this.cbHistory.Width = this.spcHistory.Panel2.Width - this.spcHistory.Panel2.Padding.Left - 2;
            this.cbHistory.Height = this.spcHistory.Panel2.Height - this.spcHistory.Panel2.Padding.Top - this.cbHistory.Margin.Top - this.ptHistory.Height - this.ptHistory.Margin.Top;
            this.ptHistory.Left = this.cbHistory.Width - this.ptHistory.Width;
            this.ptHistory.Top = this.cbHistory.Height + this.ptHistory.Margin.Top;
            this.spcHistory.Panel1.BackColor = Color.White;
            this.ptHistory.FirstPageClick += delegate
            {
                ptHistory.CurrentPage = 1;
                getHistoryHistoryMessage();
            };
            this.ptHistory.PreviousPageClick += delegate
            {
                ptHistory.CurrentPage = ptHistory.CurrentPage - 1;
                getHistoryHistoryMessage();
            };
            this.ptHistory.NextPageClick += delegate
            {
                ptHistory.CurrentPage = ptHistory.CurrentPage + 1;
                getHistoryHistoryMessage();
            };
            this.ptHistory.LastPageClick += delegate
            {
                ptHistory.CurrentPage = ptHistory.CurrentPage + 1;
                getHistoryHistoryMessage();
            };

            spcHistory.SplitterDistance = 150;

            if (this.qlbHistory.Groups.Count > 0)
            {
                return;
            }
            spcHistory.SplitterDistance = 150;
            ClassificationT<HistoryVOItem> fg1 = new ClassificationT<HistoryVOItem>("聊天历史记录");
            fg1.IsExpand = false;
            this.qlbHistory.Groups.Add(fg1);
            fg1.GroupTextIsShow = false;
            LoadChartBarsService.GetInstance().LoadHistory(fg1);
            fg1.IsExpand = true;
        }

        #endregion

        #region 系统信息
        private bool _isInitSystem = false;
        private void QlbSystem_ItemClick(CbItem sender, MouseEventArgs e)
        {
            //_currentSystemVO = sender.TagObject as ImSystemMessage;
            //this.ptFriend.CurrentPage = 1;
            //this.ptFriend.SumPage = 2;
            //getSystemSystemMessage();
        }

        private void getSystemSystemMessage()
        {
            cbSystem.ClearMessage();
            
        }
        private void initSystem()
        {
            this.cbSystem.Location = new Point(0, 2);
            this.cbSystem.Width = this.spcSystem.Panel2.Width - this.spcSystem.Panel2.Padding.Left - 2;
            this.cbSystem.Height = this.spcSystem.Panel2.Height - this.spcSystem.Panel2.Padding.Top - this.cbSystem.Margin.Top - this.ptSystem.Height - this.ptSystem.Margin.Top;
            this.ptSystem.Left = this.cbSystem.Width - this.ptSystem.Width;
            this.ptSystem.Top = this.cbSystem.Height + this.ptSystem.Margin.Top;
            this.spcSystem.Panel1.BackColor = Color.White;
            this.ptSystem.FirstPageClick += delegate
            {
                ptSystem.CurrentPage = 1;
                getSystemSystemMessage();
            };
            this.ptSystem.PreviousPageClick += delegate
            {
                ptSystem.CurrentPage = ptSystem.CurrentPage - 1;
                getSystemSystemMessage();
            };
            this.ptSystem.NextPageClick += delegate
            {
                ptSystem.CurrentPage = ptSystem.CurrentPage + 1;
                getSystemSystemMessage();
            };
            this.ptSystem.LastPageClick += delegate
            {
                ptSystem.CurrentPage = ptSystem.CurrentPage + 1;
                getSystemSystemMessage();
            };

            spcSystem.SplitterDistance = 150;

            if (this.qlbSystem.Groups.Count > 0)
            {
                return;
            }
            spcSystem.SplitterDistance = 150;
        }

        #endregion
        private void qtcContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (qtcContent.SelectedIndex == 0)
            {
                if (!_isInitFriend)
                {
                    initFriend();
                    _isInitFriend = true;
                }
            }
            else if (qtcContent.SelectedIndex == 1)
            {
                if (!_isInitGroup)
                {
                    initGroup();
                    _isInitGroup = true;
                }
            }
            else if (qtcContent.SelectedIndex == 2)
            {
                if (!_isInitHistory)
                {
                    initHistory();
                    _isInitHistory = true;
                }
            }
            else if (qtcContent.SelectedIndex == 3)
            {
                if (!_isInitSystem)
                {
                    initSystem();
                    _isInitGroup = true;
                }
            }
        }
    }
}
