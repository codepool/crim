﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.mainService;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.ContractPanel;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.Model.Chart;

namespace Team.Zyzy.Im
{
    public partial class FrmSelectFriends : FormQQFixSize
    {
        
        public FrmSelectFriends()
        {
            InitializeComponent();
            this.Load += FrmSelectFriends_Load;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.TopMost = true;
        }

        public List<FriendVO> SelectedFriends = new List<FriendVO>();

        public ImageList FriendsImage
        {
            get
            {
                return ilFriends;
            }
        }

        private void FrmSelectFriends_Load(object sender, EventArgs e)
        {
            LoadChartBarsService.GetInstance().InitChartFriends(this.qlbFriend.Groups);
            foreach (ClassificationT<FriendVOItem> group in this.qlbFriend.Groups)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    FriendVOItem item = group[i];
                    ilFriends.Images.Add(item.FriendVO.id.ToString(), HeadImageHelp.FriendHeadImage(item.FriendVO.headImage, item.FriendVO.id));
                }
            }
            qlbFriend.ItemClick += QlbFriend_ItemClick;
            lvSelectedFriends.Click += LvSelectedFriends_Click;
            scFriends.SplitterDistance = this.Width / 3;
        }

        private void LvSelectedFriends_Click(object sender, EventArgs e)
        {
            if (lvSelectedFriends.SelectedItems.Count > 0)
            {
                FriendVO friend = lvSelectedFriends.SelectedItems[0].Tag as FriendVO;
                SelectedFriends.Remove(friend);
                displaySelectedFriends();
            }
        }

        private void QlbFriend_ItemClick(YiWangYi.MsgQQ.WinUI.QQListBar.CbItem sender, MouseEventArgs e)
        {
            FriendVO friend = sender.TagObject as FriendVO;
            if (SelectedFriends.Contains(friend))
            {
                SelectedFriends.Remove(friend);
            }
            else
            {
                SelectedFriends.Add(friend);
            }
            displaySelectedFriends();
        }

        private void displaySelectedFriends()
        {
            lvSelectedFriends.Items.Clear();
            foreach(FriendVO item in SelectedFriends)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = item.nickName;
                lvItem.ImageKey = item.id.ToString();
                lvItem.Tag = item;
                lvSelectedFriends.Items.Add(lvItem);
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            SelectedFriends = new List<FriendVO>();
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
