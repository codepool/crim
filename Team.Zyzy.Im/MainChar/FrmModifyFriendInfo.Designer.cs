﻿
namespace Team.Zyzy.Im
{
    partial class FrmModifyFriendInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmModifyFriendInfo));
            this.pnlContent = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtOnlineRemark = new System.Windows.Forms.TextBox();
            this.qqLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.labType = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.rdFemale = new System.Windows.Forms.RadioButton();
            this.rdMale = new System.Windows.Forms.RadioButton();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.btnAccept = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnCancel = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtSignature = new System.Windows.Forms.TextBox();
            this.txtNickName = new System.Windows.Forms.TextBox();
            this.skinLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel3 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.pnlFriendImage = new System.Windows.Forms.Panel();
            this.picHeadImage = new System.Windows.Forms.PictureBox();
            this.skinLabel5 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqPanelFixSize1.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.pnlFriendImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImage)).BeginInit();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundImage = null;
            this.qqPanelFixSize1.Controls.Add(this.pnlContent);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(883, 718);
            this.qqPanelFixSize1.Tag = "个人信息";
            this.qqPanelFixSize1.Text = "个人信息";
            // 
            // pnlContent
            // 
            this.pnlContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlContent.Controls.Add(this.txtPhone);
            this.pnlContent.Controls.Add(this.txtOnlineRemark);
            this.pnlContent.Controls.Add(this.qqLabel2);
            this.pnlContent.Controls.Add(this.labType);
            this.pnlContent.Controls.Add(this.rdFemale);
            this.pnlContent.Controls.Add(this.rdMale);
            this.pnlContent.Controls.Add(this.qqLabel1);
            this.pnlContent.Controls.Add(this.btnAccept);
            this.pnlContent.Controls.Add(this.btnCancel);
            this.pnlContent.Controls.Add(this.txtUserName);
            this.pnlContent.Controls.Add(this.txtSignature);
            this.pnlContent.Controls.Add(this.txtNickName);
            this.pnlContent.Controls.Add(this.skinLabel1);
            this.pnlContent.Controls.Add(this.skinLabel2);
            this.pnlContent.Controls.Add(this.skinLabel3);
            this.pnlContent.Controls.Add(this.pnlFriendImage);
            this.pnlContent.Controls.Add(this.skinLabel5);
            this.pnlContent.Location = new System.Drawing.Point(3, 60);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(877, 655);
            this.pnlContent.TabIndex = 10;
            this.pnlContent.Text = "groupBox1";
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.Color.White;
            this.txtPhone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPhone.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtPhone.Location = new System.Drawing.Point(152, 295);
            this.txtPhone.Margin = new System.Windows.Forms.Padding(0);
            this.txtPhone.MinimumSize = new System.Drawing.Size(56, 42);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(494, 42);
            this.txtPhone.TabIndex = 160;
            // 
            // txtOnlineRemark
            // 
            this.txtOnlineRemark.BackColor = System.Drawing.Color.White;
            this.txtOnlineRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOnlineRemark.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtOnlineRemark.Location = new System.Drawing.Point(152, 221);
            this.txtOnlineRemark.Margin = new System.Windows.Forms.Padding(0);
            this.txtOnlineRemark.MinimumSize = new System.Drawing.Size(56, 42);
            this.txtOnlineRemark.Name = "txtOnlineRemark";
            this.txtOnlineRemark.Size = new System.Drawing.Size(494, 42);
            this.txtOnlineRemark.TabIndex = 159;
            // 
            // qqLabel2
            // 
            this.qqLabel2.AutoSize = true;
            this.qqLabel2.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel2.Location = new System.Drawing.Point(9, 228);
            this.qqLabel2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.qqLabel2.Name = "qqLabel2";
            this.qqLabel2.Size = new System.Drawing.Size(134, 31);
            this.qqLabel2.TabIndex = 158;
            this.qqLabel2.Text = "在线说明：";
            // 
            // labType
            // 
            this.labType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labType.AutoSize = true;
            this.labType.BackColor = System.Drawing.Color.Transparent;
            this.labType.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labType.Location = new System.Drawing.Point(708, 259);
            this.labType.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labType.Name = "labType";
            this.labType.Size = new System.Drawing.Size(110, 31);
            this.labType.TabIndex = 157;
            this.labType.Text = "普通用户";
            // 
            // rdFemale
            // 
            this.rdFemale.AutoSize = true;
            this.rdFemale.Location = new System.Drawing.Point(250, 158);
            this.rdFemale.Name = "rdFemale";
            this.rdFemale.Size = new System.Drawing.Size(65, 28);
            this.rdFemale.TabIndex = 156;
            this.rdFemale.TabStop = true;
            this.rdFemale.Text = "女";
            this.rdFemale.UseVisualStyleBackColor = true;
            // 
            // rdMale
            // 
            this.rdMale.AutoSize = true;
            this.rdMale.Location = new System.Drawing.Point(152, 158);
            this.rdMale.Name = "rdMale";
            this.rdMale.Size = new System.Drawing.Size(65, 28);
            this.rdMale.TabIndex = 155;
            this.rdMale.TabStop = true;
            this.rdMale.Text = "男";
            this.rdMale.UseVisualStyleBackColor = true;
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel1.Location = new System.Drawing.Point(57, 158);
            this.qqLabel1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(86, 31);
            this.qqLabel1.TabIndex = 154;
            this.qqLabel1.Text = "性别：";
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.BackColor = System.Drawing.Color.Transparent;
            this.btnAccept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.BackgroundImage")));
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.Caption = "确定";
            this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnAccept.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnAccept.Location = new System.Drawing.Point(714, 559);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(6);
            this.btnAccept.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseDownImage")));
            this.btnAccept.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseMoveImage")));
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.NormalImage")));
            this.btnAccept.Size = new System.Drawing.Size(150, 70);
            this.btnAccept.TabIndex = 149;
            this.btnAccept.ToolTip = null;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Caption = "取消";
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnCancel.Location = new System.Drawing.Point(509, 559);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(6);
            this.btnCancel.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseDownImage")));
            this.btnCancel.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseMoveImage")));
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.Size = new System.Drawing.Size(150, 70);
            this.btnCancel.TabIndex = 148;
            this.btnCancel.ToolTip = null;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.Color.White;
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Enabled = false;
            this.txtUserName.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtUserName.Location = new System.Drawing.Point(152, 87);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(0);
            this.txtUserName.MinimumSize = new System.Drawing.Size(56, 42);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(494, 42);
            this.txtUserName.TabIndex = 143;
            // 
            // txtSignature
            // 
            this.txtSignature.BackColor = System.Drawing.Color.White;
            this.txtSignature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSignature.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtSignature.Location = new System.Drawing.Point(152, 369);
            this.txtSignature.Margin = new System.Windows.Forms.Padding(0);
            this.txtSignature.MinimumSize = new System.Drawing.Size(56, 42);
            this.txtSignature.Multiline = true;
            this.txtSignature.Name = "txtSignature";
            this.txtSignature.Size = new System.Drawing.Size(712, 158);
            this.txtSignature.TabIndex = 146;
            this.txtSignature.Text = "不见面不等于不思念，不联络只是因为有了CRIM。";
            // 
            // txtNickName
            // 
            this.txtNickName.BackColor = System.Drawing.Color.White;
            this.txtNickName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNickName.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtNickName.Location = new System.Drawing.Point(152, 13);
            this.txtNickName.Margin = new System.Windows.Forms.Padding(0);
            this.txtNickName.MinimumSize = new System.Drawing.Size(56, 42);
            this.txtNickName.Name = "txtNickName";
            this.txtNickName.Size = new System.Drawing.Size(494, 42);
            this.txtNickName.TabIndex = 137;
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(57, 18);
            this.skinLabel1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(86, 31);
            this.skinLabel1.TabIndex = 138;
            this.skinLabel1.Text = "昵称：";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(33, 88);
            this.skinLabel2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(110, 31);
            this.skinLabel2.TabIndex = 139;
            this.skinLabel2.Text = "用户名：";
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(57, 298);
            this.skinLabel3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(86, 31);
            this.skinLabel3.TabIndex = 141;
            this.skinLabel3.Text = "电话：";
            // 
            // pnlFriendImage
            // 
            this.pnlFriendImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFriendImage.BackColor = System.Drawing.Color.White;
            this.pnlFriendImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlFriendImage.Controls.Add(this.picHeadImage);
            this.pnlFriendImage.Location = new System.Drawing.Point(659, 13);
            this.pnlFriendImage.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFriendImage.Name = "pnlFriendImage";
            this.pnlFriendImage.Size = new System.Drawing.Size(205, 210);
            this.pnlFriendImage.TabIndex = 147;
            // 
            // picHeadImage
            // 
            this.picHeadImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picHeadImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picHeadImage.Location = new System.Drawing.Point(0, 0);
            this.picHeadImage.Name = "picHeadImage";
            this.picHeadImage.Size = new System.Drawing.Size(205, 210);
            this.picHeadImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picHeadImage.TabIndex = 1;
            this.picHeadImage.TabStop = false;
            this.picHeadImage.Click += new System.EventHandler(this.picHeadImage_Click);
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(9, 368);
            this.skinLabel5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(134, 31);
            this.skinLabel5.TabIndex = 142;
            this.skinLabel5.Text = "个性签名：";
            // 
            // FrmModifyFriendInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 738);
            this.Name = "FrmModifyFriendInfo";
            this.Text = "个人信息";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.pnlFriendImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel pnlContent;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnAccept;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnCancel;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtSignature;
        private System.Windows.Forms.TextBox txtNickName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel2;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel3;
        private System.Windows.Forms.Panel pnlFriendImage;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel5;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private System.Windows.Forms.TextBox txtOnlineRemark;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel2;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labType;
        private System.Windows.Forms.RadioButton rdFemale;
        private System.Windows.Forms.RadioButton rdMale;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.PictureBox picHeadImage;
    }
}