﻿using Newtonsoft.Json;
using Shell32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.BLL;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.BaseControls.PopupEmotion;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.Service.ChartControl;
using YiWangYi.MsgQQ.WinUI.Service.ChartService;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.Business.enums;
using System.Threading;

namespace Team.Zyzy.Im
{
    public partial class FrmPersonChart : FrmWeiChartPersonBase
    {
        private FrmPersonChart()
            : base()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Load += FrmPersonChart_Load;
            this.SendMessageClick += FrmPersonChart_SendMessageClick;
            this.VideoClick += FrmPersonChart_VideoClick;
            this.FileClick += FrmPersonChart_FileClick;
            this.ControlClick += FrmPersonChart_ControlClick;
            this.AudioClick += FrmPersonChart_AudioClick;
            this.SelectChartHistoryClick += FrmPersonChart_SelectChartHistoryClick;
            this.chatBox1.GotFocus += delegate { setRefushUnreadCount(); };
            this.chartEdit.GotFocus += delegate { setRefushUnreadCount(); };
            this.GotFocus += delegate { setRefushUnreadCount(); };
            this.Activated += delegate { setRefushUnreadCount(); };
            initContexMenu();
            qqChartPanel1.HeadImageClick += QqChartPanel1_HeadImageClick;
            qqChartPanel1.OppositeNameClick += QqChartPanel1_HeadImageClick;
        }

        private void QqChartPanel1_HeadImageClick(object sender, EventArgs e)
        {
            FrmModifyFriendInfo friendInfo = new FrmModifyFriendInfo();
            friendInfo.UserId = this._friendInfo.FriendVO.id;
            friendInfo.TopMost = true;
            friendInfo.Show();
        }

        private void FrmPersonChart_Load(object sender, EventArgs e)
        {
        }

        #region 文件上传

        protected override void uploadScreenCutFile(Image img)
        {
            string screenFile = ScreenImageHelp.ScreenPath(img);
            uploadImageFile(screenFile);
        }

        protected override void uploadFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            this.ProgressBarVisible = true;
            try
            {
                HttpFileHelp.UploadFile(file, uploadFileCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                this.ProgressBarVisible = false;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        protected override void uploadImageFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            this.ProgressBarVisible = true;
            try
            {
                HttpFileHelp.UploadImage(file, uploadImageCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                this.ProgressBarVisible = false;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        private void uploadVoiceFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<string> resultModel = JsonConvert.DeserializeObject<Result<string>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                FileInfo file = (FileInfo)sender;
                UploadVoiceVO fileInfo = new UploadVoiceVO();
                fileInfo.url = resultModel.data;
                //获取时间方法一
                string dirName = System.IO.Path.GetDirectoryName(file.FullName);
                FileInfo fInfo = new FileInfo(file.FullName);
                string SongName = System.IO.Path.GetFileName(file.FullName);
                ShellClass sh = new ShellClass();
                Folder dir = sh.NameSpace(dirName);
                FolderItem item = dir.ParseName(SongName);
                string time = Regex.Match(dir.GetDetailsOf(item, -1), "\\d:\\d{2}:\\d{2}").Value;
                string[] timeArray = Regex.Split(time, ":");
                int hour = int.Parse(timeArray[0]);
                int min = int.Parse(timeArray[1]);
                int sec = int.Parse(timeArray[2]);
                fileInfo.duration = (hour * 3600 + min * 60 + sec);

                //获取时间方法二
                //String[] fileNameInfo = file.Name.Split('_');
                //fileInfo.duration = int.Parse(fileNameInfo[fileNameInfo.Length - 2]);

                fileInfo.name = file.Name;
                file = null;
                string content = JsonConvert.SerializeObject(fileInfo);
                int msgType = (int)MessageType.AUDIO;
                sendMessageAndDisplaySyncDB(content, msgType);
            }
            this.ProgressBarVisible = false;
        }

        private void uploadFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<string> resultModel = JsonConvert.DeserializeObject<Result<string>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                FileInfo file = (FileInfo)sender;
                UploadFileVO fileInfo = new UploadFileVO();
                fileInfo.url = resultModel.data;
                fileInfo.size = file.Length;
                fileInfo.name = file.Name;
                file = null;
                string content = JsonConvert.SerializeObject(fileInfo);
                int msgType = (int)MessageType.FILE;
                sendMessageAndDisplaySyncDB(content, msgType);
            }
            this.ProgressBarVisible = false;
        }
        private void uploadImageCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<UploadImageVO> resultModel = JsonConvert.DeserializeObject<Result<UploadImageVO>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                string content = JsonConvert.SerializeObject(resultModel.data);
                int msgType = (int)MessageType.IMAGE;
                sendMessageAndDisplaySyncDB(content, msgType);
            }
            this.ProgressBarVisible = false;
        }

        private void uploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            if (this.ProgressBar.Minimum < e.ProgressPercentage && e.ProgressPercentage < this.ProgressBar.Maximum)
                this.ProgressBar.Value = e.ProgressPercentage;
        }
        #endregion

        #region 顶部功能菜单区
        private void FrmPersonChart_AudioClick(object sender, EventArgs e)
        {
            FrmVoiceRecord frmVoiceRecord = new FrmVoiceRecord();
            frmVoiceRecord.UserID = _friendInfo.ID;
            frmVoiceRecord.StartPosition = FormStartPosition.CenterParent;
            frmVoiceRecord.SendClick += FrmVoiceRecord_SendClick;
            frmVoiceRecord.ShowDialog();
        }

        private void FrmVoiceRecord_SendClick(string fileName, int voiceTimeLength)
        {
            FileInfo file = new FileInfo(fileName);
            this.ProgressBarVisible = true;
            try
            {
                HttpFileHelp.UploadFile(file, uploadVoiceFileCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                this.ProgressBarVisible = false;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        private void FrmPersonChart_ControlClick(object sender, EventArgs e)
        {
            FrmAutoCloseMessageBox.Show("请联系升级");
        }

        private void FrmPersonChart_FileClick(object sender, EventArgs e)
        {
        }

        private void FrmPersonChart_VideoClick(object sender, EventArgs e)
        {
            if (WebRTCServer.Instance != null && WebRTCServer.Instance.IsConnected)
            {
                if (MessageBox.Show("你正在与其他好友通话，确定关闭当前通话？", "关闭对话提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    //已经在对话，先挂断
                    WebRTCServer.Instance.SendHandup();
                }
                else
                {
                    return;
                }
            }

            try
            {
                WebRTCServer.CreateInstance(true);
                if (WebRTCServer.Instance.Form == null)
                {
                    WebRTCServer.Instance.Form = new FrmRTCVideo();
                    WebRTCServer.Instance.Form.FriendInfo = this.FriendInfo;
                    WebRTCServer.Instance.ID = RequestApiInfo.SendId;
                    WebRTCServer.Instance.FriendId = _friendInfo.ID;
                    WebRTCServer.Instance.Form.IsMaster = WebRTCServer.Instance.Master;
                    WebRTCServer.Instance.Form.Show();
                }
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show(ex.Message, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region 中部功能菜单区

        private void FrmPersonChart_SelectChartHistoryClick(object sender, EventArgs e)
        {
            FrmHistoryMessage frmMessage = new FrmHistoryMessage();
            frmMessage.ShowFriendHistory(_friendInfo.ID);
        }

        private void FrmPersonChart_SendMessageClick(object sender, EventArgs e)
        {

            if (this.EditContentIsNull())
            {
                FrmAutoCloseMessageBox.Show("发送内容不能为空");
            }
            RichTextBox txt = sender as RichTextBox;
            txt.Enabled = false;

            string content = EmotionHelp.RtfToText(this.ChartEidt.Rtf);
            int msgType = (int)MessageType.TEXT;
            if (sendMessageAndDisplaySyncDB(content, msgType))
            {
                txt.Text = "";
            }
            txt.Enabled = true;
        }
        #endregion

        #region 发送信息
        public void SendReaded(long friendId)
        {
            try
            {
                string url = "/message/private/readed?friendId=" + friendId;
                HttpHelp.HttpApi<long?>(url, null, "PUT");
            }
            catch (Exception ex)
            {
            }
            ImPrivateMessageBll.Instance.UpdateMessageStatusRead(friendId, RequestApiInfo.SendId);
        }
        private bool sendMessageAndDisplaySyncDB(string content, int msgType)
        {
            PrivateMessage privateMessage = new PrivateMessage();
            privateMessage.type = msgType;
            privateMessage.content = content;
            privateMessage.recvId = _friendInfo.ID;
            privateMessage.sendId = RequestApiInfo.SendId;
            privateMessage.status = -1;
            privateMessage.setSendTime(DateTime.Now);
            AddMessage(privateMessage);
            Task.Run(() => sendMessageAndUpdateDB(privateMessage));
            return true;
        }

        private long sendMessageAndUpdateDB(PrivateMessage privateMessage)
        {
            long msgId = sendPrivateMessage(privateMessage);
            int status = msgId > 0 ? (int)MessageStatus.Send : (int)MessageStatus.UnSend;
            Action action = () =>
            {
                IMessage msg = chatBox1.getMessage(privateMessage.getSendTime(), privateMessage.id);
                msg.setStatus(status);
                msg.setMessageID(msgId);
                privateMessage.id = msgId;
                privateMessage.status = status;
                msg.Tag = privateMessage;
                FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
                frmMain.UpdateHistoryMessage(privateMessage);
            };
            Invoke(action);
            return msgId;
        }

        private  long sendPrivateMessage(PrivateMessage privateMessage)
        {
            long msgId = 0;
            try
            {
                string url = "/message/private/send";
                msgId = HttpHelp.HttpApi<long>(url, privateMessage, "POST");
            }
            catch (Exception ex)
            {
                msgId = 0;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
            return msgId;
        }

        #endregion

        #region 显示及初始化
        private void initLastMessage()
        {
            this.SuspendLayout();
            this.Text = _friendInfo.NickName;
            this.ShowInTaskbar = true;
            chatBox1.ClearMessage();
            try
            {
                List<PrivateMessage> messages = ImPrivateMessageBll.Instance.GetRecentTopList(_friendInfo.ID, 10);
                messages.Sort();
                foreach (PrivateMessage item in messages)
                {
                    ChartMessageHelp.DisplaySingle(chatBox1, item, _friendInfo.ID);
                }
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
            //this.SetEndOpacity();
            this.ResumeLayout(false);
        }

        public void LoadOverShow()
        {
            initLastMessage();
            Point location = this.Location;
            this.Location = new Point(-10000, -1000);
            this.Shown += delegate { this.Location = location; };
            this.Show();
            setRefushUnreadCount();
        }
        #endregion
        private void setRefushUnreadCount()
        {
            if (_friendInfo.MessageCount > 0)
            {
                _friendInfo.MessageCount = 0;
                ShareInfo.FormQQMain.RefushUnreadCount();
                Task.Run(() => SendReaded(_friendInfo.ID));
            }
        }

        #region 属性
        private FriendVOItem _friendInfo = new FriendVOItem();
        public FriendVOItem FriendInfo
        {
            get
            {
                return _friendInfo;
            }
            set
            {
                _friendInfo = value;
                qqChartPanel1.HeadImage = _friendInfo.HeadImage;
                qqChartPanel1.OppositeName = _friendInfo.NickName + (string.IsNullOrEmpty(_friendInfo.DisplayName) ? "" : "(" + _friendInfo.DisplayName + ")");
                qqChartPanel1.Signature = _friendInfo.PersonSignaure;
                this.Text = qqChartPanel1.OppositeName;
            }
        }
        #endregion

        #region 加入一条信息

        public void AddMessage(PrivateMessage message)
        {
            Action action = () =>
            {
                ChartMessageHelp.DisplaySingle(chatBox1, message, _friendInfo.ID);
            };
            Invoke(action);
        }

        #endregion

        #region 每个人单窗体模式相关设置

        public static FrmPersonChart GetInstance(object key)
        {
            FrmPersonChart frmChart = FindInstance<FrmPersonChart>(key);
            if (frmChart == null)
            {
                frmChart = new FrmPersonChart();
                Form previous = FindMaxLocationChartForm();
                if (previous != null)
                {
                    frmChart.Left = previous.Left + 25;
                    frmChart.Top = previous.Top + 25;
                }
                else
                {
                    frmChart.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Width - frmChart.Width) / 2;
                    frmChart.Top = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Height - frmChart.Height) / 2;
                }
                frmChart.StartPosition = FormStartPosition.Manual;
                frmChart.IdentifyKey = key;
            }
            return frmChart;
        }

        private void initInfo()
        {
            this.Tag = IdentifyKey.ToString();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        #endregion

        private void InitializeComponent()
        {

        }
        #region 单条信息处理菜单

        private void initContexMenu()
        {
            MessageContextMenuHelp.Instance.AddCallPersonClick += Instance_AddCallPersonClick;
            MessageContextMenuHelp.Instance.AddFriendClick += Instance_AddFriendClick;
            MessageContextMenuHelp.Instance.CopyMessageClick += Instance_CopyMessageClick;
            MessageContextMenuHelp.Instance.CreateGroupClick += Instance_CreateGroupClick;
            MessageContextMenuHelp.Instance.DeleteMessageClick += Instance_DeleteMessageClick;
            MessageContextMenuHelp.Instance.FriendInfoClick += Instance_FriendInfoClick;
            MessageContextMenuHelp.Instance.RecallMessageClick += Instance_RecallMessageClick;
            MessageContextMenuHelp.Instance.RetrySendMessageClick += Instance_RetrySendMessageClick;
            MessageContextMenuHelp.Instance.SendPersonMessageClick += Instance_SendPersonMessageClick;
        }

        private void Instance_SendPersonMessageClick(object sender, IMessage message, bool isFriend)
        {
            chartEdit.Focus();
        }

        private void Instance_RetrySendMessageClick(object sender, IMessage message, bool isFriend)
        {
            try
            {
                PrivateMessage privateMessage = message.Tag as PrivateMessage;
                privateMessage.status=(int)YiWangYi.MsgQQ.WinUI.Service.ChartControl.MsgStatus.Send;
                AddMessage(privateMessage);
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("重发信息失败：：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        private void Instance_RecallMessageClick(object sender, IMessage message, bool isFriend)
        {
            try
            {
                PrivateMessage privateMessage = message.Tag as PrivateMessage;
                privateMessage.type = (int)MessageType.RECALL;
                privateMessage.content = null;

                string url = "/message/private/recall/" + privateMessage.id;
                HttpHelp.HttpApi<Result<string>>(url, null, "DELETE");

                chatBox1.SetMessageStatus(privateMessage.id, (int)MessageStatus.Recall);
                //更新本地信息为已经撤回
                Task.Run(() => ImPrivateMessageBll.Instance.UpdateMessageStatusRecall(privateMessage.id));
                Task.Run(() => ImHistoryMessageBll.Instance.UpdatePrivateMessageStatusRecall(privateMessage.recvId));
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("撤回信息失败：：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        private void Instance_FriendInfoClick(object sender, IMessage message, bool isFriend)
        {
            throw new NotImplementedException();
        }

        private void Instance_DeleteMessageClick(object sender, IMessage message, bool isFriend)
        {
            FrmAutoCloseMessageBox.Show("目前信息可以撤回，不能删除！" , MessageBoxIcon.Information);
        }

        private void Instance_CreateGroupClick(object sender, IMessage message, bool isFriend)
        {
            throw new NotImplementedException();
        }

        private void Instance_CopyMessageClick(object sender, IMessage message, bool isFriend)
        {
            PrivateMessage privateMessage = message.Tag as PrivateMessage;
            if (privateMessage.type == (int)MessageType.TEXT)
            {
                Clipboard.SetDataObject(privateMessage.content);
                FrmAutoCloseMessageBox.Show("复制成功！", MessageBoxIcon.Information);
            }
            else
            {
                FrmAutoCloseMessageBox.Show("只能复制文本信息，其他内容请用转发功能！", MessageBoxIcon.Information);
            }
        }

        private void Instance_AddFriendClick(object sender, IMessage message, bool isFriend)
        {
            FrmAutoCloseMessageBox.Show("对方已经是你好友！", MessageBoxIcon.Information);
        }

        private void Instance_AddCallPersonClick(object sender, IMessage message, bool isFriend)
        {
            chartEdit.SelectedText=("@" + this.FriendInfo.NickName);
        }

        #endregion
    }
}