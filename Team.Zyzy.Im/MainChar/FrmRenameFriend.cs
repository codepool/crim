﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.mainService;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.ContractPanel;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.Model.Chart;

namespace Team.Zyzy.Im
{
    public partial class FrmRenameFriend : FormQQFixSize
    {
        private FriendVO friend = null;
        public FriendVO FriendVO
        {
            get
            {
                return friend;
            }
            set
            {
                if (friend != value)
                {
                    friend = value;
                    txtFriendRemarkName.Text = string.IsNullOrWhiteSpace(friend.remarkName) ? friend.nickName : friend.remarkName;
                }
            }
        }
        public QQContractListBase ContractListBar;
        public FrmRenameFriend()
        {
            InitializeComponent();
            this.Load += FrmClassifyManage_Load;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.TopMost = true;
        }

        private void FrmClassifyManage_Load(object sender, EventArgs e)
        {
 
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            string url = "/friend/find/" + friend.id;
            friend = HttpHelp.HttpApi<FriendVO>(url, null, "GET");
            friend.remarkName = txtFriendRemarkName.Text.Trim();
            url = "/friend/update";
            HttpHelp.HttpApi<ClassifyVO>(url, friend, "PUT");
            LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
