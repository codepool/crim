﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Control;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.Business.vo;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.BaseControls;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using Team.Zyzy.Im.Control.Model;
using System.Threading;

namespace Team.Zyzy.Im
{
    public partial class FrmAddFriendAndGroup : FormQQFixSize
    {
        public FrmAddFriendAndGroup()
        {
            InitializeComponent();
            txtInputName.LostFocus += TxtInputName_LostFocus;
            txtInputName.GotFocus += TxtInputName_GotFocus;
            txtInputGroupName.LostFocus += TxtInputGroupName_LostFocus;
            txtInputGroupName.GotFocus += TxtInputGroupName_GotFocus;
            this.Load += FrmAddFriend_Load;
        }

        private void FrmAddFriend_Load(object sender, EventArgs e)
        {
            pnlFriendContent.Controls.Clear();
            pnlGroupContent.Controls.Clear();
            picFriendLoad.Visible = false;
            picGroupLoad.Visible = false;
            pnlGroupContent.Left= qqPaneSearchGroup.Left;
            pnlGroupContent.Width = qqPaneSearchGroup.Width;
            pnlFriendContent.Left = qqPaneSearchFriend.Left;
            pnlFriendContent.Width = pnlFriendContent.Width;
        }

        public FrmAddFriendAndGroup(bool displayGroup) : this()
        {
            qtcFriendAndGroup.SelectedIndex = displayGroup ? 1 : 0;
        }

        #region 加入好友
        private void TxtInputName_LostFocus(object sender, EventArgs e)
        {
            if (txtInputName.Text == "")
            {
                txtInputName.Text = "请输入好友昵称";
                txtInputName.ForeColor = Color.FromArgb(224, 224, 224);
            }
        }

        private void TxtInputName_GotFocus(object sender, EventArgs e)
        {
            if (txtInputName.Text == "请输入好友昵称")
            {
                txtInputName.Text = "";
                txtInputName.ForeColor = Color.Black;
            }
        }

        private void btnQueryFriend_Click(object sender, EventArgs e)
        {
            pnlFriendContent.Controls.Clear();
            pnlFriendContent.Controls.Add(picFriendLoad);
            picFriendLoad.Visible = true;
            picFriendLoad.BringToFront();
            setLoading(picFriendLoad);
            List<User> users = searchFriend(txtInputName.Text.Trim());
            int top = 0;
            foreach (User item in users)
            {
                UCFriendItemInfo ucFriend = new UCFriendItemInfo();
                ucFriend.HeadImg = HeadImageHelp.FriendHeadImage(item.headImageThumb, item.id);
                ucFriend.NickName = item.nickName;
                ucFriend.OnLine = item.online;
                ucFriend.Location = new Point(2, top);
                ucFriend.Width = this.Width - 10;
                ucFriend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
                ucFriend.HaveAdd = isFriend(item.id);
                ucFriend.Tag = item;
                if (item.id == RequestApiInfo.SendId)
                {
                    ucFriend.HaveAdd = true;
                }
                ucFriend.OnAddClick += UcFriend_OnAddClick;
                top += ucFriend.Height;
                pnlFriendContent.Controls.Add(ucFriend);
            }
            this.ChangeSkin();
            picFriendLoad.Visible = false;
        }

        private void UcFriend_OnAddClick(object sender, EventArgs e)
        {
            User user = sender as User;
            try
            {
                string para = "friendId=" + user.id;
                string url = "/friend/add";
                HttpHelp.HttpApi<long?>(url, para, "POST");
                FrmMain frmMain = ShareInfo.FormQQMain as FrmMain;
                frmMain.RefreshFriends();
                FrmAutoCloseMessageBox.Show("添加成功，对方已成为您的好友");
            }
            catch
            {
            }
        }

        private bool isFriend(long userId)
        {
            FrmMain frmMain = ShareInfo.FormQQMain as FrmMain;
            foreach (ClassificationT<FriendVOItem> group in frmMain.ContractListBar.FriendsList.Groups)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    FriendVOItem item = group[i];
                    FriendVO friend = item.FriendVO;
                    if (friend.id == userId)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private List<User> searchFriend(string nickName)
        {
            try
            {
                string para = "name=" + nickName;
                string url = "/user/findByName";
                List<User> users = HttpHelp.HttpApi<List<User>>(url, para, "GET");
                return users;
            }
            catch
            {
                return new List<User>();
            }
        }

        #endregion

        #region 加入群

        private void TxtInputGroupName_LostFocus(object sender, EventArgs e)
        {
            if (txtInputGroupName.Text == "")
            {
                txtInputGroupName.Text = "请输入群名称";
                txtInputGroupName.ForeColor = Color.FromArgb(224, 224, 224);
            }
        }

        private void TxtInputGroupName_GotFocus(object sender, EventArgs e)
        {
            if (txtInputGroupName.Text == "请输入群名称")
            {
                txtInputGroupName.Text = "";
                txtInputGroupName.ForeColor = Color.Black;
            }
        }

        private void btnQueryGroup_Click(object sender, EventArgs e)
        {
            pnlGroupContent.Controls.Clear();
            picGroupLoad.Visible = true;
            picGroupLoad.BringToFront();
            pnlGroupContent.Controls.Add(picGroupLoad);
            setLoading(picGroupLoad);
            List<GroupSearchVO> groups = searchGroup(txtInputGroupName.Text.Trim());
            int top = 0;
            foreach (GroupSearchVO item in groups)
            {
                UCFriendItemInfo ucGroup = new UCFriendItemInfo();
                ucGroup.HeadImg = HeadImageHelp.GroupHeadImage(item.headImageThumb, item.id);
                ucGroup.NickName = item.name+"["+item.ownerName+"]";
                ucGroup.OnLine = item.ownerOnline;
                ucGroup.Location = new Point(2, top);
                ucGroup.Width = this.Width - 10;
                ucGroup.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
| System.Windows.Forms.AnchorStyles.Right)));
                ucGroup.HaveAdd = haveGroup(item.id);
                ucGroup.Tag = item;
                ucGroup.OnAddClick += UcGroup_OnAddClick;
                top += ucGroup.Height;
                pnlGroupContent.Controls.Add(ucGroup);
            }
            this.ChangeSkin();
            picGroupLoad.Visible = false;
        }

        private bool haveGroup(long groupId)
        {
            FrmMain frmMain = ShareInfo.FormQQMain as FrmMain;
            foreach (ClassificationT<GroupVOItem> group in frmMain.ContractListBar.GroupList.Groups)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    GroupVOItem item = group[i];
                    GroupVO friend = item.GroupVO;
                    if (friend.id == groupId)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private List<GroupSearchVO> searchGroup(string nickName)
        {
            try
            {
                string para = "name=" + nickName;
                string url = "/group/findByName";
                List<GroupSearchVO> groups = HttpHelp.HttpApi<List<GroupSearchVO>>(url, para, "GET");
                return groups;
            }
            catch
            {
                return new List<GroupSearchVO>();
            }
        }

        private void UcGroup_OnAddClick(object sender, EventArgs e)
        {
            FrmAutoCloseMessageBox.Show("个人不能加入群组，让群成员邀请你加入吧！");
            return;
            GroupSearchVO gsv = sender as GroupSearchVO;
            GroupInviteVO inviteVO = new GroupInviteVO();
            inviteVO.groupId = gsv.id;
            inviteVO.friendIds = new List<long>();
            inviteVO.friendIds.Add(RequestApiInfo.SendId);
            FrmMain frmMain = ShareInfo.FormQQMain as FrmMain;
            //foreach (ClassificationT<FriendVOItem> group in frmMain.ContractListBar.FriendsList.Groups)
            //{
            //    for (int i = 0; i < group.Count; i++)
            //    {
            //        FriendVOItem item = group[i];
            //        inviteVO.friendIds.Add(item.FriendVO.id);
            //    }
            //}
            try
            {
                string url = "/group/invite";
                HttpHelp.HttpApi<long?>(url, inviteVO, "POST");
                frmMain.RefreshGroups();
                FrmAutoCloseMessageBox.Show("好友已发起邀请，如果该群没有好友，请先添加群主为好友！");
            }
            catch
            {
            }
        }

        #endregion
        private void setLoading(PictureBox picFriendLoad)
        {
            picFriendLoad.Width = picFriendLoad.Parent.Width;
            picFriendLoad.Height = picFriendLoad.Parent.Height;
            picFriendLoad.Location = new Point(0, 0);
            picFriendLoad.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        private void qtcFriend_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Text = qtcFriendAndGroup.SelectedIndex == 0 ? "添加好友" : "加入群组";
        }

    }
}
