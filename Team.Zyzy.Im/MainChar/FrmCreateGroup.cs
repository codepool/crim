﻿using Microsoft.MixedReality.WebRTC;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using Team.Zyzy.Im.Control.Model;
using System.IO;
using System.Net;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.mainService;
using YiWangYi.MsgQQ.WinUI;

namespace Team.Zyzy.Im
{
    public partial class FrmCreateGroup : FormQQCommon
    {
        public FrmCreateGroup()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Load += FrmCreateGroup_Load;
        }
        public bool IsInvite = false;//是否是邀请动作，fale代表创建和修改
        private bool _isCreate = false;//是创建群还是修改群
        private List<FriendVO> _selectedFriends = null;
        public List<FriendVO> SelectedFriends
        {
            get
            {
                return _selectedFriends;
            }
            set
            {
                _selectedFriends = value;
            }
        }
        private GroupVO _groupVO = null;
        public GroupVO GroupVO
        {
            get
            {
                return _groupVO;
            }
            set
            {
                _groupVO = value;
            }
        }

        private void FrmCreateGroup_SizeChanged(object sender, EventArgs e)
        {
            int height = txtGroupName.Height;
            txtGroupName.Width = pnlGroupImage.Left - txtGroupName.Left - 12;
            txtGroupName.Height = height;
            txtOwnerName.Width = pnlGroupImage.Left - txtOwnerName.Left - 12;
            txtOwnerName.Height = height;
            txtOwnerRemark.Width = pnlGroupImage.Left - txtOwnerRemark.Left - 12;
            txtOwnerRemark.Height = height;
            txtGroupRemark.Width = pnlGroupImage.Left + pnlGroupImage.Width - txtGroupRemark.Left;
            txtGroupRemark.Height = height;
            txtGroupNotice.Width = pnlGroupImage.Left + pnlGroupImage.Width - txtGroupNotice.Left;
            txtGroupNotice.BringToFront();
        }

        private void FrmCreateGroup_Load(object sender, EventArgs e)
        {
            this.SizeChanged += FrmCreateGroup_SizeChanged;
            pnlContent.ChangeSkin();
            if (_groupVO == null)
            {
                _groupVO = new GroupVO();
                _groupVO.aliasName = RequestApiInfo.SendNickName;
                _groupVO.ownerId = RequestApiInfo.SendId;
                _groupVO.name = RequestApiInfo.SendNickName + "创建群聊";
                _groupVO.remark = _groupVO.name;
                _groupVO.notice = "入群者，请修改备注为：部门+姓名！";
                _isCreate = true;
            }
            else
            {
                _isCreate = false;
                if(_groupVO.ownerId== RequestApiInfo.SendId)
                {
                    this.Text = "查看/修改群组";
                }
                else
                {
                    this.Text = "查看群组";
                }
                string url = "/group/find/"+_groupVO.id;
                _groupVO = HttpHelp.HttpApi<GroupVO>(url, null, "GET");
            }
            txtGroupName.Text = _groupVO.name;
            txtGroupNotice.Text = _groupVO.notice;
            txtOwnerName.Text = RequestApiInfo.SendNickName;
            txtOwnerRemark.Text = RequestApiInfo.SendNickName;
            txtGroupRemark.Text= _groupVO.remark;
            picHeadImage.Image= HeadImageHelp.GroupHeadImage(_groupVO.headImageThumb, _groupVO.id);
            lvSelectedFriends.Click += LvSelectedFriends_Click;
            if (IsInvite)
            {
                txtGroupName.Enabled = false;
                txtGroupNotice.Enabled = false;
                txtOwnerName.Enabled = false;
                txtOwnerRemark.Enabled = false;
                txtGroupRemark.Enabled = false;
                picHeadImage.Enabled = false;
            }
        }

        private void LvSelectedFriends_Click(object sender, EventArgs e)
        {
            if (lvSelectedFriends.SelectedItems.Count > 0)
            {
                FriendVO friend = lvSelectedFriends.SelectedItems[0].Tag as FriendVO;
                SelectedFriends.Remove(friend);
                displaySelectedFriends();
            }
        }

        private void displaySelectedFriends()
        {
            lvSelectedFriends.Items.Clear();
            foreach (FriendVO item in SelectedFriends)
            {
                ListViewItem lvItem = new ListViewItem();
                lvItem.Text = item.nickName;
                lvItem.ImageKey = item.id.ToString();
                lvItem.Tag = item;
                lvSelectedFriends.Items.Add(lvItem);
            }
        }


        private void btnInvite_Click(object sender, EventArgs e)
        {
            FrmSelectFriends frmSelectFriends = new FrmSelectFriends();
            frmSelectFriends.ShowDialog();
            lvSelectedFriends.LargeImageList = frmSelectFriends.FriendsImage;
            this.SelectedFriends = frmSelectFriends.SelectedFriends;
            displaySelectedFriends();
        }


        private void btnCreateClick(object sender, EventArgs e)
        {
            if (IsInvite)
            {
                if (SelectedFriends == null || SelectedFriends.Count == 0)
                {
                    FrmAutoCloseMessageBox.Show("请先选择需要邀请的群组成员！");
                    return;
                }
                inviteGroupMembers();
                FrmAutoCloseMessageBox.Show("邀请成功，请打开群查看！");
                this.Close();
            }
            else
            {
                if (_groupVO.id <= 0 && _isCreate)
                {
                    if (SelectedFriends == null || SelectedFriends.Count == 0)
                    {
                        FrmAutoCloseMessageBox.Show("请先邀请群组成员！");
                        return;
                    }
                }
                if (_headImagePath != null)
                {
                    uploadImageFile(_headImagePath);
                }
                else
                {
                    saveGroupInfo();
                }
            }
        }

        public void saveGroupInfo()
        {
            _groupVO.name = txtGroupName.Text.Trim();
            _groupVO.notice = txtGroupNotice.Text.Trim();
            _groupVO.headImage = _headImageUrl;
            _groupVO.headImageThumb = _headImageThumbUrl;
            _groupVO.aliasName = txtOwnerRemark.Text.Trim();
            _groupVO.remark = txtGroupRemark.Text.Trim();
            if (_groupVO.id <= 0)
            {
                string url = "/group/create";
                _groupVO = HttpHelp.HttpApi<GroupVO>(url, _groupVO, "POST");
            }
            else
            {
                string url = "/group/modify";
                _groupVO = HttpHelp.HttpApi<GroupVO>(url, _groupVO, "PUT");
            }

            inviteGroupMembers();
            if (_isCreate)
            {
                FrmAutoCloseMessageBox.Show("创建群组成功，请在“群聊”分组中查看！");
            }
            else
            {
                FrmAutoCloseMessageBox.Show("更新群聊信息成功！");
            }
            FrmMain frmMain = ShareInfo.FormQQMain as FrmMain;
            frmMain.RefreshGroups();
            this.Close();
        }

        private void inviteGroupMembers()
        {
            if(SelectedFriends==null || SelectedFriends.Count == 0)
            {
                return;
            }
            GroupInviteVO groupInvite = new GroupInviteVO();
            groupInvite.groupId = _groupVO.id;
            groupInvite.friendIds = new List<long>();
            foreach (FriendVO item in SelectedFriends)
            {
                groupInvite.friendIds.Add(item.id);
            }
            string url2 = "/group/invite";
            HttpHelp.HttpApi<GroupVO>(url2, groupInvite, "POST");
        }

        #region 上传头像图片
        private string _headImagePath = null;
        private string _headImageUrl = null;
        private string _headImageThumbUrl = null;
        private void picHeadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "打开图片(Open Image)";
            ofd.FileName = "";
            ofd.Filter = "Image Files(*.bmp;*.jpg;*.gif;*.png)|*.bmp;*.jpg;*.gif;*.png";
            ofd.ValidateNames = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    _headImagePath = ofd.FileName;
                    Image img = Image.FromFile(_headImagePath);
                    picHeadImage.Image = img;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        protected  void uploadImageFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            try
            {
                HttpFileHelp.UploadImage(file, uploadImageCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }
        private void uploadImageCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<UploadImageVO> resultModel = JsonConvert.DeserializeObject<Result<UploadImageVO>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                _headImageThumbUrl = resultModel.data.thumbUrl;
                _headImageUrl = resultModel.data.originUrl;
                saveGroupInfo();
            }
        }

        private void uploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
        }
        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

