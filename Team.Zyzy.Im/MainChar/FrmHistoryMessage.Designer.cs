﻿
namespace Team.Zyzy.Im
{
    partial class FrmHistoryMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHistoryMessage));
            this.qtcContent = new YiWangYi.MsgQQ.WinUI.BaseControls.QQTabControl();
            this.tpFriend = new System.Windows.Forms.TabPage();
            this.spcFriend = new YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer();
            this.qlbFriend = new YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar();
            this.ptFriend = new YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn();
            this.cbFriend = new YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox();
            this.tpGroup = new System.Windows.Forms.TabPage();
            this.spcGroup = new YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer();
            this.qlbGroup = new YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar();
            this.tpHistory = new System.Windows.Forms.TabPage();
            this.spcHistory = new YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer();
            this.qlbHistory = new YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar();
            this.tpSystem = new System.Windows.Forms.TabPage();
            this.spcSystem = new YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer();
            this.qlbSystem = new YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar();
            this.ptGroup = new YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn();
            this.cbGroup = new YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox();
            this.ptHistory = new YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn();
            this.cbHistory = new YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox();
            this.ptSystem = new YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn();
            this.cbSystem = new YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox();
            this.qqFrmRound.SuspendLayout();
            this.qtcContent.SuspendLayout();
            this.tpFriend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcFriend)).BeginInit();
            this.spcFriend.Panel1.SuspendLayout();
            this.spcFriend.Panel2.SuspendLayout();
            this.spcFriend.SuspendLayout();
            this.tpGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcGroup)).BeginInit();
            this.spcGroup.Panel1.SuspendLayout();
            this.spcGroup.Panel2.SuspendLayout();
            this.spcGroup.SuspendLayout();
            this.tpHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcHistory)).BeginInit();
            this.spcHistory.Panel1.SuspendLayout();
            this.spcHistory.Panel2.SuspendLayout();
            this.spcHistory.SuspendLayout();
            this.tpSystem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcSystem)).BeginInit();
            this.spcSystem.Panel1.SuspendLayout();
            this.spcSystem.Panel2.SuspendLayout();
            this.spcSystem.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqFrmRound
            // 
            this.qqFrmRound.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqFrmRound.BackgroundImage")));
            this.qqFrmRound.Controls.Add(this.qtcContent);
            this.qqFrmRound.Location = new System.Drawing.Point(5, 5);
            this.qqFrmRound.Size = new System.Drawing.Size(1232, 955);
            this.qqFrmRound.Text = "消息管理器";
            // 
            // qtcContent
            // 
            this.qtcContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qtcContent.Controls.Add(this.tpFriend);
            this.qtcContent.Controls.Add(this.tpGroup);
            this.qtcContent.Controls.Add(this.tpHistory);
            this.qtcContent.Controls.Add(this.tpSystem);
            this.qtcContent.Location = new System.Drawing.Point(0, 53);
            this.qtcContent.myBackColor = System.Drawing.SystemColors.Control;
            this.qtcContent.Name = "qtcContent";
            this.qtcContent.SelectedIndex = 0;
            this.qtcContent.Size = new System.Drawing.Size(1232, 902);
            this.qtcContent.TabIndex = 0;
            this.qtcContent.SelectedIndexChanged += new System.EventHandler(this.qtcContent_SelectedIndexChanged);
            // 
            // tpFriend
            // 
            this.tpFriend.BackColor = System.Drawing.Color.White;
            this.tpFriend.Controls.Add(this.spcFriend);
            this.tpFriend.Location = new System.Drawing.Point(8, 35);
            this.tpFriend.Name = "tpFriend";
            this.tpFriend.Padding = new System.Windows.Forms.Padding(3);
            this.tpFriend.Size = new System.Drawing.Size(1216, 859);
            this.tpFriend.TabIndex = 0;
            this.tpFriend.Text = "联系人";
            // 
            // spcFriend
            // 
            this.spcFriend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcFriend.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcFriend.Location = new System.Drawing.Point(3, 3);
            this.spcFriend.Name = "spcFriend";
            // 
            // spcFriend.Panel1
            // 
            this.spcFriend.Panel1.Controls.Add(this.qlbFriend);
            // 
            // spcFriend.Panel2
            // 
            this.spcFriend.Panel2.Controls.Add(this.ptFriend);
            this.spcFriend.Panel2.Controls.Add(this.cbFriend);
            this.spcFriend.Size = new System.Drawing.Size(1210, 853);
            this.spcFriend.SplitterDistance = 293;
            this.spcFriend.SplitterWidth = 1;
            this.spcFriend.TabIndex = 0;
            // 
            // qlbFriend
            // 
            this.qlbFriend.AllowDragItem = false;
            this.qlbFriend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qlbFriend.FlatStyle = YiWangYi.MsgQQ.WinUI.QQListBar.QFlatStyle.None;
            this.qlbFriend.GroupHeaderBackColor = System.Drawing.Color.Gray;
            this.qlbFriend.GroupTextColor = System.Drawing.Color.Black;
            this.qlbFriend.HeightV = 853;
            this.qlbFriend.IsShowBackgroundImage = true;
            this.qlbFriend.ItemStyle = YiWangYi.MsgQQ.WinUI.QQListBar.CbItemStyle.PushButton;
            this.qlbFriend.LeftX = 0;
            this.qlbFriend.Location = new System.Drawing.Point(0, 0);
            this.qlbFriend.Name = "qlbFriend";
            this.qlbFriend.Size = new System.Drawing.Size(293, 853);
            this.qlbFriend.TabIndex = 0;
            this.qlbFriend.TopY = 0;
            this.qlbFriend.View = YiWangYi.MsgQQ.WinUI.QQListBar.CbViewStyle.LargeIcon;
            this.qlbFriend.WidthH = 293;
            // 
            // ptFriend
            // 
            this.ptFriend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ptFriend.CurrentPage = 1;
            this.ptFriend.Location = new System.Drawing.Point(709, 793);
            this.ptFriend.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ptFriend.Name = "ptFriend";
            this.ptFriend.Size = new System.Drawing.Size(210, 54);
            this.ptFriend.SumPage = 1;
            this.ptFriend.TabIndex = 2;
            // 
            // cbFriend
            // 
            this.cbFriend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFriend.BackColor = System.Drawing.Color.Transparent;
            this.cbFriend.Location = new System.Drawing.Point(8, 6);
            this.cbFriend.Margin = new System.Windows.Forms.Padding(6);
            this.cbFriend.Name = "cbFriend";
            this.cbFriend.Size = new System.Drawing.Size(911, 788);
            this.cbFriend.TabIndex = 1;
            // 
            // tpGroup
            // 
            this.tpGroup.BackColor = System.Drawing.Color.White;
            this.tpGroup.Controls.Add(this.spcGroup);
            this.tpGroup.Location = new System.Drawing.Point(8, 35);
            this.tpGroup.Name = "tpGroup";
            this.tpGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tpGroup.Size = new System.Drawing.Size(1216, 859);
            this.tpGroup.TabIndex = 1;
            this.tpGroup.Text = "群聊";
            // 
            // spcGroup
            // 
            this.spcGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcGroup.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcGroup.Location = new System.Drawing.Point(3, 3);
            this.spcGroup.Name = "spcGroup";
            // 
            // spcGroup.Panel1
            // 
            this.spcGroup.Panel1.Controls.Add(this.qlbGroup);
            // 
            // spcGroup.Panel2
            // 
            this.spcGroup.Panel2.Controls.Add(this.ptGroup);
            this.spcGroup.Panel2.Controls.Add(this.cbGroup);
            this.spcGroup.Size = new System.Drawing.Size(1210, 853);
            this.spcGroup.SplitterDistance = 293;
            this.spcGroup.SplitterWidth = 1;
            this.spcGroup.TabIndex = 1;
            // 
            // qlbGroup
            // 
            this.qlbGroup.AllowDragItem = false;
            this.qlbGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qlbGroup.FlatStyle = YiWangYi.MsgQQ.WinUI.QQListBar.QFlatStyle.None;
            this.qlbGroup.GroupHeaderBackColor = System.Drawing.Color.Gray;
            this.qlbGroup.GroupTextColor = System.Drawing.Color.Black;
            this.qlbGroup.HeightV = 853;
            this.qlbGroup.IsShowBackgroundImage = true;
            this.qlbGroup.ItemStyle = YiWangYi.MsgQQ.WinUI.QQListBar.CbItemStyle.PushButton;
            this.qlbGroup.LeftX = 0;
            this.qlbGroup.Location = new System.Drawing.Point(0, 0);
            this.qlbGroup.Name = "qlbGroup";
            this.qlbGroup.Size = new System.Drawing.Size(293, 853);
            this.qlbGroup.TabIndex = 0;
            this.qlbGroup.TopY = 0;
            this.qlbGroup.View = YiWangYi.MsgQQ.WinUI.QQListBar.CbViewStyle.LargeIcon;
            this.qlbGroup.WidthH = 293;
            // 
            // tpHistory
            // 
            this.tpHistory.BackColor = System.Drawing.Color.White;
            this.tpHistory.Controls.Add(this.spcHistory);
            this.tpHistory.Location = new System.Drawing.Point(8, 35);
            this.tpHistory.Name = "tpHistory";
            this.tpHistory.Size = new System.Drawing.Size(1216, 859);
            this.tpHistory.TabIndex = 2;
            this.tpHistory.Text = "历史会话";
            // 
            // spcHistory
            // 
            this.spcHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcHistory.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcHistory.Location = new System.Drawing.Point(0, 0);
            this.spcHistory.Name = "spcHistory";
            // 
            // spcHistory.Panel1
            // 
            this.spcHistory.Panel1.Controls.Add(this.qlbHistory);
            // 
            // spcHistory.Panel2
            // 
            this.spcHistory.Panel2.Controls.Add(this.ptHistory);
            this.spcHistory.Panel2.Controls.Add(this.cbHistory);
            this.spcHistory.Size = new System.Drawing.Size(1216, 859);
            this.spcHistory.SplitterDistance = 293;
            this.spcHistory.SplitterWidth = 1;
            this.spcHistory.TabIndex = 1;
            // 
            // qlbHistory
            // 
            this.qlbHistory.AllowDragItem = false;
            this.qlbHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qlbHistory.FlatStyle = YiWangYi.MsgQQ.WinUI.QQListBar.QFlatStyle.None;
            this.qlbHistory.GroupHeaderBackColor = System.Drawing.Color.Gray;
            this.qlbHistory.GroupTextColor = System.Drawing.Color.Black;
            this.qlbHistory.HeightV = 859;
            this.qlbHistory.IsShowBackgroundImage = true;
            this.qlbHistory.ItemStyle = YiWangYi.MsgQQ.WinUI.QQListBar.CbItemStyle.PushButton;
            this.qlbHistory.LeftX = 0;
            this.qlbHistory.Location = new System.Drawing.Point(0, 0);
            this.qlbHistory.Name = "qlbHistory";
            this.qlbHistory.Size = new System.Drawing.Size(293, 859);
            this.qlbHistory.TabIndex = 0;
            this.qlbHistory.TopY = 0;
            this.qlbHistory.View = YiWangYi.MsgQQ.WinUI.QQListBar.CbViewStyle.LargeIcon;
            this.qlbHistory.WidthH = 293;
            // 
            // tpSystem
            // 
            this.tpSystem.BackColor = System.Drawing.Color.White;
            this.tpSystem.Controls.Add(this.spcSystem);
            this.tpSystem.Location = new System.Drawing.Point(8, 35);
            this.tpSystem.Name = "tpSystem";
            this.tpSystem.Size = new System.Drawing.Size(1216, 859);
            this.tpSystem.TabIndex = 3;
            this.tpSystem.Text = "系统信息";
            // 
            // spcSystem
            // 
            this.spcSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spcSystem.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.spcSystem.Location = new System.Drawing.Point(0, 0);
            this.spcSystem.Name = "spcSystem";
            // 
            // spcSystem.Panel1
            // 
            this.spcSystem.Panel1.Controls.Add(this.qlbSystem);
            // 
            // spcSystem.Panel2
            // 
            this.spcSystem.Panel2.Controls.Add(this.ptSystem);
            this.spcSystem.Panel2.Controls.Add(this.cbSystem);
            this.spcSystem.Size = new System.Drawing.Size(1216, 859);
            this.spcSystem.SplitterDistance = 293;
            this.spcSystem.SplitterWidth = 1;
            this.spcSystem.TabIndex = 1;
            // 
            // qlbSystem
            // 
            this.qlbSystem.AllowDragItem = false;
            this.qlbSystem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qlbSystem.FlatStyle = YiWangYi.MsgQQ.WinUI.QQListBar.QFlatStyle.None;
            this.qlbSystem.GroupHeaderBackColor = System.Drawing.Color.Gray;
            this.qlbSystem.GroupTextColor = System.Drawing.Color.Black;
            this.qlbSystem.HeightV = 859;
            this.qlbSystem.IsShowBackgroundImage = true;
            this.qlbSystem.ItemStyle = YiWangYi.MsgQQ.WinUI.QQListBar.CbItemStyle.PushButton;
            this.qlbSystem.LeftX = 0;
            this.qlbSystem.Location = new System.Drawing.Point(0, 0);
            this.qlbSystem.Name = "qlbSystem";
            this.qlbSystem.Size = new System.Drawing.Size(293, 859);
            this.qlbSystem.TabIndex = 0;
            this.qlbSystem.TopY = 0;
            this.qlbSystem.View = YiWangYi.MsgQQ.WinUI.QQListBar.CbViewStyle.LargeIcon;
            this.qlbSystem.WidthH = 293;
            // 
            // ptGroup
            // 
            this.ptGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ptGroup.CurrentPage = 1;
            this.ptGroup.Location = new System.Drawing.Point(704, 793);
            this.ptGroup.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ptGroup.Name = "ptGroup";
            this.ptGroup.Size = new System.Drawing.Size(210, 54);
            this.ptGroup.SumPage = 1;
            this.ptGroup.TabIndex = 4;
            // 
            // cbGroup
            // 
            this.cbGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbGroup.BackColor = System.Drawing.Color.Transparent;
            this.cbGroup.Location = new System.Drawing.Point(3, 6);
            this.cbGroup.Margin = new System.Windows.Forms.Padding(6);
            this.cbGroup.Name = "cbGroup";
            this.cbGroup.Size = new System.Drawing.Size(911, 788);
            this.cbGroup.TabIndex = 3;
            // 
            // ptHistory
            // 
            this.ptHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ptHistory.CurrentPage = 1;
            this.ptHistory.Location = new System.Drawing.Point(707, 796);
            this.ptHistory.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ptHistory.Name = "ptHistory";
            this.ptHistory.Size = new System.Drawing.Size(210, 54);
            this.ptHistory.SumPage = 1;
            this.ptHistory.TabIndex = 6;
            // 
            // cbHistory
            // 
            this.cbHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbHistory.BackColor = System.Drawing.Color.Transparent;
            this.cbHistory.Location = new System.Drawing.Point(6, 9);
            this.cbHistory.Margin = new System.Windows.Forms.Padding(6);
            this.cbHistory.Name = "cbHistory";
            this.cbHistory.Size = new System.Drawing.Size(911, 788);
            this.cbHistory.TabIndex = 5;
            // 
            // ptSystem
            // 
            this.ptSystem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ptSystem.CurrentPage = 1;
            this.ptSystem.Location = new System.Drawing.Point(707, 796);
            this.ptSystem.Margin = new System.Windows.Forms.Padding(6, 6, 6, 6);
            this.ptSystem.Name = "ptSystem";
            this.ptSystem.Size = new System.Drawing.Size(210, 54);
            this.ptSystem.SumPage = 1;
            this.ptSystem.TabIndex = 8;
            // 
            // cbSystem
            // 
            this.cbSystem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbSystem.BackColor = System.Drawing.Color.Transparent;
            this.cbSystem.Location = new System.Drawing.Point(6, 9);
            this.cbSystem.Margin = new System.Windows.Forms.Padding(6);
            this.cbSystem.Name = "cbSystem";
            this.cbSystem.Size = new System.Drawing.Size(911, 788);
            this.cbSystem.TabIndex = 7;
            // 
            // FrmHistoryMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 965);
            this.Name = "FrmHistoryMessage";
            this.Text = "消息管理器";
            this.qqFrmRound.ResumeLayout(false);
            this.qtcContent.ResumeLayout(false);
            this.tpFriend.ResumeLayout(false);
            this.spcFriend.Panel1.ResumeLayout(false);
            this.spcFriend.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcFriend)).EndInit();
            this.spcFriend.ResumeLayout(false);
            this.tpGroup.ResumeLayout(false);
            this.spcGroup.Panel1.ResumeLayout(false);
            this.spcGroup.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcGroup)).EndInit();
            this.spcGroup.ResumeLayout(false);
            this.tpHistory.ResumeLayout(false);
            this.spcHistory.Panel1.ResumeLayout(false);
            this.spcHistory.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcHistory)).EndInit();
            this.spcHistory.ResumeLayout(false);
            this.tpSystem.ResumeLayout(false);
            this.spcSystem.Panel1.ResumeLayout(false);
            this.spcSystem.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcSystem)).EndInit();
            this.spcSystem.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQTabControl qtcContent;
        private System.Windows.Forms.TabPage tpFriend;
        private System.Windows.Forms.TabPage tpGroup;
        private System.Windows.Forms.TabPage tpHistory;
        private System.Windows.Forms.TabPage tpSystem;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer spcFriend;
        private YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar qlbFriend;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer spcGroup;
        private YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar qlbGroup;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer spcHistory;
        private YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar qlbHistory;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQSplitContainer spcSystem;
        private YiWangYi.MsgQQ.WinUI.QQListBar.QQListBar qlbSystem;
        protected YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox cbFriend;
        private YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn ptFriend;
        private YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn ptGroup;
        protected YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox cbGroup;
        private YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn ptHistory;
        protected YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox cbHistory;
        private YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQPageTurn ptSystem;
        protected YiWangYi.MsgQQ.WinUI.Service.ChartControl.ChatBox cbSystem;
    }
}