﻿
namespace Team.Zyzy.Im
{
    partial class FrmAddFriendAndGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAddFriendAndGroup));
            this.panQueryResult = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.pnlFriendContent = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.picFriendLoad = new System.Windows.Forms.PictureBox();
            this.ucFriendItemInfo1 = new Team.Zyzy.Im.Control.UCFriendItemInfo();
            this.qqPaneSearchFriend = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.txtInputName = new System.Windows.Forms.TextBox();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.btnQueryFriend = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.qtcFriendAndGroup = new YiWangYi.MsgQQ.WinUI.BaseControls.QQTabControl();
            this.tpFriend = new System.Windows.Forms.TabPage();
            this.tpGroup = new System.Windows.Forms.TabPage();
            this.pnlGroupContent = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.picGroupLoad = new System.Windows.Forms.PictureBox();
            this.ucGroupItem = new Team.Zyzy.Im.Control.UCFriendItemInfo();
            this.qqPaneSearchGroup = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.txtInputGroupName = new System.Windows.Forms.TextBox();
            this.qqLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.btnQueryGroup = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.qqPanelFixSize1.SuspendLayout();
            this.panQueryResult.SuspendLayout();
            this.pnlFriendContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picFriendLoad)).BeginInit();
            this.qqPaneSearchFriend.SuspendLayout();
            this.qtcFriendAndGroup.SuspendLayout();
            this.tpFriend.SuspendLayout();
            this.tpGroup.SuspendLayout();
            this.pnlGroupContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picGroupLoad)).BeginInit();
            this.qqPaneSearchGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.Controls.Add(this.qtcFriendAndGroup);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(1058, 891);
            this.qqPanelFixSize1.Text = "添加好友";
            // 
            // panQueryResult
            // 
            this.panQueryResult.Controls.Add(this.pnlFriendContent);
            this.panQueryResult.Controls.Add(this.qqPaneSearchFriend);
            this.panQueryResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panQueryResult.Location = new System.Drawing.Point(3, 3);
            this.panQueryResult.Name = "panQueryResult";
            this.panQueryResult.Size = new System.Drawing.Size(1036, 774);
            this.panQueryResult.TabIndex = 3;
            // 
            // pnlFriendContent
            // 
            this.pnlFriendContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFriendContent.Controls.Add(this.picFriendLoad);
            this.pnlFriendContent.Controls.Add(this.ucFriendItemInfo1);
            this.pnlFriendContent.Location = new System.Drawing.Point(-3, 90);
            this.pnlFriendContent.Name = "pnlFriendContent";
            this.pnlFriendContent.Size = new System.Drawing.Size(1036, 681);
            this.pnlFriendContent.TabIndex = 5;
            // 
            // picFriendLoad
            // 
            this.picFriendLoad.Image = global::Team.Zyzy.Im.Properties.Resources.loading124X124;
            this.picFriendLoad.Location = new System.Drawing.Point(456, 278);
            this.picFriendLoad.Name = "picFriendLoad";
            this.picFriendLoad.Size = new System.Drawing.Size(124, 124);
            this.picFriendLoad.TabIndex = 4;
            this.picFriendLoad.TabStop = false;
            // 
            // ucFriendItemInfo1
            // 
            this.ucFriendItemInfo1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ucFriendItemInfo1.HaveAdd = false;
            this.ucFriendItemInfo1.Location = new System.Drawing.Point(0, 3);
            this.ucFriendItemInfo1.Name = "ucFriendItemInfo1";
            this.ucFriendItemInfo1.NickName = "新射天狼";
            this.ucFriendItemInfo1.OnLine = false;
            this.ucFriendItemInfo1.Size = new System.Drawing.Size(1036, 91);
            this.ucFriendItemInfo1.TabIndex = 3;
            // 
            // qqPaneSearchFriend
            // 
            this.qqPaneSearchFriend.BackColor = System.Drawing.Color.White;
            this.qqPaneSearchFriend.Controls.Add(this.txtInputName);
            this.qqPaneSearchFriend.Controls.Add(this.qqLabel1);
            this.qqPaneSearchFriend.Controls.Add(this.btnQueryFriend);
            this.qqPaneSearchFriend.Dock = System.Windows.Forms.DockStyle.Top;
            this.qqPaneSearchFriend.Location = new System.Drawing.Point(0, 0);
            this.qqPaneSearchFriend.Name = "qqPaneSearchFriend";
            this.qqPaneSearchFriend.Size = new System.Drawing.Size(1036, 95);
            this.qqPaneSearchFriend.TabIndex = 2;
            // 
            // txtInputName
            // 
            this.txtInputName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputName.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtInputName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtInputName.Location = new System.Drawing.Point(140, 12);
            this.txtInputName.Name = "txtInputName";
            this.txtInputName.Size = new System.Drawing.Size(756, 62);
            this.txtInputName.TabIndex = 1;
            this.txtInputName.Text = "请输入好友昵称";
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel1.Location = new System.Drawing.Point(3, 22);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(140, 48);
            this.qqLabel1.TabIndex = 2;
            this.qqLabel1.Text = "昵称:";
            // 
            // btnQueryFriend
            // 
            this.btnQueryFriend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQueryFriend.BackColor = System.Drawing.Color.Transparent;
            this.btnQueryFriend.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQueryFriend.BackgroundImage")));
            this.btnQueryFriend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQueryFriend.Caption = "搜索";
            this.btnQueryFriend.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnQueryFriend.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnQueryFriend.Location = new System.Drawing.Point(908, 10);
            this.btnQueryFriend.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnQueryFriend.MouseDownImage")));
            this.btnQueryFriend.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnQueryFriend.MouseMoveImage")));
            this.btnQueryFriend.Name = "btnQueryFriend";
            this.btnQueryFriend.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnQueryFriend.NormalImage")));
            this.btnQueryFriend.Size = new System.Drawing.Size(120, 75);
            this.btnQueryFriend.TabIndex = 0;
            this.btnQueryFriend.ToolTip = null;
            this.btnQueryFriend.Click += new System.EventHandler(this.btnQueryFriend_Click);
            // 
            // qtcFriendAndGroup
            // 
            this.qtcFriendAndGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qtcFriendAndGroup.Controls.Add(this.tpFriend);
            this.qtcFriendAndGroup.Controls.Add(this.tpGroup);
            this.qtcFriendAndGroup.Location = new System.Drawing.Point(0, 68);
            this.qtcFriendAndGroup.myBackColor = System.Drawing.Color.White;
            this.qtcFriendAndGroup.Name = "qtcFriendAndGroup";
            this.qtcFriendAndGroup.SelectedIndex = 0;
            this.qtcFriendAndGroup.Size = new System.Drawing.Size(1058, 823);
            this.qtcFriendAndGroup.TabIndex = 4;
            this.qtcFriendAndGroup.SelectedIndexChanged += new System.EventHandler(this.qtcFriend_SelectedIndexChanged);
            // 
            // tpFriend
            // 
            this.tpFriend.BackColor = System.Drawing.Color.White;
            this.tpFriend.Controls.Add(this.panQueryResult);
            this.tpFriend.Location = new System.Drawing.Point(8, 35);
            this.tpFriend.Name = "tpFriend";
            this.tpFriend.Padding = new System.Windows.Forms.Padding(3);
            this.tpFriend.Size = new System.Drawing.Size(1042, 780);
            this.tpFriend.TabIndex = 0;
            this.tpFriend.Text = "查找好友";
            // 
            // tpGroup
            // 
            this.tpGroup.BackColor = System.Drawing.Color.White;
            this.tpGroup.Controls.Add(this.pnlGroupContent);
            this.tpGroup.Controls.Add(this.qqPaneSearchGroup);
            this.tpGroup.Location = new System.Drawing.Point(8, 35);
            this.tpGroup.Name = "tpGroup";
            this.tpGroup.Padding = new System.Windows.Forms.Padding(3);
            this.tpGroup.Size = new System.Drawing.Size(1042, 780);
            this.tpGroup.TabIndex = 1;
            this.tpGroup.Text = "查找群组";
            // 
            // pnlGroupContent
            // 
            this.pnlGroupContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGroupContent.Controls.Add(this.picGroupLoad);
            this.pnlGroupContent.Controls.Add(this.ucGroupItem);
            this.pnlGroupContent.Location = new System.Drawing.Point(0, 94);
            this.pnlGroupContent.Name = "pnlGroupContent";
            this.pnlGroupContent.Size = new System.Drawing.Size(1039, 683);
            this.pnlGroupContent.TabIndex = 7;
            // 
            // picGroupLoad
            // 
            this.picGroupLoad.Image = global::Team.Zyzy.Im.Properties.Resources.loading124X124;
            this.picGroupLoad.Location = new System.Drawing.Point(456, 278);
            this.picGroupLoad.Name = "picGroupLoad";
            this.picGroupLoad.Size = new System.Drawing.Size(124, 124);
            this.picGroupLoad.TabIndex = 4;
            this.picGroupLoad.TabStop = false;
            // 
            // ucGroupItem
            // 
            this.ucGroupItem.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ucGroupItem.HaveAdd = false;
            this.ucGroupItem.Location = new System.Drawing.Point(0, 3);
            this.ucGroupItem.Name = "ucGroupItem";
            this.ucGroupItem.NickName = "新射天狼";
            this.ucGroupItem.OnLine = false;
            this.ucGroupItem.Size = new System.Drawing.Size(1039, 91);
            this.ucGroupItem.TabIndex = 3;
            // 
            // qqPaneSearchGroup
            // 
            this.qqPaneSearchGroup.BackColor = System.Drawing.Color.White;
            this.qqPaneSearchGroup.Controls.Add(this.txtInputGroupName);
            this.qqPaneSearchGroup.Controls.Add(this.qqLabel2);
            this.qqPaneSearchGroup.Controls.Add(this.btnQueryGroup);
            this.qqPaneSearchGroup.Dock = System.Windows.Forms.DockStyle.Top;
            this.qqPaneSearchGroup.Location = new System.Drawing.Point(3, 3);
            this.qqPaneSearchGroup.Name = "qqPaneSearchGroup";
            this.qqPaneSearchGroup.Size = new System.Drawing.Size(1036, 95);
            this.qqPaneSearchGroup.TabIndex = 6;
            // 
            // txtInputGroupName
            // 
            this.txtInputGroupName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtInputGroupName.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtInputGroupName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtInputGroupName.Location = new System.Drawing.Point(140, 12);
            this.txtInputGroupName.Name = "txtInputGroupName";
            this.txtInputGroupName.Size = new System.Drawing.Size(756, 62);
            this.txtInputGroupName.TabIndex = 1;
            this.txtInputGroupName.Text = "请输入群名称";
            // 
            // qqLabel2
            // 
            this.qqLabel2.AutoSize = true;
            this.qqLabel2.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel2.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel2.Location = new System.Drawing.Point(3, 22);
            this.qqLabel2.Name = "qqLabel2";
            this.qqLabel2.Size = new System.Drawing.Size(140, 48);
            this.qqLabel2.TabIndex = 2;
            this.qqLabel2.Text = "群名:";
            // 
            // btnQueryGroup
            // 
            this.btnQueryGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQueryGroup.BackColor = System.Drawing.Color.Transparent;
            this.btnQueryGroup.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnQueryGroup.BackgroundImage")));
            this.btnQueryGroup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnQueryGroup.Caption = "搜索";
            this.btnQueryGroup.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnQueryGroup.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnQueryGroup.Location = new System.Drawing.Point(908, 10);
            this.btnQueryGroup.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnQueryGroup.MouseDownImage")));
            this.btnQueryGroup.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnQueryGroup.MouseMoveImage")));
            this.btnQueryGroup.Name = "btnQueryGroup";
            this.btnQueryGroup.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnQueryGroup.NormalImage")));
            this.btnQueryGroup.Size = new System.Drawing.Size(120, 75);
            this.btnQueryGroup.TabIndex = 0;
            this.btnQueryGroup.ToolTip = null;
            this.btnQueryGroup.Click += new System.EventHandler(this.btnQueryGroup_Click);
            // 
            // FrmAddFriendAndGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1078, 911);
            this.Name = "FrmAddFriendAndGroup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "添加好友";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.panQueryResult.ResumeLayout(false);
            this.pnlFriendContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picFriendLoad)).EndInit();
            this.qqPaneSearchFriend.ResumeLayout(false);
            this.qqPaneSearchFriend.PerformLayout();
            this.qtcFriendAndGroup.ResumeLayout(false);
            this.tpFriend.ResumeLayout(false);
            this.tpGroup.ResumeLayout(false);
            this.pnlGroupContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picGroupLoad)).EndInit();
            this.qqPaneSearchGroup.ResumeLayout(false);
            this.qqPaneSearchGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel panQueryResult;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPaneSearchFriend;
        private System.Windows.Forms.TextBox txtInputName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnQueryFriend;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQTabControl qtcFriendAndGroup;
        private System.Windows.Forms.TabPage tpFriend;
        private System.Windows.Forms.TabPage tpGroup;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel pnlFriendContent;
        private Control.UCFriendItemInfo ucFriendItemInfo1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel pnlGroupContent;
        private Control.UCFriendItemInfo ucGroupItem;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPaneSearchGroup;
        private System.Windows.Forms.TextBox txtInputGroupName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel2;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnQueryGroup;
        private System.Windows.Forms.PictureBox picFriendLoad;
        private System.Windows.Forms.PictureBox picGroupLoad;
    }
}