﻿
namespace Team.Zyzy.Im
{
    partial class FrmCreateGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCreateGroup));
            this.pnlContent = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.btnInvite = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.gbFriends = new System.Windows.Forms.GroupBox();
            this.lvSelectedFriends = new System.Windows.Forms.ListView();
            this.txtGroupRemark = new System.Windows.Forms.TextBox();
            this.txtOwnerRemark = new System.Windows.Forms.TextBox();
            this.btnCreate = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnCancel = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.txtOwnerName = new System.Windows.Forms.TextBox();
            this.txtGroupNotice = new System.Windows.Forms.TextBox();
            this.txtGroupName = new System.Windows.Forms.TextBox();
            this.skinLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel4 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel3 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.pnlGroupImage = new System.Windows.Forms.Panel();
            this.picHeadImage = new System.Windows.Forms.PictureBox();
            this.skinLabel5 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqFrmRound.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.gbFriends.SuspendLayout();
            this.pnlGroupImage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImage)).BeginInit();
            this.SuspendLayout();
            // 
            // qqFrmRound
            // 
            this.qqFrmRound.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqFrmRound.BackgroundImage")));
            this.qqFrmRound.Controls.Add(this.pnlContent);
            this.qqFrmRound.Location = new System.Drawing.Point(5, 5);
            this.qqFrmRound.Size = new System.Drawing.Size(963, 886);
            this.qqFrmRound.Tag = "正在与";
            this.qqFrmRound.Text = "创建群组";
            // 
            // pnlContent
            // 
            this.pnlContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlContent.Controls.Add(this.btnInvite);
            this.pnlContent.Controls.Add(this.gbFriends);
            this.pnlContent.Controls.Add(this.txtGroupRemark);
            this.pnlContent.Controls.Add(this.txtOwnerRemark);
            this.pnlContent.Controls.Add(this.btnCreate);
            this.pnlContent.Controls.Add(this.btnCancel);
            this.pnlContent.Controls.Add(this.txtOwnerName);
            this.pnlContent.Controls.Add(this.txtGroupNotice);
            this.pnlContent.Controls.Add(this.txtGroupName);
            this.pnlContent.Controls.Add(this.skinLabel1);
            this.pnlContent.Controls.Add(this.skinLabel2);
            this.pnlContent.Controls.Add(this.skinLabel4);
            this.pnlContent.Controls.Add(this.skinLabel3);
            this.pnlContent.Controls.Add(this.pnlGroupImage);
            this.pnlContent.Controls.Add(this.skinLabel5);
            this.pnlContent.Location = new System.Drawing.Point(3, 60);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(957, 823);
            this.pnlContent.TabIndex = 10;
            this.pnlContent.Text = "groupBox1";
            // 
            // btnInvite
            // 
            this.btnInvite.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnInvite.BackColor = System.Drawing.Color.Transparent;
            this.btnInvite.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnInvite.BackgroundImage")));
            this.btnInvite.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInvite.Caption = "邀请+";
            this.btnInvite.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnInvite.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnInvite.Location = new System.Drawing.Point(24, 737);
            this.btnInvite.Margin = new System.Windows.Forms.Padding(6);
            this.btnInvite.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnInvite.MouseDownImage")));
            this.btnInvite.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnInvite.MouseMoveImage")));
            this.btnInvite.Name = "btnInvite";
            this.btnInvite.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnInvite.NormalImage")));
            this.btnInvite.Size = new System.Drawing.Size(150, 70);
            this.btnInvite.TabIndex = 153;
            this.btnInvite.ToolTip = null;
            this.btnInvite.Click += new System.EventHandler(this.btnInvite_Click);
            // 
            // gbFriends
            // 
            this.gbFriends.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbFriends.Controls.Add(this.lvSelectedFriends);
            this.gbFriends.Location = new System.Drawing.Point(21, 508);
            this.gbFriends.Name = "gbFriends";
            this.gbFriends.Size = new System.Drawing.Size(914, 220);
            this.gbFriends.TabIndex = 152;
            this.gbFriends.TabStop = false;
            this.gbFriends.Text = "已邀请好友";
            // 
            // lvSelectedFriends
            // 
            this.lvSelectedFriends.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvSelectedFriends.HideSelection = false;
            this.lvSelectedFriends.Location = new System.Drawing.Point(3, 31);
            this.lvSelectedFriends.Name = "lvSelectedFriends";
            this.lvSelectedFriends.Size = new System.Drawing.Size(908, 186);
            this.lvSelectedFriends.TabIndex = 1;
            this.lvSelectedFriends.UseCompatibleStateImageBehavior = false;
            // 
            // txtGroupRemark
            // 
            this.txtGroupRemark.BackColor = System.Drawing.Color.White;
            this.txtGroupRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroupRemark.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtGroupRemark.Location = new System.Drawing.Point(218, 265);
            this.txtGroupRemark.Margin = new System.Windows.Forms.Padding(0);
            this.txtGroupRemark.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtGroupRemark.Name = "txtGroupRemark";
            this.txtGroupRemark.Size = new System.Drawing.Size(716, 42);
            this.txtGroupRemark.TabIndex = 151;
            // 
            // txtOwnerRemark
            // 
            this.txtOwnerRemark.BackColor = System.Drawing.Color.White;
            this.txtOwnerRemark.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOwnerRemark.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtOwnerRemark.Location = new System.Drawing.Point(218, 183);
            this.txtOwnerRemark.Margin = new System.Windows.Forms.Padding(0);
            this.txtOwnerRemark.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtOwnerRemark.Name = "txtOwnerRemark";
            this.txtOwnerRemark.Size = new System.Drawing.Size(494, 42);
            this.txtOwnerRemark.TabIndex = 150;
            // 
            // btnCreate
            // 
            this.btnCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreate.BackColor = System.Drawing.Color.Transparent;
            this.btnCreate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCreate.BackgroundImage")));
            this.btnCreate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCreate.Caption = "确定";
            this.btnCreate.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCreate.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnCreate.Location = new System.Drawing.Point(785, 737);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(6);
            this.btnCreate.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnCreate.MouseDownImage")));
            this.btnCreate.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnCreate.MouseMoveImage")));
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCreate.NormalImage")));
            this.btnCreate.Size = new System.Drawing.Size(150, 70);
            this.btnCreate.TabIndex = 149;
            this.btnCreate.ToolTip = null;
            this.btnCreate.Click += new System.EventHandler(this.btnCreateClick);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Caption = "取消";
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnCancel.Location = new System.Drawing.Point(580, 737);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(6);
            this.btnCancel.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseDownImage")));
            this.btnCancel.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseMoveImage")));
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.Size = new System.Drawing.Size(150, 70);
            this.btnCancel.TabIndex = 148;
            this.btnCancel.ToolTip = null;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtOwnerName
            // 
            this.txtOwnerName.BackColor = System.Drawing.Color.White;
            this.txtOwnerName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtOwnerName.Enabled = false;
            this.txtOwnerName.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtOwnerName.Location = new System.Drawing.Point(218, 101);
            this.txtOwnerName.Margin = new System.Windows.Forms.Padding(0);
            this.txtOwnerName.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtOwnerName.Name = "txtOwnerName";
            this.txtOwnerName.Size = new System.Drawing.Size(494, 42);
            this.txtOwnerName.TabIndex = 143;
            // 
            // txtGroupNotice
            // 
            this.txtGroupNotice.BackColor = System.Drawing.Color.White;
            this.txtGroupNotice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroupNotice.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtGroupNotice.Location = new System.Drawing.Point(218, 347);
            this.txtGroupNotice.Margin = new System.Windows.Forms.Padding(0);
            this.txtGroupNotice.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtGroupNotice.Multiline = true;
            this.txtGroupNotice.Name = "txtGroupNotice";
            this.txtGroupNotice.Size = new System.Drawing.Size(717, 158);
            this.txtGroupNotice.TabIndex = 146;
            this.txtGroupNotice.Text = "不见面不等于不思念，不联络只是因为有了CRIM。";
            // 
            // txtGroupName
            // 
            this.txtGroupName.BackColor = System.Drawing.Color.White;
            this.txtGroupName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGroupName.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtGroupName.Location = new System.Drawing.Point(218, 19);
            this.txtGroupName.Margin = new System.Windows.Forms.Padding(0);
            this.txtGroupName.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(494, 42);
            this.txtGroupName.TabIndex = 137;
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(87, 24);
            this.skinLabel1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(134, 31);
            this.skinLabel1.TabIndex = 138;
            this.skinLabel1.Text = "群聊名称：";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(135, 106);
            this.skinLabel2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(86, 31);
            this.skinLabel2.TabIndex = 139;
            this.skinLabel2.Text = "群主：";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(111, 270);
            this.skinLabel4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(110, 31);
            this.skinLabel4.TabIndex = 140;
            this.skinLabel4.Text = "群备注：";
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(15, 188);
            this.skinLabel3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(206, 31);
            this.skinLabel3.TabIndex = 141;
            this.skinLabel3.Text = "我在本群的昵称：";
            // 
            // pnlGroupImage
            // 
            this.pnlGroupImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlGroupImage.BackColor = System.Drawing.Color.White;
            this.pnlGroupImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlGroupImage.Controls.Add(this.picHeadImage);
            this.pnlGroupImage.Location = new System.Drawing.Point(724, 19);
            this.pnlGroupImage.Margin = new System.Windows.Forms.Padding(0);
            this.pnlGroupImage.Name = "pnlGroupImage";
            this.pnlGroupImage.Size = new System.Drawing.Size(210, 210);
            this.pnlGroupImage.TabIndex = 147;
            // 
            // picHeadImage
            // 
            this.picHeadImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picHeadImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picHeadImage.Location = new System.Drawing.Point(0, 0);
            this.picHeadImage.Name = "picHeadImage";
            this.picHeadImage.Size = new System.Drawing.Size(210, 210);
            this.picHeadImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picHeadImage.TabIndex = 0;
            this.picHeadImage.TabStop = false;
            this.picHeadImage.Click += new System.EventHandler(this.picHeadImage_Click);
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(111, 352);
            this.skinLabel5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(110, 31);
            this.skinLabel5.TabIndex = 142;
            this.skinLabel5.Text = "群公告：";
            // 
            // FrmCreateGroup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 896);
            this.Name = "FrmCreateGroup";
            this.Text = "创建群组";
            this.qqFrmRound.ResumeLayout(false);
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.gbFriends.ResumeLayout(false);
            this.pnlGroupImage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel pnlContent;
        private System.Windows.Forms.TextBox txtGroupRemark;
        private System.Windows.Forms.TextBox txtOwnerRemark;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnCreate;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnCancel;
        private System.Windows.Forms.TextBox txtOwnerName;
        private System.Windows.Forms.TextBox txtGroupNotice;
        private System.Windows.Forms.TextBox txtGroupName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel2;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel4;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel3;
        private System.Windows.Forms.Panel pnlGroupImage;
        private System.Windows.Forms.PictureBox picHeadImage;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel5;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnInvite;
        private System.Windows.Forms.GroupBox gbFriends;
        private System.Windows.Forms.ListView lvSelectedFriends;
    }
}