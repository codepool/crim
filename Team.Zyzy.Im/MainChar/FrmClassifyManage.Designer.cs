﻿
namespace Team.Zyzy.Im
{
    partial class FrmClassifyManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmClassifyManage));
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.txtClassifyName = new System.Windows.Forms.TextBox();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.btnCancel = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnAccept = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.nudSortKey = new System.Windows.Forms.NumericUpDown();
            this.qqLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqPanelFixSize1.SuspendLayout();
            this.qqPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSortKey)).BeginInit();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.Controls.Add(this.qqPanel1);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(550, 352);
            this.qqPanelFixSize1.Text = "分组管理";
            // 
            // qqPanel1
            // 
            this.qqPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qqPanel1.Controls.Add(this.qqLabel2);
            this.qqPanel1.Controls.Add(this.nudSortKey);
            this.qqPanel1.Controls.Add(this.txtClassifyName);
            this.qqPanel1.Controls.Add(this.qqLabel1);
            this.qqPanel1.Controls.Add(this.btnCancel);
            this.qqPanel1.Controls.Add(this.btnAccept);
            this.qqPanel1.Location = new System.Drawing.Point(3, 60);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(547, 292);
            this.qqPanel1.TabIndex = 6;
            // 
            // txtClassifyName
            // 
            this.txtClassifyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtClassifyName.Font = new System.Drawing.Font("宋体", 13.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txtClassifyName.ImeMode = System.Windows.Forms.ImeMode.HangulFull;
            this.txtClassifyName.Location = new System.Drawing.Point(63, 78);
            this.txtClassifyName.Name = "txtClassifyName";
            this.txtClassifyName.Size = new System.Drawing.Size(335, 50);
            this.txtClassifyName.TabIndex = 3;
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Location = new System.Drawing.Point(59, 38);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(202, 24);
            this.qqLabel1.TabIndex = 2;
            this.qqLabel1.Text = "请输入分组名称：";
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.BackColor = System.Drawing.Color.Transparent;
            this.btnCancel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.BackgroundImage")));
            this.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCancel.Caption = "取消";
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCancel.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnCancel.Location = new System.Drawing.Point(307, 164);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(6);
            this.btnCancel.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseDownImage")));
            this.btnCancel.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.MouseMoveImage")));
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCancel.NormalImage")));
            this.btnCancel.Size = new System.Drawing.Size(170, 88);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.ToolTip = null;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAccept
            // 
            this.btnAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAccept.BackColor = System.Drawing.Color.Transparent;
            this.btnAccept.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.BackgroundImage")));
            this.btnAccept.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAccept.Caption = "确定";
            this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnAccept.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnAccept.Location = new System.Drawing.Point(59, 164);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(6);
            this.btnAccept.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseDownImage")));
            this.btnAccept.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.MouseMoveImage")));
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnAccept.NormalImage")));
            this.btnAccept.Size = new System.Drawing.Size(170, 88);
            this.btnAccept.TabIndex = 0;
            this.btnAccept.ToolTip = null;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // nudSortKey
            // 
            this.nudSortKey.Font = new System.Drawing.Font("宋体", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.nudSortKey.Location = new System.Drawing.Point(396, 78);
            this.nudSortKey.Name = "nudSortKey";
            this.nudSortKey.Size = new System.Drawing.Size(81, 50);
            this.nudSortKey.TabIndex = 5;
            // 
            // qqLabel2
            // 
            this.qqLabel2.AutoSize = true;
            this.qqLabel2.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel2.Location = new System.Drawing.Point(392, 38);
            this.qqLabel2.Name = "qqLabel2";
            this.qqLabel2.Size = new System.Drawing.Size(82, 24);
            this.qqLabel2.TabIndex = 6;
            this.qqLabel2.Text = "排序：";
            // 
            // FrmClassifyManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 372);
            this.ImeMode = System.Windows.Forms.ImeMode.HangulFull;
            this.Name = "FrmClassifyManage";
            this.Text = "分组管理";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.qqPanel1.ResumeLayout(false);
            this.qqPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSortKey)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnCancel;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnAccept;
        private System.Windows.Forms.TextBox txtClassifyName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel2;
        private System.Windows.Forms.NumericUpDown nudSortKey;
    }
}