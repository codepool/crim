﻿using Newtonsoft.Json;
using Shell32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.BLL;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.BaseControls.PopupEmotion;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;
using YiWangYi.MsgQQ.WinUI.Service.ChartControl;
using YiWangYi.MsgQQ.WinUI.Service.ChartService;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.mainService;
using Team.Zyzy.Im.MainChar;

namespace Team.Zyzy.Im
{
    public partial class FrmGroupChart : FrmWeiChartGroupBase
    {
        private FrmGroupChart()
            : base()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Load += FrmGroupChart_Load;
            this.SendMessageClick += FrmGroupChart_SendMessageClick;
            this.VideoClick += FrmGroupChart_VideoClick;
            this.FileClick += FrmGroupChart_FileClick;
            this.ControlClick += FrmGroupChart_ControlClick;
            this.AudioClick += FrmGroupChart_AudioClick;
            this.SelectChartHistoryClick += FrmGroupChart_SelectChartHistoryClick;
            this.chatBox1.GotFocus += delegate { setRefushUnreadCount(); };
            this.chartEdit.GotFocus += delegate { setRefushUnreadCount(); };
            this.GotFocus += delegate { setRefushUnreadCount(); };
            this.Activated += delegate { setRefushUnreadCount(); };
            this.qlvGroupMember.ItemDoubleClick += QlvGroupMember_ItemDoubleClick;
            qqChartPanel1.HeadImageClick += QqChartPanel1_HeadImageClick;
            qqChartPanel1.OppositeNameClick += QqChartPanel1_HeadImageClick;
            labRightTopInfo.Click += QqChartPanel1_HeadImageClick;
        }

        private void QqChartPanel1_HeadImageClick(object sender, EventArgs e)
        {
            FrmCreateGroup frmCreateGroup = new FrmCreateGroup();
            frmCreateGroup.IsInvite = false;
            frmCreateGroup.GroupVO = this._groupInfo.GroupVO;
            frmCreateGroup.Show();
        }

        private void QlvGroupMember_ItemDoubleClick(CbItemEventArgs e)
        {
            GroupMemberVOItem item = e.Item as GroupMemberVOItem;
            FrmModifyFriendInfo friendInfo = new FrmModifyFriendInfo();
            friendInfo.UserId = item.GroupMemberVO.userId;
            friendInfo.Show();
        }

        private void FrmGroupChart_SelectChartHistoryClick(object sender, EventArgs e)
        {
            FrmHistoryMessage frmMessage = new FrmHistoryMessage();
            frmMessage.ShowGroupHistory(_groupInfo.ID);
        }

        #region 文件上传

        protected override void uploadScreenCutFile(Image img)
        {
            string screenFile = ScreenImageHelp.ScreenPath(img);
            uploadImageFile(screenFile);
        }

        protected override void uploadFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            this.ProgressBarVisible = true;
            try
            {
                HttpFileHelp.UploadFile(file, uploadFileCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                this.ProgressBarVisible = false;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        protected override void uploadImageFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            this.ProgressBarVisible = true;
            try
            {
                HttpFileHelp.UploadImage(file, uploadImageCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                this.ProgressBarVisible = false;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        private void uploadVoiceFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<string> resultModel = JsonConvert.DeserializeObject<Result<string>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                FileInfo file = (FileInfo)sender;
                UploadVoiceVO fileInfo = new UploadVoiceVO();
                fileInfo.url = resultModel.data;
                //获取时间方法一
                string dirName = System.IO.Path.GetDirectoryName(file.FullName);
                FileInfo fInfo = new FileInfo(file.FullName);
                string SongName = System.IO.Path.GetFileName(file.FullName);
                ShellClass sh = new ShellClass();
                Folder dir = sh.NameSpace(dirName);
                FolderItem item = dir.ParseName(SongName);
                string time = Regex.Match(dir.GetDetailsOf(item, -1), "\\d:\\d{2}:\\d{2}").Value;
                string[] timeArray = Regex.Split(time, ":");
                int hour = int.Parse(timeArray[0]);
                int min = int.Parse(timeArray[1]);
                int sec = int.Parse(timeArray[2]);
                fileInfo.duration = (hour * 3600 + min * 60 + sec);

                //获取时间方法二
                //String[] fileNameInfo = file.Name.Split('_');
                //fileInfo.duration = int.Parse(fileNameInfo[fileNameInfo.Length - 2]);

                fileInfo.name = file.Name;
                file = null;
                string content = JsonConvert.SerializeObject(fileInfo);
                int msgType = (int)MessageType.AUDIO;
                sendMessageAndDisplaySyncDB(content, msgType);
            }
            this.ProgressBarVisible = false;
        }

        private void uploadFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<string> resultModel = JsonConvert.DeserializeObject<Result<string>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                FileInfo file = (FileInfo)sender;
                UploadFileVO fileInfo = new UploadFileVO();
                fileInfo.url = resultModel.data;
                fileInfo.size = file.Length;
                fileInfo.name = file.Name;
                file = null;
                string content = JsonConvert.SerializeObject(fileInfo);
                int msgType = (int)MessageType.FILE;
                sendMessageAndDisplaySyncDB(content, msgType);
            }
            this.ProgressBarVisible = false;
        }
        private void uploadImageCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<UploadImageVO> resultModel = JsonConvert.DeserializeObject<Result<UploadImageVO>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                string content = JsonConvert.SerializeObject(resultModel.data);
                int msgType = (int)MessageType.IMAGE;
                sendMessageAndDisplaySyncDB(content, msgType);
            }
            this.ProgressBarVisible = false;
        }

        private void uploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            if (this.ProgressBar.Minimum < e.ProgressPercentage && e.ProgressPercentage < this.ProgressBar.Maximum)
                this.ProgressBar.Value = e.ProgressPercentage;
        }
        #endregion

        private void FrmGroupChart_AudioClick(object sender, EventArgs e)
        {
            FrmVoiceRecord frmVoiceRecord = new FrmVoiceRecord();
            frmVoiceRecord.UserID = _groupInfo.ID;
            frmVoiceRecord.StartPosition = FormStartPosition.CenterParent;
            frmVoiceRecord.SendClick += FrmVoiceRecord_SendClick;
            frmVoiceRecord.ShowDialog();
        }

        private void FrmVoiceRecord_SendClick(string fileName, int voiceTimeLength)
        {
            FileInfo file = new FileInfo(fileName);
            this.ProgressBarVisible = true;
            try
            {
                HttpFileHelp.UploadFile(file, uploadVoiceFileCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                this.ProgressBarVisible = false;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        private void FrmGroupChart_ControlClick(object sender, EventArgs e)
        {
            FrmAutoCloseMessageBox.Show("请联系升级");
        }

        private void FrmGroupChart_FileClick(object sender, EventArgs e)
        {
        }

        private void FrmGroupChart_VideoClick(object sender, EventArgs e)
        {
            FrmGroupRTCVideo frmGroupRTCVideo = new FrmGroupRTCVideo("https://www.baidu.com");
            frmGroupRTCVideo.Left = 0 - frmGroupRTCVideo.Width * 2;
            frmGroupRTCVideo.ShowInTaskbar = false;
            frmGroupRTCVideo.Show();
            FrmGroupRTCVideo frmGroupRTCVideo2 = new FrmGroupRTCVideo("https://meeting.zyzy.team/"+this._groupInfo.ID);
            frmGroupRTCVideo2.Text = "["+this._groupInfo.DisplayName + "]视频会议";
            frmGroupRTCVideo2.Show();
            frmGroupRTCVideo2.FormClosed += delegate { frmGroupRTCVideo.Close(); };
        }

        private void FrmGroupChart_SendMessageClick(object sender, EventArgs e)
        {

            if (this.EditContentIsNull())
            {
                FrmAutoCloseMessageBox.Show("发送内容不能为空");
            }
            RichTextBox txt = sender as RichTextBox;
            txt.Enabled = false;
            
            string content = EmotionHelp.RtfToText( this.ChartEidt.Rtf);
            int msgType = (int)MessageType.TEXT;
            if (sendMessageAndDisplaySyncDB(content, msgType))
            {
                txt.Text = "";
            }
            txt.Enabled = true;
        }

        #region 发送信息

        public void SendReaded(long groupId)
        {
            try
            {
                string url = "/message/group/readed?groupId=" + groupId;
                HttpHelp.HttpApi<long?>(url, null, "PUT");
            }
            catch (Exception ex)
            {
            }
            ImPrivateMessageBll.Instance.UpdateMessageStatusRead(groupId, RequestApiInfo.SendId);
        }
        private bool sendMessageAndDisplaySyncDB(string content, int msgType)
        {
            GroupMessage groupMessage = new GroupMessage();
            groupMessage.type = msgType;
            groupMessage.content = content;
            groupMessage.groupId = _groupInfo.ID;
            groupMessage.sendId = RequestApiInfo.SendId;
            groupMessage.sendNickName = RequestApiInfo.SendNickName;
            groupMessage.status = -1;
            groupMessage.setSendTime(DateTime.Now);
            AddMessage(groupMessage);
            Task.Run(() => sendMessageAndUpdateDB(groupMessage));
            return true;
        }

        private long sendMessageAndUpdateDB(GroupMessage groupMessage)
        {
            long msgId = sendPrivateMessage(groupMessage);
            int status = msgId > 0 ? (int)MessageStatus.Send : (int)MessageStatus.UnSend;
            Action action = () =>
            {
                IMessage msg = chatBox1.getMessage(groupMessage.getSendTime(), groupMessage.id);
                msg.setStatus(status);
                msg.setMessageID(msgId);
                groupMessage.id = msgId;
                groupMessage.status = status;
                msg.Tag = groupMessage;
                FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
                frmMain.UpdateHistoryMessage(groupMessage);
            };
            Invoke(action);
            return msgId;
        }

        private long sendPrivateMessage(GroupMessage groupMessage)
        {
            long msgId = 0;
            try
            {
                string url = "/message/group/send";
                msgId = HttpHelp.HttpApi<long>(url, groupMessage, "POST");
            }
            catch (Exception ex)
            {
                msgId = 0;
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
            return msgId;
        }

        #endregion
        private GroupChartMemberRightMenu mainChartBarsRightMenu;
        private void FrmGroupChart_Load(object sender, EventArgs e)
        {
            this.chatBox1.OnMouseEnterLookReaded += ChatBox1_OnMouseEnterLookReaded;
        }
        #region 已读情况统计

        private void ChatBox1_OnMouseEnterLookReaded(Label sender, object message)
        {
            GroupMessage groupMessage = message as GroupMessage;
            if (groupMessage == null)
            {
                return;
            }
            try
            {
                string url = "/message/group/readed/status?groupId=" + groupMessage.groupId+ "&msgId="+groupMessage.id;
                List<GroupMessageReadedVO> groupMessageReadedVOs= HttpHelp.HttpApi<List<GroupMessageReadedVO>>(url, null, "GET");
                StringBuilder sbTootipe = new StringBuilder();
                string strTemp = "{0}、{1}  {2}";
                int index = 1;
                foreach(GroupMemberVO memberVO in this.groupMembers)
                {
                    foreach(GroupMessageReadedVO item in groupMessageReadedVOs)
                    {
                        if (memberVO.userId == item.userId)
                        {
                            sbTootipe.AppendLine(string.Format(strTemp, index
    ++, memberVO.aliasName, item.readed ? "已读" : "未读"));
                        }
                    }
                }

                this.ToolTip.SetToolTip(sender, sbTootipe.ToString());
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region 显示及初始化
        private void initLastMessage()
        {
            this.SuspendLayout();
            this.Text = _groupInfo.DisplayName;
            this.ShowInTaskbar = true;
            chatBox1.ClearMessage();
            try
            {
                List<GroupMessage> messages = ImGroupMessageBll.Instance.GetRecentTopList(_groupInfo.ID, 10);
                messages.Sort();
                foreach (GroupMessage item in messages)
                {
                    ChartMessageHelp.DisplaySingle(chatBox1, item, RequestApiInfo.SendId);
                }
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
            //this.SetEndOpacity();
            this.ResumeLayout(false);
        }
        private List<GroupMemberVO> groupMembers = null;
        private void initGroupMember()
        {
            mainChartBarsRightMenu = new GroupChartMemberRightMenu(this.qlvGroupMember, this._groupInfo.GroupVO);
            groupMembers = mainChartBarsRightMenu.InitGroupMember(_groupInfo.GroupVO,this.qlvGroupMember);
        }



        public void LoadOverShow()
        {
            initLastMessage();
            initGroupMember();
            Point location = this.Location;
            this.Location = new Point(-10000, -1000);
            this.Shown += delegate { this.Location = location;};
            this.Show();
            setRefushUnreadCount();
        }
        #endregion
        private void setRefushUnreadCount()
        {
            if (_groupInfo.MessageCount > 0)
            {
                _groupInfo.MessageCount = 0;
                ShareInfo.FormQQMain.RefushUnreadCount();
                SendReaded(_groupInfo.ID);
            }
        }

        private GroupVOItem _groupInfo = new GroupVOItem();
        public GroupVOItem GroupInfo
        {
            get
            {
                return _groupInfo;
            }
            set
            {
                _groupInfo = value;
                qqChartPanel1.HeadImage = _groupInfo.HeadImage;
                qqChartPanel1.OppositeName = _groupInfo.DisplayName + (string.IsNullOrEmpty(_groupInfo.NickName) ? "" : "(" + _groupInfo.NickName + ")");
                qqChartPanel1.Signature = _groupInfo.PersonSignaure;
                labRightTopInfo.Text = _groupInfo.GroupVO.notice;
                this.Text = qqChartPanel1.OppositeName;

            }
        }

        #region

        public void AddMessage(GroupMessage message)
        {
            Action action = () =>
            {
                ChartMessageHelp.DisplaySingle(chatBox1, message, RequestApiInfo.SendId);
            };
            Invoke(action);
        }

        #endregion

        #region 每个人单窗体模式相关设置

        public static FrmGroupChart GetInstance(object key)
        {
            FrmGroupChart frmChart = FindInstance<FrmGroupChart>(key);
            if (frmChart == null)
            {
                frmChart = new FrmGroupChart();
                Form previous = FindMaxLocationChartForm();
                if (previous != null)
                {
                    frmChart.Left = previous.Left + 25;
                    frmChart.Top = previous.Top + 25;
                }
                else
                {
                    frmChart.Left = (System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Width - frmChart.Width)/2;
                    frmChart.Top=(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Height - frmChart.Height) / 2;
                }
                frmChart.StartPosition = FormStartPosition.Manual;
                frmChart.IdentifyKey = key;
            }
            return frmChart;
        }

        private void initInfo()
        {
            this.Tag = IdentifyKey.ToString();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        #endregion

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGroupChart));
            ((System.ComponentModel.ISupportInitialize)(this.spcGroupMember)).BeginInit();
            this.spcGroupMember.Panel2.SuspendLayout();
            this.spcGroupMember.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spcChartHisotryAndCurrent)).BeginInit();
            this.spcChartHisotryAndCurrent.Panel1.SuspendLayout();
            this.spcChartHisotryAndCurrent.SuspendLayout();
            this.pnlRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // spcGroupMember
            // 
            // 
            // spcChartHisotryAndCurrent
            // 
            // 
            // btnClosed
            // 
            this.btnClosed.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClosed.BackgroundImage")));
            this.btnClosed.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnClosed.MouseDownImage")));
            this.btnClosed.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnClosed.MouseMoveImage")));
            this.btnClosed.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnClosed.NormalImage")));
            // 
            // btnSend
            // 
            this.btnSend.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSend.BackgroundImage")));
            this.btnSend.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnSend.MouseDownImage")));
            this.btnSend.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnSend.MouseMoveImage")));
            this.btnSend.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnSend.NormalImage")));
            // 
            // qqChartPanel1
            // 
            this.qqChartPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqChartPanel1.BackgroundImage")));
            // 
            // FrmGroupChart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.ClientSize = new System.Drawing.Size(550, 500);
            this.Name = "FrmGroupChart";
            this.spcGroupMember.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcGroupMember)).EndInit();
            this.spcGroupMember.ResumeLayout(false);
            this.spcChartHisotryAndCurrent.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcChartHisotryAndCurrent)).EndInit();
            this.spcChartHisotryAndCurrent.ResumeLayout(false);
            this.pnlRight.ResumeLayout(false);
            this.ResumeLayout(false);

        }
    }
}