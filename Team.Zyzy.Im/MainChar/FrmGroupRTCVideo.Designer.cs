﻿
namespace Team.Zyzy.Im.MainChar
{
    partial class FrmGroupRTCVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmGroupRTCVideo));
            this.pnlContent = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.qqFrmRound.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqFrmRound
            // 
            this.qqFrmRound.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqFrmRound.BackgroundImage")));
            this.qqFrmRound.Controls.Add(this.pnlContent);
            this.qqFrmRound.Location = new System.Drawing.Point(5, 5);
            this.qqFrmRound.Size = new System.Drawing.Size(1302, 960);
            this.qqFrmRound.Text = "视频会议";
            // 
            // pnlContent
            // 
            this.pnlContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlContent.Location = new System.Drawing.Point(3, 51);
            this.pnlContent.Name = "pnlContent";
            this.pnlContent.Size = new System.Drawing.Size(1296, 906);
            this.pnlContent.TabIndex = 0;
            // 
            // FrmGroupRTCVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1312, 970);
            this.Name = "FrmGroupRTCVideo";
            this.Text = "视频会议";
            this.qqFrmRound.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel pnlContent;
    }
}