﻿using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI.Forms;

namespace Team.Zyzy.Im.MainChar
{
    public partial class FrmGroupRTCVideo : FormQQCommon
    {
        public FrmGroupRTCVideo()
        {
            InitializeComponent();
            string  url = "https://meeting.zyzy.team/zhouyz-20240108";
            InitBrowser(url);
        }

        public FrmGroupRTCVideo(string url)
        {
            InitializeComponent();
            InitBrowser(url);
        }

        private ChromiumWebBrowser chromeBrowser = null;

        /// <summary>
        /// 设置浏览器网页
        /// </summary>
        /// <param name="bimUrl">网页地址</param>
        private void InitBrowser(string bimUrl)
        {
            try
            {
                if (string.IsNullOrEmpty(bimUrl))
                {
                    FrmAutoCloseMessageBox.Show("网页地址为空！");
                    return;
                }
                chromeBrowser = new ChromiumWebBrowser(bimUrl);
                pnlContent.SizeChanged += PnlContent_SizeChanged;
                pnlContent.Controls.Add(chromeBrowser);
                //chromeBrowser.Dock = DockStyle.Fill;
            }
            catch (Exception x)
            {
                FrmAutoCloseMessageBox.Show("设置浏览器网页异常：" + x.Message);
            }
        }
        private void PnlContent_SizeChanged(object sender, EventArgs e)
        {
            chromeBrowser.Location = new Point(0, 0);
            chromeBrowser.Width = pnlContent.Width;
            chromeBrowser.Height = pnlContent.Height;
        }
    }
}
