﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.mainService;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.ContractPanel;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.Model.Chart;

namespace Team.Zyzy.Im
{
    public partial class FrmClassifyManage : FormQQFixSize
    {
        private ClassifyVO classifyVO = null;
        public ClassifyVO ClassifyVO
        {
            get
            {
                return classifyVO;
            }
            set
            {
                if (classifyVO != value)
                {
                    classifyVO = value;
                    nudSortKey.Value = classifyVO.sortKey;
                    txtClassifyName.Text = classifyVO.classifyName;
                }
            }
        }
        public QQContractListBase ContractListBar;
        public FrmClassifyManage()
        {
            InitializeComponent();
            this.Load += FrmClassifyManage_Load;
            this.StartPosition = FormStartPosition.CenterScreen;
            this.TopMost = true;
        }

        private void FrmClassifyManage_Load(object sender, EventArgs e)
        {
 
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            classifyVO.classifyName = txtClassifyName.Text.Trim();
            classifyVO.sortKey = Convert.ToInt32(nudSortKey.Value);
            if (LoadChartBarsService.GetInstance().ContainClassifyName(classifyVO))
            {
                FrmAutoCloseMessageBox.Show("已经有该分组，请重新输入分组名！");
                txtClassifyName.Focus();
                return;
            }
            if (classifyVO.id > 0)
            {
                string url = "/classify/modify";
                HttpHelp.HttpApi<ClassifyVO>(url, classifyVO, "PUT");
            }
            else
            {
                string url = "/classify/create";
                classifyVO = HttpHelp.HttpApi<ClassifyVO>(url, classifyVO, "POST");
            }
            LoadChartBarsService.GetInstance().InitClassify();
            if (classifyVO.type == (int)MessageSourceType.Friend)
            {
                LoadChartBarsService.GetInstance().InitClassify();
                LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
            }
            else if (classifyVO.type == (int)MessageSourceType.Group)
            {
                LoadChartBarsService.GetInstance().InitClassify();
                LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
            }
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
