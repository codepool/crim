﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.Forms;

namespace Team.Zyzy.Im
{
    public partial class FrmVoiceRecord : FormQQFixSize
    {
        public FrmVoiceRecord()
        {
            InitializeComponent();
            this.Text = "";
            btnStop.Enabled = false;
            btnSend.Enabled = false;
            timer1.Enabled = false;
        }

        public event VoiceEventHandler SendClick;

        private string _fileName = "voice_record_temp_";

        protected string VoiceFileName
        {
            get
            {
                return _fileName + _userId.ToString() +"_"+_voiceTime+"_.wav";
            }
        }
        private long _userId = 0;

        public long UserID
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        private int _voiceTime = 0;
        public int VoiceTimeLength
        {
            get
            {
                return _voiceTime;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            DirectoryInfo folder = new DirectoryInfo(System.AppDomain.CurrentDomain.BaseDirectory);
            foreach (FileInfo file in folder.GetFiles("*.wav", SearchOption.AllDirectories))
            {
                if (file.FullName.IndexOf(_fileName) > 0)
                {
                    file.Delete();
                }
            }
            VoiceRecordHelp.StartRecord();
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            _voiceTime = 0;
            timer1.Enabled = true;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            VoiceRecordHelp.StopRecord(VoiceFileName);
            btnSend.Enabled = true;
            btnStart.Enabled = true;
            timer1.Enabled = false;
        }

        private void btnStopAndSend_Click(object sender, EventArgs e)
        {
            if (SendClick != null)
            {
                SendClick(VoiceFileName, _voiceTime);
            }
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _voiceTime++;
            labTime.Text = _voiceTime.ToString() + "秒";
        }
    }
    public delegate void VoiceEventHandler(string fileName,int voiceTimeLength);
}

