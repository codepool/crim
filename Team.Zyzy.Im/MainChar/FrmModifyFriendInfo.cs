﻿using Microsoft.MixedReality.WebRTC;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.wsocket;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using Team.Zyzy.Im.Control.Model;
using System.IO;
using System.Net;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.mainService;
using YiWangYi.MsgQQ.WinUI;

namespace Team.Zyzy.Im
{
    public partial class FrmModifyFriendInfo : FormQQFixSize
    {
        public FrmModifyFriendInfo()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Load += FrmModifyFriendInfo_Load;
            this.TopMost = true;
        }

        private long _userId = 0;
        public long UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        private UserVO _userVO = null;
        public UserVO UserVO
        {
            get
            {
                return _userVO;
            }
        }

        private void FrmModifyFriendInfo_Load(object sender, EventArgs e)
        {
            pnlContent.ChangeSkin();
            loadUserInfo();
        }

        private void loadUserInfo()
        {
            string url = "/user/find/"+_userId;
            this._userVO = HttpHelp.HttpApi<UserVO>(url, null, "GET");

            txtNickName.Text = _userVO.nickName;
            txtSignature.Text = _userVO.signature;
            txtUserName.Text = _userVO.userName;
            txtPhone.Text = _userVO.phone;
            txtOnlineRemark.Text = _userVO.onlineRemark;
            if (_userVO.sex == 0)
            {
                rdMale.Checked = true;
            }
            else
            {
                rdFemale.Checked = true;
            }
            picHeadImage.Image = HeadImageHelp.FriendHeadImage(_userVO.headImageThumb, _userVO.id);
            if (_userId != RequestApiInfo.SendId)
            {
                txtNickName.Enabled = false;
                txtUserName.Enabled = false;
                rdMale.Enabled = false;
                rdFemale.Enabled = false;
                txtOnlineRemark.Enabled = false;
                txtPhone.Enabled = false;
                txtSignature.Enabled = false;
                picHeadImage.Enabled = false;
            }
            txtUserName.Enabled = false;
        }

        #region 上传头像图片
        private string _headImagePath = null;
        private string _headImageUrl = null;
        private string _headImageThumbUrl = null;
        private void picHeadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "打开图片(Open Image)";
            ofd.FileName = "";
            ofd.Filter = "Image Files(*.bmp;*.jpg;*.gif;*.png)|*.bmp;*.jpg;*.gif;*.png";
            ofd.ValidateNames = true;
            ofd.CheckFileExists = true;
            ofd.CheckPathExists = true;
            try
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    _headImagePath = ofd.FileName;
                    uploadImageFile(_headImagePath);
                    Image img = Image.FromFile(_headImagePath);
                    picHeadImage.Image = img;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        protected  void uploadImageFile(string fileName)
        {
            FileInfo file = new FileInfo(fileName);
            try
            {
                HttpFileHelp.UploadImage(file, uploadImageCompleted, uploadProgressChanged);
            }
            catch (Exception ex)
            {
                FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
            }
        }
        private void uploadImageCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            Result<UploadImageVO> resultModel = JsonConvert.DeserializeObject<Result<UploadImageVO>>(Encoding.UTF8.GetString(e.Result));
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                _headImageThumbUrl = resultModel.data.thumbUrl;
                _headImageUrl = resultModel.data.originUrl;
            }
        }

        private void uploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
        }
        #endregion

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnAccept_Click(object sender, EventArgs e)
        {
            if (_userId != RequestApiInfo.SendId)
            {
                this.Close();
                return;
            }

             _userVO.nickName= txtNickName.Text.Trim();
             _userVO.signature= txtSignature.Text.Trim();
             //_userVO.userName= txtUserName.Text.Trim();
            _userVO.phone = txtPhone.Text.Trim();
            _userVO.onlineRemark = txtOnlineRemark.Text.Trim();
            _userVO.sex = rdMale.Checked ? 0 : 1;
            if (_headImageUrl != null)
            {
                _userVO.headImage = _headImageUrl;
                _userVO.headImageThumb = _headImageThumbUrl;
            }
            string url = "/user/update";
            _userVO = HttpHelp.HttpApi<UserVO>(url, _userVO, "PUT");
            if(RequestApiInfo.SendId== _userId)
            {
                HeadImageHelp.FriendHeadImage(_headImageThumbUrl, _userId);
                FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
                frmMain.InitPersonInfo();
            }
            FrmAutoCloseMessageBox.Show("更新个人信息成功！");
            this.Close();
        }

        #region 更新在线备注信息
        private static string _onLineRemark=null;
        public static void UpdateOnLineRemark(string remark)
        {
            if (_onLineRemark != remark)
            {
                _onLineRemark = remark;
                string url = "/user/find/" + RequestApiInfo.SendId;
                UserVO userInfo = HttpHelp.HttpApi<UserVO>(url, null, "GET");
                userInfo.onlineRemark = remark.Trim();
                url = "/user/update";
                userInfo = HttpHelp.HttpApi<UserVO>(url, userInfo, "PUT");
                FrmAutoCloseMessageBox.Show("更新在线状态说明成功！");
            }
        }
        #endregion
    }
}

