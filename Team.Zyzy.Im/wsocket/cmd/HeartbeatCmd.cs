﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.dto
{
    public class HeartbeatCmd
    {
        public int cmd { get; set; }

        /// <summary>
        /// data
        /// </summary>
        public Data data { get; set; }

        public class Data
        {
        }
    }
}
