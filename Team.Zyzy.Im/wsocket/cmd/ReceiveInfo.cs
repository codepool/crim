﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.dto
{
    public class ReceiveInfo<T>
    {
        public int cmd { get; set; }

        /// <summary>
        /// data
        /// </summary>
        public T data { get; set; }
    }
}
