﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Net.WebSockets.Managed;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.dto;
using Team.Zyzy.Im.enums;

namespace Team.Zyzy.Im.wsocket
{
    public class WSocketWin7ClientHelp : IWSocketClientHelp
    {
        System.Net.WebSockets.Managed.ClientWebSocket ws = null;

        Uri uri = null;
        bool isUserClose = false;//是否最后由用户手动关闭

        /// <summary>
        /// WebSocket状态
        /// </summary>
        public WebSocketState? State { get => ws?.State; }


        /// <summary>
        /// 连接建立时触发
        /// </summary>
        public event EventHandler OnOpen;
        /// <summary>
        /// 登录成功时触发
        /// </summary>
        public event EventHandler OnLogin;
        /// <summary>
        /// 客户端接收服务端数据时触发
        /// </summary>
        public event MessageEventHandler OnMessage;
        /// <summary>
        /// 通信发生错误时触发
        /// </summary>
        public event ErrorEventHandler OnError;
        /// <summary>
        /// 通信发生错误时触发
        /// </summary>
        public event ErrorEventHandler OnSocketError;
        /// <summary>
        /// 连接关闭时触发
        /// </summary>
        public event EventHandler OnClose;

        public WSocketWin7ClientHelp(string wsUrl)
        {
            uri = new Uri(wsUrl);
            ws = new System.Net.WebSockets.Managed.ClientWebSocket();
        }

        private CancellationTokenSource ctsHeartbeat = new CancellationTokenSource();
        private Task heartbeatTask = null;
        /// <summary>
        /// 打开链接
        /// </summary>
        public void Open()
        {
            Task.Run(async () =>
            {
                if (ws.State == WebSocketState.Connecting || ws.State == WebSocketState.Open)
                    return;

                string netErr = string.Empty;
                try
                {
                    //初始化链接
                    isUserClose = false;
                    ws = new System.Net.WebSockets.Managed.ClientWebSocket();
                    await ws.ConnectAsync(uri, CancellationToken.None);

                    if (OnOpen != null)
                        OnOpen(ws, new EventArgs());

                    //全部消息容器
                    List<byte> bs = new List<byte>();
                    //缓冲区
                    var buffer = new byte[1024 * 4];
                    //监听Socket信息
                    WebSocketReceiveResult result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    //是否关闭
                    while (!result.CloseStatus.HasValue)
                    {
                        //文本消息
                        if (result.MessageType == WebSocketMessageType.Text)
                        {
                            bs.AddRange(buffer.Take(result.Count));

                            //消息是否已接收完全
                            if (result.EndOfMessage)
                            {
                                //发送过来的消息
                                string userMsg = Encoding.UTF8.GetString(bs.ToArray(), 0, bs.Count);
                                ReceiveInfo<Object> msg = JsonConvert.DeserializeObject<ReceiveInfo<Object>>(userMsg);
                                if (msg.cmd == (int)IMCmdType.LOGIN)
                                {
                                    HeartbeatCmd heartbeatCmd = new HeartbeatCmd();
                                    heartbeatCmd.cmd = 1;
                                    heartbeatCmd.data = new HeartbeatCmd.Data();
                                    Send(heartbeatCmd);
                                    if (OnLogin != null)
                                        OnLogin(ws, new EventArgs());
                                }
                                else if (msg.cmd == (int)IMCmdType.HEART_BEAT)
                                {

                                }
                                else
                                {
                                    if (OnMessage != null)
                                        OnMessage(ws, msg);
                                }
                                //清空消息容器
                                bs = new List<byte>();
                            }
                        }
                        //继续监听Socket信息
                        result = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    }
                    ctsHeartbeat.Cancel();
                    ////关闭WebSocket（服务端发起）
                    //await ws.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
                }
                catch (WebSocketException we)
                {
                    if (OnSocketError != null)
                    {
                        OnSocketError(ws, we);
                    }
                }
                catch (Exception ex)
                {
                    netErr = " .Net发生错误" + ex.Message;

                    if (OnError != null)
                        OnError(ws, ex);

                    //if (ws != null && ws.State == WebSocketState.Open)
                    //    //关闭WebSocket（客户端发起）
                    //    await ws.CloseAsync(WebSocketCloseStatus.Empty, ex.Message, CancellationToken.None);
                }
                finally
                {
                    ctsHeartbeat.Cancel();
                    if (!isUserClose)
                        Close(ws.CloseStatus.Value, ws.CloseStatusDescription + netErr);
                }
            });

            heartbeatTask = Task.Run(() =>
            {
                HeartbeatCmd heartbeatCmd = new HeartbeatCmd();
                heartbeatCmd.cmd = 1;
                heartbeatCmd.data = new HeartbeatCmd.Data();
                while (!ctsHeartbeat.Token.IsCancellationRequested)
                {
                    if (!isUserClose)
                    {
                        Send(heartbeatCmd);
                        Thread.Sleep(1000 * 5);
                    }
                }
            }, ctsHeartbeat.Token);
        }


        public bool Send(Object model)
        {
            return Send(JsonConvert.SerializeObject(model));
        }

        /// <summary>
        /// 使用连接发送文本消息
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="mess"></param>
        /// <returns>是否尝试了发送</returns>
        public bool Send(string mess)
        {
            if (ws.State != WebSocketState.Open)
                return false;

            Task.Run(async () =>
            {
                var replyMess = Encoding.UTF8.GetBytes(mess);
                //发送消息
                await ws.SendAsync(new ArraySegment<byte>(replyMess), WebSocketMessageType.Text, true, CancellationToken.None);
            });

            return true;
        }

        /// <summary>
        /// 使用连接发送字节消息
        /// </summary>
        /// <param name="ws"></param>
        /// <param name="mess"></param>
        /// <returns>是否尝试了发送</returns>
        public bool Send(byte[] bytes)
        {
            if (ws.State != WebSocketState.Open)
                return false;

            Task.Run(async () =>
            {
                //发送消息
                await ws.SendAsync(new ArraySegment<byte>(bytes), WebSocketMessageType.Binary, true, CancellationToken.None);
            });

            return true;
        }

        /// <summary>
        /// 关闭连接
        /// </summary>
        public void Close()
        {
            isUserClose = true;
            Close(WebSocketCloseStatus.NormalClosure, "用户手动关闭");
        }

        public void Close(WebSocketCloseStatus closeStatus, string statusDescription)
        {
            Task.Run(async () =>
            {
                try
                {
                    //关闭WebSocket（客户端发起）
                    await ws.CloseAsync(closeStatus, statusDescription, CancellationToken.None);
                }
                catch (Exception ex)
                {

                }

                ws.Abort();
                ws.Dispose();

                if (OnClose != null)
                    OnClose(ws, new EventArgs());
            });
        }

    }

}