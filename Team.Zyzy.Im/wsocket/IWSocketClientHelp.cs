﻿using System;
using System.Net.WebSockets;
using Team.Zyzy.Im.Business.dto;

namespace Team.Zyzy.Im.wsocket
{
    public interface IWSocketClientHelp
    {
        WebSocketState? State { get; }

        event EventHandler OnClose;
        event ErrorEventHandler OnError;
        event EventHandler OnLogin;
        event MessageEventHandler OnMessage;
        event EventHandler OnOpen;
        event ErrorEventHandler OnSocketError;

        void Close();
        void Close(WebSocketCloseStatus closeStatus, string statusDescription);
        void Open();
        bool Send(byte[] bytes);
        bool Send(object model);
        bool Send(string mess);
    }

    /// <summary>
    /// 包含一个数据的事件
    /// </summary>
    public delegate void MessageEventHandler(object sender, ReceiveInfo<Object> data);
    public delegate void ErrorEventHandler(object sender, Exception ex);
}