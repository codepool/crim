﻿using Microsoft.MixedReality.WebRTC;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.Business.vo;
using static Microsoft.MixedReality.WebRTC.PeerConnection;
using static Microsoft.MixedReality.WebRTC.Transceiver;
using Team.Zyzy.Im.Business.enums;

namespace Team.Zyzy.Im.wsocket
{
    public class WebRTCServer : IDisposable
    {
        private static WebRTCServer instance = null;
        private static readonly object padlock = new object();
        WebRTCServer(bool isMaster)
        {
            init(isMaster);
        }

        private async void init(bool isMaster)
        {
            this._master = isMaster;
            try
            {
                videoCaptureList = new List<VideoCaptureDevice>();
                var deviceList = await DeviceVideoTrackSource.GetCaptureDevicesAsync();
                foreach (var device in deviceList)
                {
                    videoCaptureList.Add(device);
                }
                pc = new PeerConnection();

                var config = new PeerConnectionConfiguration
                {
                    IceServers = this.IceServers,
                    IceTransportType = IceTransportType.All,
                    BundlePolicy = BundlePolicy.Balanced,
                    SdpSemantic = SdpSemantic.UnifiedPlan
                };
                await pc.InitializeAsync(config);

                pc.TransceiverAdded += Pc_TransceiverAdded;
                #region 初始化  addLocalInfo
                if (_master)
                {
                    webcamSource = await DeviceVideoTrackSource.CreateAsync();
                    webcamSource.I420AVideoFrameReady += WebcamSource_I420AVideoFrameReady;
                    var videoTrackConfig = new LocalVideoTrackInitConfig
                    {
                        trackName = videoTrackName
                    };
                    localVideoTrack = LocalVideoTrack.CreateFromSource(webcamSource, videoTrackConfig);

                    TransceiverInitSettings option = new TransceiverInitSettings();
                    option.Name = videoTrackName;
                    option.StreamIDs = new List<string> { videoTrackName };
                    videoTransceiver = pc.AddTransceiver(MediaKind.Video, option);
                    //videoTransceiver.DesiredDirection = Transceiver.Direction.SendReceive;
                    //videoTransceiver.LocalVideoTrack = localVideoTrack;

                    microphoneSource = await DeviceAudioTrackSource.CreateAsync();
                    var audioTrackConfig = new LocalAudioTrackInitConfig
                    {
                        trackName = voiceTrackName
                    };

                    localAudioTrack = LocalAudioTrack.CreateFromSource(microphoneSource, audioTrackConfig);
                    option = new TransceiverInitSettings();
                    option.Name = voiceTrackName;
                    option.StreamIDs = new List<string> { voiceTrackName };
                    audioTransceiver = pc.AddTransceiver(MediaKind.Audio, option);
                    //audioTransceiver.LocalAudioTrack = localAudioTrack;
                    //audioTransceiver.DesiredDirection = Transceiver.Direction.SendReceive;
                }
                #endregion
                pc.Connected += () =>
                {
                    _isConnected = true;
                    if (candidateVOs != null)
                    {
                        foreach (ExchangeCandidateVO item in candidateVOs)
                        {
                            SendCandidate(item);
                        }
                        candidateVOs = null;
                    }
                    Console.WriteLine("==========已经连接PeerConnection: connected.");
                };

                pc.IceStateChanged += (IceConnectionState newState) =>
                {
                    Console.WriteLine($"==========状态改变ICE state: {newState}");
                };
                pc.VideoTrackAdded += (RemoteVideoTrack track) =>
                {
                    track.I420AVideoFrameReady += (I420AVideoFrame frame) =>
                    {
                        if (RemoteAVideoFrameReady != null)
                        {
                            RemoteAVideoFrameReady(frame);
                        }
                    };
                };
                pc.LocalSdpReadytoSend += (SdpMessage message) =>
                {
                    ResponseAnswerIceVO answerIceVO = new ResponseAnswerIceVO();
                    string label = "zyzy";
                    if (message.Type == SdpMessageType.Answer)
                    {
                        string newSDP = message.Content
                        .Replace("a=msid-semantic: WMS", "a=msid-semantic: WMS " + label)
                        .Replace("msid:", "msid:" + label)
                        .Replace("mslabel:", "mslabel:" + label);
                        answerIceVO.sdp = newSDP;
                        answerIceVO.type = "answer";
                        SendAnswer(answerIceVO);
                    }
                    else
                    {
                        string newSDP = message.Content
                        .Replace("a=msid-semantic: WMS " + voiceTrackName + " " + videoTrackName, "a=msid-semantic: WMS " + label)
                        .Replace("a=msid-semantic: WMS " + videoTrackName + " " + voiceTrackName, "a=msid-semantic: WMS " + label)
                        .Replace("a=msid-semantic: WMS " + videoTrackName, "a=msid-semantic: WMS " + label)
                        .Replace("a=msid-semantic: WMS " + voiceTrackName, "a=msid-semantic: WMS " + label)
                        .Replace("msid:" + voiceTrackName, "msid:" + label)
                        .Replace("msid:" + videoTrackName, "msid:" + label)
                        .Replace("mslabel:" + voiceTrackName, "mslabel:" + label)
                        .Replace("mslabel:" + videoTrackName, "mslabel:" + label);
                        answerIceVO.sdp = newSDP;
                        answerIceVO.type = "offer";
                        SendCall(answerIceVO);
                    }
                };

                pc.IceCandidateReadytoSend += (IceCandidate candidate) =>
                {
                    ExchangeCandidateVO candidateVO = new ExchangeCandidateVO();
                    candidateVO.setUsernameFragmentBySdp(candidate.Content);
                    candidateVO.sdpMid = candidate.SdpMid;
                    candidateVO.sdpMLineIndex = candidate.SdpMlineIndex;

                    if (_isConnected)
                    {
                        SendCandidate(candidateVO);
                    }
                    else
                    {
                        if (candidateVOs == null)
                        {
                            candidateVOs = new List<ExchangeCandidateVO>();
                        }
                        candidateVOs.Add(candidateVO);
                    }
                };

                #region 数据通道测试

                //var dataChannelLabel = $"data_channel_{Guid.NewGuid()}";
                // dataChannel1 = await pc.AddDataChannelAsync(42, "my_channel", ordered: true, reliable: true);
                //dataChannel1.StateChanged += delegate
                //{
                //    if (dataChannel1.State == DataChannel.ChannelState.Open)
                //    {
                //        MessageBox.Show("数据通道打开成功");
                //        //byte[] chatMessage = System.Text.Encoding.UTF8.GetBytes("=======这是测试：" + DateTime.Now.ToString());
                //        //dataChannel1.SendMessage(chatMessage);
                //    }
                //};
                //dataChannel1.MessageReceived += DataChannel_MessageReceived;
                //pc.DataChannelAdded += Pc_DataChannelAdded;
                #endregion

                pc.AudioTrackAdded += Pc_AudioTrackAdded;
                setInitSuccess();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

       

        #region 数据通道测试
        private void DataChannel_MessageReceived(byte[] obj)
        {
            string text = System.Text.Encoding.UTF8.GetString(obj);
            MessageBox.Show(text);
            //AppendText($"[remote] {text}\n");
        }

        private void Pc_DataChannelAdded(DataChannel channel)
        {
            //throw new NotImplementedException();
        }
        #endregion
        private async void Pc_TransceiverAdded(Transceiver transceiver)
        {
            switch (transceiver.MediaKind)
            {
                case MediaKind.Video:
                    transceiver.DesiredDirection = Transceiver.Direction.SendReceive;
                    videoTransceiver = transceiver;


                    webcamSource = await DeviceVideoTrackSource.CreateAsync();
                    webcamSource.I420AVideoFrameReady += WebcamSource_I420AVideoFrameReady;
                    var videoTrackConfig = new LocalVideoTrackInitConfig
                    {
                        trackName = videoTrackName
                    };
                    localVideoTrack = LocalVideoTrack.CreateFromSource(webcamSource, videoTrackConfig);
                    videoTransceiver.LocalVideoTrack = localVideoTrack;

                    break;
                case MediaKind.Audio:
                    transceiver.DesiredDirection = Transceiver.Direction.SendReceive;
                    audioTransceiver = transceiver;


                    microphoneSource = await DeviceAudioTrackSource.CreateAsync();
                    var audioTrackConfig = new LocalAudioTrackInitConfig
                    {
                        trackName = voiceTrackName
                    };

                    localAudioTrack = LocalAudioTrack.CreateFromSource(microphoneSource, audioTrackConfig);
                    audioTransceiver.LocalAudioTrack = localAudioTrack;
                    break;
                    //default:
                    //    throw new ArgumentOutOfRangeException("数据传输=============");
            }
        }

        private void Pc_AudioTrackAdded(RemoteAudioTrack track)
        {
            if (RemoteAudioAdded != null)
            {
                RemoteAudioAdded(track);
            }
        }


        private void WebcamSource_I420AVideoFrameReady(I420AVideoFrame frame)
        {
            if (LocalAVideoFrameReady != null)
            {
                LocalAVideoFrameReady(frame);
            }
        }

        public static void CloseInstance()
        {
            if (instance != null)
            {
                instance.Close();
            }
        }
        public static WebRTCServer CreateInstance(bool isMaster)
        {
            lock (padlock)
            {
                if (instance != null)
                {
                    instance.Close();
                }
                instance = new WebRTCServer(isMaster);
                return instance;
            }
        }

        public static WebRTCServer Instance
        {
            get
            {
                return instance;
            }
        }

        private bool _isConnected = false;
        public bool IsConnected
        {
            get
            {
                return _isConnected;
            }
        }
        private bool _initSuccess = false;
        public bool InitSuccess
        {
            get
            {
                return _initSuccess;
            }
        }
        private long _id;
        public long ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        private long _friendId;
        public long FriendId
        {
            get
            {
                return _friendId;
            }
            set
            {
                _friendId = value;
            }
        }


        private bool _master = false;

        public bool Master
        {
            get
            {
                return _master;
            }
        }

        private RtcMsgVO _offer = null;
        public RtcMsgVO Offer
        {
            get
            {
                return _offer;
            }
        }

        private static List<IceServer> _iceServers = null;
        public List<IceServer> IceServers
        {
            get
            {
                if (_iceServers == null)
                {
                    _iceServers = new List<IceServer>();
                    try
                    {
                        string url = "/webrtc/private/iceservers";
                        List<ICEServerVO> iCEServerVOs = HttpHelp.HttpApi<List<ICEServerVO>>(url, null, "GET");
                        foreach (ICEServerVO item in iCEServerVOs)
                        {
                            IceServer ice = new IceServer();
                            ice.Urls = new List<string>();
                            ice.Urls.Add(item.urls);
                            ice.TurnUserName = item.username;
                            ice.TurnPassword = item.credential;
                            _iceServers.Add(ice);
                        }
                    }
                    catch { }
                }
                return _iceServers;
            }
            set
            {
                if (value != null)
                {
                    _iceServers = value;
                }
            }
        }

        public void SendCandidate(ExchangeCandidateVO candidate)
        {
            try
            {
                string url = "/webrtc/private/candidate?uid=" + this.FriendId;
                HttpHelp.HttpApi<long?>(url, candidate, "POST");
            }
            catch { }
            System.Console.WriteLine(JsonConvert.SerializeObject(candidate));
        }
        public void SendCall(ResponseAnswerIceVO candidate)
        {
            try
            {
                string url = "/webrtc/private/call?uid=" + this.FriendId;
                HttpHelp.HttpApi<long?>(url, candidate, "POST");
            }
            catch { }
        }
        public void SendAnswer(ResponseAnswerIceVO candidate)
        {
            try
            {
                string url = "/webrtc/private/accept?uid=" + this.FriendId;
                HttpHelp.HttpApi<long?>(url, candidate, "POST");
            }
            catch { }
        }

        public void SendHandup()
        {
            try
            {
                string url = "/webrtc/private/handup?uid=" + this.FriendId;
                HttpHelp.HttpApi<long?>(url, null, "POST");
            }
            catch { }
        }

        public void SendCancel()
        {
            try
            {
                string url = "/webrtc/private/cancel?uid=" + this.FriendId;
                HttpHelp.HttpApi<long?>(url, null, "POST");
            }
            catch { }
        }

        private FrmRTCVideo frmRTCVideo = null;
        public FrmRTCVideo Form
        {
            get
            {
                return frmRTCVideo;
            }
            set
            {
                frmRTCVideo = value;
            }
        }
        DataChannel dataChannel1 = null;
        AudioTrackSource microphoneSource = null;
        VideoTrackSource webcamSource = null;
        Transceiver audioTransceiver = null;
        Transceiver videoTransceiver = null;
        LocalAudioTrack localAudioTrack = null;
        LocalVideoTrack localVideoTrack = null;
        PeerConnection pc = null;
        List<VideoCaptureDevice> videoCaptureList = null;
        List<ExchangeCandidateVO> candidateVOs = null;
        public event AudioTrackAddedDelegate RemoteAudioAdded;
        public event I420AVideoFrameDelegate RemoteAVideoFrameReady;
        public event I420AVideoFrameDelegate LocalAVideoFrameReady;
        public event EventHandler OnInitSuccess;

        private string voiceTrackName = "microphone_track";
        private string videoTrackName = "webcam_track";
        private void setInitSuccess()
        {
            _initSuccess = true;
            if (OnInitSuccess != null)
            {
                OnInitSuccess(this, null);
            }
        }
        public async void OnReceiveMessage(PrivateMessage msg)
        {
            switch ((MessageType)msg.type)
            {
                case MessageType.RTC_CALL:
                    this._offer = JsonConvert.DeserializeObject<RtcMsgVO>(msg.content);
                    SdpMessage sdpMessage = new SdpMessage();
                    sdpMessage.Type = SdpMessageType.Offer;
                    sdpMessage.Content = this._offer.sdp;
                    await pc.SetRemoteDescriptionAsync(sdpMessage);
                    pc.CreateAnswer();
                    break;
                case MessageType.RTC_CANCEL:
                    //if (videoDialog != null)
                    //{
                    //    videoDialog.Close();
                    //}
                    MessageBoxShow("对方取消了呼叫！");
                    VoiceRecordHelp.StopPlayCall();
                    FrmRTCVideoDialog.CloseInstance(FriendId);
                    CloseVideo();
                    Close();
                    break;
                case MessageType.RTC_ACCEPT:
                    if (msg.content != null)
                    {
                        this._offer = JsonConvert.DeserializeObject<RtcMsgVO>(msg.content);
                        SdpMessage sdpMessage1 = new SdpMessage();
                        sdpMessage1.Type = SdpMessageType.Answer;
                        sdpMessage1.Content = this._offer.sdp;
                        await pc.SetRemoteDescriptionAsync(sdpMessage1);
                    }
                    break;
                case MessageType.RTC_REJECT:
                    MessageBoxShow("对方拒绝了您的视频请求");
                    CloseVideo();
                    Close();
                    break;
                case MessageType.RTC_FAILED:
                    MessageBoxShow("呼叫失败");
                    CloseVideo();
                    Close();
                    break;
                case MessageType.RTC_CANDIDATE:
                    ExchangeCandidateVO candidateVO = JsonConvert.DeserializeObject<ExchangeCandidateVO>(msg.content);
                    IceCandidate candidate = new IceCandidate();
                    candidate.Content = candidateVO.candidate;
                    candidate.SdpMid = candidateVO.sdpMid;
                    candidate.SdpMlineIndex = candidateVO.sdpMLineIndex;
                    pc.AddIceCandidate(candidate);
                    break;
                case MessageType.RTC_HANDUP:
                    MessageBoxShow("对方挂断了视频通话");
                    CloseVideo();
                    Close();
                    break;
                default:
                    break;
            }
        }

        public void Call()
        {
            _isConnected = false;
            pc.CreateOffer();
        }
        public void CloseVideo()
        {
            if (this.Form != null && this.Form.IsAccessible)
            {
                this.Form.Close();
            }
        }

        public void Close()
        {
            instance = null;
            Dispose();
        }

        public void Dispose()
        {
            try
            {
                localVideoTrack?.Dispose();
                localAudioTrack?.Dispose();
                microphoneSource?.Dispose();
                webcamSource?.Dispose();
                pc?.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("===========" + ex.Message);
            }
        }

        #region 信息弹出框

        public void MessageBoxShow(string content)
        {
            Action action = () =>
            {
                YiWangYi.MsgQQ.WinUI.Forms.FrmAutoCloseMessageBox.Show(content);
            };
            Form frm = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as Form;
            frm.Invoke(action);
        }
        public void MessageBoxShow(string content, MessageBoxIcon information)
        {
            Action action = () =>
            {
                YiWangYi.MsgQQ.WinUI.Forms.FrmAutoCloseMessageBox.Show(content, information);
            };
            Form frm = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as Form;
            frm.Invoke(action);
        }

        public void MessageBoxShow(string caption, string content, MessageBoxIcon information)
        {
            Action action = () =>
            {
                YiWangYi.MsgQQ.WinUI.Forms.FrmAutoCloseMessageBox.Show(caption, content, information);
            };
            Form frm = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as Form;
            frm.Invoke(action);
        }
        #endregion
    }
}