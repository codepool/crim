﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.ContractPanel;
using System.Data.SqlClient;
using YiWangYi.MsgQQ.WinUI.QQListBar;
using YiWangYi.MsgQQ.WinUI.Service.MainService;
using YiWangYi.MsgQQ;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.Service.ChartService;
using YiWangYi.MsgQQ.WinUI.Service.CalendarService;
using Newtonsoft.Json;
using Team.Zyzy.Im.Business.dto;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.Business.vo;
using System.Net.WebSockets;
using Team.Zyzy.Im.wsocket;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.mainService;
using Team.Zyzy.Im.config;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.Business.BLL;
using Team.Zyzy.Im.Business.Model;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.enums;
using YiWangYi.MsgQQ.WinUI.Tree;
using YiWangYi.MsgQQ.WinUI.General.GDIControls;
using YiWangYi.MsgQQ.WinUI.Model;
using YiWangYi.MsgQQ.WinUI;

namespace Team.Zyzy.Im
{
    public partial class FrmMain : FormQQMain
    {
        public FrmMain()
        {
            InitializeComponent();
            this.Load += FrmMain_Load;
            this.Text = "CRIM2023";
            this.Left = -2000;            
            this.FormClosing += FrmMain_FormClosing;
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            WebRTCServer.CloseInstance();
        }

        public IWSocketClientHelp wSocketClient = null;

        private MainChartBarsRightMenu mainChartBarsRightMenu = null;

        private void FrmMain_Load(object sender, EventArgs e)
        {
            mainChartBarsRightMenu = new MainChartBarsRightMenu(this.ContractListBar);
            TopWebsBarService twbs = TopWebsBarService.GetInstance(base.SoftSetting, this);
            LeftBarSwitchService fss = LeftBarSwitchService.GetInstance(base.FunctionSwitch, this);
            BottomToolsBarService btbs = BottomToolsBarService.GetInstance(base.CommonOper, this);
            BottomTipsBarService  itfs = BottomTipsBarService .GetInstance(base.InforTips, this);
            this.ContractListBar.FriendsList.ItemDoubleClick += new CbItemEventHandler(ChartIteamClickService.FriendsList_ItemDoubleClick);
            this.ContractListBar.GroupList.ItemDoubleClick += new CbItemEventHandler(ChartIteamClickService.GroupList_ItemDoubleClick);
            this.ContractListBar.HistoryList.ItemDoubleClick += new CbItemEventHandler(ChartIteamClickService.HistoryList_ItemDoubleClick);

            qqFrmMainBackground.MouseEnterMenu += new EventHandler(qqFrmMainBackground_MouseEnterMenu);
            qqFrmMainBackground.MouseLeaveMenu += new EventHandler(qqFrmMainBackground_MouseLeaveMenu);
            qqFrmMainBackground.MouseClickMenu += QqFrmMainBackground_MouseClickMenu;
            this.Hide();

            FrmLogin frmLogin = new FrmLogin();
            frmLogin.OnLoginSuccess += delegate
            {
                Action action = () =>
                {
                    initUserInfoAndStartSocket();
                    LoadChartBarsService.GetInstance().InitChartHistory(this.ContractListBar.HistoryList.Groups);
                    LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
                    LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
                    LoadChartBarsService.GetInstance().InitCompanyStructure(this.ContractListBar.CompanyList);
                };
                Invoke(action);
            };
            this.notifyIconWHDDT.DoubleClick += delegate
            {
                if (frmLogin != null && !frmLogin.IsDisposed)
                    frmLogin.WindowState = FormWindowState.Normal;
            };
            if (frmLogin.ShowDialog() == DialogResult.OK)
            {
                this.Show();
            }
            else
            {
                this.Opacity = 0.1;
                this.Close();
            }

            loadContractConfig();
            fss.LoadConfig();
        }

        #region 加载个人信息

        private void initUserInfoAndStartSocket()
        {
            HeadImageHelp.ClearMoreFiles();
            InitPersonInfo();

            Version currentVersion = Environment.OSVersion.Version;
            Version win8Version = new Version("6.2");
            if (currentVersion.CompareTo(win8Version) >= 0)
            {
                wSocketClient = new WSocketClientHelp(RequestApiInfo.WsURL);
            }
            else
            {
                wSocketClient = new WSocketWin7ClientHelp(RequestApiInfo.WsURL);
            }
            initWebSocket();
        }

        public void InitPersonInfo()
        {
            string url = "/user/self";
            UserVO userVO = HttpHelp.HttpApi<UserVO>(url, null, "GET");
            RequestApiInfo.SendId = userVO.id;
            RequestApiInfo.SendNickName = userVO.nickName;
            this.ClientInfo.OnlineRemark = userVO.onlineRemark;
            this.ClientInfo.GroupName = userVO.nickName;
            this.ClientInfo.GroupTootip = userVO.userName + "(" + userVO.nickName + ")";
            this.ClientInfo.UserName = userVO.signature;
            this.ClientInfo.UserSignName = userVO.signature;
            this.ClientInfo.HeadClick -= ClientInfo_HeadClick;
            this.ClientInfo.HeadClick += ClientInfo_HeadClick;
            _voiceInfoConfig = OnlineStateConfig.LoadConfig();
            this.ClientInfo.OnlineStatus = _voiceInfoConfig.OnlineStatusList[0];
            this.ClientInfo.StatusChangeEnd -= ClientInfo_StatusChangeEnd;
            this.ClientInfo.StatusChangeEnd += ClientInfo_StatusChangeEnd;
            this.ClientInfo.ContextMenuStrip = _voiceInfoConfig.GetContextMenuStrip();
            this.ClientInfo.ChangeStatusClick -= ClientInfo_ChangeStatusClick;
            this.ClientInfo.ChangeStatusClick += ClientInfo_ChangeStatusClick;
            if (userVO.headImage.Length > 10)
            {
                this.ClientInfo.HeadPhotoNormal = HeadImageHelp.FriendHeadImage(userVO.headImage, userVO.id);
            }
        }



        #region 状态菜单
        private OnlineStateConfig _voiceInfoConfig = null;
        private void ClientInfo_ChangeStatusClick(object sender, MouseEventArgs e)
        {
            DateTime fileSaveTime = _voiceInfoConfig.SaveTime;
            _voiceInfoConfig = OnlineStateConfig.LoadConfig();
            if (fileSaveTime != _voiceInfoConfig.SaveTime)
            {
                this.ClientInfo.ContextMenuStrip = _voiceInfoConfig.GetContextMenuStrip();
            }
        }

        private void ClientInfo_StatusChangeEnd(object sender, EventArgs e)
        {
            OnlineStatusModel model = sender as OnlineStatusModel;
            _voiceInfoConfig.SaveConfig(model);
            FrmModifyFriendInfo.UpdateOnLineRemark(model.OnlineRemark);
        }
        #endregion
        private void ClientInfo_HeadClick(object sender, EventArgs e)
        {
            FrmModifyFriendInfo friendInfo = new FrmModifyFriendInfo();
            friendInfo.UserId = RequestApiInfo.SendId;
            friendInfo.Show();
        }

        #endregion

        #region 加载配置信息

        private HomePageConfig _homePageConfig = null;
        private void loadContractConfig()
        {
            _homePageConfig = HomePageConfig.LoadConfig();
            this.ContractListBar.SetContractIndex(ContractType.History, _homePageConfig.ContractBar.HistoryIndex);
            this.ContractListBar.SetContractIndex(ContractType.Friend, _homePageConfig.ContractBar.FriendIndex);
            this.ContractListBar.SetContractIndex(ContractType.Group, _homePageConfig.ContractBar.GroupIndex);
            this.ContractListBar.SetContractIndex(ContractType.Company, _homePageConfig.ContractBar.CompanyIndex);
            this.ContractListBar.SetContractSelected(_homePageConfig.ContractBar.SelectIndex);
        }

        #endregion

        #region webSocket

        public void initWebSocket()
        {
            wSocketClient.OnOpen -= WSocketClient_OnOpen;
            wSocketClient.OnLogin -= WSocketClient_OnLogin;
            wSocketClient.OnMessage -= WSocketClient_OnMessage;
            wSocketClient.OnClose -= WSocketClient_OnClose;
            wSocketClient.OnError -= WSocketClient_OnError;
            wSocketClient.OnSocketError -= WSocketClient_OnSocketError;

            wSocketClient.OnOpen += WSocketClient_OnOpen;
            wSocketClient.OnLogin += WSocketClient_OnLogin;
            wSocketClient.OnMessage += WSocketClient_OnMessage;
            wSocketClient.OnClose += WSocketClient_OnClose;
            wSocketClient.OnError += WSocketClient_OnError;
            wSocketClient.OnSocketError += WSocketClient_OnSocketError;
            wSocketClient.Open();
        }

        private void WSocketClient_OnSocketError(object sender, Exception ex)
        {
            Action action1 = () =>
            {
                this.IsDock = false;
                FrmLoginRetry.GetInstance().StartLogin();
            };
            Invoke(action1);
        }

        private void WSocketClient_OnLogin(object sender, EventArgs e)
        {

        }

        private void WSocketClient_OnError(object sender, Exception ex)
        {

        }

        private void WSocketClient_OnClose(object sender, EventArgs e)
        {

        }

        private void WSocketClient_OnMessage(object sender, ReceiveInfo<Object> data)
        {
            //处理的消息错误将会忽略
            try
            {
                if (data.cmd == (int)IMCmdType.FORCE_LOGUT)
                {

                }
                else if (data.cmd == (int)IMCmdType.PRIVATE_MESSAGE)
                {
                    PrivateMessage privateMessage = JsonConvert.DeserializeObject<PrivateMessage>(data.data.ToString());
                    privateMessage.status = (int)MessageStatus.Send;
                    ChartMessageHelp.PlayTip(privateMessage);
                    FriendVOItem item = findFriend(privateMessage.sendId);
                    if (item != null)
                    {
                        Action action1 = () =>
                        {
                            item.CurrentState = CurrentState.OnLine;
                            if (privateMessage.type == (int)MessageType.TEXT ||
                            privateMessage.type == (int)MessageType.IMAGE ||
                            privateMessage.type == (int)MessageType.FILE ||
                            privateMessage.type == (int)MessageType.AUDIO ||
                            privateMessage.type == (int)MessageType.VIDEO)
                            {
                                item.MessageCount = item.MessageCount + 1;
                                RefushUnreadCount();
                            }
                            UpdateHistoryMessage(privateMessage);
                        };
                        Invoke(action1);
                        FrmPersonChart lanPersonChart = FrmPersonChart.FindInstance<FrmPersonChart>(item.ID);
                        if (lanPersonChart != null)
                        {
                            lanPersonChart.AddMessage(privateMessage);
                        }
                        //好友视频聊天
                        personWebRtc(privateMessage, item);
                    }
                    else
                    {
                        //没有好友
                        if (AddNewFriend(privateMessage.sendId))
                        {
                            Action action1 = () =>
                            {
                                RefreshFriends();
                                WSocketClient_OnMessage(sender, data);
                            };
                            Invoke(action1);
                        }
                    }
                }
                else if (data.cmd == (int)IMCmdType.GROUP_MESSAGE)
                {
                    GroupMessage groupMessage = JsonConvert.DeserializeObject<GroupMessage>(data.data.ToString());
                    groupMessage.status = (int)MessageStatus.Send;
                    ChartMessageHelp.PlayTip(groupMessage);
                    GroupVOItem item = findGroup(groupMessage.groupId);
                    if (item != null)
                    {
                        Action action1 = () =>
                        {
                            item.CurrentState = CurrentState.OnLine;
                            item.MessageCount = item.MessageCount + 1;
                            UpdateHistoryMessage(groupMessage);
                            RefushUnreadCount();
                        };
                        Invoke(action1);
                        FrmGroupChart frmGroupChart = FrmGroupChart.FindInstance<FrmGroupChart>(item.ID);
                        if (frmGroupChart != null)
                        {
                            frmGroupChart.AddMessage(groupMessage);
                        }
                    }
                    else
                    {
                        //没有群

                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        private void personWebRtc(PrivateMessage privateMessage, FriendVOItem item)
        {
            if (privateMessage.type == (int)MessageType.RTC_CALL)
            {
                Action action = () =>
                {
                    FrmRTCVideoDialog frmRTCVideoDialog = FrmRTCVideoDialog.GetInstance();
                    frmRTCVideoDialog.FriendInfo = item;
                    frmRTCVideoDialog.FriendMessage = privateMessage;
                    frmRTCVideoDialog.Show();
                };
                Invoke(action);
            }
            else
            {
                if (privateMessage.type == (int)MessageType.RTC_CANCEL)
                {
                    VoiceRecordHelp.StopPlayCall();
                    Action action = () =>
                    {
                        FrmAutoCloseMessageBox.Show("对方取消了呼叫！");
                        FrmRTCVideoDialog.CloseInstance(item.ID);
                    };
                    Invoke(action);
                }
                else if (privateMessage.type >= 100 && WebRTCServer.Instance != null)
                {
                    WebRTCServer.Instance.OnReceiveMessage(privateMessage);
                }
            }
        }

        private void WSocketClient_OnOpen(object sender, EventArgs e)
        {
            LoginCmd.Data data = new LoginCmd.Data();
            data.accessToken = RequestApiInfo.AccessToken;
            LoginCmd loginCmd = new LoginCmd();
            loginCmd.cmd = 0;
            loginCmd.data = data;
            wSocketClient.Send(loginCmd);


        }

        #endregion

        #region 鼠标事件

        void qqFrmMainBackground_MouseLeaveMenu(object sender, EventArgs e)
        {
            Point location1 = (Point)sender;
            FrmCalendarShowService.MainMenuLeaveAction(new Point(this.Left + location1.X, this.Top + location1.Y));
        }

        void qqFrmMainBackground_MouseEnterMenu(object sender, EventArgs e)
        {
            Point location1 = (Point)sender;
            FrmCalendarShowService.MainMenuEnterAction(new Point(this.Left + location1.X, this.Top + location1.Y));
        }

        private void QqFrmMainBackground_MouseClickMenu(object sender, EventArgs e)
        {
            FrmSystemService frmSysSetting = FrmSystemService.Instance();
            frmSysSetting.ShowItem("basicSettingAlarmClock");
        }

        #endregion

        #region 好友列表
        public void RefreshFriends()
        {
            LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
        }
        public FriendVOItem findFriend(long friendId)
        {
            foreach (ClassificationT<FriendVOItem> group in this.ContractListBar.FriendsList.Groups)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    FriendVOItem item = group[i];
                    if (item.FriendVO.id == friendId)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public  bool AddNewFriend(long friendId)
        {
            try
            {
                string url = "/user/find/" + friendId;
                UserVO friendVO = HttpHelp.HttpApi<UserVO>(url, null, "GET");
                if (friendVO.type > 1)
                {
                    string para = "friendId=" + friendVO.id;
                    url = "/friend/add";
                    HttpHelp.HttpApi<long?>(url, para, "POST");
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        #endregion

        #region 公司组织架构

        #endregion

        #region 群组信息
        public void RefreshGroups()
        {
            LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
        }
        public GroupVOItem findGroup(long groupId)
        {
            foreach (ClassificationT<GroupVOItem> group in this.ContractListBar.GroupList.Groups)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    GroupVOItem item = group[i];
                    if (item.GroupVO.id == groupId)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        #endregion

        #region 聊天历史
        public HistoryVOItem findHistory(long oppositeId, MessageSourceType sourceType)
        {
            foreach (ClassificationT<HistoryVOItem> group in this.ContractListBar.HistoryList.Groups)
            {
                for (int i = 0; i < group.Count; i++)
                {
                    HistoryVOItem item = group[i];
                    if (item.HistoryVO.opposite_id == oppositeId && item.HistoryVO.source_type == (int)sourceType)
                    {
                        return item;
                    }
                }
            }
            return null;
        }

        public void UpdateHistoryMessage(PrivateMessage input)
        {
            long friendId = (input.sendId == RequestApiInfo.SendId ? input.recvId : input.sendId);
            if (input.type == (int)MessageType.TEXT ||
                          input.type == (int)MessageType.IMAGE ||
                          input.type == (int)MessageType.FILE ||
                          input.type == (int)MessageType.AUDIO ||
                          input.type == (int)MessageType.VIDEO)
            {
                HistoryVOItem historyVOItem = findHistory(friendId, MessageSourceType.Friend);
                if (historyVOItem != null)
                {
                    historyVOItem.PersonSignaure = input.GetContentText(RequestApiInfo.SendId);
                    historyVOItem.UpdateTime = input.getSendTime();
                    historyVOItem.Parent.SortByUpdateTime();
                    Task.Run(() => ImPrivateMessageBll.Instance.Add(RequestApiInfo.SendId, input));
                }
                else
                {
                    long id = ImPrivateMessageBll.Instance.Add(RequestApiInfo.SendId, input);
                    ImHistoryMessage historyMessage = ImHistoryMessageBll.Instance.GetModel(id);
                    ClassificationT<HistoryVOItem> group = this.ContractListBar.HistoryList.Groups[0] as ClassificationT<HistoryVOItem>;
                    HistoryVOItem historyVOItem1 = new HistoryVOItem(historyMessage);
                    group.Add(historyVOItem1);
                    group.SortByUpdateTime();
                }
            }
            else if (input.type == (int)MessageType.RECALL)
            {
                //更新本地信息为已经撤回
                Task.Run(() => ImPrivateMessageBll.Instance.UpdateMessageStatusRecall(input.id));
                Task.Run(() => ImHistoryMessageBll.Instance.UpdatePrivateMessageStatusRecall(friendId));
            }
            else if (input.type == (int)MessageType.READED)
            {
                //对方已读不需要本地记录
                Task.Run(() => ImPrivateMessageBll.Instance.UpdateMessageStatusRead(RequestApiInfo.SendId, friendId));
                Task.Run(() => ImHistoryMessageBll.Instance.UpdatePrivateMessageStatusRead(friendId));
            }
        }


        public void UpdateHistoryMessage(GroupMessage input)
        {
            HistoryVOItem historyVOItem = findHistory(input.groupId, MessageSourceType.Group);
            if (historyVOItem != null)
            {
                historyVOItem.PersonSignaure = input.GetContentText();
                historyVOItem.UpdateTime = DateTime.Now;
                historyVOItem.Parent.SortByUpdateTime();
                Task.Run(() => ImGroupMessageBll.Instance.Add(input));
            }
            else
            {
                long id = ImGroupMessageBll.Instance.Add(input);
                ImHistoryMessage historyMessage = ImHistoryMessageBll.Instance.GetModel(id);
                ClassificationT<HistoryVOItem> group = this.ContractListBar.HistoryList.Groups[0] as ClassificationT<HistoryVOItem>;
                HistoryVOItem historyVOItem1 = new HistoryVOItem(historyMessage);
                group.Add(historyVOItem1);
                group.SortByUpdateTime();
            }
        }




        #endregion

        #region 消息未读提醒
        public override void RefushUnreadCount()
        {
            int unreadCount = 0;
            foreach (CbGroup item in this.ContractListBar.FriendsList.Groups)
            {
                CbGroup group = item;
                foreach (CbItem cb in group.SbItemsBox.Items)
                {
                    unreadCount += cb.MessageCount;
                }
            }
            foreach (CbGroup item in this.ContractListBar.GroupList.Groups)
            {
                CbGroup group = item;
                foreach (CbItem cb in group.SbItemsBox.Items)
                {
                    unreadCount += cb.MessageCount;
                }
            }
            this.UnreadMessageCount = unreadCount;
            LeftBarSwitchService instance = LeftBarSwitchService.GetInstance();
            instance.SetUnReadCount("im", this.UnreadMessageCount);
        }
        #endregion


        #region 信息弹出框

        public void MessageBoxShow(string content)
        {
            Action action = () =>
            {
                FrmAutoCloseMessageBox.Show(content);
            };
            Invoke(action);
        }

        public void MessageBoxShow(string content, MessageBoxIcon information)
        {
            Action action = () =>
            {
                FrmAutoCloseMessageBox.Show(content, information);
            };
            Invoke(action);
        }

        public void MessageBoxShow(string caption, string content, MessageBoxIcon information)
        {
            Action action = () =>
            {
                FrmAutoCloseMessageBox.Show(caption, content, information);
            };
            Invoke(action);
        }
        #endregion
    }
}