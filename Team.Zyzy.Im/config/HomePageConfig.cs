﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using System.ComponentModel;
using YiWangYi.MsgQQ.WinUI.General.GDIControls;

namespace Team.Zyzy.Im.config
{
    public class HomePageConfig
    {
        public HomePageConfig()
        {
            _cb = new ContractBar();
            leftBars = new List<LeftBar>();
        }

        private ContractBar _cb = null;

        public ContractBar ContractBar
        {
            get
            {
                return _cb;
            }
            set
            {
                _cb = value;
            }
        }

        private List<LeftBar> leftBars = null;
        public List<LeftBar> LeftBars
        {
            get
            {
                return leftBars;
            }
            set
            {
                leftBars = value;
            }
        }


        private bool _showLeftBar = true;

        public bool ShowLeftBar
        {
            get
            {
                return _showLeftBar;
            }
            set
            {
                _showLeftBar = value;
            }
        }
       
        private static string _filePath = PublicConstant.DefaultMainToolBarConfigFile;
        public static string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        public static HomePageConfig LoadConfig()
        {
            HomePageConfig config;
            if (!File.Exists(FilePath))
            {
                config = new HomePageConfig();
                config.SaveConfig();
                return config;
            }
            XmlSerializer xs = new XmlSerializer(typeof(HomePageConfig));
            StreamReader sr = new StreamReader(FilePath);
            config = xs.Deserialize(sr) as HomePageConfig;
            sr.Close();
            return config;
        }

        public void SaveConfig()
        {
            FileInfo fi = new FileInfo(FilePath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            XmlSerializer xs = new XmlSerializer(typeof(HomePageConfig));
            StreamWriter sw = new StreamWriter(FilePath);
            xs.Serialize(sw, this);
            sw.Close();
        }
    }

    public class ContractBar
    {
        public ContractBar()
        {
        }
                
        [XmlAttribute("friend")]
        [DefaultValue(2)]
        public int FriendIndex { get; set; }

        [XmlAttribute("group")]
        [DefaultValue(3)]
        public int GroupIndex { get; set; }

        [XmlAttribute("history")]
        [DefaultValue(1)]
        public int HistoryIndex { get; set; }

        [XmlAttribute("company")]
        [DefaultValue(4)]
        public int CompanyIndex { get; set; }

        [XmlAttribute("selectIndex")]
        public int _selectIndex = 1;

        [XmlIgnore]
        public ContractType SelectIndex
        {
            get
            {
                return (ContractType)_selectIndex;
            }
            set
            {
                _selectIndex = (int)value;
            }
        }

    }

    public class LeftBar: IComparable<LeftBar>
    {
        public LeftBar()
        {
        }

        [XmlAttribute("name")]
        public string Name { get; set; }

        [XmlAttribute("toolTip")]
        public string ToolTip { get; set; }

        [XmlAttribute("selected")]
        public bool Selected { get; set; }

        [XmlAttribute("sort")]
        public int SortKey { get; set; }

        public int CompareTo(LeftBar other)
        {
            return this.SortKey - other.SortKey;
        }
    }
}
