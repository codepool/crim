﻿
namespace Team.Zyzy.Im.config
{
    partial class CommonSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.SuspendLayout();
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Location = new System.Drawing.Point(392, 324);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(130, 24);
            this.qqLabel1.TabIndex = 7;
            this.qqLabel1.Text = "登录地址：";
            // 
            // CommonSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.qqLabel1);
            this.Name = "CommonSetControl";
            this.Size = new System.Drawing.Size(914, 672);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
    }
}
