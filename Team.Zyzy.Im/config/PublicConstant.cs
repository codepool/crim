﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Team.Zyzy.Im.config
{
    public class PublicConstant
    {
        public static string SynthroDictoryPath = Application.StartupPath;
        public static string UserConfigDictoryPath = Path.Combine(Application.StartupPath, "userConfig");
        public static string TemplateDictoryPath = Path.Combine(Application.StartupPath, "Template");

        public static string GridConfigDictoryPath = Path.Combine(Application.StartupPath, "gridConfig");//表格控件配置路径
        public static string DefaultHeadImagePath = Path.Combine(Application.StartupPath, "icons\\defaultProfile.png");
        public static string DefaultGroupImagePath = Path.Combine(Application.StartupPath, "icons\\defaultGroup.png");
        public static string CallAudioPath = Path.Combine(Application.StartupPath, "Resources\\audio\\call.wav");
        public static string TipAudioPath = Path.Combine(Application.StartupPath, "Resources\\audio\\tip.wav");
        public static string DefaultVoiceConfigFile = Path.Combine(Application.StartupPath, "userVoiceConfig.xml");
        
        public static string DefaultMainFormConfigFile = Path.Combine(Application.StartupPath, "mainFormConfig.xml");

        public static string SoftUpdateConfigFile = Path.Combine(Application.StartupPath, "yiwangyiUpdateConfig.xml");

        public static string UserLoginConfigFile = Path.Combine(Application.StartupPath, "userLoginConfig.xml");
        public static string CommonSetConfigFile = Path.Combine(Application.StartupPath, "commonSetConfig.xml");

        public static string GetMainFormConfig(string userLoginName)
        {
            string path = Path.Combine(Application.StartupPath, Path.Combine("userConfig", userLoginName));
            FileInfo fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            return Path.Combine(path, "mainFormConfig.xml");
        }

        public static string GetSkinConfig(string userLoginName)
        {
            string path = Path.Combine(Application.StartupPath, Path.Combine("userConfig", userLoginName));
            FileInfo fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            return Path.Combine(path, "skin.xml");
        }

        public static string DefaultMainToolBarConfigFile = Path.Combine(Application.StartupPath, "mainToolBarConfig.xml");
        public static string GetUserVoiceConfig(string userLoginName)
        {
            string path = Path.Combine(Application.StartupPath, Path.Combine("userConfig", userLoginName));
            FileInfo fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            return Path.Combine(path, "voiceConfig.xml");
        }
        public static string GetMainToolBarConfig(string userLoginName)
        {
            string path = Path.Combine(Application.StartupPath, Path.Combine("userConfig", userLoginName));
            FileInfo fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            return Path.Combine(path, "mainToolBarConfig.xml");
        }

        public static string GetStartFormConfig(string userLoginName)
        {
            string path = Path.Combine(Application.StartupPath, Path.Combine("userConfig", userLoginName));
            FileInfo fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            return Path.Combine(path, "startFormConfig.xml");
        }

        public static string DefaultDBMessageFile = Path.Combine(Application.StartupPath, "App_Data\\zyzy-im.db3");
        public static string GetUserDBConnectrion(string userLoginName)
        {
            string path = Path.Combine(Application.StartupPath, Path.Combine("userConfig", userLoginName));
            path = Path.Combine(path, "zyzy-im.db3");
            FileInfo fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }
            if (!fi.Exists) { 
                fi = new FileInfo(DefaultDBMessageFile);
                fi.CopyTo(path);
            }
            return "Data Source="+path;
        }

        public static string DefaultOnlineStatusConfigFile = Path.Combine(Application.StartupPath, "userOnlineConfig.xml");
        public static string GetUserOnlineStatusConfig(string userLoginName)
        {
            string path = Path.Combine(Application.StartupPath, Path.Combine("userConfig", userLoginName));
            FileInfo fi = new FileInfo(path);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            return Path.Combine(path, "userOnlineConfig.xml");
        }
    }
}
