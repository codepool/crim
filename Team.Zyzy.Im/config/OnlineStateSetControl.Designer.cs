﻿
namespace Team.Zyzy.Im.config
{
    partial class OnlineStateSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnlineStateSetControl));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvOnlineStatus = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusImage = new System.Windows.Forms.DataGridViewImageColumn();
            this.OnlineRemark = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdd = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnDelete = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnReset = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnSave = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOnlineStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dgvOnlineStatus);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(978, 608);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "在线状态管理";
            // 
            // dgvOnlineStatus
            // 
            this.dgvOnlineStatus.BackgroundColor = System.Drawing.Color.White;
            this.dgvOnlineStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvOnlineStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOnlineStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.statusImage,
            this.OnlineRemark,
            this.btnAdd,
            this.btnDelete});
            this.dgvOnlineStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOnlineStatus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvOnlineStatus.GridColor = System.Drawing.SystemColors.Control;
            this.dgvOnlineStatus.Location = new System.Drawing.Point(3, 31);
            this.dgvOnlineStatus.Name = "dgvOnlineStatus";
            this.dgvOnlineStatus.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvOnlineStatus.RowHeadersVisible = false;
            this.dgvOnlineStatus.RowHeadersWidth = 82;
            this.dgvOnlineStatus.RowTemplate.Height = 37;
            this.dgvOnlineStatus.Size = new System.Drawing.Size(972, 574);
            this.dgvOnlineStatus.TabIndex = 1;
            // 
            // Id
            // 
            this.Id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Id.DataPropertyName = "Id";
            this.Id.FillWeight = 40F;
            this.Id.Frozen = true;
            this.Id.HeaderText = "序号";
            this.Id.MinimumWidth = 10;
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 40;
            // 
            // statusImage
            // 
            this.statusImage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.statusImage.DataPropertyName = "statusImage";
            this.statusImage.FillWeight = 60F;
            this.statusImage.HeaderText = "状态";
            this.statusImage.MinimumWidth = 40;
            this.statusImage.Name = "statusImage";
            this.statusImage.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.statusImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.statusImage.Width = 60;
            // 
            // OnlineRemark
            // 
            this.OnlineRemark.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.OnlineRemark.DataPropertyName = "OnlineRemark";
            this.OnlineRemark.FillWeight = 10F;
            this.OnlineRemark.HeaderText = "显示信息";
            this.OnlineRemark.MinimumWidth = 10;
            this.OnlineRemark.Name = "OnlineRemark";
            this.OnlineRemark.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // btnAdd
            // 
            this.btnAdd.FillWeight = 80F;
            this.btnAdd.HeaderText = "";
            this.btnAdd.MinimumWidth = 10;
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.ReadOnly = true;
            this.btnAdd.Text = "新增";
            this.btnAdd.ToolTipText = "新增相同状态的提示信息";
            this.btnAdd.UseColumnTextForButtonValue = true;
            this.btnAdd.Width = 45;
            // 
            // btnDelete
            // 
            this.btnDelete.FillWeight = 80F;
            this.btnDelete.HeaderText = "";
            this.btnDelete.MinimumWidth = 10;
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ReadOnly = true;
            this.btnDelete.Text = "删除";
            this.btnDelete.ToolTipText = "删除该行数据";
            this.btnDelete.UseColumnTextForButtonValue = true;
            this.btnDelete.Width = 45;
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.Transparent;
            this.btnReset.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReset.BackgroundImage")));
            this.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReset.Caption = "重置";
            this.btnReset.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnReset.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnReset.Location = new System.Drawing.Point(670, 634);
            this.btnReset.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnReset.MouseDownImage")));
            this.btnReset.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnReset.MouseMoveImage")));
            this.btnReset.Name = "btnReset";
            this.btnReset.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnReset.NormalImage")));
            this.btnReset.Size = new System.Drawing.Size(116, 58);
            this.btnReset.TabIndex = 20;
            this.btnReset.ToolTip = null;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Caption = "保存";
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnSave.Location = new System.Drawing.Point(835, 634);
            this.btnSave.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnSave.MouseDownImage")));
            this.btnSave.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnSave.MouseMoveImage")));
            this.btnSave.Name = "btnSave";
            this.btnSave.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnSave.NormalImage")));
            this.btnSave.Size = new System.Drawing.Size(116, 58);
            this.btnSave.TabIndex = 21;
            this.btnSave.ToolTip = null;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // OnlineStateSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.groupBox1);
            this.Name = "OnlineStateSetControl";
            this.Size = new System.Drawing.Size(984, 707);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOnlineStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvOnlineStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn from;
        private System.Windows.Forms.DataGridViewImageColumn subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewImageColumn statusImage;
        private System.Windows.Forms.DataGridViewTextBoxColumn OnlineRemark;
        private System.Windows.Forms.DataGridViewButtonColumn btnAdd;
        private System.Windows.Forms.DataGridViewButtonColumn btnDelete;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnReset;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnSave;
    }
}
