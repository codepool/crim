﻿
namespace Team.Zyzy.Im.config
{
    partial class HomeAlarmClockSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkOpenAlarmClock = new System.Windows.Forms.CheckBox();
            this.ilLeftBar = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // chkOpenAlarmClock
            // 
            this.chkOpenAlarmClock.AutoSize = true;
            this.chkOpenAlarmClock.Location = new System.Drawing.Point(28, 44);
            this.chkOpenAlarmClock.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.chkOpenAlarmClock.Name = "chkOpenAlarmClock";
            this.chkOpenAlarmClock.Size = new System.Drawing.Size(186, 28);
            this.chkOpenAlarmClock.TabIndex = 20;
            this.chkOpenAlarmClock.Text = "默认开启闹钟";
            this.chkOpenAlarmClock.UseVisualStyleBackColor = true;
            this.chkOpenAlarmClock.CheckedChanged += new System.EventHandler(this.chkShowLeftBar_CheckedChanged);
            // 
            // ilLeftBar
            // 
            this.ilLeftBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.ilLeftBar.ImageSize = new System.Drawing.Size(30, 30);
            this.ilLeftBar.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // HomeAlarmClockSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkOpenAlarmClock);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "HomeAlarmClockSetControl";
            this.Size = new System.Drawing.Size(1016, 834);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox chkOpenAlarmClock;
        private System.Windows.Forms.ImageList ilLeftBar;
    }
}
