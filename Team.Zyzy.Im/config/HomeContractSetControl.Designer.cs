﻿
namespace Team.Zyzy.Im.config
{
    partial class HomeContractSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeContractSetControl));
            this.ilContract = new System.Windows.Forms.ImageList(this.components);
            this.cmbContract = new System.Windows.Forms.ComboBox();
            this.qqLabel5 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qlvHomeContract = new YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQListView();
            this.SuspendLayout();
            // 
            // ilContract
            // 
            this.ilContract.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilContract.ImageStream")));
            this.ilContract.TransparentColor = System.Drawing.Color.Transparent;
            this.ilContract.Images.SetKeyName(0, "groupHistory2.png");
            this.ilContract.Images.SetKeyName(1, "groupFriends2.png");
            this.ilContract.Images.SetKeyName(2, "groupCluster2.png");
            this.ilContract.Images.SetKeyName(3, "groupCompany2.png");
            // 
            // cmbContract
            // 
            this.cmbContract.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbContract.FormattingEnabled = true;
            this.cmbContract.Location = new System.Drawing.Point(163, 226);
            this.cmbContract.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbContract.Name = "cmbContract";
            this.cmbContract.Size = new System.Drawing.Size(838, 32);
            this.cmbContract.TabIndex = 27;
            // 
            // qqLabel5
            // 
            this.qqLabel5.AutoSize = true;
            this.qqLabel5.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel5.Location = new System.Drawing.Point(27, 234);
            this.qqLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.qqLabel5.Name = "qqLabel5";
            this.qqLabel5.Size = new System.Drawing.Size(130, 24);
            this.qqLabel5.TabIndex = 26;
            this.qqLabel5.Text = "默认选中：";
            // 
            // qlvHomeContract
            // 
            this.qlvHomeContract.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qlvHomeContract.Location = new System.Drawing.Point(6, 6);
            this.qlvHomeContract.Margin = new System.Windows.Forms.Padding(12, 12, 12, 12);
            this.qlvHomeContract.Name = "qlvHomeContract";
            this.qlvHomeContract.Size = new System.Drawing.Size(1004, 216);
            this.qlvHomeContract.TabIndex = 36;
            // 
            // HomeContractSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.qlvHomeContract);
            this.Controls.Add(this.cmbContract);
            this.Controls.Add(this.qqLabel5);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "HomeContractSetControl";
            this.Size = new System.Drawing.Size(1016, 834);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbContract;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel5;
        private System.Windows.Forms.ImageList ilContract;
        private YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQListView qlvHomeContract;
    }
}
