﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace Team.Zyzy.Im.config
{
    public class UpdateConfig
    {
        public UpdateConfig()
        {
            _swd = new List<LocalFile>();
            this.Enabled = true;
            this.ForcedUpdate = true;
            this.SameDirectories = true;
            this.IsStartUpdate = true;
        }

        private bool  _enable = true;

        public bool Enabled
        {
            get
            {
                return _enable;
            }
            set
            {
                _enable = value;
            }
        }
        private bool _forcedUpdate = true;

        public bool ForcedUpdate
        {
            get
            {
                return _forcedUpdate;
            }
            set
            {
                _forcedUpdate = value;
            }
        }
        public bool SameDirectories { get; set; }
        public bool IsStartUpdate { get; set; }
        private string _lastUpdateDate = "1986-01-09";

        public string LastUpdateDate
        {
            get
            {
                return _lastUpdateDate;
            }
            set
            {
                _lastUpdateDate = value;
            }
        }
        private string _softVersion = "1.0.0.0";

        public string SoftVersion
        {
            get
            {
                return _softVersion;
            }
            set
            {
                _softVersion = value;
            }
        }

        private string _softName = "CRMI";

        public string SoftName
        {
            get
            {
                return _softName;
            }
            set
            {
                _softName = value;
            }
        }

        private string _serverUrl = "http://www.zyzy.team/soft/im/NeedUpdate.xml";

        public string ServerUrl
        {
            get
            {
                return _serverUrl;
            }
            set
            {
                _serverUrl = value;
            }
        }

        private string _startPrograms = "CRMI.exe";

        public string StartPrograms
        {
            get
            {
                return _startPrograms;
            }
            set
            {
                _startPrograms = value;
            }
        }

        private List<LocalFile> _swd = null;

        public List<LocalFile> UpdateFileList
        {
            get
            {
                return _swd;
            }
            set
            {
                _swd = value;
            }
        }

        private static string _filePath = PublicConstant.SoftUpdateConfigFile;
        public static string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        public static UpdateConfig LoadConfig()
        {
            UpdateConfig config;
            if (!File.Exists(FilePath))
            {
                config = new UpdateConfig();
                config.SaveConfig();
                return config;
            }
            XmlSerializer xs = new XmlSerializer(typeof(UpdateConfig));
            StreamReader sr = new StreamReader(FilePath);
            config = xs.Deserialize(sr) as UpdateConfig;
            sr.Close();
            return config;
        }

        public void SaveConfig()
        {
            FileInfo fi = new FileInfo(FilePath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            XmlSerializer xs = new XmlSerializer(typeof(UpdateConfig));
            StreamWriter sw = new StreamWriter(FilePath);
            xs.Serialize(sw, this);
            sw.Close();
        }
    }

    public class LocalFile
    {
        public LocalFile()
        {
        }

        private string _name = "";
        [XmlAttribute("name")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private string _lastver = "";
        [XmlAttribute("lastver")]
        public string Lastver
        {
            get
            {
                return _lastver;
            }
            set
            {
                _lastver = value;
            }
        }

        private long _size = 0;
        [XmlAttribute("size")]
        public long Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }
    }
}
