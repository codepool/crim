﻿
namespace Team.Zyzy.Im.config
{
    partial class VoiceSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.chkOpenTipVoice = new System.Windows.Forms.CheckBox();
            this.chkOpenCallVoice = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // chkOpenTipVoice
            // 
            this.chkOpenTipVoice.AutoSize = true;
            this.chkOpenTipVoice.Location = new System.Drawing.Point(46, 47);
            this.chkOpenTipVoice.Name = "chkOpenTipVoice";
            this.chkOpenTipVoice.Size = new System.Drawing.Size(210, 28);
            this.chkOpenTipVoice.TabIndex = 5;
            this.chkOpenTipVoice.Text = "开启信息提示音";
            this.chkOpenTipVoice.UseVisualStyleBackColor = true;
            // 
            // chkOpenCallVoice
            // 
            this.chkOpenCallVoice.AutoSize = true;
            this.chkOpenCallVoice.Location = new System.Drawing.Point(46, 126);
            this.chkOpenCallVoice.Name = "chkOpenCallVoice";
            this.chkOpenCallVoice.Size = new System.Drawing.Size(234, 28);
            this.chkOpenCallVoice.TabIndex = 6;
            this.chkOpenCallVoice.Text = "开启音视频提示音";
            this.chkOpenCallVoice.UseVisualStyleBackColor = true;
            // 
            // VoiceSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkOpenCallVoice);
            this.Controls.Add(this.chkOpenTipVoice);
            this.Name = "VoiceSetControl";
            this.Size = new System.Drawing.Size(984, 707);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkOpenTipVoice;
        private System.Windows.Forms.CheckBox chkOpenCallVoice;
    }
}
