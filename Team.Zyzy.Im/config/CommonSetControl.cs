﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Interface;

namespace Team.Zyzy.Im.config
{
    public partial class CommonSetControl : UserControl, IChangeSkin
    {
        public CommonSetControl()
        {
            InitializeComponent();
        }

        public void ChangeSkin()
        {
            this.BackColor = ShareInfo.ControlBorderBackColor;
            this.Invalidate();
        }
    }
}
