﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace Team.Zyzy.Im.config
{
    public class VoiceInfoConfig
    {
        public VoiceInfoConfig()
        {
            _swd = new VoiceInfoData();
        }

        private VoiceInfoData _swd = null;

        public VoiceInfoData VoiceInfoData
        {
            get
            {
                return _swd;
            }
            set
            {
                _swd = value;
            }
        }

        private static string _filePath = PublicConstant.DefaultVoiceConfigFile;
        public static string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        public static VoiceInfoConfig LoadConfig()
        {
            VoiceInfoConfig config;
            if (!File.Exists(FilePath))
            {
                config = new VoiceInfoConfig();
                config.SaveConfig();
                return config;
            }
            XmlSerializer xs = new XmlSerializer(typeof(VoiceInfoConfig));
            StreamReader sr = new StreamReader(FilePath);
            config = xs.Deserialize(sr) as VoiceInfoConfig;
            sr.Close();
            return config;
        }

        public void SaveConfig()
        {
            FileInfo fi = new FileInfo(FilePath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            XmlSerializer xs = new XmlSerializer(typeof(VoiceInfoConfig));
            StreamWriter sw = new StreamWriter(FilePath);
            xs.Serialize(sw, this);
            sw.Close();
        }
    }

    public class VoiceInfoData
    {
        public VoiceInfoData()
        {
        }

        private bool _openTipVoice = false;
        [XmlAttribute("tipVoice")]
        public bool OpenTipVoice
        {
            get
            {
                return _openTipVoice;
            }
            set
            {
                _openTipVoice = value;
            }
        }

        private bool _openCallVoice = true;
        [XmlAttribute("callVoice")]
        public bool OpenCallVoice
        {
            get
            {
                return _openCallVoice;
            }
            set
            {
                _openCallVoice = value;
            }
        }
    }
}
