﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Interface;

namespace Team.Zyzy.Im.config
{
    public partial class LoginSetControl : UserControl,IChangeSkin
    {
        private LoginInfoConfig _loginInfoConfig = null;
        public LoginSetControl()
        {
            InitializeComponent();
            this.Load += LoginSetControl_Load;
            this.ControlRemoved += LoginSetControl_ControlRemoved;
        }

        private void LoginSetControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            saveConfig();
        }

        private void saveConfig()
        {
            if (txtApiUrl.Text.Trim().Length > 5)
            {
                _loginInfoConfig.LoginInfoData.BaseApi = txtApiUrl.Text.Trim();
            }
            if (txtSocketUrl.Text.Trim().Length > 5)
            {
                _loginInfoConfig.LoginInfoData.SockApi = txtSocketUrl.Text.Trim();
            }
            _loginInfoConfig.LoginInfoData.AutoLogin = chkAutoLogin.Checked;
            _loginInfoConfig.SaveConfig();
        }

        private void LoginSetControl_Load(object sender, EventArgs e)
        {
            _loginInfoConfig = LoginInfoConfig.LoadConfig();
            if (_loginInfoConfig.LoginInfoData.BaseApi.Length > 5)
            {
                txtApiUrl.Text= _loginInfoConfig.LoginInfoData.BaseApi;
            }
            if (_loginInfoConfig.LoginInfoData.SockApi.Length > 5)
            {
                txtSocketUrl.Text= _loginInfoConfig.LoginInfoData.SockApi;
            }
            chkAutoLogin.Checked = _loginInfoConfig.LoginInfoData.AutoLogin;
            this.chkAutoLogin.CheckedChanged += delegate { saveConfig(); };
            this.txtApiUrl.LostFocus += delegate { saveConfig(); };
            this.txtSocketUrl.LostFocus += delegate { saveConfig(); };

        }

        public void ChangeSkin()
        {
            this.BackColor = ShareInfo.ControlBorderBackColor;
            this.Invalidate();
        }
    }
}
