﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace Team.Zyzy.Im.config
{
    public class LoginInfoConfig
    {
        public LoginInfoConfig()
        {
            _swd = new LoginInfoData();
        }

        private LoginInfoData _swd = null;

        public LoginInfoData LoginInfoData
        {
            get
            {
                return _swd;
            }
            set
            {
                _swd = value;
            }
        }

        private static string _filePath = PublicConstant.UserLoginConfigFile;
        public static string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        public static LoginInfoConfig LoadConfig()
        {
            LoginInfoConfig config;
            if (!File.Exists(FilePath))
            {
                config = new LoginInfoConfig();
                config.SaveConfig();
                return config;
            }
            XmlSerializer xs = new XmlSerializer(typeof(LoginInfoConfig));
            StreamReader sr = new StreamReader(FilePath);
            config = xs.Deserialize(sr) as LoginInfoConfig;
            sr.Close();
            return config;
        }

        public void SaveConfig()
        {
            FileInfo fi = new FileInfo(FilePath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            XmlSerializer xs = new XmlSerializer(typeof(LoginInfoConfig));
            StreamWriter sw = new StreamWriter(FilePath);
            xs.Serialize(sw, this);
            sw.Close();
        }
    }

    public class LoginInfoData
    {
        public LoginInfoData()
        {
        }

        private bool _savePassword = false;
        [XmlAttribute("savePassword")]
        public bool SavePassword
        {
            get
            {
                return _savePassword;
            }
            set
            {
                _savePassword = value;
            }
        }

        private bool _autoStart = true;
        [XmlAttribute("autoStart")]
        public bool AutoStart
        {
            get
            {
                return _autoStart;
            }
            set
            {
                _autoStart = value;
            }
        }

        private bool _autoLogin = false;
        [XmlAttribute("autoLogin")]
        public bool AutoLogin
        {
            get
            {
                return _autoLogin;
            }
            set
            {
                _autoLogin = value;
            }
        }

        private string _userName = "";
        [XmlAttribute("userName")]
        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        private string _password= "";
        [XmlAttribute("password")]
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        private string _baseApi = "";
        [XmlAttribute("baseApi")]
        public string BaseApi
        {
            get
            {
                return _baseApi;
            }
            set
            {
                _baseApi = value;
            }
        }

        private string _sockApi = "";
        [XmlAttribute("sockApi")]
        public string SockApi
        {
            get
            {
                return _sockApi;
            }
            set
            {
                _sockApi = value;
            }
        }
    }
}
