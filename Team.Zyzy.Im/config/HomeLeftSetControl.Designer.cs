﻿
namespace Team.Zyzy.Im.config
{
    partial class HomeLeftSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.chkShowLeftBar = new System.Windows.Forms.CheckBox();
            this.ilLeftBar = new System.Windows.Forms.ImageList(this.components);
            this.cmbLeftBar = new System.Windows.Forms.ComboBox();
            this.qqLabel5 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qlvHomeLeft = new YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQListView();
            this.SuspendLayout();
            // 
            // chkShowLeftBar
            // 
            this.chkShowLeftBar.AutoSize = true;
            this.chkShowLeftBar.Location = new System.Drawing.Point(14, 22);
            this.chkShowLeftBar.Margin = new System.Windows.Forms.Padding(2);
            this.chkShowLeftBar.Name = "chkShowLeftBar";
            this.chkShowLeftBar.Size = new System.Drawing.Size(108, 16);
            this.chkShowLeftBar.TabIndex = 20;
            this.chkShowLeftBar.Text = "默认显示侧边栏";
            this.chkShowLeftBar.UseVisualStyleBackColor = true;
            this.chkShowLeftBar.CheckedChanged += new System.EventHandler(this.chkShowLeftBar_CheckedChanged);
            // 
            // ilLeftBar
            // 
            this.ilLeftBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.ilLeftBar.ImageSize = new System.Drawing.Size(30, 30);
            this.ilLeftBar.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cmbLeftBar
            // 
            this.cmbLeftBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbLeftBar.FormattingEnabled = true;
            this.cmbLeftBar.Location = new System.Drawing.Point(80, 174);
            this.cmbLeftBar.Margin = new System.Windows.Forms.Padding(2);
            this.cmbLeftBar.Name = "cmbLeftBar";
            this.cmbLeftBar.Size = new System.Drawing.Size(418, 20);
            this.cmbLeftBar.TabIndex = 34;
            // 
            // qqLabel5
            // 
            this.qqLabel5.AutoSize = true;
            this.qqLabel5.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel5.Location = new System.Drawing.Point(12, 178);
            this.qqLabel5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.qqLabel5.Name = "qqLabel5";
            this.qqLabel5.Size = new System.Drawing.Size(65, 12);
            this.qqLabel5.TabIndex = 33;
            this.qqLabel5.Text = "默认选中：";
            // 
            // qlvHomeLeft
            // 
            this.qlvHomeLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qlvHomeLeft.Location = new System.Drawing.Point(3, 43);
            this.qlvHomeLeft.Name = "qlvHomeLeft";
            this.qlvHomeLeft.Size = new System.Drawing.Size(502, 126);
            this.qlvHomeLeft.TabIndex = 35;
            // 
            // HomeLeftSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.qlvHomeLeft);
            this.Controls.Add(this.cmbLeftBar);
            this.Controls.Add(this.qqLabel5);
            this.Controls.Add(this.chkShowLeftBar);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "HomeLeftSetControl";
            this.Size = new System.Drawing.Size(508, 417);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox chkShowLeftBar;
        private System.Windows.Forms.ComboBox cmbLeftBar;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel5;
        private System.Windows.Forms.ImageList ilLeftBar;
        private YiWangYi.MsgQQ.WinUI.Service.BaseControls.QQListView qlvHomeLeft;
    }
}
