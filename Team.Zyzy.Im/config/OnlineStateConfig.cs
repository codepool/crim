﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using YiWangYi.MsgQQ.WinUI.Model;

namespace Team.Zyzy.Im.config
{
    public class OnlineStateConfig
    {
        public OnlineStateConfig()
        {
            _onlineStatusList = new List<OnlineStatusModel>();
        }

        public void LoadDefaultStatus()
        {
            _onlineStatusList = new List<OnlineStatusModel>();
            for (int i = 1; i <= 8; i++)
            {
                OnlineStatusModel item = new OnlineStatusModel();
                item.Id = i;
                item.OnlineStatus = (OnlineStatus)i;
                item.OnlineRemark = item.DefaultText;
                item.OnlineRemarkTootip = item.OnlineRemark;
                _onlineStatusList.Add(item);
            }
        }

        public DateTime SaveTime;

        private List<OnlineStatusModel> _onlineStatusList = null;

        public List<OnlineStatusModel> OnlineStatusList
        {
            get
            {
                return _onlineStatusList;
            }
            set
            {
                _onlineStatusList = value;
            }
        }

        private static string _filePath = PublicConstant.DefaultOnlineStatusConfigFile;
        public static string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
            }
        }

        public static OnlineStateConfig LoadConfig()
        {
            OnlineStateConfig config;
            if (!File.Exists(FilePath))
            {
                config = new OnlineStateConfig();
                config.LoadDefaultStatus();
                config.SaveConfig();
                return config;
            }
            XmlSerializer xs = new XmlSerializer(typeof(OnlineStateConfig));
StreamReader sr = new StreamReader(FilePath);
            config = xs.Deserialize(sr) as OnlineStateConfig;
            sr.Close();
            FileInfo fileInfo = new FileInfo(FilePath);
            config.SaveTime = fileInfo.LastWriteTime;
            return config;
        }

        public void SaveConfig()
        {
            FileInfo fi = new FileInfo(FilePath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
                fi = null;
            }
            XmlSerializer xs = new XmlSerializer(typeof(OnlineStateConfig));
            StreamWriter sw = new StreamWriter(FilePath);
            xs.Serialize(sw, this);
            sw.Close();
        }

        public void SaveConfig(OnlineStatusModel model)
        {
            bool contain = false;
            for (int i = 0; i < OnlineStatusList.Count; i++)
            {
                if (OnlineStatusList[i].OnlineRemark == model.OnlineRemark)
                {
                    contain = true;
                    break;
                }
            }
            if (!contain)
            {
                OnlineStatusList.Add(model);
                OnlineStatusList.Sort();
                SaveConfig();
            }
        }

        public ContextMenuStrip GetContextMenuStrip()
        {
            ContextMenuStrip cmsStatus = new ContextMenuStrip();
            for(int i=0;i< OnlineStatusList.Count; i++)
            {
                ToolStripMenuItem item = new ToolStripMenuItem();
                item.Image = OnlineStatusList[i].GetSingleImage();
                item.Text = OnlineStatusList[i].OnlineRemark;
                item.Tag = OnlineStatusList[i];
                cmsStatus.Items.Add(item);
            }

            return cmsStatus;
        }
    }

}
