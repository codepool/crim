﻿
namespace Team.Zyzy.Im.config
{
    partial class ModifyPasswordControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModifyPasswordControl));
            this.txtOldPassword = new System.Windows.Forms.TextBox();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.txtNewPassword = new System.Windows.Forms.TextBox();
            this.qqLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.txtPasswordAccept = new System.Windows.Forms.TextBox();
            this.qqLabel3 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.btnSave = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.SuspendLayout();
            // 
            // txtOldPassword
            // 
            this.txtOldPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOldPassword.Location = new System.Drawing.Point(171, 60);
            this.txtOldPassword.Name = "txtOldPassword";
            this.txtOldPassword.PasswordChar = '*';
            this.txtOldPassword.Size = new System.Drawing.Size(645, 35);
            this.txtOldPassword.TabIndex = 7;
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Location = new System.Drawing.Point(46, 60);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(130, 24);
            this.qqLabel1.TabIndex = 6;
            this.qqLabel1.Text = "旧 密 码：";
            // 
            // txtNewPassword
            // 
            this.txtNewPassword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNewPassword.Location = new System.Drawing.Point(171, 128);
            this.txtNewPassword.Name = "txtNewPassword";
            this.txtNewPassword.PasswordChar = '*';
            this.txtNewPassword.Size = new System.Drawing.Size(645, 35);
            this.txtNewPassword.TabIndex = 9;
            // 
            // qqLabel2
            // 
            this.qqLabel2.AutoSize = true;
            this.qqLabel2.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel2.Location = new System.Drawing.Point(46, 130);
            this.qqLabel2.Name = "qqLabel2";
            this.qqLabel2.Size = new System.Drawing.Size(130, 24);
            this.qqLabel2.TabIndex = 8;
            this.qqLabel2.Text = "新 密 码：";
            // 
            // txtPasswordAccept
            // 
            this.txtPasswordAccept.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPasswordAccept.Location = new System.Drawing.Point(171, 196);
            this.txtPasswordAccept.Name = "txtPasswordAccept";
            this.txtPasswordAccept.PasswordChar = '*';
            this.txtPasswordAccept.Size = new System.Drawing.Size(645, 35);
            this.txtPasswordAccept.TabIndex = 11;
            // 
            // qqLabel3
            // 
            this.qqLabel3.AutoSize = true;
            this.qqLabel3.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel3.Location = new System.Drawing.Point(46, 200);
            this.qqLabel3.Name = "qqLabel3";
            this.qqLabel3.Size = new System.Drawing.Size(130, 24);
            this.qqLabel3.TabIndex = 10;
            this.qqLabel3.Text = "确认密码：";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Transparent;
            this.btnSave.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSave.BackgroundImage")));
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.Caption = "确定";
            this.btnSave.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnSave.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnSave.Location = new System.Drawing.Point(700, 514);
            this.btnSave.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnSave.MouseDownImage")));
            this.btnSave.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnSave.MouseMoveImage")));
            this.btnSave.Name = "btnSave";
            this.btnSave.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnSave.NormalImage")));
            this.btnSave.Size = new System.Drawing.Size(116, 58);
            this.btnSave.TabIndex = 22;
            this.btnSave.ToolTip = null;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ModifyPasswordControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtPasswordAccept);
            this.Controls.Add(this.qqLabel3);
            this.Controls.Add(this.txtNewPassword);
            this.Controls.Add(this.qqLabel2);
            this.Controls.Add(this.txtOldPassword);
            this.Controls.Add(this.qqLabel1);
            this.Name = "ModifyPasswordControl";
            this.Size = new System.Drawing.Size(863, 707);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private System.Windows.Forms.TextBox txtOldPassword;
        private System.Windows.Forms.TextBox txtNewPassword;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel2;
        private System.Windows.Forms.TextBox txtPasswordAccept;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel3;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnSave;
    }
}
