﻿
namespace Team.Zyzy.Im.config
{
    partial class UpdateSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateSetControl));
            this.txtServerUrl = new System.Windows.Forms.TextBox();
            this.qqLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.txtLastUpdateDate = new System.Windows.Forms.TextBox();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.chkEnableUpdate = new System.Windows.Forms.CheckBox();
            this.txtSoftName = new System.Windows.Forms.TextBox();
            this.qqLabel3 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.txtSoftVersion = new System.Windows.Forms.TextBox();
            this.qqLabel4 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.btnCheckUpdate = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.SuspendLayout();
            // 
            // txtServerUrl
            // 
            this.txtServerUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtServerUrl.Location = new System.Drawing.Point(187, 362);
            this.txtServerUrl.Name = "txtServerUrl";
            this.txtServerUrl.Size = new System.Drawing.Size(793, 35);
            this.txtServerUrl.TabIndex = 14;
            this.txtServerUrl.Text = "wss://im.zyzy.team/ws_api/im";
            // 
            // qqLabel2
            // 
            this.qqLabel2.AutoSize = true;
            this.qqLabel2.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel2.Location = new System.Drawing.Point(29, 362);
            this.qqLabel2.Name = "qqLabel2";
            this.qqLabel2.Size = new System.Drawing.Size(130, 24);
            this.qqLabel2.TabIndex = 13;
            this.qqLabel2.Text = "更新地址：";
            // 
            // txtLastUpdateDate
            // 
            this.txtLastUpdateDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLastUpdateDate.Enabled = false;
            this.txtLastUpdateDate.Location = new System.Drawing.Point(187, 298);
            this.txtLastUpdateDate.Name = "txtLastUpdateDate";
            this.txtLastUpdateDate.Size = new System.Drawing.Size(793, 35);
            this.txtLastUpdateDate.TabIndex = 12;
            this.txtLastUpdateDate.Text = "https://im.zyzy.team/im_api";
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Location = new System.Drawing.Point(29, 298);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(130, 24);
            this.qqLabel1.TabIndex = 11;
            this.qqLabel1.Text = "最后更新：";
            // 
            // chkForcedUpdate
            // 
            this.chkEnableUpdate.AutoSize = true;
            this.chkEnableUpdate.Location = new System.Drawing.Point(33, 226);
            this.chkEnableUpdate.Name = "chkForcedUpdate";
            this.chkEnableUpdate.Size = new System.Drawing.Size(234, 28);
            this.chkEnableUpdate.TabIndex = 10;
            this.chkEnableUpdate.Text = "软件启动检查更新";
            this.chkEnableUpdate.UseVisualStyleBackColor = true;
            // 
            // txtSoftName
            // 
            this.txtSoftName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoftName.Enabled = false;
            this.txtSoftName.Location = new System.Drawing.Point(187, 72);
            this.txtSoftName.Name = "txtSoftName";
            this.txtSoftName.Size = new System.Drawing.Size(793, 35);
            this.txtSoftName.TabIndex = 16;
            this.txtSoftName.Text = "https://im.zyzy.team/im_api";
            // 
            // qqLabel3
            // 
            this.qqLabel3.AutoSize = true;
            this.qqLabel3.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel3.Location = new System.Drawing.Point(29, 72);
            this.qqLabel3.Name = "qqLabel3";
            this.qqLabel3.Size = new System.Drawing.Size(130, 24);
            this.qqLabel3.TabIndex = 15;
            this.qqLabel3.Text = "软件名称：";
            // 
            // txtSoftVersion
            // 
            this.txtSoftVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoftVersion.Enabled = false;
            this.txtSoftVersion.Location = new System.Drawing.Point(187, 144);
            this.txtSoftVersion.Name = "txtSoftVersion";
            this.txtSoftVersion.Size = new System.Drawing.Size(793, 35);
            this.txtSoftVersion.TabIndex = 18;
            this.txtSoftVersion.Text = "https://im.zyzy.team/im_api";
            // 
            // qqLabel4
            // 
            this.qqLabel4.AutoSize = true;
            this.qqLabel4.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel4.Location = new System.Drawing.Point(29, 144);
            this.qqLabel4.Name = "qqLabel4";
            this.qqLabel4.Size = new System.Drawing.Size(130, 24);
            this.qqLabel4.TabIndex = 17;
            this.qqLabel4.Text = "当前版本：";
            // 
            // btnCheckUpdate
            // 
            this.btnCheckUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCheckUpdate.BackgroundImage")));
            this.btnCheckUpdate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCheckUpdate.Caption = "检查更新";
            this.btnCheckUpdate.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCheckUpdate.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnCheckUpdate.Location = new System.Drawing.Point(832, 465);
            this.btnCheckUpdate.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnCheckUpdate.MouseDownImage")));
            this.btnCheckUpdate.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnCheckUpdate.MouseMoveImage")));
            this.btnCheckUpdate.Name = "btnCheckUpdate";
            this.btnCheckUpdate.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnCheckUpdate.NormalImage")));
            this.btnCheckUpdate.Size = new System.Drawing.Size(148, 58);
            this.btnCheckUpdate.TabIndex = 19;
            this.btnCheckUpdate.ToolTip = null;
            this.btnCheckUpdate.Click += new System.EventHandler(this.btnCheckUpdate_Click);
            // 
            // UpdateSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnCheckUpdate);
            this.Controls.Add(this.txtSoftVersion);
            this.Controls.Add(this.qqLabel4);
            this.Controls.Add(this.txtSoftName);
            this.Controls.Add(this.qqLabel3);
            this.Controls.Add(this.txtServerUrl);
            this.Controls.Add(this.qqLabel2);
            this.Controls.Add(this.txtLastUpdateDate);
            this.Controls.Add(this.qqLabel1);
            this.Controls.Add(this.chkEnableUpdate);
            this.Name = "UpdateSetControl";
            this.Size = new System.Drawing.Size(1016, 834);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtServerUrl;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel2;
        private System.Windows.Forms.TextBox txtLastUpdateDate;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private System.Windows.Forms.CheckBox chkEnableUpdate;
        private System.Windows.Forms.TextBox txtSoftName;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel3;
        private System.Windows.Forms.TextBox txtSoftVersion;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel4;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnCheckUpdate;
    }
}
