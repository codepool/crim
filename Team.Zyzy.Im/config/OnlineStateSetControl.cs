﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Interface;
using YiWangYi.MsgQQ.WinUI.Model;

namespace Team.Zyzy.Im.config
{
    public partial class OnlineStateSetControl : UserControl, IChangeSkin
    {
        private OnlineStateConfig _voiceInfoConfig = null;
        public OnlineStateSetControl()
        {
            InitializeComponent();
            this.Load += VoiceSetControl_Load;
            this.ControlRemoved += VoiceSetControl_ControlRemoved;
            dgvOnlineStatus.AutoGenerateColumns = false;
            DgvStyle1(dgvOnlineStatus);
        }

        private List<OnlineStatusModel> OnlineStatusList;

        private void VoiceSetControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            saveConfig();
        }

        private void saveConfig()
        {
            //_voiceInfoConfig.VoiceInfoData.OpenTipVoice = chkOpenTipVoice.Checked;
            //_voiceInfoConfig.VoiceInfoData.OpenCallVoice = chkOpenCallVoice.Checked;
            //_voiceInfoConfig.SaveConfig();
        }

        private void VoiceSetControl_Load(object sender, EventArgs e)
        {
            _voiceInfoConfig = OnlineStateConfig.LoadConfig();
            OnlineStatusList = _voiceInfoConfig.OnlineStatusList;
            setDataSourceIndex();
            dgvOnlineStatus.DataSource = OnlineStatusList;
            dgvOnlineStatus.CellFormatting += DgvOnlineStatus_CellFormatting;
            dgvOnlineStatus.CellClick += DgvOnlineStatus_CellClick;
            this.ChangeSkin();
        }

        private void DgvOnlineStatus_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            OnlineStatusModel online = (OnlineStatusModel)dgvOnlineStatus.Rows[e.RowIndex].DataBoundItem;
            if (e.ColumnIndex == 3)
            {
                OnlineStatusList.Add(online.Copy());
                dgvOnlineStatus.DataSource = null;
                setDataSourceIndex();
                dgvOnlineStatus.DataSource = OnlineStatusList;
            }
            else if (e.ColumnIndex == 4)
            {
                OnlineStatusList.Remove(online);
                dgvOnlineStatus.DataSource = null;
                setDataSourceIndex();
                dgvOnlineStatus.DataSource = OnlineStatusList;
            }
        }

        private void setDataSourceIndex()
        {
            OnlineStatusList.Sort();
            for (int i = 0; i < OnlineStatusList.Count; i++)
            {
                OnlineStatusList[i].Id = i + 1;
            }
        }

        private void DgvOnlineStatus_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            OnlineStatusModel online = (OnlineStatusModel)dgvOnlineStatus.Rows[e.RowIndex].DataBoundItem;
            if (e.ColumnIndex == 1)
                e.Value = online.GetSingleImage();
        }

        public void DgvStyle1(DataGridView dgv)
        {
            //奇数行的背景色
            dgv.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.AliceBlue;
            dgv.AlternatingRowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Blue;
            dgv.AlternatingRowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dgv.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //默认的行样式
            dgv.RowsDefaultCellStyle.BackColor = System.Drawing.Color.White;
            dgv.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dgv.RowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Blue;
            //数据网格颜色
            //dgv.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            //列标题的宽度
            dgv.ColumnHeadersHeight = 28;

            //dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        public void ChangeSkin()
        {
            this.BackColor = ShareInfo.ControlBorderBackColor;
            this.Invalidate();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            _voiceInfoConfig.LoadDefaultStatus();
            OnlineStatusList = _voiceInfoConfig.OnlineStatusList;
            dgvOnlineStatus.DataSource = null;
            setDataSourceIndex();
            dgvOnlineStatus.DataSource = OnlineStatusList;
            _voiceInfoConfig.SaveConfig();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            setDataSourceIndex();
            _voiceInfoConfig.OnlineStatusList = OnlineStatusList;
            _voiceInfoConfig.SaveConfig();
            FrmAutoCloseMessageBox.Show("保存成功！");
        }
    }
}