﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Interface;

namespace Team.Zyzy.Im.config
{
    public partial class VoiceSetControl : UserControl, IChangeSkin
    {
        private VoiceInfoConfig _voiceInfoConfig = null;
        public VoiceSetControl()
        {
            InitializeComponent();
            this.Load += VoiceSetControl_Load;
            this.ControlRemoved += VoiceSetControl_ControlRemoved;
        }

        private void VoiceSetControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            saveConfig();
        }

        private void saveConfig()
        {
            _voiceInfoConfig.VoiceInfoData.OpenTipVoice = chkOpenTipVoice.Checked;
            _voiceInfoConfig.VoiceInfoData.OpenCallVoice = chkOpenCallVoice.Checked;
            _voiceInfoConfig.SaveConfig();
        }

        private void VoiceSetControl_Load(object sender, EventArgs e)
        {
            _voiceInfoConfig = VoiceInfoConfig.LoadConfig();
            chkOpenTipVoice.Checked = _voiceInfoConfig.VoiceInfoData.OpenTipVoice;
            chkOpenCallVoice.Checked = _voiceInfoConfig.VoiceInfoData.OpenCallVoice;
            this.chkOpenCallVoice.CheckedChanged += delegate { saveConfig(); };
            this.chkOpenTipVoice.CheckedChanged += delegate { saveConfig(); };
        }

        public void ChangeSkin()
        {
            this.BackColor = ShareInfo.ControlBorderBackColor;
            this.Invalidate();
        }
    }
}