﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.Service.BaseControls;

namespace Team.Zyzy.Im.config
{
    public partial class HomeContractSetControl : UserControl
    {
        public HomeContractSetControl()
        {
            InitializeComponent();
            this.Load += HomeContractSetControl_Load;
            qlvHomeContract.ImageList = this.ilContract;
        }

        private void CmbContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            saveConfig();
        }

        private void HomeContractSetControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            saveConfig();
        }

        private HomePageConfig _homePageConfig = null;

        QQListViewItem itemHistory = new QQListViewItem();
        QQListViewItem itemFriend = new QQListViewItem();
        QQListViewItem itemGroup = new QQListViewItem();
        QQListViewItem itemCompany = new QQListViewItem();
        private void HomeContractSetControl_Load(object sender, EventArgs e)
        {
            _homePageConfig = HomePageConfig.LoadConfig();

            
            itemHistory.Text = "历史信息";
            itemHistory.Name = "itemHistory";
            itemHistory.ImageIndex = 0; 
            itemHistory.SortKey = _homePageConfig.ContractBar.HistoryIndex;
            
            itemFriend.Text = "好友信息";
            itemFriend.Name = "itemFriend";
            itemFriend.ImageIndex = 1;
            itemFriend.SortKey = _homePageConfig.ContractBar.FriendIndex;
            
            itemGroup.Text = "群组信息";
            itemGroup.Name = "itemGroup";
            itemGroup.ImageIndex = 2;
            itemGroup.SortKey = _homePageConfig.ContractBar.GroupIndex;
            
            itemCompany.Text = "组织架构";
            itemCompany.Name = "itemCompany";
            itemCompany.ImageIndex = 1;
            itemCompany.SortKey = _homePageConfig.ContractBar.CompanyIndex;
            qlvHomeContract.Clear();
            qlvHomeContract.Add(itemHistory);
            qlvHomeContract.Add(itemFriend);
            qlvHomeContract.Add(itemGroup);
            qlvHomeContract.Add(itemCompany);
            qlvHomeContract.Sort();

            cmbContract.Items.Add(itemFriend.Text);
            cmbContract.Items.Add(itemGroup.Text);
            cmbContract.Items.Add(itemCompany.Text);
            cmbContract.Items.Add(itemHistory.Text);
            cmbContract.SelectedIndex =(int) _homePageConfig.ContractBar.SelectIndex-1;
            qlvHomeContract.SelectedIndexChanged += LvContract_SelectedIndexChanged;
            this.ControlRemoved += HomeContractSetControl_ControlRemoved;
            cmbContract.SelectedIndexChanged += CmbContract_SelectedIndexChanged;
            qlvHomeContract.ItemChanged += delegate
            {
                saveConfig();
            };
        }

        private void LvContract_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (qlvHomeContract.ListView.SelectedItems.Count > 0)
            {
                for(int i = 0; i < cmbContract.Items.Count; i++)
                {
                    if (cmbContract.Items[i].ToString() == qlvHomeContract.ListView.SelectedItems[0].Text)
                    {
                        cmbContract.SelectedIndex = i;
                    }
                }
            }
        }

        private void saveConfig()
        {
            _homePageConfig.ContractBar.HistoryIndex=itemHistory.Index;
            _homePageConfig.ContractBar.FriendIndex = itemFriend.Index;
            _homePageConfig.ContractBar.GroupIndex = itemGroup.Index;
            _homePageConfig.ContractBar.CompanyIndex = itemCompany.Index;
            _homePageConfig.ContractBar.SelectIndex = (ContractType)(cmbContract.SelectedIndex+1);
            _homePageConfig.SaveConfig();
        }

    }
}
