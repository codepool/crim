﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.mainService;
using YiWangYi.MsgQQ.WinUI.General.GDIControls;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.Model;
using YiWangYi.MsgQQ.WinUI.Service.BaseControls;
using static System.Windows.Forms.ListView;

namespace Team.Zyzy.Im.config
{
    public partial class HomeLeftSetControl : UserControl
    {
        public HomeLeftSetControl()
        {
            InitializeComponent();
            this.Load += HomeLeftBarSetControl_Load;
            qlvHomeLeft.ImageList = this.ilLeftBar;
        }

        private void CmbLeftBar_SelectedIndexChanged(object sender, EventArgs e)
        {
            saveConfig();
        }

        private void HomeLeftBarSetControl_ControlRemoved(object sender, ControlEventArgs e)
        {
            saveConfig();
        }

        private HomePageConfig _homePageConfig = null;

        private void HomeLeftBarSetControl_Load(object sender, EventArgs e)
        {
            _homePageConfig = HomePageConfig.LoadConfig();
            qlvHomeLeft.Items.Clear();
            ilLeftBar.Images.Clear();
            foreach (GButton item in LeftBarSwitchService.FunctionSwitch.Buttons)
            {
                ilLeftBar.Images.Add(item.NormalImage);
                QQListViewItem lvi = new QQListViewItem();
                lvi.Text = item.ToolTip;
                lvi.Name = item.Name;
                lvi.ImageIndex = ilLeftBar.Images.Count - 1;
                lvi.SortKey = item.SortKey;
                qlvHomeLeft.Items.Add(lvi);
                cmbLeftBar.Items.Add(item.ToolTip);
            }
            qlvHomeLeft.Sort();
            chkShowLeftBar.Checked = _homePageConfig.ShowLeftBar;

            this.qlvHomeLeft.SelectedIndexChanged += LvLeftBar_SelectedIndexChanged;
            this.ControlRemoved += HomeLeftBarSetControl_ControlRemoved;
            cmbLeftBar.SelectedIndexChanged += CmbLeftBar_SelectedIndexChanged;
            qlvHomeLeft.ItemChanged += delegate
            {
                saveConfig();
            };
        }

        private void LvLeftBar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (qlvHomeLeft.ListView.SelectedItems.Count > 0)
            {
                for (int i = 0; i < cmbLeftBar.Items.Count; i++)
                {
                    if (cmbLeftBar.Items[i].ToString() == qlvHomeLeft.ListView.SelectedItems[0].Text)
                    {
                        cmbLeftBar.SelectedIndex = i;
                    }
                }
            }
        }

        private void saveConfig()
        {
            _homePageConfig.LeftBars = new List<LeftBar>();
            foreach(ListViewItem itemKey in this.qlvHomeLeft.Items)
            {
                QQListViewItem item = itemKey as QQListViewItem;
                LeftBar lb = new LeftBar();
                lb.Name = item.Name;
                lb.Selected = false;
                if (cmbLeftBar.Text.Trim() == item.Text.Trim())
                {
                    lb.Selected = true;
                }
                lb.ToolTip = item.Text;
                lb.SortKey = item.SortKey;
                _homePageConfig.LeftBars.Add(lb);
            }
            _homePageConfig.LeftBars.Sort();
            _homePageConfig.ShowLeftBar = chkShowLeftBar.Checked;
            _homePageConfig.SaveConfig();
        }

        private void chkShowLeftBar_CheckedChanged(object sender, EventArgs e)
        {
            _homePageConfig.ShowLeftBar = chkShowLeftBar.Checked;
            _homePageConfig.SaveConfig();
        }
    }
}
