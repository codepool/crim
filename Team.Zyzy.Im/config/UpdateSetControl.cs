﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using YiWangYi.AutoUpdater.UpdateHelper;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Interface;

namespace Team.Zyzy.Im.config
{
    public partial class UpdateSetControl : UserControl, IChangeSkin
    {
        private UpdateConfig _softUpdateConfig = null;
        public UpdateSetControl()
        {
            InitializeComponent();
            this.Load += SoftUpdateSetControl_Load;
        }

        public void ChangeSkin()
        {
            this.BackColor = ShareInfo.ControlBorderBackColor;
            this.Invalidate();
        }

        private void SoftUpdateSetControl_Load(object sender, EventArgs e)
        {
            this.ChangeSkin();
            _softUpdateConfig = UpdateConfig.LoadConfig();
            txtSoftName.Text = _softUpdateConfig.SoftName;
            txtSoftVersion.Text = _softUpdateConfig.SoftVersion;
            chkEnableUpdate.Checked = _softUpdateConfig.Enabled;
            chkEnableUpdate.CheckedChanged += delegate {
                _softUpdateConfig.Enabled = chkEnableUpdate.Checked;
                _softUpdateConfig.SaveConfig();
            };
            try
            {
                txtLastUpdateDate.Text = DateTime.Parse(_softUpdateConfig.LastUpdateDate).ToString("yyyy年MM月dd日 HH:mm:ss");
            }
            catch
            {
                txtLastUpdateDate.Text = _softUpdateConfig.LastUpdateDate;
            }
            txtServerUrl.Text = _softUpdateConfig.ServerUrl;
            txtServerUrl.TextChanged += delegate {
                _softUpdateConfig.ServerUrl = txtServerUrl.Text.Trim();
                _softUpdateConfig.SaveConfig();
            };
        }

        private void btnCheckUpdate_Click(object sender, EventArgs e)
        {
            _softUpdateConfig.Enabled = true;
            _softUpdateConfig.SaveConfig();
            updateApplication();
            _softUpdateConfig.Enabled = chkEnableUpdate.Checked;
            _softUpdateConfig.SaveConfig();
        }

        #region 执行更新相关操作

        //更新应用程序
        private static void updateApplication()
        {
            #region check and download new version program

            bool bHasError = false;
            NetAutoUpdater autoUpdater = new NetAutoUpdater();
            if (!autoUpdater.ServerUrlIsExist())//如果更新地址不存在则跳过更新
            {
                return;
            }
            try
            {
                autoUpdater.Update();
            }
            catch (WebException exp)
            {
                FrmAutoCloseMessageBox.Show(exp.Message, "找不到指定资源，可能是服务器地址配置错误！");
                bHasError = true;
            }
            catch (XmlException exp)
            {
                bHasError = true;
                FrmAutoCloseMessageBox.Show(exp.Message, "下载升级文件错误！");
            }
            catch (NotSupportedException exp)
            {
                bHasError = true;
                FrmAutoCloseMessageBox.Show(exp.Message, "升级地址配置错误！");
            }
            catch (ArgumentException exp)
            {
                bHasError = true;
                FrmAutoCloseMessageBox.Show(exp.Message, "下载升级文件错误！");
            }
            catch (Exception exp)
            {
                bHasError = true;
                FrmAutoCloseMessageBox.Show(exp.Message, "一个未知错误发生在升级过程！");
            }
            finally
            {
                if (bHasError == true)
                {
                    try
                    {
                        autoUpdater.RollBack();
                    }
                    catch (Exception)
                    {
                        //Log the message to your file or database
                    }
                }
            }
            #endregion
        }

        #endregion
    }
}
