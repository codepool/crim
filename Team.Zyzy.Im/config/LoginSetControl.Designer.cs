﻿
namespace Team.Zyzy.Im.config
{
    partial class LoginSetControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.chkAutoLogin = new System.Windows.Forms.CheckBox();
            this.txtApiUrl = new System.Windows.Forms.TextBox();
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.txtSocketUrl = new System.Windows.Forms.TextBox();
            this.qqLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.SuspendLayout();
            // 
            // chkAutoLogin
            // 
            this.chkAutoLogin.AutoSize = true;
            this.chkAutoLogin.Location = new System.Drawing.Point(46, 47);
            this.chkAutoLogin.Name = "chkAutoLogin";
            this.chkAutoLogin.Size = new System.Drawing.Size(426, 28);
            this.chkAutoLogin.TabIndex = 5;
            this.chkAutoLogin.Text = "开机自动登录（最后一次登录账号）";
            this.chkAutoLogin.UseVisualStyleBackColor = true;
            // 
            // txtApiUrl
            // 
            this.txtApiUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtApiUrl.Location = new System.Drawing.Point(206, 116);
            this.txtApiUrl.Name = "txtApiUrl";
            this.txtApiUrl.Size = new System.Drawing.Size(747, 35);
            this.txtApiUrl.TabIndex = 7;
            this.txtApiUrl.Text = "https://im.zyzy.team/im_api";
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Location = new System.Drawing.Point(46, 116);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(130, 24);
            this.qqLabel1.TabIndex = 6;
            this.qqLabel1.Text = "登录地址：";
            // 
            // txtSocketUrl
            // 
            this.txtSocketUrl.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSocketUrl.Location = new System.Drawing.Point(206, 180);
            this.txtSocketUrl.Name = "txtSocketUrl";
            this.txtSocketUrl.Size = new System.Drawing.Size(747, 35);
            this.txtSocketUrl.TabIndex = 9;
            this.txtSocketUrl.Text = "wss://im.zyzy.team/ws_api/im";
            // 
            // qqLabel2
            // 
            this.qqLabel2.AutoSize = true;
            this.qqLabel2.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel2.Location = new System.Drawing.Point(46, 180);
            this.qqLabel2.Name = "qqLabel2";
            this.qqLabel2.Size = new System.Drawing.Size(154, 24);
            this.qqLabel2.TabIndex = 8;
            this.qqLabel2.Text = "Socket地址：";
            // 
            // LoginSetControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtSocketUrl);
            this.Controls.Add(this.qqLabel2);
            this.Controls.Add(this.txtApiUrl);
            this.Controls.Add(this.qqLabel1);
            this.Controls.Add(this.chkAutoLogin);
            this.Name = "LoginSetControl";
            this.Size = new System.Drawing.Size(984, 707);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkAutoLogin;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private System.Windows.Forms.TextBox txtApiUrl;
        private System.Windows.Forms.TextBox txtSocketUrl;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel2;
    }
}
