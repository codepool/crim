﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.dto;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Interface;

namespace Team.Zyzy.Im.config
{
    public partial class ModifyPasswordControl : UserControl, IChangeSkin
    {
        public ModifyPasswordControl()
        {
            InitializeComponent();
            this.Load += LoginSetControl_Load;
        }

        private void LoginSetControl_Load(object sender, EventArgs e)
        {

            ChangeSkin();
        }

        public void ChangeSkin()
        {
            this.BackColor = ShareInfo.ControlBorderBackColor;
            this.Invalidate();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtNewPassword.Text.Trim().Length < 6)
            {
                FrmAutoCloseMessageBox.Show("密码长度不能小于6位数！", MessageBoxIcon.Information);
                txtNewPassword.Focus();
                return;
            }
            if (txtNewPassword.Text.Trim() != txtPasswordAccept.Text.Trim())
            {
                FrmAutoCloseMessageBox.Show("两次输入密码不一致，请重新输入！", MessageBoxIcon.Information);
                txtNewPassword.Focus();
                return;
            }
            ModifyPwdDTO modifyPwd = new ModifyPwdDTO();
            modifyPwd.oldPassword = txtOldPassword.Text.Trim();
            modifyPwd.newPassword = txtNewPassword.Text.Trim();
            string url = "/modifyPwd";
            HttpHelp.HttpApi<ModifyPwdDTO>(url, modifyPwd, "PUT");
            FrmLoginRetry.LoginUser.password = modifyPwd.newPassword;
            FrmAutoCloseMessageBox.Show("修改密码成功，请重新登录！", MessageBoxIcon.Information);
        }
    }
}