﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.config;
using Team.Zyzy.Im.Business.dto;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.Business.vo;
using YiWangYi.MsgQQ.Common;
using YiWangYi.MsgQQ.WinUI.BaseControls;
using YiWangYi.MsgQQ.WinUI.Config;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.DBUtility;

namespace Team.Zyzy.Im
{
    public partial class FrmLogin : FormQQLogin
    {
        private System.Windows.Forms.Timer timer1;
        private IContainer components;
        private LinkLabel lilRegister;
        private CheckBox chkAutoStart;
        private CheckBox chkSavePassword;
        private LProgressBar proLogin;

        public FrmLogin()
        {
            InitializeComponent();
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            proLogin.Visible = false;
            setLotion();
            this.ShowIcon = true;
            this.ShowInTaskbar = true;
            this.Text = "CRIM2023";
            //this.UserID = "admin";
            //this.UserPassword = "123456";
            this.MinimumSizeChanged += delegate { this.ShowInTaskbar = true; };
            this.chkAutoStart.CheckedChanged += saveLoginInfo;  
            this.chkSavePassword.CheckedChanged += saveLoginInfo;
            this.Load += FrmLogin_Load;
            lilRegister.Click += LilRegister_Click;
        }

        private void LilRegister_Click(object sender, EventArgs e)
        {
            FrmRegister frmRegister = new FrmRegister();
            if (frmRegister.ShowDialog() == DialogResult.OK)
            {
                this.UserID = frmRegister.RegisteredUser.userName;
                this.UserPassword = frmRegister.RegisteredUser.password;
                loginInfo.SetLoginClick();
            }
        }

        private LoginInfoConfig _loginInfoConfig = null;
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            _loginInfoConfig = LoginInfoConfig.LoadConfig();
            setLoginUrl();
            if (_loginInfoConfig.LoginInfoData.SavePassword)
            {
                this.UserID = _loginInfoConfig.LoginInfoData.UserName;
                this.UserPassword = AesHelper.Decode( _loginInfoConfig.LoginInfoData.Password);
                chkAutoStart.Checked = _loginInfoConfig.LoginInfoData.AutoStart;
                chkSavePassword.Checked = _loginInfoConfig.LoginInfoData.SavePassword;
            }
            if (_loginInfoConfig.LoginInfoData.AutoLogin)
            {
                loginInfo.SetLoginClick();
            }
            loginInfo.BackColor = Color.FromArgb(39, 146, 236);
            qqFrmRound.BackColor = Color.FromArgb(39, 146, 236);
        }

        private void saveLoginInfo(object sender, EventArgs e)
        {
            if (chkAutoStart.Checked)
            {
                chkSavePassword.Checked = true;
            }
            _loginInfoConfig.LoginInfoData.AutoStart = chkAutoStart.Checked;
            _loginInfoConfig.LoginInfoData.SavePassword = chkSavePassword.Checked;
            _loginInfoConfig.SaveConfig();
            setStartup(chkAutoStart.Checked);
        }

        private void setLoginUrl()
        {
            if (_loginInfoConfig.LoginInfoData.BaseApi.Length > 5)
            {
                RequestApiInfo.BaseApi = _loginInfoConfig.LoginInfoData.BaseApi;
            }
            if (_loginInfoConfig.LoginInfoData.SockApi.Length > 5)
            {
                RequestApiInfo.WsURL = _loginInfoConfig.LoginInfoData.SockApi;
            }
        }
        private void setStartup(bool autoStart)
        {
            string SoftWare = "CRIM";
            if (autoStart)
            {
                string path = Application.ExecutablePath;
                RegistryKey rk = Registry.CurrentUser;
                try
                {
                    RegistryKey rk2 = rk.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run");
                    string old_path = (string)rk2.GetValue(SoftWare);
                    if (old_path == null || !path.Equals(old_path))
                    {
                        rk2.SetValue(SoftWare, path);
                    }
                    rk2.Close();
                    rk.Close();
                }
                catch (Exception ex)
                {
                    FrmAutoCloseMessageBox.Show("开机自启动失败");
                }
            }
            else
            {
                string path = Application.ExecutablePath;
                RegistryKey rk = Registry.CurrentUser;
                try
                {
                    RegistryKey rk2 = rk.CreateSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run");
                    string old_path = (string)rk2.GetValue(SoftWare);
                    rk2.DeleteValue(SoftWare, false);
                    rk2.Close();
                    rk.Close();
                }
                catch (Exception ex)
                {
                    FrmAutoCloseMessageBox.Show("取消开机自启动失败");
                }
            }
        }

        private void setLotion()
        {
            Size size = new Size(380, 260);
            //Size size = new Size(300,700);
            this.loginInfo.Top = 80;
            this.ClientSize = size;
            this.MaximumSize = size;
            this.MinimumSize = size;

            chkSavePassword.Left = 42;
            chkAutoStart.Left = (this.Width - chkAutoStart.Width) / 2+11;
            lilRegister.Left = (this.Width - lilRegister.Width - 42);

        }

        protected override void CancelDeal()
        {
            proLogin.Visible = false;
        }

        protected override void AsynchDeal()
        {
            try
            {
                Action action1 = () =>
                {
                    proLogin.Visible = true;
                };
                Invoke(action1);

                String userName = this.TxtUserID.Text;
                String userPassword = this.TxtUserPassword.Text;

                string url = RequestApiInfo.BaseApi + "/login";
                LoginDTO user = new LoginDTO();
                user.userName = userName;
                user.password = userPassword;
                FrmLoginRetry.LoginUser = user;
                Result<LoginVO> result = HttpHelp.HttpApi<LoginVO>(url, user, "POST", null);
                if (result.code != (int)ResultCode.SUCCESS)
                {
                    Action action = () =>
                    {
                        proLogin.Visible = false;
                        this.IsLogin = false;
                        if (FrmAutoCloseMessageBox.Show( "信息提示", result.message, MessageBoxButtons.OK) != DialogResult.Cancel)
                        {
                            this.ChangeBkBtnLogin();
                            this.IsLogin = true;
                        }
                    };
                    Invoke(action);
                }
                else
                {
                    _loginInfoConfig.LoginInfoData.UserName = this.UserID;
                    _loginInfoConfig.LoginInfoData.Password = AesHelper.Encode(this.UserPassword); ;
                    _loginInfoConfig.SaveConfig();

                    RequestApiInfo.AccessToken = result.data.accessToken;
                    RequestApiInfo.AccessTokenExpiresIn = result.data.accessTokenExpiresIn;
                    RequestApiInfo.RefreshToken = result.data.refreshToken;
                    RequestApiInfo.RefreshTokenExpiresIn = result.data.refreshTokenExpiresIn;
                    Action action = () =>
                    {
                        PublicQQSkinConstant.SkinPath = PublicConstant.GetSkinConfig(user.userName);
                        HomePageConfig.FilePath = PublicConstant.GetMainToolBarConfig(user.userName);
                        VoiceInfoConfig.FilePath = PublicConstant.GetUserVoiceConfig(user.userName);
                        DbHelperSQLite.connectionString= PublicConstant.GetUserDBConnectrion(user.userName);
                        OnlineStateConfig.FilePath = PublicConstant.GetUserOnlineStatusConfig(user.userName);
                        this.SetLoginSuccess(user);
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    };
                    Invoke(action);
                }
            }
            catch(ThreadAbortException tae)
            {

            }
            catch(Exception ex)
            {
                FrmAutoCloseMessageBox.Show("登录异常：" + ex.Message,MessageBoxIcon.Error);
            }
        }

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.proLogin = new YiWangYi.MsgQQ.WinUI.BaseControls.LProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.chkSavePassword = new System.Windows.Forms.CheckBox();
            this.chkAutoStart = new System.Windows.Forms.CheckBox();
            this.lilRegister = new System.Windows.Forms.LinkLabel();
            this.loginInfo.SuspendLayout();
            this.qqFrmRound.SuspendLayout();
            this.SuspendLayout();
            // 
            // loginInfo
            // 
            this.loginInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(161)))), ((int)(((byte)(248)))));
            this.loginInfo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("loginInfo.BackgroundImage")));
            this.loginInfo.Controls.Add(this.lilRegister);
            this.loginInfo.Controls.Add(this.chkAutoStart);
            this.loginInfo.Controls.Add(this.chkSavePassword);
            this.loginInfo.Controls.Add(this.proLogin);
            // 
            // qqFrmRound
            // 
            this.qqFrmRound.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqFrmRound.BackgroundImage")));
            // 
            // proLogin
            // 
            this.proLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(163)))), ((int)(((byte)(231)))));
            this.proLogin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.proLogin.ForeColor = System.Drawing.Color.Transparent;
            this.proLogin.L_ArcSize = 4;
            this.proLogin.L_BlockSize = 15;
            this.proLogin.L_DrawMargin = 4;
            this.proLogin.L_MarqueeStyle = YiWangYi.MsgQQ.WinUI.BaseControls.MarqueeStyle.Swing;
            this.proLogin.L_MarqueeTime = 1000;
            this.proLogin.L_MarqueeType = YiWangYi.MsgQQ.WinUI.BaseControls.MarqueeType.SlowInSlowOut;
            this.proLogin.L_ProgressMode = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressMode.Known;
            this.proLogin.L_ProgressStyle = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressStyle.Continuous;
            this.proLogin.L_ProgressText = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressText.None;
            this.proLogin.L_ProgressType = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressType.Bar;
            this.proLogin.L_ShrinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(161)))), ((int)(((byte)(248)))));
            this.proLogin.L_ShrinkSize = 0;
            this.proLogin.L_SliderColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(55)))), ((int)(((byte)(36)))));
            this.proLogin.L_SurroundLineColor = System.Drawing.Color.Empty;
            this.proLogin.L_SurroundLineSize = 0;
            this.proLogin.L_TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.proLogin.L_Value = 0;
            this.proLogin.Location = new System.Drawing.Point(0, 230);
            this.proLogin.Margin = new System.Windows.Forms.Padding(40, 3, 40, 3);
            this.proLogin.Name = "proLogin";
            this.proLogin.Size = new System.Drawing.Size(630, 138);
            this.proLogin.TabIndex = 2;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // chkSavePassword
            // 
            this.chkSavePassword.AutoSize = true;
            this.chkSavePassword.Location = new System.Drawing.Point(42, 277);
            this.chkSavePassword.Name = "chkSavePassword";
            this.chkSavePassword.Size = new System.Drawing.Size(138, 28);
            this.chkSavePassword.TabIndex = 3;
            this.chkSavePassword.Text = "记住密码";
            this.chkSavePassword.UseVisualStyleBackColor = true;
            // 
            // chkAutoStart
            // 
            this.chkAutoStart.AutoSize = true;
            this.chkAutoStart.Location = new System.Drawing.Point(280, 277);
            this.chkAutoStart.Name = "chkAutoStart";
            this.chkAutoStart.Size = new System.Drawing.Size(138, 28);
            this.chkAutoStart.TabIndex = 4;
            this.chkAutoStart.Text = "开机启动";
            this.chkAutoStart.UseVisualStyleBackColor = true;
            // 
            // lilRegister
            // 
            this.lilRegister.AutoSize = true;
            this.lilRegister.Location = new System.Drawing.Point(494, 277);
            this.lilRegister.Name = "lilRegister";
            this.lilRegister.Size = new System.Drawing.Size(106, 24);
            this.lilRegister.TabIndex = 5;
            this.lilRegister.TabStop = true;
            this.lilRegister.Text = "注册账号";
            // 
            // FrmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.ClientSize = new System.Drawing.Size(640, 446);
            this.Name = "FrmLogin";
            this.loginInfo.ResumeLayout(false);
            this.loginInfo.PerformLayout();
            this.qqFrmRound.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (proLogin.L_Value == 100)
            {
                proLogin.L_Value = 0;
            }
            else
            {
                proLogin.L_Value = proLogin.L_Value + 1;
            }
        }
    }
}