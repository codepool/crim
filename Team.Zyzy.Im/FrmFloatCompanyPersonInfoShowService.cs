﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Team.Zyzy.Im;
using System.Windows.Forms;

namespace YiWangYi.MsgQQ.WinUI.Service.CalendarService
{
    public class FrmFloatCompanyPersonInfoShowService
    {
        private static FrmFloatCompanyPersonInfo _frmCalendar;

        public static void CompanyList_OnMoveHidenPerson(object sender, EventArgs e)
        {
            _frmCalendar = FrmFloatCompanyPersonInfo.GetInstance();
            _frmCalendar.Hide();
        }

        public static void CompanyList_OnMoveShowPerson(TreeNode treeNode, MouseEventArgs e)
        {
            _frmCalendar = FrmFloatCompanyPersonInfo.GetInstance();
            _frmCalendar.ResetShow();
            _frmCalendar.UserName = treeNode.Text;
            int x = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Size.Width;
            Form frm = ShareInfo.FormQQMain as Form;
            if (frm.Left + frm.Width + _frmCalendar.Width<= x)
            {
                x = frm.Left + frm.Width-8;
            }
            else
            {
                x = frm.Left- _frmCalendar.Width+8;
            }
            _frmCalendar.Location = new Point(x, e.Y+_frmCalendar.Height);
            _frmCalendar.Show();
        }
    }
}
