﻿
namespace Team.Zyzy.Im
{
    partial class FrmLoginRetry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLoginRetry));
            this.qqLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.proLogin = new YiWangYi.MsgQQ.WinUI.BaseControls.LProgressBar();
            this.btnRetry = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnClose = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.qqPanelFixSize1.SuspendLayout();
            this.qqPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.Controls.Add(this.qqPanel1);
            this.qqPanelFixSize1.Margin = new System.Windows.Forms.Padding(2);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(247, 169);
            this.qqPanelFixSize1.Text = "登录提示！";
            // 
            // qqLabel1
            // 
            this.qqLabel1.AutoSize = true;
            this.qqLabel1.BackColor = System.Drawing.Color.Transparent;
            this.qqLabel1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.qqLabel1.Location = new System.Drawing.Point(4, 25);
            this.qqLabel1.Name = "qqLabel1";
            this.qqLabel1.Size = new System.Drawing.Size(232, 16);
            this.qqLabel1.TabIndex = 0;
            this.qqLabel1.Text = "网络异常，重新登录中......";
            // 
            // qqPanel1
            // 
            this.qqPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qqPanel1.Controls.Add(this.proLogin);
            this.qqPanel1.Controls.Add(this.btnRetry);
            this.qqPanel1.Controls.Add(this.btnClose);
            this.qqPanel1.Controls.Add(this.qqLabel1);
            this.qqPanel1.Location = new System.Drawing.Point(3, 26);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(241, 140);
            this.qqPanel1.TabIndex = 1;
            // 
            // proLogin
            // 
            this.proLogin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(163)))), ((int)(((byte)(231)))));
            this.proLogin.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.proLogin.ForeColor = System.Drawing.Color.Transparent;
            this.proLogin.L_ArcSize = 4;
            this.proLogin.L_BlockSize = 15;
            this.proLogin.L_DrawMargin = 4;
            this.proLogin.L_MarqueeStyle = YiWangYi.MsgQQ.WinUI.BaseControls.MarqueeStyle.Swing;
            this.proLogin.L_MarqueeTime = 1000;
            this.proLogin.L_MarqueeType = YiWangYi.MsgQQ.WinUI.BaseControls.MarqueeType.SlowInSlowOut;
            this.proLogin.L_ProgressMode = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressMode.Known;
            this.proLogin.L_ProgressStyle = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressStyle.Continuous;
            this.proLogin.L_ProgressText = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressText.None;
            this.proLogin.L_ProgressType = YiWangYi.MsgQQ.WinUI.BaseControls.ProgressType.Bar;
            this.proLogin.L_ShrinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(161)))), ((int)(((byte)(248)))));
            this.proLogin.L_ShrinkSize = 0;
            this.proLogin.L_SliderColor = System.Drawing.Color.FromArgb(((int)(((byte)(179)))), ((int)(((byte)(55)))), ((int)(((byte)(36)))));
            this.proLogin.L_SurroundLineColor = System.Drawing.Color.Empty;
            this.proLogin.L_SurroundLineSize = 0;
            this.proLogin.L_TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.proLogin.L_Value = 0;
            this.proLogin.Location = new System.Drawing.Point(0, 123);
            this.proLogin.Margin = new System.Windows.Forms.Padding(20, 2, 20, 2);
            this.proLogin.Name = "proLogin";
            this.proLogin.Size = new System.Drawing.Size(241, 17);
            this.proLogin.TabIndex = 3;
            // 
            // btnRetry
            // 
            this.btnRetry.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRetry.BackgroundImage")));
            this.btnRetry.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRetry.Caption = "重试";
            this.btnRetry.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnRetry.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnRetry.Location = new System.Drawing.Point(34, 79);
            this.btnRetry.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnRetry.MouseDownImage")));
            this.btnRetry.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnRetry.MouseMoveImage")));
            this.btnRetry.Name = "btnRetry";
            this.btnRetry.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnRetry.NormalImage")));
            this.btnRetry.Size = new System.Drawing.Size(78, 35);
            this.btnRetry.TabIndex = 2;
            this.btnRetry.ToolTip = null;
            this.btnRetry.Click += new System.EventHandler(this.btnRetry_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Caption = "关闭";
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnClose.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnClose.Location = new System.Drawing.Point(135, 79);
            this.btnClose.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseDownImage")));
            this.btnClose.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseMoveImage")));
            this.btnClose.Name = "btnClose";
            this.btnClose.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnClose.NormalImage")));
            this.btnClose.Size = new System.Drawing.Size(78, 35);
            this.btnClose.TabIndex = 1;
            this.btnClose.ToolTip = null;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmLoginRetry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 179);
            this.Margin = new System.Windows.Forms.Padding(3);
            this.Name = "FrmLoginRetry";
            this.Text = "登录提示！";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.qqPanel1.ResumeLayout(false);
            this.qqPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnClose;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel qqLabel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnRetry;
        private YiWangYi.MsgQQ.WinUI.BaseControls.LProgressBar proLogin;
        private System.Windows.Forms.Timer timer1;
    }
}