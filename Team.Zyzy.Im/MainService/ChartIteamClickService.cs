﻿using System;
using System.Collections.Generic;
using System.Text;
using YiWangYi.MsgQQ.WinUI.QQListBar;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.Forms;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.Business.Model;

namespace Team.Zyzy.Im.mainService
{
    public class ChartIteamClickService
    {

        private static object _lock = new object();

        public static void FriendsList_ItemDoubleClick(CbItemEventArgs e)
        {
            lock (_lock)
            {
                FriendVOItem item = e.Item as FriendVOItem;
                FrmPersonChart lanPersonChart = FrmPersonChart.GetInstance(item.ID);
                lanPersonChart.FriendInfo = item;
                if (lanPersonChart.Visible)
                {
                    lanPersonChart.Activate();
                }
                else
                {
                    lanPersonChart.LoadOverShow();
                }
            }
        }

        public static void GroupList_ItemDoubleClick(CbItemEventArgs e)
        {
            lock (_lock)
            {
                GroupVOItem item = e.Item as GroupVOItem;
                FrmGroupChart lanGroupChart = FrmGroupChart.GetInstance(item.ID);
                lanGroupChart.GroupInfo = item;
                if (lanGroupChart.Visible)
                {
                    lanGroupChart.Activate();
                }
                else
                {
                    lanGroupChart.LoadOverShow();
                }
            }
        }

        public static void HistoryList_ItemDoubleClick(CbItemEventArgs e)
        {
            lock (_lock)
            {
                if (e.Item.TagObject is ImHistoryMessage)
                {
                    ImHistoryMessage message = e.Item.TagObject as ImHistoryMessage;
                    if (message.source_type == 0)
                    {
                        long friendId = message.opposite_id;
                        FrmPersonChart lanPersonChart = FrmPersonChart.GetInstance(friendId);
                        if (lanPersonChart.Visible)
                        {
                            lanPersonChart.Activate();
                        }
                        else
                        {
                            FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
                            FriendVOItem friendVOItem= frmMain.findFriend(friendId);
                            if (friendVOItem != null)
                            {
                                lanPersonChart.FriendInfo = friendVOItem;
                                lanPersonChart.LoadOverShow();
                            }
                            else
                            {
                                FrmAutoCloseMessageBox.Show("对方不是好友，不能发送信息！");
                                lanPersonChart.Close();
                            }
                        }
                    }
                    else if (message.source_type == 1)
                    {
                        long groupId = message.opposite_id;
                        FrmGroupChart lanGroupChart = FrmGroupChart.GetInstance(groupId);
                        if (lanGroupChart.Visible)
                        {
                            lanGroupChart.Activate();
                        }
                        else
                        {
                            FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
                            GroupVOItem friendVOItem = frmMain.findGroup(groupId);
                            if (friendVOItem != null)
                            {
                                lanGroupChart.GroupInfo = friendVOItem;
                                lanGroupChart.LoadOverShow();
                            }
                            else
                            {
                                FrmAutoCloseMessageBox.Show("没有加入群组，不能发送信息！");
                                lanGroupChart.Close();
                            }
                        }
                    }

                }

            }
        }
    }
}