﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.ContractPanel;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;

namespace Team.Zyzy.Im.mainService
{
    public class GroupChartMemberRightMenu
    {
        public GroupChartMemberRightMenu(QQListBar clb,GroupVO group)
        {
            InitializeComponent();
            this.ContractListBar = clb;
            this.GroupVO = group;
            this.ContractListBar.ContextMenuStrip = cmsChartBars;
        }

        private QQListBar ContractListBar;
        private GroupVO GroupVO;

        private System.Windows.Forms.ContextMenuStrip cmsChartBars;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupGroupMember;
        private System.Windows.Forms.ToolStripMenuItem tsmiInviteGroupMember;
        private System.Windows.Forms.ToolStripMenuItem tsmiKickGroupMember;
        private System.Windows.Forms.ToolStripMenuItem tsmiRefreshGroupMember;
        private void InitializeComponent()
        {
            this.cmsChartBars = new System.Windows.Forms.ContextMenuStrip();
            this.tsmiLookupGroupMember = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInviteGroupMember = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiKickGroupMember = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRefreshGroupMember = new System.Windows.Forms.ToolStripMenuItem();
            // 
            // cmsChartBars
            // 
            this.cmsChartBars.ImageScalingSize = new System.Drawing.Size(16, 16);
            this.cmsChartBars.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRefreshGroupMember,
            this.tsmiInviteGroupMember,
            this.tsmiLookupGroupMember,
            this.tsmiKickGroupMember});
            this.cmsChartBars.Name = "cmsChartBars";
            this.cmsChartBars.Opening += CmsChartBars_Opening;

            // 
            // tsmiRefreshGroupMember
            // 
            this.tsmiRefreshGroupMember.Image = global::Team.Zyzy.Im.Properties.Resources.refreshGroup;
            this.tsmiRefreshGroupMember.Name = "tsmiRefreshGroupMember";
            this.tsmiRefreshGroupMember.Text = "刷新群聊";
            this.tsmiRefreshGroupMember.Click += new System.EventHandler(this.tsmiRefreshGroupMember_Click);
            //  
            // tsmiKickGroupMember
            // 
            this.tsmiKickGroupMember.Image = global::Team.Zyzy.Im.Properties.Resources.deleteGroup;
            this.tsmiKickGroupMember.Name = "tsmiKickGroupMember";
            this.tsmiKickGroupMember.Text = "踢出群聊";
            this.tsmiKickGroupMember.Click += new System.EventHandler(this.tsmiKickGroupMember_Click);
            // 
            // tsmiInviteGroup
            // 
            this.tsmiInviteGroupMember.Image = global::Team.Zyzy.Im.Properties.Resources.inviteGroup;
            this.tsmiInviteGroupMember.Name = "tsmiInviteGroup";
            this.tsmiInviteGroupMember.Text = "邀请好友加入群聊";
            this.tsmiInviteGroupMember.Click += new System.EventHandler(this.tsmiInviteGroupMember_Click);
            //  
            // tsmiModifyGroup
            // 
            this.tsmiLookupGroupMember.Image = global::Team.Zyzy.Im.Properties.Resources.modifyGroup;
            this.tsmiLookupGroupMember.Name = "tsmiLookupGroupMember";
            this.tsmiLookupGroupMember.Text = "查看信息";
            this.tsmiLookupGroupMember.Click += new System.EventHandler(this.tsmiLookupGroupMember_Click);

        }

        private void CmsChartBars_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }
        private void ContractListBar_SelcetPagChanged(YiWangYi.MsgQQ.WinUI.QQTab.QTabPagEventArgs e)
        {
        }

        #region 群组管理
        private void tsmiCreateGroup_Click(object sender, EventArgs e)
        {
            FrmCreateGroup frmCreateGroup = new FrmCreateGroup();
            frmCreateGroup.Show();
        }
        private void tsmiLookupGroupMember_Click(object sender, EventArgs e)
        {
            GroupMemberVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupMemberVOItem;
            if (groupVOItem == null)
            {
                FrmAutoCloseMessageBox.Show("请先选择群组！");
                return;
            }
            FrmModifyFriendInfo friendInfo = new FrmModifyFriendInfo();
            friendInfo.UserId = groupVOItem.GroupMemberVO.userId;
            friendInfo.Show();
        }
        private void tsmiInviteGroupMember_Click(object sender, EventArgs e)
        {
            FrmCreateGroup frmCreateGroup = new FrmCreateGroup();
            frmCreateGroup.IsInvite = true;
            frmCreateGroup.GroupVO = this.GroupVO;
            frmCreateGroup.ShowDialog();
            InitGroupMember(this.GroupVO, this.ContractListBar);
        }

        private void tsmiKickGroupMember_Click(object sender, EventArgs e)
        {
            GroupMemberVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupMemberVOItem;
            if (groupVOItem == null)
            {
                FrmAutoCloseMessageBox.Show("请先选择要解散的群组！");
                return;
            }

            if (this.GroupVO.ownerId != RequestApiInfo.SendId)
            {
                FrmAutoCloseMessageBox.Show("你不是群主，不能踢出群成员！");
                return;
            }

            FrmAutoCloseMessageBox frmDialog = new FrmAutoCloseMessageBox();
            frmDialog.StartPosition = FormStartPosition.CenterScreen;
            FrmAutoCloseMessageBox.Show(frmDialog, "踢出群成员确认", "确定踢出【" + groupVOItem.DisplayName + "】吗？", MessageBoxButtons.YesNo);
            frmDialog.OnDialogResultChanged += delegate (DialogResult result)
            {
                if (result == DialogResult.Yes)
                {
                    string url = "/group/kick/" + this.GroupVO.id+ "?userId="+ groupVOItem.GroupMemberVO.userId;
                    HttpHelp.HttpApi<GroupVO>(url, null, "DELETE");
                    LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
                    InitGroupMember(this.GroupVO, this.ContractListBar);
                }
            };
        }
        private void tsmiRefreshGroupMember_Click(object sender, EventArgs e)
        {
            InitGroupMember(this.GroupVO, this.ContractListBar);
        }

        #endregion

        #region 初始化群组成员

        public List<GroupMemberVO> InitGroupMember(GroupVO group, QQListBar qlvGroupMember)
        {
            qlvGroupMember.Groups.Clear();
            qlvGroupMember.View = CbViewStyle.SmallIcon;
            List<GroupMemberVO>  groupMembers = getMembers(group.id);
            ClassificationT<GroupMemberVOItem> fg1 = new ClassificationT<GroupMemberVOItem>(string.Format("群成员"));
            fg1.IsExpand = false;
            fg1.GroupTextIsShow = false;
            qlvGroupMember.Groups.Add(fg1);
            foreach (GroupMemberVO info in groupMembers)
            {
                if (!info.quit)
                {
                    GroupMemberVOItem item = new GroupMemberVOItem(info);
                    if (info.userId == group.ownerId)
                    {
                        item.IsManager = true;
                    }
                    fg1.Add(item);
                }
            }
            fg1.IsExpand = true;
            fg1.GroupTextIsShow = true;
            return groupMembers;
        }

        public List<GroupMemberVO> getMembers(long groupId)
        {
            try
            {
                string url = "/group/members/" + groupId;
                List<GroupMemberVO> groupMembers = HttpHelp.HttpApi<List<GroupMemberVO>>(url, null, "GET");
                return groupMembers;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        #endregion

    }
}