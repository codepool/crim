﻿using System;
using System.Collections.Generic;
using System.Text;
using YiWangYi.MsgQQ.WinUI.General.FunctionSwitch;
using YiWangYi.MsgQQ.WinUI.Model;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Service.Properties;
using YiWangYi.MsgQQ.WinUI.General.InforTips;
using YiWangYi.MsgQQ.WinUI.Service.MainService;

namespace Team.Zyzy.Im.mainService
{
    public class BottomTipsBarService : InforTipsService
    {
        private static BottomTipsBarService  instance;
        private static object _lock = new object();

        protected BottomTipsBarService ()
        {
            if (InforTips == null)
            {
                return;
            }
            base.init();
        }

        public static  BottomTipsBarService  GetInstance(InforTips ift, FrmMain frmMain)
        {
            Form = frmMain;
            if (ift != InforTips)
            {
                InforTips = ift;
                ift.Click += new EventHandler(ift_Click);
            }
            InforTips = ift;
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new BottomTipsBarService ();
                    }
                }
            }
            return instance;
        }

        private static void ift_Click(object sender, EventArgs e)
        {
            string selectedName = sender.ToString().Trim();
            switch (selectedName)
            {
                case "systemSetting":
                    FrmSystemService frmSysSetting = FrmSystemService.Instance();
                    frmSysSetting.Show();
                    break;
                case "msgManage":
                    FrmHistoryMessage frmMessage = new FrmHistoryMessage();
                    frmMessage.Show();
                    break;
                case "search":
                    FrmAddFriendAndGroup addFriend = new FrmAddFriendAndGroup();
                    addFriend.Show();
                    break;
                default :
                    break;
            }
           
        }


    }
}
