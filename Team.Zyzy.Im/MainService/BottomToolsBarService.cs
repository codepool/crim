﻿using System;
using System.Collections.Generic;
using System.Text;
using YiWangYi.MsgQQ.WinUI.General.FunctionSwitch;
using YiWangYi.MsgQQ.WinUI.Model;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Service.Properties;
using YiWangYi.MsgQQ.WinUI.General.CommonOper;
using YiWangYi.MsgQQ.WinUI.Service.MainService;
using Team.Zyzy.Im.MainService;
using Team.Zyzy.Im.MainChar;

namespace Team.Zyzy.Im.mainService
{
    public class BottomToolsBarService: CommonOperService
    {
        private static BottomToolsBarService instance;
        private static object _lock = new object();

        protected BottomToolsBarService()
        {
            if (CommonOper == null)
            {
                return;
            }
            base.init();
        }

        protected override void init()
        {
            CommonOperModel presidentMailbox = new CommonOperModel();
            presidentMailbox.Index = 1;
            presidentMailbox.MouseMoveImage = MainServiceResource.presidentMailbox1;
            presidentMailbox.MouseSelectedImage = MainServiceResource.presidentMailbox2;
            presidentMailbox.NormalImage = MainServiceResource.presidentMailbox;
            presidentMailbox.Name = "presidentMailbox";
            presidentMailbox.Text = null;
            presidentMailbox.TextAlign = ButtonTextAlign.Right;
            presidentMailbox.ToolTip = "总裁信箱";
            presidentMailbox.Width = 20;
            CommonOper.Add(presidentMailbox);
            CommonOperModel repairReport = new CommonOperModel();
            repairReport.Index = 1;
            repairReport.MouseMoveImage = MainServiceResource.repair1;
            repairReport.MouseSelectedImage = MainServiceResource.repair2;
            repairReport.NormalImage = MainServiceResource.repair;
            repairReport.Name = "repairReport";
            repairReport.Text = null;
            repairReport.TextAlign = ButtonTextAlign.Right;
            repairReport.ToolTip = "维修报告";
            repairReport.Width = 20;
            CommonOper.Add(repairReport);
        }


        public static  BottomToolsBarService GetInstance(CommonOper ift, FrmMain frmMain)
        {
            Form = frmMain;
            if (ift != CommonOper)
            {
                CommonOper = ift;
                ift.Click += new EventHandler(ift_Click);
            }
            CommonOper = ift;
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new BottomToolsBarService();
                    }
                }
            }
            return instance;
        }

        private static void ift_Click(object sender, EventArgs e)
        {
            string selectedName = sender.ToString().Trim();
            switch (selectedName)
            {
                case "presidentMailbox":
                    string url1 = string.Format("https://www.bing.com/search?q={0}", "www.zyzy.team");
                    //System.Diagnostics.Process.Start(url1);
                    FrmGroupRTCVideo frmGroupRTCVideo = new FrmGroupRTCVideo("https://www.baidu.com");
                    frmGroupRTCVideo.Show();
                    break;
                case "repairReport":
                    string url2 = string.Format("https://mail.qq.com/");
                    System.Diagnostics.Process.Start(url2);
                    break;
                case "search":
                    FrmAddFriendAndGroup addFriend = new FrmAddFriendAndGroup();
                    addFriend.Show();
                    break;
                default :
                    break;
            }
           
        }


    }
}
