﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.BLL;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Model.Chart;
using YiWangYi.MsgQQ.WinUI.QQListBar;
using YiWangYi.MsgQQ.WinUI.Service.CalendarService;
using YiWangYi.MsgQQ.WinUI.Tree;

namespace Team.Zyzy.Im.mainService
{
    public class LoadChartBarsService
    {
        private static LoadChartBarsService instance=null;
        private static object _lock = new object();


        private LoadChartBarsService()
        {
        }

        public static LoadChartBarsService GetInstance()
        {
            if (instance == null)
            {
                lock (_lock)
                {
                    instance = new LoadChartBarsService();
                    instance.InitClassify();
                }
            }
            return instance;
        }

        #region 初始化归类
        private List<ClassifyVO> _classifyVOs = null;

        public List<ClassifyVO> ClassifyVOs
        {
            get
            {
                return _classifyVOs;
            }
            set
            {
                _classifyVOs = value;
            }
        }

        public void InitClassify()
        {
            string url = "/classify/list";
            try
            {
                _classifyVOs = HttpHelp.HttpApi<List<ClassifyVO>>(url, null, "GET");
            }
            catch
            {
                _classifyVOs = null;
            }
            if (_classifyVOs == null)
            {
                _classifyVOs = new List<ClassifyVO>();
            }
            _classifyVOs.Sort();
        }

        public bool ContainClassifyName(ClassifyVO newItem)
        {
            foreach(ClassifyVO item in _classifyVOs)
            {
                if(newItem.id !=item.id && newItem.type==item.type && newItem.classifyName == item.classifyName)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region 初始化好友列表

        public void InitChartFriends(CbGroupCollection cbGroups)
        {
            try
            {
                cbGroups.Clear();

                ClassificationT<FriendVOItem> defaultGroup = new ClassificationT<FriendVOItem>("我的好友");
                cbGroups.Add(defaultGroup);
                foreach (ClassifyVO classify in _classifyVOs)
                {
                    if (classify.type == (int)MessageSourceType.Friend)
                    {
                        ClassificationT<FriendVOItem> fg = new ClassificationT<FriendVOItem>(classify.classifyName);
                        fg.ID = classify.id;
                        fg.Tag = classify;
                        cbGroups.Add(fg);
                    }
                }
                LoadFriend(cbGroups, defaultGroup);
                //拉取未读消息
                Task.Run(() => loadPrivateMessage());
            }
            catch (Exception m)
            {
                FrmAutoCloseMessageBox.Show(m.Message.ToString(), MessageBoxIcon.Error);
            }
            finally
            {
            }
        }

        public void LoadFriend(CbGroupCollection cbGroups, ClassificationT<FriendVOItem> defaultGroup)
        {
            string url = "/friend/list";
            List<FriendVO> friends = HttpHelp.HttpApi<List<FriendVO>>(url, null, "GET");
            List<long> userIds = new List<long>();
            foreach (FriendVO info in friends)
            {
                FriendVOItem item = new FriendVOItem(info);
                item.OnTerminalTypeChanged += updateFriendHistoryCountAndState;
                item.OnCurrentStateChanged += updateFriendHistoryCountAndState;
                item.OnMessageCountChanged += updateFriendHistoryCountAndState;
                bool isAdd = false;
                foreach (ClassificationT<FriendVOItem> group in cbGroups)
                {
                    if (group.ID == info.classifyId)
                    {
                        group.Add(item);
                        isAdd = true;
                    }
                }
                if (!isAdd)
                {
                    defaultGroup.Add(item);
                }
                userIds.Add(info.id);
            }
            Task.Run(() => ImFriendBll.Instance.SyncFriends(RequestApiInfo.SendId, friends));
            Task.Run(() => SetFriendOnLineStatus(userIds));
        }

        public void SetFriendOnLineStatus(List<long> friendIds)
        {
            string userIds = string.Join(",", friendIds);
            if (userIds.Trim() == "")
            {
                userIds = "0";
            }
            string url = "/user/terminal/online?userIds=" + userIds;
            List<OnlineTerminalVO> onlineTerminals = HttpHelp.HttpApi<List<OnlineTerminalVO>>(url, null, "GET");

            FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
            foreach (OnlineTerminalVO info in onlineTerminals)
            {
                FriendVOItem friendVOItem = frmMain.findFriend(info.userId);
                if (friendVOItem != null)
                {
                    friendVOItem.TerminalType = info.GetTerminalType();
                }
            }
        }

        private void updateFriendHistoryCountAndState(CbItem sender, EventArgs e)
        {
            FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
            FriendVOItem friendVOItem = sender as FriendVOItem;
            HistoryVOItem historyVOItem = frmMain.findHistory(friendVOItem.FriendVO.id, MessageSourceType.Friend);
            if (historyVOItem != null)
            {
                historyVOItem.MessageCount = friendVOItem.MessageCount;
                historyVOItem.CurrentState = friendVOItem.CurrentState;
                historyVOItem.UpdateTime = DateTime.Now;
                historyVOItem.Parent.SortByUpdateTime();
            }
        }

        private void loadPrivateMessage()
        {
            #region 拉取最新100条信息
            //拉取未读私聊消息
            long privateMaxId = ImHistoryMessageBll.Instance.GetPrivateMaxId();
            string url = "/message/private/loadMessage?minId=" + privateMaxId;
            List<PrivateMessage> privateMessages = HttpHelp.HttpApi<List<PrivateMessage>>(url, null, "GET");
            List<PrivateMessage> recentMessages = new List<PrivateMessage>();
            recentMessages.AddRange(privateMessages);
            while (privateMessages.Count >= 99)
            {
                foreach (PrivateMessage item in privateMessages)
                {
                    if (item.id > privateMaxId)
                    {
                        privateMaxId = item.id;
                    }
                }
                url = "/message/private/loadMessage?minId=" + privateMaxId;
                privateMessages=HttpHelp.HttpApi<List<PrivateMessage>>(url, null, "GET");
                recentMessages.AddRange(privateMessages);
                while (recentMessages.Count >= 99)
                {
                    recentMessages.RemoveAt(0);
                }
            }
            #endregion
            recentMessages.Sort();
            FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
            Action action1 = () =>
            {
                foreach (PrivateMessage privateMessage in recentMessages)
                {
                    if (privateMessage.status != (int)MessageStatus.Read && privateMessage.sendId != RequestApiInfo.SendId)
                    {
                        FriendVOItem item = frmMain.findFriend(privateMessage.sendId);
                        if (item != null)
                        {
                            item.MessageCount = item.MessageCount + 1;
                            frmMain.RefushUnreadCount();
                        }
                        else
                        {
                            //没有好友
                            if(frmMain.AddNewFriend(privateMessage.sendId))
                            {
                                frmMain.RefreshFriends();
                                item = frmMain.findFriend(privateMessage.sendId);
                                if (item != null)
                                {
                                    item.MessageCount = item.MessageCount + 1;
                                    frmMain.RefushUnreadCount();
                                }
                            }
                        }
                    }
                    frmMain.UpdateHistoryMessage(privateMessage);
                }
            };
            frmMain.Invoke(action1);
        }
        #endregion

        #region 初始化公司组织架构
        public void InitCompanyStructure(CompanyTree companyTree)
        {
            companyTree.Nodes.Add("重庆分公司");
            TreeNode rootNode = companyTree.Nodes[0];
            rootNode.Nodes.Add("研发部");
            rootNode.Nodes.Add("采购部");
            rootNode.Nodes.Add("销售部");
            rootNode.Nodes[1].Nodes.Add("张三");
            rootNode.ExpandAll();
            companyTree.Nodes.Add("集团通信（上海）");
            TreeNode rootNode1 = companyTree.Nodes[1];
            rootNode1.Nodes.Add("采购部门");
            rootNode1.Nodes.Add("计算机中心");
            rootNode1.Nodes.Add("销售部门");

            companyTree.OnMoveHidenPerson += FrmFloatCompanyPersonInfoShowService.CompanyList_OnMoveHidenPerson;
            companyTree.OnMoveShowPerson += FrmFloatCompanyPersonInfoShowService.CompanyList_OnMoveShowPerson;

        }
        #endregion

        #region 初始化群组信息
        public void InitChartGroups(CbGroupCollection groupParent)
        {
            groupParent.Clear();
            ClassificationT<GroupVOItem> defaultGroup = new ClassificationT<GroupVOItem>("群聊");
            groupParent.Add(defaultGroup);
            foreach (ClassifyVO classify in _classifyVOs)
            {
                if (classify.type == (int)MessageSourceType.Group)
                {
                    ClassificationT<GroupVOItem> fg = new ClassificationT<GroupVOItem>(classify.classifyName);
                    fg.ID = classify.id;
                    fg.Tag = classify;
                    groupParent.Add(fg);
                }
            }
            LoadGroups(groupParent, defaultGroup);
            //拉取未读消息
            Task.Run(() => loadGroupMessage());
        }

      
        public void LoadGroups(CbGroupCollection groupParent, ClassificationT<GroupVOItem> defaultGroup)
        {
            string url = "/group/list";
            List<GroupVO> groups = HttpHelp.HttpApi<List<GroupVO>>(url, null, "GET");
            foreach (GroupVO info in groups)
            {
                GroupVOItem item = new GroupVOItem(info);
                item.OnTerminalTypeChanged += updateGroupHistoryCountAndState;
                item.OnCurrentStateChanged += updateGroupHistoryCountAndState;
                item.OnMessageCountChanged += updateGroupHistoryCountAndState;
                bool isAdd = false;
                foreach (ClassificationT<GroupVOItem> group in groupParent)
                {
                    if (group.ID == info.classifyId)
                    {
                        group.Add(item);
                        isAdd = true;
                    }
                }
                if (!isAdd)
                {
                    defaultGroup.Add(item);
                }
            }
            Task.Run(() => ImGroupBll.Instance.SyncGroups(groups));
        }

        private void updateGroupHistoryCountAndState(CbItem sender, EventArgs e)
        {
            FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
            GroupVOItem groupVOItem = sender as GroupVOItem;
            HistoryVOItem historyVOItem = frmMain.findHistory(groupVOItem.GroupVO.id, MessageSourceType.Group);
            if (historyVOItem != null)
            {
                historyVOItem.MessageCount = groupVOItem.MessageCount;
                historyVOItem.CurrentState = groupVOItem.CurrentState;
                historyVOItem.UpdateTime = DateTime.Now;
                historyVOItem.Parent.SortByUpdateTime();
            }
        }

        private void loadGroupMessage()
        {
            #region 拉取最新100条信息
            //拉取未读私聊消息
            long groupMaxId = ImHistoryMessageBll.Instance.GetGroupMaxId();
            string url = "/message/group/loadMessage?minId=" + groupMaxId;
            List<GroupMessage> privateMessages = HttpHelp.HttpApi<List<GroupMessage>>(url, null, "GET");
            List<GroupMessage> recentMessages = new List<GroupMessage>();
            recentMessages.AddRange(privateMessages);
            while (privateMessages.Count >= 99)
            {
                foreach (GroupMessage item in privateMessages)
                {
                    if (item.id > groupMaxId)
                    {
                        groupMaxId = item.id;
                    }
                }
                url = "/message/group/loadMessage?minId=" + groupMaxId;
                privateMessages = HttpHelp.HttpApi<List<GroupMessage>>(url, null, "GET");
                recentMessages.AddRange(privateMessages);
                while (recentMessages.Count >= 99)
                {
                    recentMessages.RemoveAt(0);
                }
            }
            #endregion
            recentMessages.Sort();
            FrmMain frmMain = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as FrmMain;
            Action action1 = () =>
            {
                foreach (GroupMessage groupMessage in recentMessages)
                {
                    if (groupMessage.status != (int)MessageStatus.Read && groupMessage.sendId != RequestApiInfo.SendId)
                    {
                        GroupVOItem item = frmMain.findGroup(groupMessage.groupId);
                        if (item != null)
                        {
                            item.MessageCount = item.MessageCount + 1;
                            frmMain.RefushUnreadCount();
                        }
                    }
                    frmMain.UpdateHistoryMessage(groupMessage);
                }
            };
            frmMain.Invoke(action1);
        }
        #endregion

        #region 初始化聊天历史
        public void InitChartHistory(CbGroupCollection cbGroups)
        {
            try
            {
                cbGroups.Clear();
                ClassificationT<HistoryVOItem> fg1 = new ClassificationT<HistoryVOItem>("聊天历史记录");
                fg1.IsExpand = true;
                fg1.GroupTextIsShow = false;
                cbGroups.Add(fg1);
                LoadHistory(fg1);
            }
            catch (Exception m)
            {
                FrmAutoCloseMessageBox.Show(m.Message.ToString(), MessageBoxIcon.Error);
            }
            finally
            {
            }
        }
        public void LoadHistory(ClassificationT<HistoryVOItem> sg)
        {
            List<ImHistoryMessage> messages = ImHistoryMessageBll.Instance.GetImHistoryList();
            foreach (ImHistoryMessage info in messages)
            {
                HistoryVOItem item = new HistoryVOItem(info);
                sg.Add(item);
            }
        }

        #endregion

    }
}
