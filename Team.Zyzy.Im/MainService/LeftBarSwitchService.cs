﻿using System;
using System.Collections.Generic;
using System.Text;
using YiWangYi.MsgQQ.WinUI.General.FunctionSwitch;
using YiWangYi.MsgQQ.WinUI.Model;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Service.Properties;
using YiWangYi.MsgQQ.WinUI.Service.MainService;
using Team.Zyzy.Im.Control;
using Team.Zyzy.Im.config;
using YiWangYi.MsgQQ.WinUI.General.GDIControls;
using System.Drawing;
using Team.Zyzy.Im.MainService;

namespace Team.Zyzy.Im.mainService
{
    public class LeftBarSwitchService: FunctionSwitchService
    {
        private static LeftBarSwitchService instance;
        private static object _lock = new object();


        private LeftBarSwitchService()
        {
            if (FunctionSwitch == null)
            {
                return;
            }
            initLeftButton();
        }

        public static LeftBarSwitchService GetInstance()
        {
            return instance;
        }

        private HomePageConfig _homePageConfig = null;
        protected void initLeftButton()
        {
            FunctionSwitch.Clear();
            FunctionSwitchModel home = new FunctionSwitchModel();
            home.Index = 1;
            home.MouseMoveImage = LeftBarImage.home3;
            home.MouseSelectedImage = LeftBarImage.home2;
            home.NormalImage = LeftBarImage.home1;
            home.EnableIsFalseImg = LeftBarImage.home4;
            home.Name = "home";
            home.Text = null;
            home.TextAlign = ButtonTextAlign.Right;
            home.ToolTip = "工作台";
            home.Selected = false;
            home.UnReadCount = 0;
            FunctionSwitch.Add(home);
            FunctionSwitchModel im = new FunctionSwitchModel();
            im.Index = 2;
            im.MouseMoveImage = LeftBarImage.im3;
            im.MouseSelectedImage = LeftBarImage.im2;
            im.NormalImage = LeftBarImage.im1;
            im.EnableIsFalseImg = LeftBarImage.im4;
            im.Name = "im";
            im.Text = null;
            im.TextAlign = ButtonTextAlign.Right;
            im.ToolTip = "即时通讯";
            im.Selected = true;
            im.UnReadCount =3;
            FunctionSwitch.Add(im);
            FunctionSwitchModel hand = new FunctionSwitchModel();
            hand.Index = 4;
            hand.MouseMoveImage = LeftBarImage.hand3;
            hand.MouseSelectedImage = LeftBarImage.hand2;
            hand.NormalImage = LeftBarImage.hand1;
            hand.EnableIsFalseImg = LeftBarImage.hand4;
            hand.Name = "hand";
            hand.Text = null;
            hand.TextAlign = ButtonTextAlign.Right;
            hand.ToolTip = "待办事项";
            hand.Selected = false;
            hand.UnReadCount = 0;
            FunctionSwitch.Add(hand);
            FunctionSwitchModel mail = new FunctionSwitchModel();
            mail.Index = 3;
            mail.MouseMoveImage = LeftBarImage.mail3;
            mail.MouseSelectedImage = LeftBarImage.mail2;
            mail.NormalImage = LeftBarImage.mail1;
            mail.EnableIsFalseImg = LeftBarImage.mail4;
            mail.Name = "mail";
            mail.Text = null;
            mail.TextAlign = ButtonTextAlign.Right;
            mail.ToolTip = "电子邮件";
            mail.Selected = false;
            mail.UnReadCount = 0;
            FunctionSwitch.Add(mail);
            FunctionSwitchModel notice = new FunctionSwitchModel();
            notice.Index = 4;
            notice.MouseMoveImage = LeftBarImage.notice3;
            notice.MouseSelectedImage = LeftBarImage.notice2;
            notice.NormalImage = LeftBarImage.notice1;
            notice.EnableIsFalseImg = LeftBarImage.notice4;
            notice.Name = "notice";
            notice.Text = null;
            notice.TextAlign = ButtonTextAlign.Right;
            notice.ToolTip = "通知公告";
            notice.Selected = false;
            notice.UnReadCount = 0;
            FunctionSwitch.Add(notice);
            FunctionSwitchModel disk = new FunctionSwitchModel();
            disk.Index = 5;
            disk.MouseMoveImage = LeftBarImage.disk3;
            disk.MouseSelectedImage = LeftBarImage.disk2;
            disk.NormalImage = LeftBarImage.disk1;
            disk.EnableIsFalseImg = LeftBarImage.disk4;
            disk.Name = "disk";
            disk.Text = null;
            disk.TextAlign = ButtonTextAlign.Right;
            disk.ToolTip = "网络硬盘";
            disk.Selected = false;
            FunctionSwitch.Add(disk);
            FunctionSwitchModel meeting = new FunctionSwitchModel();
            meeting.Index = 5;
            meeting.MouseMoveImage = LeftBarImage.meeting3;
            meeting.MouseSelectedImage = LeftBarImage.meeting2;
            meeting.NormalImage = LeftBarImage.meeting1;
            meeting.EnableIsFalseImg = LeftBarImage.meeting4;
            meeting.Name = "meeting";
            meeting.Text = null;
            meeting.TextAlign = ButtonTextAlign.Right;
            meeting.ToolTip = "视频会议";
            meeting.Selected = false;
            FunctionSwitch.Add(meeting);
            FunctionSwitchModel test = new FunctionSwitchModel();
            test.Index = 6;
            test.MouseMoveImage = LeftBarImage.test3;
            test.MouseSelectedImage = LeftBarImage.test2;
            test.NormalImage = LeftBarImage.test1;
            test.EnableIsFalseImg = LeftBarImage.test4;
            test.Name = "test";
            test.Text = null;
            test.TextAlign = ButtonTextAlign.Right;
            test.ToolTip = "演示测试";
            test.Selected = false;
            FunctionSwitch.Add(test);
        }

        public void LoadConfig()
        {
            _homePageConfig = HomePageConfig.LoadConfig();
            Form.LeftBarIsShow= _homePageConfig.ShowLeftBar;
            foreach(GButtonLB item in FunctionSwitch.Buttons)
            {
                foreach(LeftBar lb in _homePageConfig.LeftBars)
                {
                    if (item.Name == lb.Name)
                    {
                        item.SortKey = lb.SortKey;
                        item.Selected = lb.Selected;
                    }
                }
            }
            FunctionSwitch.Sort();
        }

        public static  LeftBarSwitchService GetInstance(FunctionSwitch fs,FrmMain frmMain)
        {
            Form = frmMain;
            if (fs != FunctionSwitch)
            {
                FunctionSwitch = fs;
                fs.Click += new FunctionSwitchEventHandler(leftItem_Click);
            }
            FunctionSwitch = fs;
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new LeftBarSwitchService();
                    }
                }
            }
            return instance;
        }

        public void SetUnReadCount(string name, int unReadCount)
        {
            FunctionSwitch.SetUnReadCount(name, unReadCount);
        }

        protected static void leftItem_Click(FunctionSwitchModel sender, string fsName)
        {
            sender.Selected = true;
            FunctionSwitch.Refresh(sender);
            switch (sender.Name)
            {
                case "home":
                    HomeControl home = new HomeControl();
                    SetContent(home);
                    break;
                case "im":
                    SetContent(Form.ContractListBar);
                    break;
                case "mail":
                    MailControl mail = new MailControl();
                    SetContent(mail);
                    break;
                case "notice":
//                    公司新闻
//业务资讯
//党群活动
//党群公告
//党群教育
//党群论坛
//安全质量
//生产运行
//服务质量
//运行通告
//企业管理
//综合管理
//人事公告
//业务公告
                    break;
                case "flight":
                    break;
                case "test":
                    LeftTestControl test = new LeftTestControl();
                    SetContent(test);                    
                    break;
                default:
                    break;
            }
        }

    }
}
