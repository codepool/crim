﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.Business.vo;
using Team.Zyzy.Im.Control.Model;
using Team.Zyzy.Im.help;
using YiWangYi.MsgQQ.WinUI.ContractPanel;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;
using YiWangYi.MsgQQ.WinUI.QQListBar;

namespace Team.Zyzy.Im.mainService
{
    public class MainChartBarsRightMenu
    {
        public MainChartBarsRightMenu(QQContractListBase clb)
        {
            InitializeComponent();
            this.ContractListBar = clb;
            this.ContractListBar.ContextMenuStrip = cmsChartBars;
            this.ContractListBar.SelcetPagChanged += ContractListBar_SelcetPagChanged;
            initClassifyMenu();
        }

        private QQContractListBase ContractListBar;

        private System.Windows.Forms.ContextMenuStrip cmsChartBars;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddFriend;
        private System.Windows.Forms.ToolStripMenuItem tsmiRenameFriend;
        private System.Windows.Forms.ToolStripMenuItem tsmiMoveFriend;
        private System.Windows.Forms.ToolStripMenuItem tsmiRefreshFriend;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteFriend;
        private System.Windows.Forms.ToolStripMenuItem tsmiCreateGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiModifyGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiRemarkGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiInviteGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiMoveGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiQuitGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiRefreshGroup;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddClassify;
        private System.Windows.Forms.ToolStripMenuItem tsmiRenameClassify;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteClassify;
        private void InitializeComponent()
        {
            this.cmsChartBars = new System.Windows.Forms.ContextMenuStrip();
            this.tsmiAddFriend = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRenameFriend = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMoveFriend = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRefreshFriend = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteFriend = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCreateGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiModifyGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRemarkGroup=new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiInviteGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMoveGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiQuitGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRefreshGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAddClassify = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiRenameClassify = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteClassify = new System.Windows.Forms.ToolStripMenuItem();
            // 
            // cmsChartBars
            // 
            this.cmsChartBars.ImageScalingSize = new System.Drawing.Size(16, 16);
            this.cmsChartBars.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiRefreshFriend,
            this.tsmiAddFriend,
            this.tsmiRenameFriend,
            this.tsmiMoveFriend,
            this.tsmiDeleteFriend,
            this.tsmiRefreshGroup,
            this.tsmiCreateGroup,
            this.tsmiInviteGroup,
            this.tsmiRemarkGroup,
            this.tsmiAddGroup,
            this.tsmiModifyGroup,
            this.tsmiMoveGroup,
            this.tsmiQuitGroup,
            this.tsmiDeleteGroup,
            this.tsmiAddClassify,
            this.tsmiRenameClassify,
            this.tsmiDeleteClassify});
            this.cmsChartBars.Name = "cmsChartBars";
            this.cmsChartBars.Opening += CmsChartBars_Opening;
            // 
            // tsmiAddFriend
            // 
            this.tsmiAddFriend.Image = global::Team.Zyzy.Im.Properties.Resources.addFriend;
            this.tsmiAddFriend.Name = "tsmiAddFriend";
            this.tsmiAddFriend.Text = "添加好友";
            this.tsmiAddFriend.Click += new System.EventHandler(this.tsmiAddFriend_Click);
            // 
            // tsmiReNameFriend
            // 
            this.tsmiRenameFriend.Image = global::Team.Zyzy.Im.Properties.Resources.renameFriend;
            this.tsmiRenameFriend.Name = "tsmiRenameFriend";
            this.tsmiRenameFriend.Text = "修改好友备注";
            this.tsmiRenameFriend.Click += new System.EventHandler(this.tsmiRenameFriend_Click);
            // 
            // tsmiDeleteFriend
            // 
            this.tsmiDeleteFriend.Image = global::Team.Zyzy.Im.Properties.Resources.deleteFriend;
            this.tsmiDeleteFriend.Name = "tsmiDeleteFriend";
            this.tsmiDeleteFriend.Text = "删除好友";
            this.tsmiDeleteFriend.Click += new System.EventHandler(this.tsmiDeleteFriend_Click);
            // 
            // tsmiMoveFriend
            // 
            this.tsmiMoveFriend.Image = global::Team.Zyzy.Im.Properties.Resources.moveFriend;
            this.tsmiMoveFriend.Name = "tsmiMoveFriend";
            this.tsmiMoveFriend.Text = "移动好友至";
            // 
            // tsmiAddClassify
            // 
            this.tsmiAddClassify.Image = global::Team.Zyzy.Im.Properties.Resources.addClassify;
            this.tsmiAddClassify.Name = "tsmiAddClassify";
            this.tsmiAddClassify.Text = "添加分组";
            this.tsmiAddClassify.Click += new System.EventHandler(this.tsmiAddClassify_Click);
            // 
            // tsmiMoveGroup
            // 
            this.tsmiMoveGroup.Image = global::Team.Zyzy.Im.Properties.Resources.moveGroup;
            this.tsmiMoveGroup.Name = "tsmiMoveGroup";
            this.tsmiMoveGroup.Text = "移动群组至";
            // 
            // tsmiRenameClassify
            // 
            this.tsmiRenameClassify.Image = global::Team.Zyzy.Im.Properties.Resources.renameClassify;
            this.tsmiRenameClassify.Name = "tsmiRenameClassify";
            this.tsmiRenameClassify.Text = "重命名分组";
            this.tsmiRenameClassify.Click += new System.EventHandler(this.tsmiRenameClassify_Click);
            // 
            // tsmiDeleteClassify
            // 
            this.tsmiDeleteClassify.Image = global::Team.Zyzy.Im.Properties.Resources.removeClassify;
            this.tsmiDeleteClassify.Name = "tsmiDeleteClassify";
            this.tsmiDeleteClassify.Text = "删除分组";
            this.tsmiDeleteClassify.Click += new System.EventHandler(this.tsmiDeleteClassify_Click);
            // 
            // tsmiCreateGroup
            // 
            this.tsmiCreateGroup.Image = global::Team.Zyzy.Im.Properties.Resources.createGroup;
            this.tsmiCreateGroup.Name = "tsmiCreateGroup";
            this.tsmiCreateGroup.Text = "创建群聊";
            this.tsmiCreateGroup.Click += new System.EventHandler(this.tsmiCreateGroup_Click);
            // 
            // tsmiAddGroup
            // 
            this.tsmiAddGroup.Image = global::Team.Zyzy.Im.Properties.Resources.addGroup;
            this.tsmiAddGroup.Name = "tsmiAddGroup";
            this.tsmiAddGroup.Text = "加入群聊";
            this.tsmiAddGroup.Click += new System.EventHandler(this.tsmiAddGroup_Click);
            // 
            // tsmiRefreshGroup
            // 
            this.tsmiRefreshGroup.Image = global::Team.Zyzy.Im.Properties.Resources.refreshGroup;
            this.tsmiRefreshGroup.Name = "tsmiRefreshGroup";
            this.tsmiRefreshGroup.Text = "刷新群聊";
            this.tsmiRefreshGroup.Click += new System.EventHandler(this.tsmiRefreshGroup_Click);
            // 
            // tsmiRefreshFriend
            // 
            this.tsmiRefreshFriend.Image = global::Team.Zyzy.Im.Properties.Resources.refreshFriend;
            this.tsmiRefreshFriend.Name = "tsmiRefreshFriend";
            this.tsmiRefreshFriend.Text = "刷新好友列表";
            this.tsmiRefreshFriend.Click += new System.EventHandler(this.tsmiRefreshFriend_Click);
            // 
            // tsmiQuitGroup
            // 
            this.tsmiQuitGroup.Image = global::Team.Zyzy.Im.Properties.Resources.quitGroup;
            this.tsmiQuitGroup.Name = "tsmiQuitGroup";
            this.tsmiQuitGroup.Text = "退出群聊";
            this.tsmiQuitGroup.Click += new System.EventHandler(this.tsmiQuitGroup_Click);
            // 
            // tsmiDeleteGroup
            // 
            this.tsmiDeleteGroup.Image = global::Team.Zyzy.Im.Properties.Resources.deleteGroup;
            this.tsmiDeleteGroup.Name = "tsmiDeleteGroup";
            this.tsmiDeleteGroup.Text = "解散群聊";
            this.tsmiDeleteGroup.Click += new System.EventHandler(this.tsmiDeleteGroup_Click);
            // 
            // tsmiInviteGroup
            // 
            this.tsmiInviteGroup.Image = global::Team.Zyzy.Im.Properties.Resources.inviteGroup;
            this.tsmiInviteGroup.Name = "tsmiInviteGroup";
            this.tsmiInviteGroup.Text = "邀请好友加入群聊";
            this.tsmiInviteGroup.Click += new System.EventHandler(this.tsmiInviteGroup_Click);
            // 
            // tsmiRemarkGroup
            // 
            this.tsmiRemarkGroup.Image = global::Team.Zyzy.Im.Properties.Resources.remarkGroup;
            this.tsmiRemarkGroup.Name = "tsmiRemarkGroup";
            this.tsmiRemarkGroup.Text = "修改群聊备注";
            this.tsmiRemarkGroup.Click += new System.EventHandler(this.tsmiRemarkGroup_Click);
            // 
            // tsmiModifyGroup
            // 
            this.tsmiModifyGroup.Image = global::Team.Zyzy.Im.Properties.Resources.modifyGroup;
            this.tsmiModifyGroup.Name = "tsmiModifyGroup";
            this.tsmiModifyGroup.Text = "修改群聊信息";
            this.tsmiModifyGroup.Click += new System.EventHandler(this.tsmiModifyGroup_Click);

        }

        private void CmsChartBars_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }
        private void ContractListBar_SelcetPagChanged(YiWangYi.MsgQQ.WinUI.QQTab.QTabPagEventArgs e)
        {
            initClassifyMenu();
        }


        private void initClassifyMenu()
        {
            ContractType contractType = this.ContractListBar.SelectType;
            if (contractType == ContractType.Friend)
            {
                tsmiMoveFriend.DropDownItems.Clear();
                ToolStripMenuItem tsmi = new ToolStripMenuItem();
                tsmi.Image = global::Team.Zyzy.Im.Properties.Resources.classify;
                tsmi.Text = "我的好友";
                tsmi.Click += new System.EventHandler(this.tsmiMoveFriend_Click);
                tsmiMoveFriend.DropDownItems.Add(tsmi);
                foreach (ClassifyVO item in LoadChartBarsService.GetInstance().ClassifyVOs)
                {
                    if (item.type == (int)MessageSourceType.Friend)
                    {
                        tsmi = new ToolStripMenuItem();
                        tsmi.Image = global::Team.Zyzy.Im.Properties.Resources.classify;
                        tsmi.Text = item.classifyName;
                        tsmi.Tag = item;
                        tsmi.Click += new System.EventHandler(this.tsmiMoveFriend_Click);
                        tsmiMoveFriend.DropDownItems.Add(tsmi);
                    }
                }
                tsmiAddFriend.Visible = true;
                tsmiRenameFriend.Visible = true;
                tsmiMoveFriend.Visible = true;
                tsmiRefreshFriend.Visible = true;
                tsmiDeleteFriend.Visible = true;
                tsmiCreateGroup.Visible = false;
                tsmiAddGroup.Visible = false;
                tsmiMoveGroup.Visible = false;
                tsmiRefreshGroup.Visible = false;
                tsmiDeleteGroup.Visible = false;
                tsmiInviteGroup.Visible = false;
                tsmiQuitGroup.Visible = false;
                tsmiRemarkGroup.Visible = false;
                tsmiModifyGroup.Visible = false;
            }
            else if (contractType == ContractType.Group)
            {
                tsmiMoveGroup.DropDownItems.Clear();
                ToolStripMenuItem tsmi = new ToolStripMenuItem();
                tsmi.Image = global::Team.Zyzy.Im.Properties.Resources.classify;
                tsmi.Text = "群聊";
                tsmi.Click += new System.EventHandler(this.tsmiMoveGroup_Click);
                tsmiMoveGroup.DropDownItems.Add(tsmi);
                foreach (ClassifyVO item in LoadChartBarsService.GetInstance().ClassifyVOs)
                {
                    if (item.type == (int)MessageSourceType.Group)
                    {
                        tsmi = new ToolStripMenuItem();
                        tsmi.Image = global::Team.Zyzy.Im.Properties.Resources.classify;
                        tsmi.Text = item.classifyName;
                        tsmi.Tag = item;
                        tsmi.Click += new System.EventHandler(this.tsmiMoveGroup_Click);
                        tsmiMoveGroup.DropDownItems.Add(tsmi);
                    }
                }
                tsmiAddFriend.Visible = false;
                tsmiRenameFriend.Visible = false;
                tsmiMoveFriend.Visible = false;
                tsmiRefreshFriend.Visible = false;
                tsmiDeleteFriend.Visible = false;
                tsmiCreateGroup.Visible = true;
                tsmiAddGroup.Visible = true;
                tsmiMoveGroup.Visible = true;
                tsmiRefreshGroup.Visible = true;
                tsmiDeleteGroup.Visible = true;
                tsmiInviteGroup.Visible = true;
                tsmiQuitGroup.Visible = true;
                tsmiRemarkGroup.Visible = true;
                tsmiModifyGroup.Visible = true;
            }
        }

        #region 好友管理
        private void tsmiAddFriend_Click(object sender, EventArgs e)
        {
            FrmAddFriendAndGroup addFriend = new FrmAddFriendAndGroup();
            addFriend.ShowDialog();
        }
        private void tsmiRenameFriend_Click(object sender, EventArgs e)
        {
            ContractType contractType = this.ContractListBar.SelectType;
            if (contractType == ContractType.Friend)
            {
                FriendVOItem friendVOItem = this.ContractListBar.FriendsList.GetSelectedItem() as FriendVOItem;
                FrmRenameFriend renameFriend = new FrmRenameFriend();
                renameFriend.FriendVO = friendVOItem.FriendVO;
                renameFriend.ContractListBar = this.ContractListBar;
                renameFriend.Show();
            }
        }
        private void tsmiMoveFriend_Click(object sender, EventArgs e)
        {
            ContractType contractType = this.ContractListBar.SelectType;
            if (contractType == ContractType.Friend)
            {
                FriendVOItem friendVOItem = this.ContractListBar.FriendsList.GetSelectedItem() as FriendVOItem;
                if (friendVOItem == null)
                {
                    FrmAutoCloseMessageBox.Show("请先选择要移动的好友！");
                    return;
                }
                FriendVO friend = null;
                string url = "/friend/find/"+ friendVOItem.FriendVO.id;
                friend = HttpHelp.HttpApi<FriendVO>(url, null, "GET");
                ClassifyVO item = (sender as ToolStripMenuItem).Tag as ClassifyVO;
                friend.classifyId = (item == null) ? 0 : item.id;
                url = "/friend/update";
                HttpHelp.HttpApi<ClassifyVO>(url, friend, "PUT");
                LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
            }
        }
        private void tsmiRefreshFriend_Click(object sender, EventArgs e)
        {
            LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
        }
        private void tsmiDeleteFriend_Click(object sender, EventArgs e)
        {
            FriendVOItem friendVOItem = this.ContractListBar.FriendsList.GetSelectedItem() as FriendVOItem;
            if (friendVOItem == null)
            {
                FrmAutoCloseMessageBox.Show("请先选择要删除的好友！");
                return;
            }

            FrmAutoCloseMessageBox frmDialog = new FrmAutoCloseMessageBox();
            frmDialog.StartPosition = FormStartPosition.CenterScreen;
            FrmAutoCloseMessageBox.Show(frmDialog, "删除好友确认", "确定删除【" + friendVOItem.FriendVO.nickName + "】吗？", MessageBoxButtons.YesNo);
            frmDialog.OnDialogResultChanged += delegate (DialogResult result)
            {
                if (result == DialogResult.Yes)
                {
                    string url = "/friend/delete/" + friendVOItem.FriendVO.id;
                    HttpHelp.HttpApi<FriendVO>(url, null, "DELETE");
                    LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
                }
            };
        }
        #endregion 
        #region 分组管理
        private void tsmiAddClassify_Click(object sender, EventArgs e)
        {
            FrmClassifyManage frmClassify = new FrmClassifyManage();
            frmClassify.Text = "新建分组";
            ClassifyVO classify= new ClassifyVO();
            classify.sortKey = LoadChartBarsService.GetInstance().ClassifyVOs.Count;
            classify.classifyName = "新建分组";
            ContractType contractType = this.ContractListBar.SelectType;
            if (contractType == ContractType.Friend)
            {
                classify.type = (int)MessageSourceType.Friend;
            }
            else if (contractType == ContractType.Group)
            {
                classify.type = (int)MessageSourceType.Group;
            }
            frmClassify.ClassifyVO = classify;
            frmClassify.ContractListBar = this.ContractListBar;
            frmClassify.ShowDialog();
            LoadChartBarsService.GetInstance().InitClassify();
            initClassifyMenu();
        }
        private void tsmiDeleteClassify_Click(object sender, EventArgs e)
        {
            CbGroup group = getCbGroup();
            ClassifyVO classifyVO = group.Tag as ClassifyVO;
            if (classifyVO == null)
            {
                FrmAutoCloseMessageBox.Show("默认分组不能删除！");
                return;
            }
            FrmAutoCloseMessageBox frmDialog = new FrmAutoCloseMessageBox();
            frmDialog.StartPosition = FormStartPosition.CenterScreen;
            FrmAutoCloseMessageBox.Show(frmDialog, "删除分组确认", "确定删除【" + classifyVO.classifyName + "】吗？", MessageBoxButtons.YesNo);
            frmDialog.OnDialogResultChanged += delegate (DialogResult result)
            {
                if(result== DialogResult.Yes)
                {
                    string url = "/classify/delete/" + classifyVO.id;
                    HttpHelp.HttpApi<ClassifyVO>(url, null, "DELETE");
                    LoadChartBarsService.GetInstance().InitClassify();
                    initClassifyMenu();
                    if (classifyVO.type == (int)MessageSourceType.Friend)
                    {
                        LoadChartBarsService.GetInstance().InitChartFriends(this.ContractListBar.FriendsList.Groups);
                    }
                    else if (classifyVO.type == (int)MessageSourceType.Group)
                    {
                        LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
                    }
                }
            };
        }

        private void tsmiRenameClassify_Click(object sender, EventArgs e)
        {
            CbGroup group = getCbGroup();
            ClassifyVO classifyVO = group.Tag as ClassifyVO;
            if (classifyVO == null)
            {
                FrmAutoCloseMessageBox.Show("默认分组不能重命名！");
                return;
            }
            FrmClassifyManage frmClassify = new FrmClassifyManage();
            frmClassify.Text = "修改分组";
            frmClassify.ClassifyVO = classifyVO;
            frmClassify.ContractListBar = this.ContractListBar;
            frmClassify.ShowDialog();
            LoadChartBarsService.GetInstance().InitClassify();
            initClassifyMenu();
        }

        private CbGroup getCbGroup()
        {
            CbGroup group = null;
            ContractType contractType = this.ContractListBar.SelectType;
            Point point = ContractListBar.PointToClient(new Point(cmsChartBars.Left, cmsChartBars.Top));
            if (contractType == ContractType.Friend)
            {
                group = this.ContractListBar.FriendsList.GetRecentCbGroup(point);
            }
            else if (contractType == ContractType.Group)
            {
                group = this.ContractListBar.GroupList.GetRecentCbGroup(point);
            }

            return group;
        }
        #endregion
        #region 群组管理
        private void tsmiCreateGroup_Click(object sender, EventArgs e)
        {
            FrmCreateGroup frmCreateGroup = new FrmCreateGroup();
            frmCreateGroup.Show();
        }
        private void tsmiModifyGroup_Click(object sender, EventArgs e)
        {
            GroupVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupVOItem;
            if (groupVOItem == null)
            {
                FrmAutoCloseMessageBox.Show("请先选择群组！");
                return;
            }
            FrmCreateGroup frmCreateGroup = new FrmCreateGroup();
            frmCreateGroup.IsInvite = false;
            frmCreateGroup.GroupVO = groupVOItem.GroupVO;
            frmCreateGroup.Show();
        }
        private void tsmiInviteGroup_Click(object sender, EventArgs e)
        {
            GroupVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupVOItem;
            if (groupVOItem == null)
            {
                FrmAutoCloseMessageBox.Show("请先选择群组！");
                return;
            }
            FrmCreateGroup frmCreateGroup = new FrmCreateGroup();
            frmCreateGroup.IsInvite = true;
            frmCreateGroup.GroupVO = groupVOItem.GroupVO;
            frmCreateGroup.Show();
        }

        private void tsmiDeleteGroup_Click(object sender, EventArgs e)
        {
            GroupVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupVOItem;
            if (groupVOItem == null)
            {
                FrmAutoCloseMessageBox.Show("请先选择要解散的群组！");
                return;
            }

            if (groupVOItem.GroupVO.ownerId != RequestApiInfo.SendId)
            {
                FrmAutoCloseMessageBox.Show("你不是群主，不能解散群组！");
                return;
            }

            FrmAutoCloseMessageBox frmDialog = new FrmAutoCloseMessageBox();
            frmDialog.StartPosition = FormStartPosition.CenterScreen;
            FrmAutoCloseMessageBox.Show(frmDialog, "解散群组确认", "确定解散【" + groupVOItem.GroupVO.name + "】吗？", MessageBoxButtons.YesNo);
            frmDialog.OnDialogResultChanged += delegate (DialogResult result)
            {
                if (result == DialogResult.Yes)
                {
                    string url = "/group/delete/" + groupVOItem.GroupVO.id;
                    HttpHelp.HttpApi<GroupVO>(url, null, "DELETE");
                    LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
                }
            };
        }
        private void tsmiQuitGroup_Click(object sender, EventArgs e)
        {
            GroupVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupVOItem;
            if (groupVOItem == null)
            {
                FrmAutoCloseMessageBox.Show("请先选择要退出的群组！");
                return;
            }

            if(groupVOItem.GroupVO.ownerId== RequestApiInfo.SendId)
            {
                FrmAutoCloseMessageBox.Show("你是群组不能退出群聊，只能解散群聊！");
                return;
            }

            FrmAutoCloseMessageBox frmDialog = new FrmAutoCloseMessageBox();
            frmDialog.StartPosition = FormStartPosition.CenterScreen;
            FrmAutoCloseMessageBox.Show(frmDialog, "退出群组确认", "确定退出【" + groupVOItem.GroupVO.name + "】吗？", MessageBoxButtons.YesNo);
            frmDialog.OnDialogResultChanged += delegate (DialogResult result)
            {
                if (result == DialogResult.Yes)
                {
                    string url = "/group/quit/" + groupVOItem.GroupVO.id;
                    HttpHelp.HttpApi<GroupVO>(url, null, "DELETE");
                    LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
                }
            };
        }
        private void tsmiRemarkGroup_Click(object sender, EventArgs e)
        {
            ContractType contractType = this.ContractListBar.SelectType;
            if (contractType == ContractType.Group)
            {
                GroupVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupVOItem;
                FrmRenameGroup renameFriend = new FrmRenameGroup();
                renameFriend.GroupVO = groupVOItem.GroupVO;
                renameFriend.ContractListBar = this.ContractListBar;
                renameFriend.Show();
            }
        }
        private void tsmiAddGroup_Click(object sender, EventArgs e)
        {
            FrmAddFriendAndGroup addFriend = new FrmAddFriendAndGroup(true);
            addFriend.ShowDialog();
        }
        private void tsmiRefreshGroup_Click(object sender, EventArgs e)
        {
            LoadChartBarsService.GetInstance().InitClassify();
            initClassifyMenu();
           LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
        }
        private void tsmiMoveGroup_Click(object sender, EventArgs e)
        {
            ContractType contractType = this.ContractListBar.SelectType;
            if (contractType == ContractType.Group)
            {
                GroupVOItem groupVOItem = this.ContractListBar.GroupList.GetSelectedItem() as GroupVOItem;
                if (groupVOItem == null)
                {
                    FrmAutoCloseMessageBox.Show("请先选择要移动的群组！");
                    return;
                }
                GroupVO group = groupVOItem.GroupVO;
                ClassifyVO item = (sender as ToolStripMenuItem).Tag as ClassifyVO;
                group.classifyId = (item == null) ? 0 : item.id;
                string url = "/group/classify?groupId=" + group.id+ "&classifyId="+ group.classifyId;
                HttpHelp.HttpApi<ClassifyVO>(url, group, "PUT");
                LoadChartBarsService.GetInstance().InitChartGroups(this.ContractListBar.GroupList.Groups);
            }
        }


        #endregion
    }
}