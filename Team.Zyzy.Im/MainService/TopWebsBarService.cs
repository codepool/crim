﻿using System;
using System.Collections.Generic;
using System.Text;
using YiWangYi.MsgQQ.WinUI.General.FunctionSwitch;
using YiWangYi.MsgQQ.WinUI.Model;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Service.Properties;
using YiWangYi.MsgQQ.WinUI.General.CommonOper;
using YiWangYi.MsgQQ.WinUI.Service.MainService;
using YiWangYi.MsgQQ.WinUI.General.SoftSetting;
using Team.Zyzy.Im.MainService;

namespace Team.Zyzy.Im.mainService
{
    public class TopWebsBarService : SoftSettingService
    {
        private static TopWebsBarService instance;
        private static object _lock = new object();

        protected TopWebsBarService()
        {
            if (SoftSetting == null)
            {
                return;
            }
            this.init();
        }

        protected override void init()
        {
            CommonOperModel sogou = new CommonOperModel();
            sogou.Index = 1;
            sogou.MouseMoveImage = MainServiceResource.sogou1;
            sogou.MouseSelectedImage = MainServiceResource.sogou2;
            sogou.NormalImage = MainServiceResource.sogou;
            sogou.Name = "sogou";
            sogou.Text = null;
            sogou.TextAlign = ButtonTextAlign.Right;
            sogou.ToolTip = "搜狗搜索";
            sogou.Width = 20;
            SoftSetting.Add(sogou);
            CommonOperModel eCommerce = new CommonOperModel();
            eCommerce.Index = 1;
            eCommerce.MouseMoveImage = MainServiceResource.eCommerce2;
            eCommerce.MouseSelectedImage = MainServiceResource.eCommerce1;
            eCommerce.NormalImage = MainServiceResource.eCommerce;
            eCommerce.Name = "eCommerce";
            eCommerce.Text = null;
            eCommerce.TextAlign = ButtonTextAlign.Right;
            eCommerce.ToolTip = "电商平台";
            eCommerce.Width = 20;
            SoftSetting.Add(eCommerce);
            CommonOperModel forum = new CommonOperModel();
            forum.Index = 1;
            forum.MouseMoveImage = MainServiceResource.forum1;
            forum.MouseSelectedImage = MainServiceResource.forum2;
            forum.NormalImage = MainServiceResource.forum;
            forum.Name = "forum";
            forum.Text = null;
            forum.TextAlign = ButtonTextAlign.Right;
            forum.ToolTip = "单位论坛";
            forum.Width = 20;
            SoftSetting.Add(forum);
        }


        public static TopWebsBarService GetInstance(SoftSetting ift, FrmMain frmMain)
        {
            Form = frmMain;
            if (ift != SoftSetting)
            {
                SoftSetting = ift;
                ift.Click += new EventHandler(ift_Click);
            }
            SoftSetting = ift;
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new TopWebsBarService();
                    }
                }
            }
            return instance;
        }

        private static void ift_Click(object sender, EventArgs e)
        {
            string selectedName = sender.ToString().Trim();
            switch (selectedName)
            {
                case "sogou":
                    string url1 = string.Format("https://www.sogou.com/");
                    System.Diagnostics.Process.Start(url1);
                    break;
                case "eCommerce":
                    string url2 = string.Format("http://www.sszhgyl.com/");
                    System.Diagnostics.Process.Start(url2);
                    break;
                case "forum":
                    string url3 = string.Format("https://www.zyzy.team/site.html");
                    System.Diagnostics.Process.Start(url3);
                    break;
                default :
                    break;
            }
           
        }


    }
}
