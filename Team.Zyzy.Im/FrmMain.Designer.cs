﻿
namespace Team.Zyzy.Im
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.SuspendLayout();
            // 
            // qqFrmMainBackground
            // 
            this.qqFrmMainBackground.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqFrmMainBackground.BackgroundImage")));
            this.qqFrmMainBackground.Size = new System.Drawing.Size(440, 1012);
            this.qqFrmMainBackground.Text = "FrmMain";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 1032);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmMain";
            this.Opacity = 1D;
            this.Text = "FrmMain";
            this.Controls.SetChildIndex(this.qqFrmMainBackground, 0);
            this.ResumeLayout(false);

        }

        #endregion


    }
}