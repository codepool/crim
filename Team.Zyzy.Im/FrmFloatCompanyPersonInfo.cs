﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI.Forms;

namespace Team.Zyzy.Im
{
    public partial class FrmFloatCompanyPersonInfo : FormQQAutoCloseRound
    {
        public FrmFloatCompanyPersonInfo()
        {
            InitializeComponent();
            this.Load += FrmFloatCompanyPersonInfo_Load;
        }

        private void FrmFloatCompanyPersonInfo_Load(object sender, EventArgs e)
        {
            timAutoClose.Enabled = true;
        }

        #region

        public Image UserImage
        {
            get
            {
                return picHeaderImag.Image;
            }
            set
            {
                picHeaderImag.Image=value;
            }
        }

        public string UserName
        {
            get
            {
                return labName.Text;
            }
            set
            {
                labName.Text = value;
            }
        }

        public string UserPhone
        {
            get
            {
                return labPhone.Text;
            }
            set
            {
                labPhone.Text = value;
            }
        }

        public string UserPosition
        {
            get
            {
                return labPosition.Text;
            }
            set
            {
                labPosition.Text = value;
            }
        }

        public string UserQQ
        {
            get
            {
                return labQQ.Text;
            }
            set
            {
                labQQ.Text = value;
            }
        }

        public string UserWechar
        {
            get
            {
                return labWechar.Text;
            }
            set
            {
                labWechar.Text = value;
            }
        }

        public string UserEmail
        {
            get
            {
                return labEmail.Text;
            }
            set
            {
                labEmail.Text = value;
            }
        }
        #endregion

        #region 单例模式相关设置
        private static FrmFloatCompanyPersonInfo instance;
        private static object _lock = new object();

        public static FrmFloatCompanyPersonInfo GetInstance()
        {
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new FrmFloatCompanyPersonInfo();
                    }
                }
            }
            if (instance.WindowState != FormWindowState.Normal)
            {
                instance.WindowState = FormWindowState.Normal;
            }
            return instance;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            instance = null;
        }

        #endregion

        private int hiddenSecond = 0;
        private int autoCloseSecond = 20;
        public void ResetShow()
        {
            hiddenSecond = 0;
        }
        private void timAutoClose_Tick(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                hiddenSecond = 0;
                return;
            }
            hiddenSecond++;
            if (hiddenSecond > autoCloseSecond)
            {
                this.Close();
            }
        }
    }
}
