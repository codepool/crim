﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.dto;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.Business.vo;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Forms;

namespace Team.Zyzy.Im
{
    public partial class FrmLoginRetry : FormQQDialog
    {
        protected FrmLoginRetry()
        {
            InitializeComponent();
        }
        private FrmMain _frmMain = null;
        public Thread loginThread = null;
        public FrmMain FrmMain
        {
            get
            {
                if (_frmMain == null)
                {
                    _frmMain = ShareInfo.FormQQMain as FrmMain;
                }
                return _frmMain;
            }
        }
        public static LoginDTO LoginUser { get; set; }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void StartLogin()
        {
            this.StartPosition = FormStartPosition.CenterParent;
            this.Load += FrmLoginRetry_Load;
            this.Shown += FrmLoginRetry_Shown;
            this.ShowDialog();
        }

        private void FrmLoginRetry_Shown(object sender, EventArgs e)
        {
            try
            {
                proLogin.L_Value = 0;
                proLogin.Visible = true;
                if (loginThread != null)
                {
                    loginThread.Abort();
                    loginThread = null;
                }
                if (loginThread == null)
                {
                    loginThread = new Thread(delegate () { retryLogin(); });//线程
                    loginThread.Start();
                }
            }
            catch
            {
                this.btnRetry.Visible = true;
            }
        }

        private void FrmLoginRetry_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            this.TopLevel = true;
            this.TopMost = true;
            this.Left = FrmMain.Width > this.Width ? (FrmMain.Left + (FrmMain.Width - this.Width) / 2) : FrmMain.Left;
        }

        private void retryLogin()
        {
            try
            {
                Action action3 = () =>
                {
                    proLogin.Visible = true;
                    btnRetry.Visible = false;
                };
                Invoke(action3);
                string url = RequestApiInfo.BaseApi + "/login";
                Result<LoginVO> result = HttpHelp.HttpApi<LoginVO>(url, LoginUser, "POST", null);
                if (result.code != (int)ResultCode.SUCCESS)
                {
                    Action action = () =>
                    {
                        proLogin.Visible = false;
                        btnRetry.Visible = true;
                    };
                    Invoke(action);
                }
                else
                {
                    RequestApiInfo.AccessToken = result.data.accessToken;
                    RequestApiInfo.AccessTokenExpiresIn = result.data.accessTokenExpiresIn;
                    RequestApiInfo.RefreshToken = result.data.refreshToken;
                    RequestApiInfo.RefreshTokenExpiresIn = result.data.refreshTokenExpiresIn;
                    Action action = () =>
                    {
                        this.DialogResult = DialogResult.OK;
                        this.Close();
                    };
                    Invoke(action);
                }
            }
            catch
            {
                Action action = () =>
                {
                    proLogin.Visible = false;
                    btnRetry.Visible = true;
                };
                Invoke(action);
            }
        }


        #region 单例模式相关设置
        private static FrmLoginRetry instance;
        private static object _lock = new object();

        public static FrmLoginRetry GetInstance()
        {
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new FrmLoginRetry();
                        instance.StartPosition = FormStartPosition.CenterParent;
                    }
                }
            }
            if (instance.WindowState != FormWindowState.Normal)
            {
                instance.WindowState = FormWindowState.Normal;
            }
            return instance;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (loginThread != null)
            {
                loginThread.Abort();
                loginThread = null;
            }
            instance = null;
        }

        #endregion

        private void btnRetry_Click(object sender, EventArgs e)
        {
            try
            {
                FrmLoginRetry_Shown(sender, e);
            }
            catch
            {
                this.btnRetry.Visible = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (proLogin.L_Value == 100)
            {
                proLogin.L_Value = 0;
            }
            else
            {
                proLogin.L_Value = proLogin.L_Value + 1;
            }
        }
    }
}
