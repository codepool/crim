﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.result;
using YiWangYi.MsgQQ.WinUI.Forms;

namespace Team.Zyzy.Im
{
    public partial class FrmRegister :  FormQQFixSize
    {
        public FrmRegister()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        #region RegisteredUser
        private User registeredUser = null;
        public User RegisteredUser
        {
            get
            {
                return this.registeredUser;
            }
        }
        #endregion

        private void skinButton1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            string userID = this.txtUserName.Text.Trim();
            if (userID.Length == 0)
            {
                this.txtUserName.Focus();
                this.DialogResult = System.Windows.Forms.DialogResult.None;
                FrmAutoCloseMessageBox.Show("帐号不能为空！");
                return;
            }

            string pwd = this.txtPassword2.Text;
            if (pwd != this.txtPassword.Text)
            {
                FrmAutoCloseMessageBox.Show("两次输入的密码不一致！");
                this.txtPassword2.SelectAll();
                this.txtPassword2.Focus();
                this.DialogResult = System.Windows.Forms.DialogResult.None;
                return;
            }

            try
            {
                User user = new User();
                user.userName = txtUserName.Text.Trim();
                user.password = txtPassword.Text.Trim();
                user.signature = txtSignature.Text.Trim();
                user.nickName = txtNickName.Text.Trim();
                user.headImage = "";
                user.headImageThumb = "";

                string url = RequestApiInfo.BaseApi + "/register";
                Result<long?> result = HttpHelp.HttpApi<long?>(url, user, "POST", null);
                if (result.code != (int)ResultCode.SUCCESS)
                {
                    FrmAutoCloseMessageBox.Show(result.message);
                }
                else { 
                    this.registeredUser = user;
                    this.DialogResult = System.Windows.Forms.DialogResult.OK;
                    this.Close();
                }
            }
            catch (Exception ee)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.None;
                FrmAutoCloseMessageBox.Show("注册失败！" + ee.Message);
            }
        }

        private void picHeadImage_Click(object sender, EventArgs e)
        {

        }
    }
}
