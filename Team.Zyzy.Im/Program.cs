﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Interface;

namespace Team.Zyzy.Im
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //LanChartProgramInit.InstanceStart();
            FrmMain f = new FrmMain();
            ShareInfo.FormQQMain = (IFormQQMain)f;
            //FrmLogin f = new FrmLogin();
            //Form1 f = new Form1();
            Application.Run(f);
        }
    }
}
