﻿
namespace Team.Zyzy.Im
{
    partial class FrmFloatCompanyPersonInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmFloatCompanyPersonInfo));
            this.picHeaderImag = new System.Windows.Forms.PictureBox();
            this.labName = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.labPosition = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.labPhone = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.labWechar = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.labEmail = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.labQQ = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.timAutoClose = new System.Windows.Forms.Timer(this.components);
            this.qqPanelRoundFrame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeaderImag)).BeginInit();
            this.SuspendLayout();
            // 
            // qqPanelRoundFrame1
            // 
            this.qqPanelRoundFrame1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelRoundFrame1.BackgroundImage")));
            this.qqPanelRoundFrame1.Controls.Add(this.labQQ);
            this.qqPanelRoundFrame1.Controls.Add(this.labEmail);
            this.qqPanelRoundFrame1.Controls.Add(this.labWechar);
            this.qqPanelRoundFrame1.Controls.Add(this.labPhone);
            this.qqPanelRoundFrame1.Controls.Add(this.labPosition);
            this.qqPanelRoundFrame1.Controls.Add(this.labName);
            this.qqPanelRoundFrame1.Controls.Add(this.picHeaderImag);
            this.qqPanelRoundFrame1.Size = new System.Drawing.Size(608, 281);
            // 
            // picHeaderImag
            // 
            this.picHeaderImag.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.picHeaderImag.Image = global::Team.Zyzy.Im.Properties.Resources.personHead1;
            this.picHeaderImag.Location = new System.Drawing.Point(11, 13);
            this.picHeaderImag.Name = "picHeaderImag";
            this.picHeaderImag.Size = new System.Drawing.Size(219, 256);
            this.picHeaderImag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picHeaderImag.TabIndex = 0;
            this.picHeaderImag.TabStop = false;
            // 
            // labName
            // 
            this.labName.BackColor = System.Drawing.Color.Transparent;
            this.labName.Font = new System.Drawing.Font("宋体", 13.875F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labName.Location = new System.Drawing.Point(246, 14);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(347, 48);
            this.labName.TabIndex = 1;
            this.labName.Text = "张三";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labPosition
            // 
            this.labPosition.BackColor = System.Drawing.Color.Transparent;
            this.labPosition.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labPosition.Location = new System.Drawing.Point(246, 62);
            this.labPosition.Name = "labPosition";
            this.labPosition.Size = new System.Drawing.Size(347, 48);
            this.labPosition.TabIndex = 2;
            this.labPosition.Text = "总经理(飞行楼303)";
            this.labPosition.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labPhone
            // 
            this.labPhone.BackColor = System.Drawing.Color.Transparent;
            this.labPhone.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labPhone.Location = new System.Drawing.Point(246, 110);
            this.labPhone.Name = "labPhone";
            this.labPhone.Size = new System.Drawing.Size(347, 40);
            this.labPhone.TabIndex = 3;
            this.labPhone.Text = "电话：1389619****";
            this.labPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labWechar
            // 
            this.labWechar.BackColor = System.Drawing.Color.Transparent;
            this.labWechar.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labWechar.Location = new System.Drawing.Point(246, 150);
            this.labWechar.Name = "labWechar";
            this.labWechar.Size = new System.Drawing.Size(347, 40);
            this.labWechar.TabIndex = 4;
            this.labWechar.Text = "微信：zhou86****";
            this.labWechar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labEmail
            // 
            this.labEmail.BackColor = System.Drawing.Color.Transparent;
            this.labEmail.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labEmail.Location = new System.Drawing.Point(246, 230);
            this.labEmail.Name = "labEmail";
            this.labEmail.Size = new System.Drawing.Size(347, 40);
            this.labEmail.TabIndex = 5;
            this.labEmail.Text = "邮箱：zhouyz@88.com";
            this.labEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labQQ
            // 
            this.labQQ.BackColor = System.Drawing.Color.Transparent;
            this.labQQ.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labQQ.Location = new System.Drawing.Point(246, 190);
            this.labQQ.Name = "labQQ";
            this.labQQ.Size = new System.Drawing.Size(347, 40);
            this.labQQ.TabIndex = 6;
            this.labQQ.Text = "Q  Q：55018****";
            this.labQQ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timAutoClose
            // 
            this.timAutoClose.Interval = 1000;
            this.timAutoClose.Tick += new System.EventHandler(this.timAutoClose_Tick);
            // 
            // FrmFloatCompanyPersonInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 301);
            this.Name = "FrmFloatCompanyPersonInfo";
            this.Text = "FrmFloatCompanyPersonInfo";
            this.qqPanelRoundFrame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHeaderImag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labName;
        private System.Windows.Forms.PictureBox picHeaderImag;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labPosition;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labQQ;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labEmail;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labWechar;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel labPhone;
        private System.Windows.Forms.Timer timAutoClose;
    }
}