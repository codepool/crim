﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.enums
{
    public enum ChatVideoState
    {
        [Description("已连接")]
        CONNECTED = 0,
        [Description("(呼叫中)")]
        CONNECTING = 1,
        [Description("未连接")]
        NOT_CONNECTED = 2,
    }
}
