﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.enums
{
    /**
 * 响应码枚举
 *
 *
 **/
    public enum ResultCode
    {
        [Description("成功")]
        SUCCESS = 200,
        [Description("未登录")]
        NO_LOGIN = 400,
        [Description("token无效或已过期")]
        INVALID_TOKEN = 401,
        [Description("系统繁忙，请稍后再试")]
        PROGRAM_ERROR = 500,
        [Description("密码不正确")]
        PASSWOR_ERROR = 10001,
        [Description("该用户名已注册")]
        USERNAME_ALREADY_REGISTER = 10003,
        [Description("请不要输入非法内容")]
        XSS_PARAM_ERROR = 10004,
    }
}