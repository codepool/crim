﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.enums
{
    public class EnumHelp
    {
        /// <summary>
        /// 获取枚举值的描述文本
        /// </summary>
        /// <param name="e">枚举值</param>
        /// <returns></returns>
        public static string Description(Enum e)
        {
            Type enumType = e.GetType();
            var fieldInfo = enumType.GetFields().FirstOrDefault(a => a.Name == Enum.GetName(enumType, e));
            object[] obj = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (obj == null || obj.Length == 0) return null;

            DescriptionAttribute des = (DescriptionAttribute)obj[0];
            return des.Description;
        }
    }
}
