﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.enums
{
    public enum FileType
    {
        [Description("文件")]
        FILE = 0,
        [Description("图片")]
        IMAGE = 1,
        [Description("视频")]
        VIDEO = 2,
        [Description("声音")]
        AUDIO = 3,
    }
}
