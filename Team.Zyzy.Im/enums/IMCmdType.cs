﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.enums
{
    public enum IMCmdType
    {
        [Description("登录")]
        LOGIN = 0,
        [Description("心跳")]
        HEART_BEAT = 1,
        [Description("强制下线")]
        FORCE_LOGUT = 2,
        [Description("私聊消息")]
        PRIVATE_MESSAGE = 3,
        [Description("群发消息")]
        GROUP_MESSAGE = 4,
    }
}
