﻿
using System.Windows.Forms;

namespace Team.Zyzy.Im
{
    partial class FrmRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegister));
            this.pnlTx = new System.Windows.Forms.Panel();
            this.picHeadImage = new System.Windows.Forms.PictureBox();
            this.txtSignature = new System.Windows.Forms.TextBox();
            this.txtNickName = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.skinLabel5 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel3 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel4 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel2 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.skinLabel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel();
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.btnRegister = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.btnClose = new YiWangYi.MsgQQ.WinUI.BaseControls.QQButton();
            this.qqPanelFixSize1.SuspendLayout();
            this.pnlTx.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImage)).BeginInit();
            this.qqPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.Controls.Add(this.qqPanel1);
            this.qqPanelFixSize1.Size = new System.Drawing.Size(885, 604);
            this.qqPanelFixSize1.Text = "注册";
            // 
            // pnlTx
            // 
            this.pnlTx.BackColor = System.Drawing.Color.White;
            this.pnlTx.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pnlTx.Controls.Add(this.picHeadImage);
            this.pnlTx.Location = new System.Drawing.Point(654, 47);
            this.pnlTx.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTx.Name = "pnlTx";
            this.pnlTx.Size = new System.Drawing.Size(210, 210);
            this.pnlTx.TabIndex = 134;
            // 
            // picHeadImage
            // 
            this.picHeadImage.BackgroundImage = global::Team.Zyzy.Im.Properties.Resources.personHead1;
            this.picHeadImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picHeadImage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picHeadImage.Location = new System.Drawing.Point(0, 0);
            this.picHeadImage.Name = "picHeadImage";
            this.picHeadImage.Size = new System.Drawing.Size(210, 210);
            this.picHeadImage.TabIndex = 0;
            this.picHeadImage.TabStop = false;
            this.picHeadImage.Click += new System.EventHandler(this.picHeadImage_Click);
            // 
            // txtSignature
            // 
            this.txtSignature.BackColor = System.Drawing.Color.White;
            this.txtSignature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSignature.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtSignature.Location = new System.Drawing.Point(148, 335);
            this.txtSignature.Margin = new System.Windows.Forms.Padding(0);
            this.txtSignature.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtSignature.Name = "txtSignature";
            this.txtSignature.Size = new System.Drawing.Size(717, 42);
            this.txtSignature.TabIndex = 5;
            this.txtSignature.Text = "不见面不等于不思念，不联络只是因为有了CRIM。";
            // 
            // txtNickName
            // 
            this.txtNickName.BackColor = System.Drawing.Color.White;
            this.txtNickName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNickName.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtNickName.Location = new System.Drawing.Point(148, 119);
            this.txtNickName.Margin = new System.Windows.Forms.Padding(0);
            this.txtNickName.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtNickName.Name = "txtNickName";
            this.txtNickName.Size = new System.Drawing.Size(494, 42);
            this.txtNickName.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.Color.White;
            this.txtPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtPassword.Location = new System.Drawing.Point(148, 191);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(0);
            this.txtPassword.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(494, 42);
            this.txtPassword.TabIndex = 2;
            // 
            // txtPassword2
            // 
            this.txtPassword2.BackColor = System.Drawing.Color.White;
            this.txtPassword2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPassword2.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtPassword2.Location = new System.Drawing.Point(148, 263);
            this.txtPassword2.Margin = new System.Windows.Forms.Padding(0);
            this.txtPassword2.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(494, 42);
            this.txtPassword2.TabIndex = 3;
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.Color.White;
            this.txtUserName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUserName.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.txtUserName.Location = new System.Drawing.Point(148, 47);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(0);
            this.txtUserName.MinimumSize = new System.Drawing.Size(56, 56);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(494, 42);
            this.txtUserName.TabIndex = 0;
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(74, 343);
            this.skinLabel5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(86, 31);
            this.skinLabel5.TabIndex = 0;
            this.skinLabel5.Text = "签名：";
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(26, 271);
            this.skinLabel3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(134, 31);
            this.skinLabel3.TabIndex = 0;
            this.skinLabel3.Text = "确认密码：";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(74, 199);
            this.skinLabel4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(86, 31);
            this.skinLabel4.TabIndex = 0;
            this.skinLabel4.Text = "密码：";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(74, 127);
            this.skinLabel2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(86, 31);
            this.skinLabel2.TabIndex = 0;
            this.skinLabel2.Text = "姓名：";
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(74, 55);
            this.skinLabel1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(86, 31);
            this.skinLabel1.TabIndex = 0;
            this.skinLabel1.Text = "帐号：";
            // 
            // qqPanel1
            // 
            this.qqPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.qqPanel1.Controls.Add(this.btnRegister);
            this.qqPanel1.Controls.Add(this.btnClose);
            this.qqPanel1.Controls.Add(this.txtNickName);
            this.qqPanel1.Controls.Add(this.txtPassword2);
            this.qqPanel1.Controls.Add(this.txtPassword);
            this.qqPanel1.Controls.Add(this.txtSignature);
            this.qqPanel1.Controls.Add(this.txtUserName);
            this.qqPanel1.Controls.Add(this.skinLabel1);
            this.qqPanel1.Controls.Add(this.skinLabel2);
            this.qqPanel1.Controls.Add(this.skinLabel4);
            this.qqPanel1.Controls.Add(this.skinLabel3);
            this.qqPanel1.Controls.Add(this.pnlTx);
            this.qqPanel1.Controls.Add(this.skinLabel5);
            this.qqPanel1.Location = new System.Drawing.Point(3, 70);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(879, 531);
            this.qqPanel1.TabIndex = 137;
            // 
            // btnRegister
            // 
            this.btnRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRegister.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRegister.BackgroundImage")));
            this.btnRegister.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegister.Caption = "确定";
            this.btnRegister.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnRegister.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnRegister.Location = new System.Drawing.Point(235, 430);
            this.btnRegister.Margin = new System.Windows.Forms.Padding(6);
            this.btnRegister.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnRegister.MouseDownImage")));
            this.btnRegister.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnRegister.MouseMoveImage")));
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnRegister.NormalImage")));
            this.btnRegister.Size = new System.Drawing.Size(150, 70);
            this.btnRegister.TabIndex = 136;
            this.btnRegister.ToolTip = null;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Caption = "取消";
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnClose.ImageTransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.btnClose.Location = new System.Drawing.Point(509, 430);
            this.btnClose.Margin = new System.Windows.Forms.Padding(6);
            this.btnClose.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseDownImage")));
            this.btnClose.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseMoveImage")));
            this.btnClose.Name = "btnClose";
            this.btnClose.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnClose.NormalImage")));
            this.btnClose.Size = new System.Drawing.Size(150, 70);
            this.btnClose.TabIndex = 135;
            this.btnClose.ToolTip = null;
            this.btnClose.Click += new System.EventHandler(this.skinButton1_Click);
            // 
            // FrmRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 624);
            this.Name = "FrmRegister";
            this.Text = "注册";
            this.qqPanelFixSize1.ResumeLayout(false);
            this.pnlTx.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picHeadImage)).EndInit();
            this.qqPanel1.ResumeLayout(false);
            this.qqPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel2;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel4;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel5;
        private TextBox txtUserName;
        private TextBox txtPassword2;
        private TextBox txtNickName;
        private TextBox txtSignature;
        private Panel pnlTx;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQLabel skinLabel3;
        private TextBox txtPassword;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnRegister;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQButton btnClose;
        private PictureBox picHeadImage;
    }
}