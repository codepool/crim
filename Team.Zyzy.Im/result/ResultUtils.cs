﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.enums;

namespace Team.Zyzy.Im.result
{
    public class ResultUtils
    {

        public static Result<String> success()
        {
            Result<String> result = new Result<String>();
            result.setCode((int)ResultCode.SUCCESS);
            result.setMessage(EnumHelp.Description(ResultCode.SUCCESS));
            return result;
        }

        public static Result<T> success<T>(T data)
        {
            Result<T> result = new Result<T>();
            result.setCode(((int)ResultCode.SUCCESS));
            result.setMessage(EnumHelp.Description(ResultCode.SUCCESS));
            result.setData(data);
            return result;
        }

        public static Result<T> success<T>(T data, String messsage)
        {
            Result<T> result = new Result<T>();
            result.setCode((int)ResultCode.SUCCESS);
            result.setMessage(messsage);
            result.setData(data);
            return result;
        }

        public static Result<T> success<T>(String messsage)
        {
            Result<T> result = new Result<T>();
            result.setCode((int)ResultCode.SUCCESS);
            result.setMessage(messsage);
            return result;
        }

        public static Result<T> error<T>(int code, String messsage)
        {
            Result<T> result = new Result<T>();
            result.setCode(code);
            result.setMessage(messsage);
            return result;
        }


        public static Result<T> error<T>(ResultCode resultCode, String messsage)
        {
            Result<T> result = new Result<T>();
            result.setCode((int)resultCode);
            result.setMessage(messsage);
            return result;
        }

        public static Result<T> error<T>(ResultCode resultCode, String messsage, T data)
        {
            Result<T> result = new Result<T>();
            result.setCode((int)resultCode);
            result.setMessage(messsage);
            result.setData(data);
            return result;
        }

        public static Result<T> error<T>(ResultCode resultCode)
        {
            Result<T> result = new Result<T>();
            result.setCode((int)resultCode);
            result.setMessage(EnumHelp.Description(resultCode));
            return result;
        }
    }
}
