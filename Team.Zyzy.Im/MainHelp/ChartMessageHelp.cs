﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.enums;
using YiWangYi.MsgQQ.WinUI.Service.ChartControl;

namespace Team.Zyzy.Im.help
{
    public class ChartMessageHelp
    {
        #region 群组信息显示
        public static void PlayTip(GroupMessage msg)
        {
            switch ((MessageType)msg.type)
            {
                case MessageType.TEXT:
                    VoiceRecordHelp.PlayTip();
                    break;
                case MessageType.FILE:
                    VoiceRecordHelp.PlayTip();
                    break;
                case MessageType.IMAGE:
                    VoiceRecordHelp.PlayTip();
                    break;
                case MessageType.AUDIO:
                    VoiceRecordHelp.PlayTip();
                    break;
                default:

                    break;
            }
        }
        public static void DisplaySingle(ChatBox chatBox1, GroupMessage msg, long ownerId)
        {
            bool showRight = (msg.sendId == ownerId);
            switch ((MessageType)msg.type)
            {
                case MessageType.TEXT:
                    addTextMessage(chatBox1, msg, showRight,true);
                    break;
                case MessageType.FILE:
                    addFileMessage(chatBox1, msg, showRight, true);
                    break;
                case MessageType.IMAGE:
                    addImageMessage(chatBox1, msg, showRight, true);
                    break;
                case MessageType.AUDIO:
                    addVoiceMessage(chatBox1, msg, showRight, true);
                    break;
                default:

                    break;
            }
        }

        private static void addTextMessage(ChatBox chatBox1, GroupMessage msg, bool showRight,bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(msg.sendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addChatBubble(direction, msg.id, msg.content, msg.sendNickName, msg.getSendTime(), msg.sendId, headImg,msg.status, isGroupMessage, msg);
        }

        private static void addFileMessage(ChatBox chatBox1, GroupMessage msg, bool showRight, bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(msg.sendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addFileBubble(direction, msg.id, msg.content, msg.sendNickName, msg.getSendTime(), msg.sendId, headImg, msg.status, isGroupMessage, msg);
        }

        private static void addImageMessage(ChatBox chatBox1, GroupMessage msg, bool showRight, bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(msg.sendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addImgBubble(direction, msg.id, msg.content, msg.sendNickName, msg.getSendTime(), msg.sendId, headImg, msg.status, isGroupMessage, msg);
        }

        private static void addVoiceMessage(ChatBox chatBox1, GroupMessage msg, bool showRight, bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(msg.sendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addVoiceBubble(direction, msg.id, msg.content, msg.sendNickName, msg.getSendTime(), msg.sendId, headImg, msg.status,isGroupMessage, msg);
        }

        #endregion
        #region 好友信息显示
        public static void PlayTip(PrivateMessage msg)
        {
            switch ((MessageType)msg.type)
            {
                case MessageType.TEXT:
                    VoiceRecordHelp.PlayTip();
                    break;
                case MessageType.FILE:
                    VoiceRecordHelp.PlayTip();
                    break;
                case MessageType.IMAGE:
                    VoiceRecordHelp.PlayTip();
                    break;
                case MessageType.AUDIO:
                    VoiceRecordHelp.PlayTip();
                    break;
                default:

                    break;
            }
        }
        public static void DisplaySingle(ChatBox chatBox1, PrivateMessage msg,long friendId)
        {
            bool showRight = (msg.recvId == friendId);
            switch ((MessageType)msg.type)
            {
                case MessageType.TEXT:
                    addTextMessage(chatBox1, msg, showRight, friendId,false);
                    break;
                case MessageType.FILE:
                    addFileMessage(chatBox1, msg, showRight, friendId, false);
                    break;
                case MessageType.IMAGE:
                    addImageMessage(chatBox1, msg, showRight, friendId, false);
                    break;
                case MessageType.AUDIO:
                    addVoiceMessage(chatBox1, msg, showRight, friendId, false);
                    break;
                case MessageType.RECALL:
                    //撤回
                    Action action = () =>
                    {
                        chatBox1.SetMessageStatus(msg.id, (int)MessageStatus.Recall);
                    };
                    chatBox1.Invoke(action);
                    break;
                case MessageType.READED:
                    //已读
                    Action action2 = () =>
                    {
                        chatBox1.SetMessageRead(RequestApiInfo.SendId);
                    };
                    chatBox1.Invoke(action2);
                    break;
                default:

                    break;
            }
        }

        private static void addTextMessage(ChatBox chatBox1, PrivateMessage msg, bool showRight, long friendId, bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(friendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addChatBubble(direction, msg.id, msg.content, null, msg.getSendTime(), msg.sendId, headImg, msg.status, isGroupMessage, msg);
        }

        private static void addFileMessage(ChatBox chatBox1, PrivateMessage msg, bool showRight, long friendId, bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(friendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addFileBubble(direction, msg.id, msg.content, null, msg.getSendTime(), msg.sendId, headImg, msg.status, isGroupMessage, msg);
        }

        private static void addImageMessage(ChatBox chatBox1, PrivateMessage msg, bool showRight, long friendId, bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(friendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addImgBubble(direction, msg.id, msg.content, null, msg.getSendTime(), msg.sendId, headImg, msg.status, isGroupMessage, msg);
        }

        private static void addVoiceMessage(ChatBox chatBox1, PrivateMessage msg, bool showRight, long friendId,bool isGroupMessage)
        {
            string headImg = showRight ? HeadImageHelp.FriendHeadPath(RequestApiInfo.SendId) : HeadImageHelp.FriendHeadPath(friendId);
            ChatBox.BubbleSide direction = showRight ? ChatBox.BubbleSide.RIGHT : ChatBox.BubbleSide.LEFT;
            chatBox1.addVoiceBubble(direction, msg.id, msg.content, null, msg.getSendTime(), msg.sendId, headImg, msg.status, isGroupMessage, msg);
        }
        #endregion
    }
}
