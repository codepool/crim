﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Team.Zyzy.Im.config;

namespace Team.Zyzy.Im.help
{
    public class HeadImageHelp
    {
        private static string _saveFriendHeadPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "icons\\friend");
        private static string _saveGroupHeadPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "icons\\group");
        public static string getFriendImagePath
        {
            get
            {
                return _saveFriendHeadPath;
            }
        }

        private static void clearFriendHead(long id)
        {
            DirectoryInfo folder = new DirectoryInfo(_saveFriendHeadPath);
            foreach (FileInfo file in folder.GetFiles("*.*", SearchOption.AllDirectories))
            {
                string headKey = "" + id + "_";
                string filePath = Path.Combine(_saveFriendHeadPath, headKey);
                if (file.FullName.IndexOf(filePath) == 0)
                {
                    try
                    {
                        file.Delete();
                    }
                    catch
                    {

                    }
                }
            }
        }
        public static Image FriendHeadImage(long id)
        {
            try
            {
                return Image.FromFile(HeadImageHelp.FriendHeadPath(id));
            }
            catch
            {
                return global::Team.Zyzy.Im.Properties.Resources.defaultProfile;
            }
        }
        public static string FriendHeadPath(long id)
        {
            DirectoryInfo folder = new DirectoryInfo(_saveFriendHeadPath);
            FileInfo[] fileInfos = folder.GetFiles("*.*", SearchOption.AllDirectories);
            foreach (FileInfo file in fileInfos)
            {
                string headKey = "" + id + "_";
                string filePath = Path.Combine(_saveFriendHeadPath, headKey);
                if (file.FullName.IndexOf(filePath) == 0)
                {
                    return file.FullName;
                }
            }
            return PublicConstant.DefaultHeadImagePath;
        }


        public static Image FriendHeadImage(string url, long id)
        {
            try
            {
                return Image.FromFile(HeadImageHelp.FriendHeadPath(url, id));
            }
            catch { 
                return global::Team.Zyzy.Im.Properties.Resources.defaultProfile;
            }
        }

        public static string FriendHeadPath(string url, long id)
        {
            if (url == null || url.Length < 10)
            {
                return PublicConstant.DefaultHeadImagePath;
            }
            if (!Directory.Exists(_saveFriendHeadPath))
            {
                Directory.CreateDirectory(_saveFriendHeadPath);
            }
            string[] uris = url.Split('/');
            string filename = uris[uris.Length - 1];
            filename=filename.Replace(filename.Substring(filename.IndexOf('.')), ".png");
            string headKey = "" + id + "_" + filename;
            string filePath = Path.Combine(_saveFriendHeadPath, headKey);
            if (File.Exists(filePath))
            {
                return filePath;
            }
            clearFriendHead(id);
            Image img = Image.FromStream(System.Net.WebRequest.Create(url).GetResponse().GetResponseStream());
            ImageThumbnaiHelp.MakeRoundedThumbnails(img, 110, 110, filePath);
            return filePath;
        }

        public static string getGroupImagePath
        {
            get
            {
                return _saveGroupHeadPath;
            }
        }
        private static void clearGroupHead(long id)
        {
            DirectoryInfo folder = new DirectoryInfo(_saveGroupHeadPath);
            foreach (FileInfo file in folder.GetFiles("*.*", SearchOption.AllDirectories))
            {
                string headKey = "" + id + "_";
                string filePath = Path.Combine(_saveGroupHeadPath, headKey);
                if (file.FullName.IndexOf(filePath) == 0)
                {
                    try
                    {
                        file.Delete();
                    }
                    catch
                    {

                    }
                }
            }
        }
        public static string GroupHeadPath(long id)
        {
            DirectoryInfo folder = new DirectoryInfo(_saveGroupHeadPath);
            foreach (FileInfo file in folder.GetFiles("*.*", SearchOption.AllDirectories))
            {
                string headKey = "" + id + "_";
                string filePath = Path.Combine(_saveGroupHeadPath, headKey);
                if (file.FullName.IndexOf(filePath) == 0)
                {
                    return file.FullName;
                }
            }
            return PublicConstant.DefaultGroupImagePath;
        }

        public static string GroupHeadPath(string url, long id)
        {
            if (url == null || url.Length < 10)
            {
                return PublicConstant.DefaultGroupImagePath;
            }
            if (!Directory.Exists(_saveGroupHeadPath))
            {
                Directory.CreateDirectory(_saveGroupHeadPath);
            }
            string[] uris = url.Split('/');
            string filename = uris[uris.Length - 1];
            filename = filename.Replace(filename.Substring(filename.IndexOf('.')), ".png");
            string headKey = "" + id + "_" + filename;
            string filePath = Path.Combine(_saveGroupHeadPath, headKey);
            if (File.Exists(filePath))
            {
                return filePath;
            }
            clearGroupHead(id);
            Image img = Image.FromStream(System.Net.WebRequest.Create(url).GetResponse().GetResponseStream());
            ImageThumbnaiHelp.MakeRoundedThumbnails(img, 110, 110, filePath);
            return filePath;
        }
        public static Image GroupHeadImage(long id)
        {
            try
            {
                return Image.FromFile(HeadImageHelp.GroupHeadPath(id));
            }
            catch
            {
                return global::Team.Zyzy.Im.Properties.Resources.defaultProfile;
            }
        }
        public static Image GroupHeadImage(string url, long id)
        {
            try
            {
                return Image.FromFile(HeadImageHelp.GroupHeadPath(url, id));
            }
            catch
            {
                return global::Team.Zyzy.Im.Properties.Resources.defaultProfile;
            }
        }
        #region 删除旧头像
        public class FIleNameLastTimeComparer : IComparer<FileInfo>
        {
            char key = '_';
            public int Compare(FileInfo x, FileInfo y)
            {
                if (x.Name.IndexOf(key) > 0 && y.Name.IndexOf(key) > 0)
                {
                    try
                    {
                        long x1 = long.Parse(x.Name.Split(key)[0]);
                        long y1 = long.Parse(y.Name.Split(key)[0]);
                        if (x1 == y1)
                        {
                            return y.LastWriteTime.CompareTo(x.LastWriteTime);
                        }
                        else
                        {
                            return x1.CompareTo(y1);
                        }
                    }
                    catch
                    {
                        return y.LastWriteTime.CompareTo(x.LastWriteTime);
                    }
                }
                else
                {
                    return y.LastWriteTime.CompareTo(x.LastWriteTime);
                }
            }
        }

        public static void ClearMoreFiles()
        {
            try
            {
                DirectoryInfo folder = new DirectoryInfo(_saveFriendHeadPath);
                clearMoreFiles(folder);
                folder = new DirectoryInfo(_saveGroupHeadPath);
                clearMoreFiles(folder);
            }
            catch
            {

            }
        }

        private static void clearMoreFiles(DirectoryInfo folder)
        {
            FileInfo[] fileInfos = folder.GetFiles("*.*", SearchOption.AllDirectories);
            Array.Sort<FileInfo>(fileInfos, new FIleNameLastTimeComparer());
            long id = 0;
            char key = '_';
            for (int i = 0; i < fileInfos.Length; i++)
            {
                long id1 = long.Parse(fileInfos[i].Name.Split(key)[0]);
                if (id == id1)
                {
                    try
                    {
                        fileInfos[i].Delete();
                    }
                    catch { }
                }
                else
                {
                    id = id1;
                }
            }
        }
        #endregion

    }
}