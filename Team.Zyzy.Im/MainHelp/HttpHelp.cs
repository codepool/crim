﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.Business.vo;

namespace Team.Zyzy.Im.help
{
    public class HttpHelp
    {
        #region 调用API

        public static T HttpApi<T>(string url, Object model, string type = "POST")
        {
            if (url.IndexOf(RequestApiInfo.BaseApi) == 0)
            {
                url = url.Replace(RequestApiInfo.BaseApi, "");
            }
            Result<T> resultModel = HttpHelp.HttpApi<T>(RequestApiInfo.BaseApi + url, model, type, RequestApiInfo.AccessToken);
            if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                return resultModel.data;
            }
            else if (resultModel.code == (int)ResultCode.INVALID_TOKEN)
            {
                string refreshTokenUrl = RequestApiInfo.BaseApi + "/refreshToken";
                Result<LoginVO> result = HttpApi<LoginVO>(refreshTokenUrl, null, "PUT", null);
                RequestApiInfo.AccessToken = result.data.accessToken;
                RequestApiInfo.AccessTokenExpiresIn = result.data.accessTokenExpiresIn;
                RequestApiInfo.RefreshToken = result.data.refreshToken;
                RequestApiInfo.RefreshTokenExpiresIn = result.data.refreshTokenExpiresIn;
                resultModel = HttpHelp.HttpApi<T>(RequestApiInfo.BaseApi + url, model, type, RequestApiInfo.AccessToken);
                return resultModel.data;
            }
            else if (resultModel.code == (int)ResultCode.NO_LOGIN)
            {
                MessageBoxShow("未登录",MessageBoxIcon.Error);
            }
            else if (resultModel.code == (int)ResultCode.INVALID_TOKEN)
            {
                MessageBoxShow("token无效或已过期", MessageBoxIcon.Error);
            }
            else if (resultModel.code == (int)ResultCode.PROGRAM_ERROR)
            {
                MessageBoxShow(resultModel.message, MessageBoxIcon.Error);
            }
            else if (resultModel.code == (int)ResultCode.PASSWOR_ERROR)
            {
                MessageBoxShow("密码不正确", MessageBoxIcon.Error);
            }
            else if (resultModel.code == (int)ResultCode.SUCCESS)
            {
                return resultModel.data;
            }
            throw new Exception(resultModel.message);
        }

        public static Result<T> HttpApi<T>(string url, Object model, string type = "POST", string token = null)
        {
            var jsonContent = "";
            if (type.ToUpper() == "GET" || type.ToUpper() == "DELETE" || model is string)
            {
                url = getQueryString(url, model);
            }
            else
            {
                jsonContent = (model == null) ? "" : JsonConvert.SerializeObject(model);
            }
            String result = HttpHelp.HttpApi(url, jsonContent, type, token);
            Result<T> resultModel = JsonConvert.DeserializeObject<Result<T>>(result);
            return resultModel;
        }

        /// <summary>
        /// Post调用API返回JSON
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="dic">参数值</param>
        /// <param name="type">调用类型</param>
        /// <returns></returns>
        public static string HttpApi(string url, string jsonStr = "", string type = "POST", string token = null)
        {
            if (string.IsNullOrEmpty(url))
            {
                //LogHelper.WriteLog("API链接地址为空，配置INI文件缺失");
                return null;
            }

            string result = "";//返回结果
            try
            {
                Encoding encoding = Encoding.UTF8;
                HttpWebResponse response;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);//webrequest请求api地址  
                if (token != null)//是否加上Token令牌
                {
                    //var headers = request.Headers;
                    //headers["Authorization"] = "Bearer " + Globas.token.token;
                    //request.Headers = headers;
                    //request.Headers.Add("Authorization", "Bearer " + token);
                    request.Headers.Add("accessToken", token);
                    request.Credentials = CredentialCache.DefaultCredentials;
                }
                request.Accept = "text/html,application/xhtml+xml,*/*";
                request.ContentType = "application/json";
                request.Method = type.ToUpper().ToString();//get或者post
                if (!string.IsNullOrEmpty(jsonStr))//Get请求无需拼接此参数
                {
                    byte[] buffer = encoding.GetBytes(jsonStr);
                    request.ContentLength = buffer.Length;
                    request.GetRequestStream().Write(buffer, 0, buffer.Length);
                }

                try
                {
                    response = (HttpWebResponse)request.GetResponse();
                }
                catch (WebException ex)
                {
                    response = (HttpWebResponse)ex.Response;
                }
                using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
                {
                    result = reader.ReadToEnd();
                    reader.Close();
                }
                if (response.StatusCode != HttpStatusCode.OK)//返回响应码非成功格式化数据后返回
                {
                    result = "Exception:" + JsonConvert.DeserializeObject<string>(result);
                }
                return result;
            }
            catch (WebException ex)
            {
                return "Exception:" + ex.Message;
            }
        }

        #endregion

        #region 拼接Get请求地址
        /// <summary>
        /// 将对象属性拼接到Url
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="req">实例对象</param>
        /// <returns></returns>
        public static string getQueryString(string url, object req)
        {
            if (req == null)
            {
                return url;
            }
            StringBuilder query = new StringBuilder();
            if (!url.Contains("&"))
                query.Append("?");
            if (req is string)
            {
                query.Append(req);
            }
            else
            {
                PropertyInfo[] propertys = req.GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.CanRead)
                    {
                        query.Append($@"{pi.Name}={pi.GetValue(req, null)}&");
                    }
                }
            }
            return url + query.ToString();
        }
        #endregion

        #region 信息弹出框

        public static void MessageBoxShow(string content)
        {
            Action action = () =>
            {
                YiWangYi.MsgQQ.WinUI.Forms.FrmAutoCloseMessageBox.Show(content);
            };
            Form frm = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as Form;
            frm.Invoke(action);
        }
        public static void MessageBoxShow(string content, MessageBoxIcon information)
        {
            Action action = () =>
            {
                YiWangYi.MsgQQ.WinUI.Forms.FrmAutoCloseMessageBox.Show(content, information);
            };
            Form frm = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as Form;
            if (frm.IsHandleCreated)
            {
                frm.Invoke(action);
            }
        }

        public static void MessageBoxShow(string caption, string content, MessageBoxIcon information)
        {
            Action action = () =>
            {
                YiWangYi.MsgQQ.WinUI.Forms.FrmAutoCloseMessageBox.Show(caption, content, information);
            };
            Form frm = YiWangYi.MsgQQ.WinUI.ShareInfo.FormQQMain as Form;
            frm.Invoke(action);
        }
        #endregion

    }
}