﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.help
{
    public class ImageThumbnaiHelp
    {
        public static void MakeThumbnail(Image img_OriginalImage, string thumbnailPath, int width, int height, ThumbnailMode mode)
        {
            ImageFormat tFormat = img_OriginalImage.RawFormat;
            generateImage(thumbnailPath, width, height, mode, img_OriginalImage, tFormat);
        }
        public static void MakeThumbnail(string originalImagePath, string thumbnailPath, int width, int height, ThumbnailMode mode)
        {
            System.Drawing.Image img_OriginalImage = null;
            ImageFormat tFormat = null;
            try
            {
                img_OriginalImage = (Bitmap)System.Drawing.Image.FromFile(originalImagePath);
                tFormat = img_OriginalImage.RawFormat;
            }
            catch (Exception ex)
            {
                if (img_OriginalImage != null)
                {
                    img_OriginalImage.Dispose();
                }
                throw new Exception("压缩图片:" + originalImagePath + "失败！\r\n" + ex.Message);
            }

            generateImage(thumbnailPath, width, height, mode, img_OriginalImage, tFormat);
        }

        private static void generateImage( string thumbnailPath, int width, int height, ThumbnailMode mode, Image img_OriginalImage, ImageFormat tFormat)
        {
            int i_ToWidth = width;
            int i_ToHeight = height;

            int x = 0;
            int y = 0;

            int i_OriginalWidth = img_OriginalImage.Width;
            int i_OriginalHeight = img_OriginalImage.Height;

            switch (mode)
            {
                case ThumbnailMode.Width:
                    i_ToHeight = img_OriginalImage.Height * width / img_OriginalImage.Width;
                    break;
                case ThumbnailMode.Height:
                    i_ToWidth = img_OriginalImage.Width * height / img_OriginalImage.Height;
                    break;
                case ThumbnailMode.Cut:
                    if ((double)img_OriginalImage.Width / (double)img_OriginalImage.Height > (double)i_ToWidth / (double)i_ToHeight)
                    {
                        i_OriginalHeight = img_OriginalImage.Height;
                        i_OriginalWidth = img_OriginalImage.Height * i_ToWidth / i_ToHeight;
                        y = 0;
                        x = (img_OriginalImage.Width - i_OriginalWidth) / 2;
                    }
                    else
                    {
                        i_OriginalWidth = img_OriginalImage.Width;
                        i_OriginalHeight = img_OriginalImage.Width * height / i_ToWidth;
                        x = 0;
                        y = (img_OriginalImage.Height - i_OriginalHeight) / 2;
                    }
                    break;
                default:
                    break;
            }

            //新建一个BMP图片
            System.Drawing.Image img_BitMap = new System.Drawing.Bitmap(i_ToWidth, i_ToHeight);
            //新建一个画板
            System.Drawing.Graphics gp = Graphics.FromImage(img_BitMap);
            //设置高质量插值法
            gp.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            //设置高质量、低速度呈现平滑程度
            gp.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            gp.CompositingQuality = CompositingQuality.HighQuality;

            //清空画布并以透明背景色填充
            gp.Clear(Color.Transparent);
            //指定位置并按大小绘制原图片的指定部分
            gp.DrawImage(img_OriginalImage, new Rectangle(0, 0, i_ToWidth, i_ToHeight), new Rectangle(x, y, i_OriginalWidth, i_OriginalHeight), GraphicsUnit.Pixel);

            try
            {
                EncoderParameters ep = new EncoderParameters();
                long[] qy = new long[1];
                qy[0] = 100;//设置压缩的比例1-100  
                EncoderParameter eParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, qy);
                ep.Param[0] = eParam;

                ImageCodecInfo[] arrayICI = ImageCodecInfo.GetImageEncoders();
                ImageCodecInfo jpegICIinfo = null;
                for (int i = 0; i < arrayICI.Length; i++)
                {
                    if (arrayICI[i].FormatDescription.Equals("JPEG"))
                    {
                        jpegICIinfo = arrayICI[i];
                        break;
                    }
                }
                if (jpegICIinfo != null && Equals(tFormat, ImageFormat.Jpeg))
                {
                    //以JPG格式保存图片
                    img_BitMap.Save(thumbnailPath, jpegICIinfo, ep);
                    //Const.iSuccessCount++;
                }
                else
                {
                    img_BitMap.Save(thumbnailPath, tFormat);
                    //Const.iSuccessCount++;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("复制图片失败！\r\n" + ex.Message + ex.StackTrace);
            }
            finally
            {
                if (img_OriginalImage != null)
                {
                    img_OriginalImage.Dispose();
                }
                if (img_BitMap != null)
                {
                    img_BitMap.Dispose();
                }
                if (gp != null)
                {
                    gp.Dispose();
                }
                //GC.Collect();
            }
        }

        public enum ThumbnailMode
        {
            /// <summary>
            /// 指定宽度，高度按照比例缩放
            /// </summary>
            Width = 0,

            /// <summary>
            /// 指定高度，宽度按照比例缩放
            /// </summary>
            Height = 1,

            /// <summary>
            /// 按照指定的高度和宽度剪裁
            /// </summary>
            Cut = 2,

            /// <summary>
            /// 指定宽度，高度按照比例缩放
            /// </summary>
            None = 3
        }

        #region 生成圆角缩略图
        public static void MakeRoundedThumbnails(Image myImage,  int newWidth, int newHeight,string newSaveFilePath)
        {
            //先将图片处理为圆角
            myImage = DrawTransparentRoundCornerImage(myImage);
            //取得图片大小
            System.Drawing.Size mySize = new Size(newWidth, newHeight);
            //新建一个bmp图片
            System.Drawing.Image bitmap = new System.Drawing.Bitmap(mySize.Width, mySize.Height);
            //新建一个画板
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
            //设置高质量插值法
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            //设置高质量,低速度呈现平滑程度
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //清空一下画布
            g.Clear(Color.Transparent);
            //在指定位置画图
            g.DrawImage(myImage, new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
            new System.Drawing.Rectangle(0, 0, myImage.Width, myImage.Height),
            System.Drawing.GraphicsUnit.Pixel);
            //保存缩略图,强制保存为png格式，否则圆角部分无法呈现透明
            bitmap.Save(newSaveFilePath, ImageFormat.Png);
            g.Dispose();
            myImage.Dispose();
            bitmap.Dispose();
        }

        //图片处理为圆角
        public static System.Drawing.Image DrawTransparentRoundCornerImage(System.Drawing.Image image)
        {
            Bitmap bm = new Bitmap(image.Width, image.Height);
            Graphics g = Graphics.FromImage(bm);
            g.FillRectangle(Brushes.Transparent, new Rectangle(0, 0, image.Width, image.Height));

            using (System.Drawing.Drawing2D.GraphicsPath path = CreateRoundedRectanglePath(new Rectangle(0, 0, image.Width, image.Height), image.Width / 10))
            {
                g.SetClip(path);
            }

            g.DrawImage(image, new Rectangle(0, 0, image.Width, image.Height), new Rectangle(0, 0, image.Width, image.Height), GraphicsUnit.Pixel);
            g.Dispose();

            return bm;
        }
        //设置图片四个边角弧度
        private static System.Drawing.Drawing2D.GraphicsPath CreateRoundedRectanglePath(Rectangle rect, int cornerRadius)
        {
            System.Drawing.Drawing2D.GraphicsPath roundedRect = new System.Drawing.Drawing2D.GraphicsPath();
            roundedRect.AddArc(rect.X, rect.Y, cornerRadius * 2, cornerRadius * 2, 180, 90);
            roundedRect.AddLine(rect.X + cornerRadius, rect.Y, rect.Right - cornerRadius * 2, rect.Y);
            roundedRect.AddArc(rect.X + rect.Width - cornerRadius * 2, rect.Y, cornerRadius * 2, cornerRadius * 2, 270, 90);
            roundedRect.AddLine(rect.Right, rect.Y + cornerRadius * 2, rect.Right, rect.Y + rect.Height - cornerRadius * 2);
            roundedRect.AddArc(rect.X + rect.Width - cornerRadius * 2, rect.Y + rect.Height - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 0, 90);
            roundedRect.AddLine(rect.Right - cornerRadius * 2, rect.Bottom, rect.X + cornerRadius * 2, rect.Bottom);
            roundedRect.AddArc(rect.X, rect.Bottom - cornerRadius * 2, cornerRadius * 2, cornerRadius * 2, 90, 90);
            roundedRect.AddLine(rect.X, rect.Bottom - cornerRadius * 2, rect.X, rect.Y + cornerRadius * 2);
            roundedRect.CloseFigure();
            return roundedRect;
        }
        #endregion
    }
}