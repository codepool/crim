﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.help
{
    public class ScreenImageHelp
    {
        private static string _savePath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "temp\\screen");
        public static string getImagePath
        {
            get
            {
                return _savePath;
            }
        }

        private static void clearTemp()
        {
            DirectoryInfo folder = new DirectoryInfo(_savePath);
            foreach (FileInfo file in folder.GetFiles("*.*", SearchOption.AllDirectories))
            {
                    file.Delete();
            }
        }

        public static string ScreenPath(Image img)
        {
            if (!Directory.Exists(_savePath))
            {
                Directory.CreateDirectory(_savePath);
            }
            string filename = DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";
            string filePath = Path.Combine(_savePath, filename);
            clearTemp();
            img.Save(filePath);
            return filePath;
        }
    }
}