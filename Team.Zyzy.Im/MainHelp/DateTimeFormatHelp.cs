﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.help
{
    public class DateTimeFormatHelp
    {
		public static bool IsYestday(DateTime dateTime)
		{
			var timeDiff = DateTime.Now.Subtract(dateTime).TotalHours;
			if(timeDiff<24 && timeDiff >= 0)
            {
				return true;
            }
			return false;
		}
		public static bool IsYear(DateTime dateTime)
		{
			if (DateTime.Now.Year==dateTime.Year)
			{
				return true;
			}
			return false;
		}
		public static string ToTimeText(DateTime dateTime)
		{
			var timeDiff = DateTime.Now.Subtract(dateTime).TotalMilliseconds;
			var timeText = "";
			if (timeDiff <= 60000)
			{ //一分钟内
				timeText = "刚刚";
			}
			else if (timeDiff > 60000 && timeDiff < 3600000)
			{
				//1小时内
				timeText = Math.Floor(timeDiff / 60000) + "分钟前";
			}
			else if (timeDiff >= 3600000 && timeDiff < 86400000 && !IsYestday(dateTime))
			{
				//今日
				timeText = dateTime.ToString("HH:mm");
			}
			else if (IsYestday(dateTime))
			{
				//昨天
				timeText = "昨天" + dateTime.ToString("HH:mm");
			}else if (IsYear(dateTime))
			{
				//今年
				timeText = dateTime.ToString("MM/dd HH:mm");
			}
			else
			{
				//不属于今年
				timeText = dateTime.ToString("yyyy-MM-dd");
			}
			return timeText;
		}
    }
}
