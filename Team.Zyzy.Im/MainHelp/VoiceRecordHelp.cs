﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.config;

namespace Team.Zyzy.Im.help
{
    public class VoiceRecordHelp
    {
        private static VoiceInfoConfig voiceInfoConfig = null;

        [DllImport("winmm.dll")]
        public static extern int mciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);


        [DllImport("winmm.DLL", EntryPoint = "PlaySound", SetLastError = true, CharSet = CharSet.Unicode, ThrowOnUnmappableChar = true)]
        private static extern bool PlaySound(string szSound, System.IntPtr hMod, PlaySoundFlags flags);

        public static void StartRecord()
        {
            mciSendString("set wave bitpersample 8", "", 0, 0);  //设为8位
            mciSendString("set wave samplespersec 11025", "", 0, 0);//设置每个声道播放和记录时的样本频率为20Hz-20000Hz
            mciSendString("set wave channels 2", "", 0, 0);  //设为立体声
            mciSendString("set wave format tag pcm", "", 0, 0);//设置PCM（脉冲编码调制）
            mciSendString("open new type WAVEAudio alias movie", "", 0, 0); //打开一个新的录音文件
            mciSendString("record movie", "", 0, 0);  //开始录音
        }

        public static void StopRecord()
        {
            mciSendString("stop movie", "", 0, 0);//停止录音
            mciSendString("save movie" + " " + "recordvoice.wav", "", 0, 0);//保存录音文件，recordvoice.wav为录音文件
            mciSendString("close movie", "", 0, 0);   //关闭录音
        }

        public static void StopRecord(string fileName)
        {
            mciSendString("stop movie", "", 0, 0);//停止录音
            mciSendString("save movie" + " " + fileName, "", 0, 0);//保存录音文件，recordvoice.wav为录音文件
            mciSendString("close movie", "", 0, 0);   //关闭录音
        }

        public static void PlayTip()
        {
            if (voiceInfoConfig == null)
            {
                voiceInfoConfig = VoiceInfoConfig.LoadConfig();
            }
            if (voiceInfoConfig.VoiceInfoData.OpenTipVoice)
            {
                PlaySound(PublicConstant.TipAudioPath, new System.IntPtr(), PlaySoundFlags.SND_ASYNC);
            }
        }

        private static System.Media.SoundPlayer _sp = null;
        public static void PlayCall()
        {
            if (voiceInfoConfig == null)
            {
                voiceInfoConfig = VoiceInfoConfig.LoadConfig();
            }
            if (voiceInfoConfig.VoiceInfoData.OpenTipVoice)
            {
                _sp = new System.Media.SoundPlayer(PublicConstant.CallAudioPath);
                _sp.PlayLooping();
            }
        }
        public static void StopPlayCall()
        {
            if (_sp != null)
            {
                _sp.Stop();
            }
        }
    }

    [System.Flags]
    public enum PlaySoundFlags : int
    {
        SND_SYNC = 0x0000,
        SND_ASYNC = 0x0001,
        SND_NODEFAULT = 0x0002,
        SND_LOOP = 0x0008,
        SND_NOSTOP = 0x0010,
        SND_NOWAIT = 0x00002000,
        SND_FILENAME = 0x00020000,
        SND_RESOURCE = 0x00040004
    }
}
