﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.enums;
using Team.Zyzy.Im.result;
using Team.Zyzy.Im.Business.vo;

namespace Team.Zyzy.Im.help
{
    public class HttpFileHelp
    {
        public static void UploadImage(FileInfo file, UploadFileCompletedEventHandler success, UploadProgressChangedEventHandler change)
        {
            string url = RequestApiInfo.BaseApi + "/image/upload";
            using (WebClient client = new WebClient())
            {
                client.UploadFileCompleted += delegate (object sender, UploadFileCompletedEventArgs e) { success(file,e); };
                client.Headers.Add("accessToken", RequestApiInfo.AccessToken);
                client.UploadProgressChanged += change;
                client.UploadFileAsync(new Uri(url), "POST", file.FullName);
            }
        }

        public static void UploadFile(FileInfo file, UploadFileCompletedEventHandler success, UploadProgressChangedEventHandler change)
        {
            string url = RequestApiInfo.BaseApi + "/file/upload";
            using (WebClient client = new WebClient())
            {
                client.UploadFileCompleted += delegate (object sender, UploadFileCompletedEventArgs e) { success(file, e); };
                client.Headers.Add("accessToken", RequestApiInfo.AccessToken);
                client.UploadProgressChanged += change;
                client.UploadFileAsync(new Uri(url), "POST", file.FullName);
            }
        }

        /// <summary>
        /// Post调用API返回JSON
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="dic">参数值</param>
        /// <param name="type">调用类型</param>
        /// <returns></returns>
        public static void DownloadFile2(string url, string filename, string jsonStr, string type, System.Windows.Forms.ProgressBar prog, String token = null)
        {
            string result = "";//返回结果
            try
            {
                Encoding encoding = Encoding.UTF8;
                HttpWebResponse response;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);//webrequest请求api地址  
                if (token != null)//是否加上Token令牌
                {
                    //var headers = request.Headers;
                    //headers["Authorization"] = "Bearer " + Globas.token.token;
                    //request.Headers = headers;
                    request.Headers.Add("Authorization", "Bearer " + token);
                    request.Credentials = CredentialCache.DefaultCredentials;
                }
                request.Accept = "text/html,application/xhtml+xml,*/*";
                //request.ContentType = "application/json";
                request.ContentType = "application/x-gzip";
                request.Method = type.ToUpper().ToString();//get或者post
                if (!string.IsNullOrEmpty(jsonStr))//Get请求无需拼接此参数
                {
                    byte[] buffer = encoding.GetBytes(jsonStr);
                    request.ContentLength = buffer.Length;
                    request.GetRequestStream().Write(buffer, 0, buffer.Length);
                }

                try
                {
                    System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)request.GetResponse();
                    long totalBytes = myrp.ContentLength;

                    if (prog != null && totalBytes>0)
                    {
                        prog.Maximum = (int)totalBytes;
                    }

                    System.IO.Stream st = myrp.GetResponseStream();
                    System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
                    long totalDownloadedByte = 0;
                    byte[] by = new byte[1024];
                    int osize = st.Read(by, 0, (int)by.Length);
                    while (osize > 0)
                    {
                        totalDownloadedByte = osize + totalDownloadedByte;
                        System.Windows.Forms.Application.DoEvents();
                        so.Write(by, 0, osize);

                        if (prog != null && totalDownloadedByte>0)
                        {
                            prog.Value = (int)totalDownloadedByte;
                        }
                        osize = st.Read(by, 0, (int)by.Length);
                    }
                    so.Close();
                    st.Close();
                }
                catch (System.Exception ex)
                {
                    throw;
                }
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }

            /// <summary>
            /// 下载文件(显示进度)
            /// </summary>
            /// <param name="URL"></param>
            /// <param name="filename"></param>
            /// <param name="prog"></param>
            public static void DownloadFile(string URL, string filename, System.Windows.Forms.ProgressBar prog, String token = null)
        {
            try
            {
                System.Net.HttpWebRequest Myrq = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(URL);
                if (token != null)//是否加上Token令牌
                {
                    Myrq.Headers.Add("Authorization", "Bearer " + token);
                    Myrq.Credentials = CredentialCache.DefaultCredentials;
                }
                System.Net.HttpWebResponse myrp = (System.Net.HttpWebResponse)Myrq.GetResponse();
                long totalBytes = myrp.ContentLength;

                if (prog != null)
                {
                    prog.Maximum = (int)totalBytes;
                }

                System.IO.Stream st = myrp.GetResponseStream();
                System.IO.Stream so = new System.IO.FileStream(filename, System.IO.FileMode.Create);
                long totalDownloadedByte = 0;
                byte[] by = new byte[1024];
                int osize = st.Read(by, 0, (int)by.Length);
                while (osize > 0)
                {
                    totalDownloadedByte = osize + totalDownloadedByte;
                    System.Windows.Forms.Application.DoEvents();
                    so.Write(by, 0, osize);

                    if (prog != null)
                    {
                        prog.Value = (int)totalDownloadedByte;
                    }
                    osize = st.Read(by, 0, (int)by.Length);
                }
                so.Close();
                st.Close();
            }
            catch (System.Exception ex)
            {
                throw;
            }
        }
    }
}