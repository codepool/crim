﻿
namespace Team.Zyzy.Im
{
    partial class FrmSystemService
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            YiWangYi.MsgQQ.WinUI.General.SideBar.SbGroup sbGroup1 = new YiWangYi.MsgQQ.WinUI.General.SideBar.SbGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSystemService));
            ((System.ComponentModel.ISupportInitialize)(this.spcContent)).BeginInit();
            this.spcContent.Panel1.SuspendLayout();
            this.spcContent.Panel2.SuspendLayout();
            this.spcContent.SuspendLayout();
            this.pnlBottom.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.qqPanelFixSize1.SuspendLayout();
            this.SuspendLayout();
            // 
            // spcContent
            // 
            this.spcContent.Location = new System.Drawing.Point(3, 0);
            this.spcContent.Size = new System.Drawing.Size(1266, 850);
            // 
            // qSideBar1
            // 
            this.qSideBar1.BackColor = System.Drawing.Color.Black;
            this.qSideBar1.Size = new System.Drawing.Size(151, 850);
            sbGroup1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(19)))), ((int)(((byte)(145)))), ((int)(((byte)(211)))));
            sbGroup1.Bounds = new System.Drawing.Rectangle(0, 0, 151, 32);
            sbGroup1.Enabled = true;
            sbGroup1.Height = 32;
            sbGroup1.LeftX = 0;
            sbGroup1.MouseState = YiWangYi.MsgQQ.WinUI.QQListBar.MouseState.Normal;
            sbGroup1.Name = null;
            sbGroup1.Tag = null;
            sbGroup1.Text = "基本设置";
            sbGroup1.TopY = 0;
            sbGroup1.Visible = true;
            sbGroup1.Width = 151;
            this.qSideBar1.VisibleGroup = sbGroup1;
            this.qSideBar1.VisibleGroupIndex = 0;
            // 
            // qpnlContent
            // 
            this.qpnlContent.BackColor = System.Drawing.Color.Black;
            this.qpnlContent.Size = new System.Drawing.Size(1111, 850);
            // 
            // btnClose
            // 
            this.btnClose.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnClose.BackgroundImage")));
            this.btnClose.Location = new System.Drawing.Point(1138, 0);
            this.btnClose.MouseDownImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseDownImage")));
            this.btnClose.MouseMoveImage = ((System.Drawing.Image)(resources.GetObject("btnClose.MouseMoveImage")));
            this.btnClose.NormalImage = ((System.Drawing.Image)(resources.GetObject("btnClose.NormalImage")));
            // 
            // pnlContent
            // 
            this.pnlContent.Location = new System.Drawing.Point(4, 35);
            this.pnlContent.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.pnlContent.Size = new System.Drawing.Size(1272, 850);
            // 
            // qqPanelFixSize1
            // 
            this.qqPanelFixSize1.BackgroundColor = ((System.Drawing.Bitmap)(resources.GetObject("qqPanelFixSize1.BackgroundColor")));
            this.qqPanelFixSize1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("qqPanelFixSize1.BackgroundImage")));
            this.qqPanelFixSize1.BackgroundPattern = ((System.Drawing.Bitmap)(resources.GetObject("qqPanelFixSize1.BackgroundPattern")));
            // 
            // FrmSystemService
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundColor = ((System.Drawing.Bitmap)(resources.GetObject("$this.BackgroundColor")));
            this.ClientSize = new System.Drawing.Size(1300, 940);
            this.Name = "FrmSystemService";
            this.spcContent.Panel1.ResumeLayout(false);
            this.spcContent.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spcContent)).EndInit();
            this.spcContent.ResumeLayout(false);
            this.pnlBottom.ResumeLayout(false);
            this.pnlContent.ResumeLayout(false);
            this.qqPanelFixSize1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}