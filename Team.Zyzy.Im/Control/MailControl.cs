﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Team.Zyzy.Im.Control
{
    public partial class MailControl : UserControl
    {
        public MailControl()
        {
            InitializeComponent();
            DgvStyle1(this.dgvMail);
            this.Load += MailControl_Load;
            this.SizeChanged += MailControl_SizeChanged;
        }

        private void MailControl_SizeChanged(object sender, EventArgs e)
        {
            picLoad.Width = this.Width;
            picLoad.Height = this.Height;
            picLoad.Location = new Point(0, 0);
            picLoad.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        private void MailControl_Load(object sender, EventArgs e)
        {
            Task t = new Task(() =>
            {
                initMails();
            });
            t.Start();
        }

        public void DgvStyle1(DataGridView dgv)
        {
            //奇数行的背景色
            dgv.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.AliceBlue;
            dgv.AlternatingRowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Blue;
            dgv.AlternatingRowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dgv.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            //默认的行样式
            dgv.RowsDefaultCellStyle.BackColor = System.Drawing.Color.White;
            dgv.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            dgv.RowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Blue;
            //数据网格颜色
            //dgv.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            //列标题的宽度
            dgv.ColumnHeadersHeight = 28;
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void initMails()
        {
            Addresser addresser = new Addresser();
            addresser.account = "www.zyzy.team@qq.com";
            addresser.port = 993;
            addresser.password = "ctvejorxuhxvbdee";
            //addresser.password = "zhoutx@88.com";
            addresser.server = "imap.qq.com";
            List<MailInfo> messages = MailControlHelp.GetUnreadEmails(addresser);
            Action action1 = () =>
            {
                dgvMail.DataSource = messages;
                if (dgvMail.Rows.Count > 0)
                {
                    picLoad.Visible = false;
                    dgvMail.Rows[0].Selected = false;
                }
            };
            Invoke(action1);
        }
    }
}
