﻿using CefSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;
using Team.Zyzy.Im.help;
using Team.Zyzy.Im.MainChar;
using Team.Zyzy.Im.mainService;
using YiWangYi.MsgQQ.WinUI.Forms;
using YiWangYi.MsgQQ.WinUI.Service.MainService;

namespace Team.Zyzy.Im.Control
{
    public partial class LeftTestControl : UserControl
    {
        public LeftTestControl()
        {
            InitializeComponent();
            this.Load += LeftTestControl_Load;
        }

        private void LeftTestControl_Load(object sender, EventArgs e)
        {

        }

        private bool _testFirst = true;
        private void btnTestNewMessage_Click(object sender, EventArgs e)
        {
            LeftBarSwitchService instance = LeftBarSwitchService.GetInstance();
            instance.SetUnReadCount("mail", _testFirst ? 20 : 0);
            instance.SetUnReadCount("im", _testFirst ? 8 : 0);
            instance.SetUnReadCount("hand", _testFirst ? 9 : 0);
            instance.SetUnReadCount("notice", _testFirst ? 125 : 0);
            _testFirst = !_testFirst;
        }

        private void btnTestSendSystemMessage_Click(object sender, EventArgs e)
        {
            PrivateMessage privateMessage = new PrivateMessage();
            privateMessage.type = (int)MessageType.TEXT;
            privateMessage.content = "系统发送信息测试";
            privateMessage.sendId = RequestApiInfo.SendId;
            privateMessage.status = -1;
            privateMessage.setSendTime(DateTime.Now);
            for(int i = 1; i < 8; i++)
            {
                privateMessage.recvId = i;
                sendPrivateMessage(privateMessage);
            }

        }

        private long sendPrivateMessage(PrivateMessage privateMessage)
        {
            long msgId = 0;
            try
            {
                string url = "/message/private/send";
                msgId = HttpHelp.HttpApi<long>(url, privateMessage, "POST");
            }
            catch (Exception ex)
            {

                try
                {
                    string para = "friendId=" + privateMessage.recvId;
                    string url2 = "/friend/add";
                    HttpHelp.HttpApi<long?>(url2, para, "POST");
                    sendPrivateMessage(privateMessage);
                }
                catch
                {
                    msgId = 0;
                    FrmAutoCloseMessageBox.Show("系统错误：" + ex.Message, MessageBoxIcon.Error);
                }
            }
            return msgId;
        }

        private void btnTestBrowser_Click(object sender, EventArgs e)
        {
            FrmGroupRTCVideo frmGroupRTCVideo = new FrmGroupRTCVideo("https://www.baidu.com");
            frmGroupRTCVideo.Left = 0 - frmGroupRTCVideo.Width * 2;
            frmGroupRTCVideo.ShowInTaskbar = false;
            frmGroupRTCVideo.Show();
            FrmGroupRTCVideo frmGroupRTCVideo2 = new FrmGroupRTCVideo();
            frmGroupRTCVideo2.Show();
            frmGroupRTCVideo2.FormClosed += delegate { frmGroupRTCVideo.Close(); };
        }
    }
}
