﻿
namespace Team.Zyzy.Im.Control
{
    partial class LeftTestControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTestNewMessage = new System.Windows.Forms.Button();
            this.btnTestSendSystemMessage = new System.Windows.Forms.Button();
            this.btnTestBrowser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnTestNewMessage
            // 
            this.btnTestNewMessage.Location = new System.Drawing.Point(40, 20);
            this.btnTestNewMessage.Name = "btnTestNewMessage";
            this.btnTestNewMessage.Size = new System.Drawing.Size(235, 56);
            this.btnTestNewMessage.TabIndex = 0;
            this.btnTestNewMessage.Text = "测试有新消息";
            this.btnTestNewMessage.UseVisualStyleBackColor = true;
            this.btnTestNewMessage.Click += new System.EventHandler(this.btnTestNewMessage_Click);
            // 
            // btnTestSendSystemMessage
            // 
            this.btnTestSendSystemMessage.Location = new System.Drawing.Point(40, 104);
            this.btnTestSendSystemMessage.Name = "btnTestSendSystemMessage";
            this.btnTestSendSystemMessage.Size = new System.Drawing.Size(235, 56);
            this.btnTestSendSystemMessage.TabIndex = 1;
            this.btnTestSendSystemMessage.Text = "测试发送系统信息";
            this.btnTestSendSystemMessage.UseVisualStyleBackColor = true;
            this.btnTestSendSystemMessage.Click += new System.EventHandler(this.btnTestSendSystemMessage_Click);
            // 
            // btnTestBrowser
            // 
            this.btnTestBrowser.Location = new System.Drawing.Point(40, 192);
            this.btnTestBrowser.Name = "btnTestBrowser";
            this.btnTestBrowser.Size = new System.Drawing.Size(235, 56);
            this.btnTestBrowser.TabIndex = 2;
            this.btnTestBrowser.Text = "测试浏览器支持情况";
            this.btnTestBrowser.UseVisualStyleBackColor = true;
            this.btnTestBrowser.Click += new System.EventHandler(this.btnTestBrowser_Click);
            // 
            // LeftTestControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnTestBrowser);
            this.Controls.Add(this.btnTestSendSystemMessage);
            this.Controls.Add(this.btnTestNewMessage);
            this.Name = "LeftTestControl";
            this.Size = new System.Drawing.Size(823, 684);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTestNewMessage;
        private System.Windows.Forms.Button btnTestSendSystemMessage;
        private System.Windows.Forms.Button btnTestBrowser;
    }
}
