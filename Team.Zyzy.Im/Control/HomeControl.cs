﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI.BaseControls.ObjectListView;
using static System.Windows.Forms.ListViewItem;

namespace Team.Zyzy.Im.Control
{
    public partial class HomeControl : UserControl
    {
        public HomeControl()
        {
            InitializeComponent();            
            this.Load += HomeControl_Load;
        }

        private void HomeControl_Load(object sender, EventArgs e)
        {
            init();
        }
        private void init()
        {
            dlvHome.View = View.LargeIcon;
            dlvHome.FullRowSelect = true;
            dlvHome.ShowGroups = true;
            dlvHome.ShowItemToolTips = true;
            dlvHome.ShowHeaderInAllViews = true;
            dlvHome.HideSelection = false;
            dlvHome.OwnerDraw = true;
            dlvHome.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;

            OLVColumn olv1 = new OLVColumn();
            olv1.AspectName = "Title";
            olv1.Text = "标题";
            olv1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            olv1.UseInitialLetterForGroup = false;
            olv1.ImageGetter= delegate (object row) {
                HomeViewInfo p = (HomeViewInfo)row;
                return p.ImageIndex;
            };
            olv1.AspectGetter = delegate (object x) {
                return ((HomeViewInfo)x).Title;
            };
            olv1.GroupKeyGetter = delegate (object x) {
                return ((HomeViewInfo)x).Group;
            };
            dlvHome.Columns.Add(olv1);
            OLVColumn olv2 = new OLVColumn();
            olv2.AspectName = "Group";
            olv2.IsTileViewColumn = false;
            olv2.Text = "类别";
            olv2.UseInitialLetterForGroup = true;
            dlvHome.Columns.Add(olv2);
ImageRenderer imageRenderer1 = new ImageRenderer();
            OLVColumn olvImage = new OLVColumn();
            olvImage.AspectName = "Describe";
            olvImage.Text = "描述";
            olvImage.UseInitialLetterForGroup = false;
            olvImage.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            dlvHome.Columns.Add(olvImage);

            List<HomeViewInfo> homeViews = getDataSource();
            dlvHome.DataSource = homeViews;
        }

        private static List<HomeViewInfo> getDataSource()
        {
            List<HomeViewInfo> homeViews = new List<HomeViewInfo>();
            HomeViewInfo item = new HomeViewInfo();
            item.Title = "FOC系统";
            item.ImageIndex = 19;
            item.Group = "运行";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "安全系统";
            item.ImageIndex = 3;
            item.Group = "运行";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "飞管系统";
            item.ImageIndex = 7;
            item.Group = "运行";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "排班系统";
            item.ImageIndex = 10;
            item.Group = "运行";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "人资系统";
            item.ImageIndex = 5;
            item.Group = "办公";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "BPM系统";
            item.ImageIndex = 1;
            item.Group = "办公";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "维修系统";
            item.ImageIndex = 16;
            item.Group = "运行";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "邮件系统";
            item.ImageIndex = 1;
            item.Group = "办公";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "指标监控";
            item.ImageIndex = 17;
            item.Group = "办公";
            homeViews.Add(item);
            item = new HomeViewInfo();
            item.Title = "SAP系统";
            item.ImageIndex = 2;
            item.Group = "办公";
            homeViews.Add(item);
            return homeViews;
        }


        private void dlvHome_CellClick(object sender, CellClickEventArgs e)
        {
            if (e.Item != null)
            {
                HomeViewInfo selectItem = e.Item.RowObject as HomeViewInfo;
            }
        }
    }

    public class HomeViewInfo
    {
        public string Title { get; set; }
        public string Describe { get; set; }
        public string Group { get; set; }

        public string Path { get; set; }

        public int ImageIndex { get; set; }

        public object Tag { get; set; }
    }
}
