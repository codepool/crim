﻿
using System.Windows.Forms;
using YiWangYi.MsgQQ.WinUI.BaseControls;

namespace Team.Zyzy.Im.Control
{
    partial class HomeControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeControl));
            this.imlHome = new System.Windows.Forms.ImageList(this.components);
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.dlvHome = new YiWangYi.MsgQQ.WinUI.BaseControls.ObjectListView.DataListView();
            this.imlHomeSmall = new System.Windows.Forms.ImageList(this.components);
            this.qqPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlvHome)).BeginInit();
            this.SuspendLayout();
            // 
            // imlHome
            // 
            this.imlHome.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlHome.ImageStream")));
            this.imlHome.TransparentColor = System.Drawing.Color.Transparent;
            this.imlHome.Images.SetKeyName(0, "1_4.png");
            this.imlHome.Images.SetKeyName(1, "2_4.png");
            this.imlHome.Images.SetKeyName(2, "3_4.png");
            this.imlHome.Images.SetKeyName(3, "4_4.png");
            this.imlHome.Images.SetKeyName(4, "5_4.png");
            this.imlHome.Images.SetKeyName(5, "6_4.png");
            this.imlHome.Images.SetKeyName(6, "7_4.png");
            this.imlHome.Images.SetKeyName(7, "8_4.png");
            this.imlHome.Images.SetKeyName(8, "9_4.png");
            this.imlHome.Images.SetKeyName(9, "10_4.png");
            this.imlHome.Images.SetKeyName(10, "11_4.png");
            this.imlHome.Images.SetKeyName(11, "12_4.png");
            this.imlHome.Images.SetKeyName(12, "13_4.png");
            this.imlHome.Images.SetKeyName(13, "14_4.png");
            this.imlHome.Images.SetKeyName(14, "15_4.png");
            this.imlHome.Images.SetKeyName(15, "16_4.png");
            this.imlHome.Images.SetKeyName(16, "17_4.png");
            this.imlHome.Images.SetKeyName(17, "18_4.png");
            this.imlHome.Images.SetKeyName(18, "19_4.png");
            this.imlHome.Images.SetKeyName(19, "20_4.png");
            this.imlHome.Images.SetKeyName(20, "21_4.png");
            // 
            // qqPanel1
            // 
            this.qqPanel1.Controls.Add(this.dlvHome);
            this.qqPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qqPanel1.Location = new System.Drawing.Point(0, 0);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(711, 780);
            this.qqPanel1.TabIndex = 0;
            // 
            // dlvHome
            // 
            this.dlvHome.DataSource = null;
            this.dlvHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dlvHome.EmptyListMsg = "请联系管理员开通权限";
            this.dlvHome.FullRowSelect = true;
            this.dlvHome.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.dlvHome.HideSelection = false;
            this.dlvHome.LargeImageList = this.imlHome;
            this.dlvHome.Location = new System.Drawing.Point(0, 0);
            this.dlvHome.MultiSelect = false;
            this.dlvHome.Name = "dlvHome";
            this.dlvHome.Size = new System.Drawing.Size(711, 780);
            this.dlvHome.SmallImageList = this.imlHomeSmall;
            this.dlvHome.TabIndex = 0;
            this.dlvHome.UseCompatibleStateImageBehavior = false;
            this.dlvHome.View = System.Windows.Forms.View.Details;
            this.dlvHome.CellClick += new System.EventHandler<YiWangYi.MsgQQ.WinUI.BaseControls.ObjectListView.CellClickEventArgs>(this.dlvHome_CellClick);
            // 
            // imlHomeSmall
            // 
            this.imlHomeSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlHomeSmall.ImageStream")));
            this.imlHomeSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imlHomeSmall.Images.SetKeyName(0, "1_4.png");
            this.imlHomeSmall.Images.SetKeyName(1, "2_4.png");
            this.imlHomeSmall.Images.SetKeyName(2, "3_4.png");
            this.imlHomeSmall.Images.SetKeyName(3, "4_4.png");
            this.imlHomeSmall.Images.SetKeyName(4, "5_4.png");
            this.imlHomeSmall.Images.SetKeyName(5, "6_4.png");
            this.imlHomeSmall.Images.SetKeyName(6, "7_4.png");
            this.imlHomeSmall.Images.SetKeyName(7, "8_4.png");
            this.imlHomeSmall.Images.SetKeyName(8, "9_4.png");
            this.imlHomeSmall.Images.SetKeyName(9, "10_4.png");
            this.imlHomeSmall.Images.SetKeyName(10, "11_4.png");
            this.imlHomeSmall.Images.SetKeyName(11, "12_4.png");
            this.imlHomeSmall.Images.SetKeyName(12, "13_4.png");
            this.imlHomeSmall.Images.SetKeyName(13, "14_4.png");
            this.imlHomeSmall.Images.SetKeyName(14, "15_4.png");
            this.imlHomeSmall.Images.SetKeyName(15, "16_4.png");
            this.imlHomeSmall.Images.SetKeyName(16, "17_4.png");
            this.imlHomeSmall.Images.SetKeyName(17, "18_4.png");
            this.imlHomeSmall.Images.SetKeyName(18, "19_4.png");
            this.imlHomeSmall.Images.SetKeyName(19, "20_4.png");
            this.imlHomeSmall.Images.SetKeyName(20, "21_4.png");
            // 
            // HomeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.qqPanel1);
            this.Name = "HomeControl";
            this.Size = new System.Drawing.Size(711, 780);
            this.qqPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlvHome)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private System.Windows.Forms.ImageList imlHome;
        private System.Windows.Forms.ImageList imlHomeSmall;
        private YiWangYi.MsgQQ.WinUI.BaseControls.ObjectListView.DataListView dlvHome;
    }
}
