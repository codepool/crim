﻿
namespace Team.Zyzy.Im.Control
{
    partial class MailControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailControl));
            this.imlHome = new System.Windows.Forms.ImageList(this.components);
            this.imlHomeSmall = new System.Windows.Forms.ImageList(this.components);
            this.qqPanel1 = new YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel();
            this.picLoad = new System.Windows.Forms.PictureBox();
            this.dgvMail = new System.Windows.Forms.DataGridView();
            this.from = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subject = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.body = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsMail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.打开邮件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.回复邮件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.标记为已读ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.写信ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qqPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLoad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMail)).BeginInit();
            this.cmsMail.SuspendLayout();
            this.SuspendLayout();
            // 
            // imlHome
            // 
            this.imlHome.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlHome.ImageStream")));
            this.imlHome.TransparentColor = System.Drawing.Color.Transparent;
            this.imlHome.Images.SetKeyName(0, "1_4.png");
            this.imlHome.Images.SetKeyName(1, "2_4.png");
            this.imlHome.Images.SetKeyName(2, "3_4.png");
            this.imlHome.Images.SetKeyName(3, "4_4.png");
            this.imlHome.Images.SetKeyName(4, "5_4.png");
            this.imlHome.Images.SetKeyName(5, "6_4.png");
            this.imlHome.Images.SetKeyName(6, "7_4.png");
            this.imlHome.Images.SetKeyName(7, "8_4.png");
            this.imlHome.Images.SetKeyName(8, "9_4.png");
            this.imlHome.Images.SetKeyName(9, "10_4.png");
            this.imlHome.Images.SetKeyName(10, "11_4.png");
            this.imlHome.Images.SetKeyName(11, "12_4.png");
            this.imlHome.Images.SetKeyName(12, "13_4.png");
            this.imlHome.Images.SetKeyName(13, "14_4.png");
            this.imlHome.Images.SetKeyName(14, "15_4.png");
            this.imlHome.Images.SetKeyName(15, "16_4.png");
            this.imlHome.Images.SetKeyName(16, "17_4.png");
            this.imlHome.Images.SetKeyName(17, "18_4.png");
            this.imlHome.Images.SetKeyName(18, "19_4.png");
            this.imlHome.Images.SetKeyName(19, "20_4.png");
            this.imlHome.Images.SetKeyName(20, "21_4.png");
            // 
            // imlHomeSmall
            // 
            this.imlHomeSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imlHomeSmall.ImageStream")));
            this.imlHomeSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imlHomeSmall.Images.SetKeyName(0, "1_4.png");
            this.imlHomeSmall.Images.SetKeyName(1, "2_4.png");
            this.imlHomeSmall.Images.SetKeyName(2, "3_4.png");
            this.imlHomeSmall.Images.SetKeyName(3, "4_4.png");
            this.imlHomeSmall.Images.SetKeyName(4, "5_4.png");
            this.imlHomeSmall.Images.SetKeyName(5, "6_4.png");
            this.imlHomeSmall.Images.SetKeyName(6, "7_4.png");
            this.imlHomeSmall.Images.SetKeyName(7, "8_4.png");
            this.imlHomeSmall.Images.SetKeyName(8, "9_4.png");
            this.imlHomeSmall.Images.SetKeyName(9, "10_4.png");
            this.imlHomeSmall.Images.SetKeyName(10, "11_4.png");
            this.imlHomeSmall.Images.SetKeyName(11, "12_4.png");
            this.imlHomeSmall.Images.SetKeyName(12, "13_4.png");
            this.imlHomeSmall.Images.SetKeyName(13, "14_4.png");
            this.imlHomeSmall.Images.SetKeyName(14, "15_4.png");
            this.imlHomeSmall.Images.SetKeyName(15, "16_4.png");
            this.imlHomeSmall.Images.SetKeyName(16, "17_4.png");
            this.imlHomeSmall.Images.SetKeyName(17, "18_4.png");
            this.imlHomeSmall.Images.SetKeyName(18, "19_4.png");
            this.imlHomeSmall.Images.SetKeyName(19, "20_4.png");
            this.imlHomeSmall.Images.SetKeyName(20, "21_4.png");
            // 
            // qqPanel1
            // 
            this.qqPanel1.Controls.Add(this.picLoad);
            this.qqPanel1.Controls.Add(this.dgvMail);
            this.qqPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qqPanel1.Location = new System.Drawing.Point(0, 0);
            this.qqPanel1.Name = "qqPanel1";
            this.qqPanel1.Size = new System.Drawing.Size(683, 915);
            this.qqPanel1.TabIndex = 1;
            // 
            // picLoad
            // 
            this.picLoad.Image = global::Team.Zyzy.Im.Properties.Resources.loading124X124;
            this.picLoad.Location = new System.Drawing.Point(158, 176);
            this.picLoad.Name = "picLoad";
            this.picLoad.Size = new System.Drawing.Size(124, 124);
            this.picLoad.TabIndex = 1;
            this.picLoad.TabStop = false;
            // 
            // dgvMail
            // 
            this.dgvMail.BackgroundColor = System.Drawing.Color.White;
            this.dgvMail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.from,
            this.subject,
            this.date,
            this.address,
            this.body});
            this.dgvMail.ContextMenuStrip = this.cmsMail;
            this.dgvMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMail.GridColor = System.Drawing.SystemColors.Control;
            this.dgvMail.Location = new System.Drawing.Point(0, 0);
            this.dgvMail.Name = "dgvMail";
            this.dgvMail.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvMail.RowHeadersVisible = false;
            this.dgvMail.RowHeadersWidth = 82;
            this.dgvMail.RowTemplate.Height = 37;
            this.dgvMail.Size = new System.Drawing.Size(683, 915);
            this.dgvMail.TabIndex = 0;
            // 
            // from
            // 
            this.from.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.from.DataPropertyName = "from";
            this.from.FillWeight = 10F;
            this.from.HeaderText = "发件人";
            this.from.MinimumWidth = 10;
            this.from.Name = "from";
            this.from.Width = 200;
            // 
            // subject
            // 
            this.subject.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.subject.DataPropertyName = "subject";
            this.subject.FillWeight = 200F;
            this.subject.HeaderText = "主题";
            this.subject.MinimumWidth = 200;
            this.subject.Name = "subject";
            // 
            // date
            // 
            this.date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.date.DataPropertyName = "date";
            this.date.FillWeight = 10F;
            this.date.HeaderText = "时间";
            this.date.MinimumWidth = 10;
            this.date.Name = "date";
            this.date.Width = 200;
            // 
            // address
            // 
            this.address.DataPropertyName = "address";
            this.address.HeaderText = "发件号";
            this.address.MinimumWidth = 10;
            this.address.Name = "address";
            this.address.Visible = false;
            this.address.Width = 200;
            // 
            // body
            // 
            this.body.DataPropertyName = "body";
            this.body.HeaderText = "内容";
            this.body.MinimumWidth = 10;
            this.body.Name = "body";
            this.body.Visible = false;
            this.body.Width = 200;
            // 
            // cmsMail
            // 
            this.cmsMail.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.cmsMail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开邮件ToolStripMenuItem,
            this.回复邮件ToolStripMenuItem,
            this.标记为已读ToolStripMenuItem,
            this.写信ToolStripMenuItem});
            this.cmsMail.Name = "cmsMail";
            this.cmsMail.Size = new System.Drawing.Size(209, 156);
            // 
            // 打开邮件ToolStripMenuItem
            // 
            this.打开邮件ToolStripMenuItem.Name = "打开邮件ToolStripMenuItem";
            this.打开邮件ToolStripMenuItem.Size = new System.Drawing.Size(208, 38);
            this.打开邮件ToolStripMenuItem.Text = "打开邮件";
            // 
            // 回复邮件ToolStripMenuItem
            // 
            this.回复邮件ToolStripMenuItem.Name = "回复邮件ToolStripMenuItem";
            this.回复邮件ToolStripMenuItem.Size = new System.Drawing.Size(208, 38);
            this.回复邮件ToolStripMenuItem.Text = "回复邮件";
            // 
            // 标记为已读ToolStripMenuItem
            // 
            this.标记为已读ToolStripMenuItem.Name = "标记为已读ToolStripMenuItem";
            this.标记为已读ToolStripMenuItem.Size = new System.Drawing.Size(208, 38);
            this.标记为已读ToolStripMenuItem.Text = "标记为已读";
            // 
            // 写信ToolStripMenuItem
            // 
            this.写信ToolStripMenuItem.Name = "写信ToolStripMenuItem";
            this.写信ToolStripMenuItem.Size = new System.Drawing.Size(208, 38);
            this.写信ToolStripMenuItem.Text = "写信";
            // 
            // MailControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.qqPanel1);
            this.Name = "MailControl";
            this.Size = new System.Drawing.Size(683, 915);
            this.qqPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLoad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMail)).EndInit();
            this.cmsMail.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imlHome;
        private System.Windows.Forms.ImageList imlHomeSmall;
        private YiWangYi.MsgQQ.WinUI.BaseControls.QQPanel qqPanel1;
        private System.Windows.Forms.DataGridView dgvMail;
        private System.Windows.Forms.DataGridViewTextBoxColumn from;
        private System.Windows.Forms.DataGridViewTextBoxColumn subject;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn address;
        private System.Windows.Forms.DataGridViewTextBoxColumn body;
        private System.Windows.Forms.PictureBox picLoad;
        private System.Windows.Forms.ContextMenuStrip cmsMail;
        private System.Windows.Forms.ToolStripMenuItem 打开邮件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 回复邮件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 标记为已读ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 写信ToolStripMenuItem;
    }
}
