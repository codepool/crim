﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.dto;

namespace Team.Zyzy.Im{
    public class RequestApiInfo
    {
        private static string _baseApi = "https://im.zyzy.team/im_api";
        private static string _wsURL = "wss://im.zyzy.team/ws_api/im";

        //private static string _baseApi = "http://127.0.0.1:8200";
        //private static string _wsURL = "ws://localhost:8111/im";

        public static string BaseApi
        {
            get
            {
                return _baseApi;
            }
            set
            {
                _baseApi = value;
            }
        }
       public static string WsURL
        {
            get
            {
                return _wsURL;
            }
            set
            {
                _wsURL = value;
            }
        }
        //@ApiModelProperty(value = "每次请求都必须在header中携带accessToken")
        private static string accessToken;
        public static string AccessToken
        {
            get
            {
                return accessToken;
            }
            set
            {
                accessToken = value;
            }
        }
        //@ApiModelProperty(value = "accessToken过期时间(秒)")
        private static int accessTokenExpiresIn;
        public static int AccessTokenExpiresIn
        {
            get
            {
                return accessTokenExpiresIn;
            }
            set
            {
                accessTokenExpiresIn = value;
            }
        }
        //@ApiModelProperty(value = "accessToken过期后，通过refreshToken换取新的token")
        private static string refreshToken;
        public static string RefreshToken
        {
            get
            {
                return refreshToken;
            }
            set
            {
                refreshToken = value;
            }
        }
        //@ApiModelProperty(value = "refreshToken过期时间(秒)")
        private static int refreshTokenExpiresIn;
        public static int RefreshTokenExpiresIn
        {
            get
            {
                return refreshTokenExpiresIn;
            }
            set
            {
                refreshTokenExpiresIn = value;
            }
        }

        public static long SendId { get; set; }
        public static string SendNickName { get; set; }
    }
}