﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Team.Zyzy.Im.config;
using YiWangYi.MsgQQ.WinUI.General.SideBar;
using YiWangYi.MsgQQ.WinUI.Service.ConfigService;

namespace Team.Zyzy.Im
{
    public partial class FrmSystemService : FrmSystemServiceBase
    {
        private FrmSystemService()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
            this.qqPanelFixSize1.SetButtonTop(3);
            this.Load += FrmSystemService_Load;
        }

        private void FrmSystemService_Load(object sender, EventArgs e)
        {
            
        }

        #region 单例模式相关设置
        private static FrmSystemService instance;
        private static object _lock = new object();

        public  static FrmSystemService Instance()
        {
            if (instance == null)
            {
                lock (_lock)
                {
                    if (instance == null)
                    {
                        instance = new FrmSystemService();
                    }
                }
            }
            if (instance.WindowState != FormWindowState.Normal)
            {
                instance.WindowState = FormWindowState.Normal;
            }
            return instance;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            instance = null;
        }

        #endregion

        protected override void InitControlSize()
        {
            base.InitControlSize();
        }
        protected override void InitLeft()
        {
            qSideBar1.AddGroup("基本设置");
            qSideBar1.AddGroup("状态和提醒");
            qSideBar1.AddGroup("好友和聊天");
            qSideBar1.AddGroup("安全和隐私");

            SbItem item = new SbItem("登录", 0);
            item.Tag = "basicSettingLogin";
            item.IsSelected = true;
            qSideBar1.Groups[0].Items.Add(item);
            item = new SbItem("首页设置", 1);
            item.Tag = "basicSettingHome";
            qSideBar1.Groups[0].Items.Add(item);            
            item = new SbItem("声音", 2);
            item.Tag = "basicSettingVoice";
            qSideBar1.Groups[0].Items.Add(item);
            item = new SbItem("其他装扮", 3);
            item.Tag = "basicSettingDressUp";
            qSideBar1.Groups[0].Items.Add(item);
            item = new SbItem("文件记录", 4);
            item.Tag = "basicSettingFileRecord";
            qSideBar1.Groups[0].Items.Add(item);
            item = new SbItem("网络连接", 5);
            item.Tag = "basicSettingNetwork";
            qSideBar1.Groups[0].Items.Add(item);
            item = new SbItem("软件更新", 6);
            item.Tag = "basicSettingSoftUpdate";
            qSideBar1.Groups[0].Items.Add(item);


            item = new SbItem("即时通讯", 7);
            item.Tag = "stateHomeContract";
            item.IsSelected = true;
            qSideBar1.Groups[0].Items.Add(item);
            item = new SbItem("在线状态", 8);
            item.Tag = "stateAlertOnline";
            item.IsSelected = true;
            qSideBar1.Groups[1].Items.Add(item);
            item = new SbItem("闹钟提醒", 9);
            item.Tag = "basicSettingAlarmClock";
            qSideBar1.Groups[1].Items.Add(item);
            item = new SbItem("自动回复", 9);
            item.Tag = "stateAlertAutoReplay";
            qSideBar1.Groups[1].Items.Add(item);
            item = new SbItem("即时共享", 10);
            //base.InitLeft();
            item = new SbItem("修改密码", 11);
            item.Tag = "securityModifyPassword";
            qSideBar1.Groups[3].Items.Add(item);

            SetContent(new LoginSetControl());
        }

        public override void ShowItem(String key)
        {
            if (instance.Visible)
            {
                instance.Focus();
            }
            else
            {
                instance.Show();
                instance.Shown += delegate
                {
                    for (int i = 0; i < qSideBar1.Groups.Count; i++)
                    {
                        SbGroup sg = qSideBar1.Groups[i];
                        for (int j = 0; j < sg.Items.Count; j++)
                        {
                            if (sg.Items[j].Tag.ToString().IndexOf(key) >= 0 || sg.Items[j].Text.IndexOf(key) >= 0)
                            {
                                SbItemEventArgs siea = new SbItemEventArgs();
                                siea.Item = sg.Items[j];
                                LeftBar_ItemClick(siea);
                                SetItemSelected(sg.Items[j]);
                                break;
                            }
                        }
                    }
                };
            }
        }
        protected override void LeftBar_ItemClick(SbItemEventArgs e)
        {
            switch (e.Item.Tag.ToString())
            {
                case "basicSettingDressUp":
                    SetContent(new CommonSetControl());
                    break;
                case "basicSettingLogin":
                    SetContent(new LoginSetControl());
                    break;
                case "basicSettingSoftUpdate":
                    SetContent(new UpdateSetControl());
                    break;
                case "basicSettingHome":
                    SetContent(new HomeLeftSetControl());
                    break;
                case "stateHomeContract":
                    SetContent(new HomeContractSetControl());
                    break;
                case "basicSettingVoice":
                    SetContent(new VoiceSetControl());
                    break;
                case "basicSettingAlarmClock":
                    SetContent(new HomeAlarmClockSetControl());
                    break;
                case "stateAlertOnline":
                    SetContent(new OnlineStateSetControl());
                    break;
                case "securityModifyPassword":
                    SetContent(new ModifyPasswordControl());
                    break;
                case "":

                    break;
            }
            
        }

    }
}
