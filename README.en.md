# Enterprise level IM free solution


![GPL3 Protocol]( https://img.shields.io/badge/license-GPL3-red )
[![star](https://gitee.com/zyzyteam/crim/badge/star.svg )]( https://gitee.com/zyzyteam/crim )
[![star](https://img.shields.io/github/stars/zyzyteam/crim.svg?style=flat&logo=GitHub )]( https://github.com/zyzyteam/crim )


- CRIM is a chat tool that mimics QQ 2010. The backend is implemented using Springboot+Netty, the frontend uses Vue, the mobile end uses Uniapp, and the PC end uses C #. It supports private chat, group chat, offline messaging, sending pictures, files, voice, emoji expressions, video chat, video conferencing, and other functions.
- The backend can refer to the box IM: https://gitee.com/bluexsx/box-im
- Video conference reference: https://github.com/jitsi/jitsi-me


#### Recent updates

- Release version 1.0:

- Server Description: In order to respect the labor achievements of Box IM, this code will not be released separately related to Box IM. This project should be compatible with versions after Box IM 1.0, as long as the URL remains compatible. Box IM Project Address: https://gitee.com/bluexsx/box-im

- The corresponding modification needs to be made to add a PC terminal for the terminal type, with the code as follows:

- Public enum IMTerminalType{
- /**
- *Web
- */
- WEB (0, "web"),
- /**
- *App
- */
- APP (1, "app"),
- /**
- *PC
- */
- PC (2, "PC");

- Video conferencing function: Similarly, Jitsi Meet related code will not be released separately for this project. If you need related functions, please refer to: https://github.com/jitsi/jitsi-meet

- Software download address: https://www.zyzy.team/soft/CRIM.exe
- dubug download address: https://www.zyzy.team/soft/crim_local.exe

#### Online experience

- 1. Download address: https://www.zyzy.team/soft/CRIM.exe
- 2. Download address: https://www.zyzy.team/soft/crim_zyzy.exe
- 3. Download address: https://www.zyzy.team/soft/crim_box.exe
- Account: admin Password: 123456
- You can also register your own account


#### Project Structure

|Module | Function|
|-------------|------------|
|Team Zyzy Im | Core Business Function Section|
|Team Zyzy Im App | Software updates and startup section|
|Team Zyzy Im Business | Business logic and database processing section|
|Team Zyzy.tool | Toolkit, mainly designed for quick image processing|



#### Interface screenshot
![输入图片说明](https://gitee.com/zyzyteam/crim/raw/master/doc/CRIM.png)
Welcome to join the group and communicate with everyone. Please make sure to start before applying to join the group**


#### Click on star now
If the project is helpful to you, please light up the star in the upper right corner to support the author!

#### Explain a few points

1. This system is allowed for commercial use without charge (voluntary coin insertion)** But remember not to use it for any illegal purposes * *, the author of this software will not be held responsible for this
2. For projects that are open source again after secondary development of this system, please indicate the source of the reference to avoid unnecessary misunderstandings


#### Participate in contributions

1. Master's warehouse
2. Create a new Feature_ Xxx branch
3. Submit code
4. Create a new Pull Request


#### Other

1. Author's blog https://www.cnblogs.com/zhouyz
2. Author's official website https://www.zyzy.team/