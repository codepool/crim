﻿
namespace Team.Zyzy.tool
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelectFrom = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFromPath = new System.Windows.Forms.TextBox();
            this.txtToPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectTo = new System.Windows.Forms.Button();
            this.btnGenerateSamll = new System.Windows.Forms.Button();
            this.btnGenerateSamll4 = new System.Windows.Forms.Button();
            this.pic3Back = new System.Windows.Forms.PictureBox();
            this.pic2Back = new System.Windows.Forms.PictureBox();
            this.rdb30 = new System.Windows.Forms.RadioButton();
            this.rdb48 = new System.Windows.Forms.RadioButton();
            this.rdb96 = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.pic3Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2Back)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSelectFrom
            // 
            this.btnSelectFrom.Location = new System.Drawing.Point(781, 39);
            this.btnSelectFrom.Name = "btnSelectFrom";
            this.btnSelectFrom.Size = new System.Drawing.Size(126, 60);
            this.btnSelectFrom.TabIndex = 0;
            this.btnSelectFrom.Text = "选择";
            this.btnSelectFrom.UseVisualStyleBackColor = true;
            this.btnSelectFrom.Click += new System.EventHandler(this.btnSelectFrom_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(202, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "选择图片文件夹：";
            // 
            // txtFromPath
            // 
            this.txtFromPath.Location = new System.Drawing.Point(199, 54);
            this.txtFromPath.Name = "txtFromPath";
            this.txtFromPath.Size = new System.Drawing.Size(552, 35);
            this.txtFromPath.TabIndex = 2;
            // 
            // txtToPath
            // 
            this.txtToPath.Location = new System.Drawing.Point(199, 137);
            this.txtToPath.Name = "txtToPath";
            this.txtToPath.Size = new System.Drawing.Size(552, 35);
            this.txtToPath.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 140);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(202, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "选择保存文件夹：";
            // 
            // btnSelectTo
            // 
            this.btnSelectTo.Location = new System.Drawing.Point(781, 122);
            this.btnSelectTo.Name = "btnSelectTo";
            this.btnSelectTo.Size = new System.Drawing.Size(126, 60);
            this.btnSelectTo.TabIndex = 3;
            this.btnSelectTo.Text = "选择";
            this.btnSelectTo.UseVisualStyleBackColor = true;
            this.btnSelectTo.Click += new System.EventHandler(this.btnSelectTo_Click);
            // 
            // btnGenerateSamll
            // 
            this.btnGenerateSamll.Location = new System.Drawing.Point(33, 411);
            this.btnGenerateSamll.Name = "btnGenerateSamll";
            this.btnGenerateSamll.Size = new System.Drawing.Size(239, 91);
            this.btnGenerateSamll.TabIndex = 6;
            this.btnGenerateSamll.Text = "生成30X30";
            this.btnGenerateSamll.UseVisualStyleBackColor = true;
            this.btnGenerateSamll.Click += new System.EventHandler(this.btnGenerateSamll_Click);
            // 
            // btnGenerateSamll4
            // 
            this.btnGenerateSamll4.Location = new System.Drawing.Point(331, 411);
            this.btnGenerateSamll4.Name = "btnGenerateSamll4";
            this.btnGenerateSamll4.Size = new System.Drawing.Size(239, 91);
            this.btnGenerateSamll4.TabIndex = 7;
            this.btnGenerateSamll4.Text = "生成30X30 4";
            this.btnGenerateSamll4.UseVisualStyleBackColor = true;
            this.btnGenerateSamll4.Click += new System.EventHandler(this.btnGenerateSamll4_Click);
            // 
            // pic3Back
            // 
            this.pic3Back.Image = global::Team.Zyzy.Tool.Properties.Resources.funcationSwitch3;
            this.pic3Back.Location = new System.Drawing.Point(855, 232);
            this.pic3Back.Name = "pic3Back";
            this.pic3Back.Size = new System.Drawing.Size(95, 91);
            this.pic3Back.TabIndex = 10;
            this.pic3Back.TabStop = false;
            // 
            // pic2Back
            // 
            this.pic2Back.Image = global::Team.Zyzy.Tool.Properties.Resources.funcationSwitch2;
            this.pic2Back.Location = new System.Drawing.Point(731, 232);
            this.pic2Back.Name = "pic2Back";
            this.pic2Back.Size = new System.Drawing.Size(95, 91);
            this.pic2Back.TabIndex = 9;
            this.pic2Back.TabStop = false;
            // 
            // rdb30
            // 
            this.rdb30.AutoSize = true;
            this.rdb30.Location = new System.Drawing.Point(44, 232);
            this.rdb30.Name = "rdb30";
            this.rdb30.Size = new System.Drawing.Size(101, 28);
            this.rdb30.TabIndex = 11;
            this.rdb30.TabStop = true;
            this.rdb30.Text = "30X30";
            this.rdb30.UseVisualStyleBackColor = true;
            this.rdb30.CheckedChanged += new System.EventHandler(this.rdb30_CheckedChanged);
            // 
            // rdb48
            // 
            this.rdb48.AutoSize = true;
            this.rdb48.Location = new System.Drawing.Point(254, 232);
            this.rdb48.Name = "rdb48";
            this.rdb48.Size = new System.Drawing.Size(101, 28);
            this.rdb48.TabIndex = 12;
            this.rdb48.TabStop = true;
            this.rdb48.Text = "48X48";
            this.rdb48.UseVisualStyleBackColor = true;
            this.rdb48.CheckedChanged += new System.EventHandler(this.rdb30_CheckedChanged);
            // 
            // rdb96
            // 
            this.rdb96.AutoSize = true;
            this.rdb96.Location = new System.Drawing.Point(453, 232);
            this.rdb96.Name = "rdb96";
            this.rdb96.Size = new System.Drawing.Size(101, 28);
            this.rdb96.TabIndex = 13;
            this.rdb96.TabStop = true;
            this.rdb96.Text = "96X96";
            this.rdb96.UseVisualStyleBackColor = true;
            this.rdb96.CheckedChanged += new System.EventHandler(this.rdb30_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 661);
            this.Controls.Add(this.rdb96);
            this.Controls.Add(this.rdb48);
            this.Controls.Add(this.rdb30);
            this.Controls.Add(this.pic3Back);
            this.Controls.Add(this.pic2Back);
            this.Controls.Add(this.btnGenerateSamll4);
            this.Controls.Add(this.btnGenerateSamll);
            this.Controls.Add(this.txtToPath);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSelectTo);
            this.Controls.Add(this.txtFromPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSelectFrom);
            this.Name = "Form1";
            this.Text = "图标生成";
            ((System.ComponentModel.ISupportInitialize)(this.pic3Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2Back)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelectFrom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtFromPath;
        private System.Windows.Forms.TextBox txtToPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectTo;
        private System.Windows.Forms.Button btnGenerateSamll;
        private System.Windows.Forms.Button btnGenerateSamll4;
        private System.Windows.Forms.PictureBox pic2Back;
        private System.Windows.Forms.PictureBox pic3Back;
        private System.Windows.Forms.RadioButton rdb30;
        private System.Windows.Forms.RadioButton rdb48;
        private System.Windows.Forms.RadioButton rdb96;
    }
}

