﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Team.Zyzy.tool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int _width = 30;

        private void btnSelectFrom_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.RootFolder = Environment.SpecialFolder.Desktop;
            if (txtFromPath.Text.Length > 10)
            {
                dialog.SelectedPath = txtFromPath.Text;
            }
            else
            {
                dialog.RootFolder = Environment.SpecialFolder.Desktop;
            }
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtFromPath.Text = dialog.SelectedPath;
            }
        }

        private void btnSelectTo_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();

            if (txtToPath.Text.Length > 10)
            {
                dialog.SelectedPath = txtToPath.Text;
            }
            else
            {
                dialog.RootFolder = Environment.SpecialFolder.Desktop;
            }
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtToPath.Text = dialog.SelectedPath;
            }
        }

        private void btnGenerateSamll_Click(object sender, EventArgs e)
        {
            string[] files = Directory.GetFiles(txtFromPath.Text, "*.png", SearchOption.AllDirectories);
            int index = 1;
            foreach (string file in files)
            {
                Bitmap image = new Bitmap(file);
                Bitmap image1 = new Bitmap(_width, _width);
                Graphics g = Graphics.FromImage(image1);
                Rectangle rect = new Rectangle(0, 0, _width, _width);
                g.DrawImage(image, rect);
                image1.Save(Path.Combine(txtToPath.Text, ""+index+"_1.png"), ImageFormat.Png);

                Bitmap image2 = new Bitmap(_width, _width);
                Graphics g2 = Graphics.FromImage(image2);
                g2.DrawImage(pic2Back.Image, rect);
                g2.DrawImage(image, rect);
                image2.Save(Path.Combine(txtToPath.Text, "" + index + "_2.png"), ImageFormat.Png);

                Bitmap image3 = new Bitmap(_width, _width);
                Graphics g3 = Graphics.FromImage(image3);
                g3.DrawImage(pic3Back.Image, rect);
                g3.DrawImage(image, rect);
                image3.Save(Path.Combine(txtToPath.Text, "" + index + "_3.png"), ImageFormat.Png);


                index++;
            }
        }

        private void btnGenerateSamll4_Click(object sender, EventArgs e)
        {
            string[] files = Directory.GetFiles(txtFromPath.Text, "*.png", SearchOption.AllDirectories);
            int index = 1;
            foreach (string file in files)
            {
                Bitmap image = new Bitmap(file);
                Bitmap image1 = new Bitmap(_width, _width);
                Graphics g = Graphics.FromImage(image1);
                Rectangle rect = new Rectangle(0, 0, _width, _width);
                g.DrawImage(image, rect);
                image1.Save(Path.Combine(txtToPath.Text, "" + index + "_4.png"), ImageFormat.Png);

                index++;
            }
        }

        private void rdb30_CheckedChanged(object sender, EventArgs e)
        {
            if (rdb30.Checked)
            {
                _width = 30;
            }else if (rdb48.Checked)
            {
                _width = 48;
            }
            else if (rdb96.Checked)
            {
                _width = 96;
            }
        }
    }
}
