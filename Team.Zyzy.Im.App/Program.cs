﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using YiWangYi.AutoUpdater.UpdateHelper;
using System.Net;
using System.Xml;
using YiWangYi.MsgQQ.WinUI;
using YiWangYi.MsgQQ.WinUI.Interface;
using YiWangYi.MsgQQ.WinUI.Forms;
using CefSharp;

namespace Team.Zyzy.Im.App
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.ThreadException += Application_ThreadException;
            //AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                updateApplication();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "更新失败！", MessageBoxButtons.OK);
            }
            FrmMain f = new FrmMain();
            ShareInfo.FormQQMain = (IFormQQMain)f;
            Application.Run(f);
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show("CurrentDomain"+e.ToString());
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            if (e.Exception.Source == "Microsoft.MixedReality.WebRTC")
            {
            }
            else
            {
                MessageBox.Show("Application" + e.Exception.StackTrace);
            }
        }


        #region 执行更新相关操作

        //更新应用程序
        private static void updateApplication()
        {
            #region check and download new version program

            bool bHasError = false;
            NetAutoUpdater autoUpdater = new NetAutoUpdater();
            if (!autoUpdater.ServerUrlIsExist())//如果更新地址不存在则跳过更新
            {
                return;
            }
            try
            {
                autoUpdater.Update();
            }
            catch (WebException exp)
            {
                MessageBox.Show(exp.Message, "找不到指定资源，可能是服务器地址配置错误！", MessageBoxButtons.OK);
                bHasError = true;
            }
            catch (XmlException exp)
            {
                bHasError = true;
                MessageBox.Show(exp.Message, "下载升级文件错误！", MessageBoxButtons.OK);
            }
            catch (NotSupportedException exp)
            {
                bHasError = true;
                MessageBox.Show(exp.Message, "升级地址配置错误！", MessageBoxButtons.OK);
            }
            catch (ArgumentException exp)
            {
                bHasError = true;
                MessageBox.Show(exp.Message, "下载升级文件错误！", MessageBoxButtons.OK);
            }
            catch (Exception exp)
            {
                bHasError = true;
                MessageBox.Show(exp.Message, "一个未知错误发生在升级过程！", MessageBoxButtons.OK);
            }
            finally
            {
                if (bHasError == true)
                {
                    try
                    {
                        autoUpdater.RollBack();
                    }
                    catch (Exception)
                    {
                        //Log the message to your file or database
                    }
                }
            }
            #endregion
        }

        #endregion
    }
}
