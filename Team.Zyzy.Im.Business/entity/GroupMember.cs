﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.entity
{
    public class GroupMember
    {
        public long id;
        public long groupId;
        public long userId;
        public String aliasName;
        public String headImage;
        public String remark;
        public Boolean quit;
        public DateTime createdTime;
    }
}
