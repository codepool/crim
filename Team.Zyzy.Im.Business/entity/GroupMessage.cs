﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.enums;

namespace Team.Zyzy.Im.Business.entity
{
    public class GroupMessage : IComparable<GroupMessage>, IEquatable<GroupMessage>
    {
        public long id;
        public long groupId;
        public long sendId;
        public String content;
        public String sendNickName;
        public List<long> atUserIds;
        public int type;
        public int status;
        public long sendTime;

        public string getAtUserIds()
        {
            if (atUserIds != null && atUserIds.Count > 0)
            {
                return String.Join(",", this.atUserIds);
            }
            return "";
        }

        public void setAtUserIds(string atUserIds)
        {
            if(atUserIds!=null && atUserIds.Length > 0)
            {
                string[] ids = atUserIds.Split(',');
                this.atUserIds = new List<long>();
                foreach(string id in ids)
                {
                    this.atUserIds.Add(long.Parse(id));
                }

            }
        }

        public DateTime getSendTime()
        {
            if (sendTime.Equals(0))
            {
                return DateTime.Now;
            }
            return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).AddMilliseconds(sendTime);
        }
        public void setSendTime(DateTime dateTime)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            sendTime = (long)(dateTime - startTime).TotalMilliseconds;
        }

        public bool Equals(GroupMessage other)
        {
            return this.id == other.id;
        }
        public int CompareTo(GroupMessage other)
        {
            if (this.id > other.id)
                return 1;
            if (this.id == other.id)
                return 0;
            return -1;
        }

        public string GetContentText()
        {
            switch ((MessageStatus)this.status)
            {
                case MessageStatus.Recall:
                    return sendNickName + "已经撤回";
                case MessageStatus.UnSend:
                    return sendNickName + "发送失败";
            }
            switch (this.type)
            {
                case (int)MessageType.TEXT:
                    return this.sendNickName + "：" + this.content;
                case (int)MessageType.IMAGE:
                    return this.sendNickName + "：" + "图片";
                case (int)MessageType.FILE:
                    return this.sendNickName + "：" + "文件";
                case (int)MessageType.AUDIO:
                    return this.sendNickName + "：" + "语音";
                case (int)MessageType.VIDEO:
                    return this.sendNickName + "：" + "视频";
            }
            return this.sendNickName + "：" + "其他";
        }
    }
}
