﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.entity
{
    public class Group
    {
        public long id;
        public String name;
        public long ownerId;
        public String headImage;
        public String headImageThumb;
        public String notice;
        public Boolean deleted;
        public DateTime createdTime;
    }
}
