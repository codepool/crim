﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.entity
{
    public class Friend
    {
        public long id;
        public long userId;
        public long friendId;
        public String friendNickName;
        public String friendHeadImage;
        public DateTime createdTime;
    }
}