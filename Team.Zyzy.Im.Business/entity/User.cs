﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.entity
{
    public class User
    {
        public long id;
        public String userName;
        public String nickName;
        public int sex;
        public String headImage;
        public String headImageThumb;
        public String signature;
        public String password;
        public DateTime lastLoginTime;
        public DateTime createdTime;
        public bool online;
    }
}
