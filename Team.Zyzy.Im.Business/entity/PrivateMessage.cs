﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Team.Zyzy.Im.Business.enums;

namespace Team.Zyzy.Im.Business.entity
{
    public class PrivateMessage:IComparable<PrivateMessage>, IEquatable<PrivateMessage>
    {
        public long id;
        public long sendId;
        public long recvId;
        public String content;
        public int type;
        public int status;
        public long? sendTime;

        public DateTime getSendTime()
        {
            if (sendTime == null)
            {
                return DateTime.Now;
            }
            if (sendTime.Equals(0))
            {
                return DateTime.Now;
            }
            return TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).AddMilliseconds(sendTime.Value);
        }

        public void setSendTime(DateTime dateTime)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            sendTime=(long)(dateTime - startTime).TotalMilliseconds;
        }

        public bool Equals(PrivateMessage other)
        {
            return this.id == other.id;
        }
        public int CompareTo(PrivateMessage other)
        {
            if (this.id > other.id)
                return 1;
            if (this.id == other.id)
                return 0;
            return -1;
        }

        public string GetContentText(long userId)
        {
            string sendName = (this.sendId == userId) ? "我：" : "对方：";
            switch ((MessageStatus)this.status)
            {
                case MessageStatus.Recall:
                    return sendName + "已经撤回";
                case MessageStatus.UnSend:
                    return sendName + "发送失败";
            }
            switch (this.type)
            {
                case (int)MessageType.TEXT:
                    return sendName+this.content;
                case (int)MessageType.IMAGE:
                    return sendName + "图片";
                case (int)MessageType.FILE:
                    return sendName + "文件";
                case (int)MessageType.AUDIO:
                    return sendName + "语音";
                case (int)MessageType.VIDEO:
                    return sendName + "视频";
            }
            return sendName + "其他";
        }
    }
}
