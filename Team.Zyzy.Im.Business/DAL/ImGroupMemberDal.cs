﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using YiWangYi.DBUtility;
using Team.Zyzy.Im.Business.Model;

namespace Team.Zyzy.Im.Business.DAL
{
	 	//im_group_member
		public partial class ImGroupMemberDal
	{
		public bool Exists(long id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from im_group_member");
			strSql.Append(" where ");
			                                       strSql.Append(" id = @id  ");
                            			SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			return DbHelperSQLite.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public long Add(ImGroupMember model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into im_group_member(");			
            strSql.Append("id,group_id,user_id,alias_name,head_image,remark,quit,created_time");
			strSql.Append(") values (");
            strSql.Append("@id,@group_id,@user_id,@alias_name,@head_image,@remark,@quit,@created_time");            
            strSql.Append(") ");            
            strSql.Append(";");		
			SQLiteParameter[] parameters = {
				        new SQLiteParameter("@id", DbType.Int64) ,
						new SQLiteParameter("@group_id", DbType.Int64) ,            
                        new SQLiteParameter("@user_id", DbType.Int64) ,            
                        new SQLiteParameter("@alias_name", DbType.String) ,            
                        new SQLiteParameter("@head_image", DbType.String) ,            
                        new SQLiteParameter("@remark", DbType.String) ,            
                        new SQLiteParameter("@quit", DbType.Byte) ,            
                        new SQLiteParameter("@created_time", DbType.DateTime)             
              
            };
			parameters[0].Value = model.id;
			parameters[1].Value = model.group_id;                        
            parameters[2].Value = model.user_id;                        
            parameters[3].Value = model.alias_name;                        
            parameters[4].Value = model.head_image;                        
            parameters[5].Value = model.remark;                        
            parameters[6].Value = model.quit;                        
            parameters[7].Value = model.created_time;                        
			   
			object obj = DbHelperSQLite.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt64(obj);
                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(ImGroupMember model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update im_group_member set ");
			                                                
            strSql.Append(" group_id = @group_id , ");                                    
            strSql.Append(" user_id = @user_id , ");                                    
            strSql.Append(" alias_name = @alias_name , ");                                    
            strSql.Append(" head_image = @head_image , ");                                    
            strSql.Append(" remark = @remark , ");                                    
            strSql.Append(" quit = @quit , ");                                    
            strSql.Append(" created_time = @created_time  ");            			
			strSql.Append(" where id=@id ");
						
SQLiteParameter[] parameters = {
			            new SQLiteParameter("@id", DbType.Int64) ,            
                        new SQLiteParameter("@group_id", DbType.Int64) ,            
                        new SQLiteParameter("@user_id", DbType.Int64) ,            
                        new SQLiteParameter("@alias_name", DbType.String) ,            
                        new SQLiteParameter("@head_image", DbType.String) ,            
                        new SQLiteParameter("@remark", DbType.String) ,            
                        new SQLiteParameter("@quit", DbType.Byte) ,            
                        new SQLiteParameter("@created_time", DbType.DateTime)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.group_id;                        
            parameters[2].Value = model.user_id;                        
            parameters[3].Value = model.alias_name;                        
            parameters[4].Value = model.head_image;                        
            parameters[5].Value = model.remark;                        
            parameters[6].Value = model.quit;                        
            parameters[7].Value = model.created_time;                        
            int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		///设置删除标志   
		/// </summary>
		public bool SetDeleteTag(int id ,bool isDelete)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update im_group_member set IsDelete="+(isDelete ? 1 : 0));
			strSql.Append(" where id=@id ");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(long id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from im_group_member ");
			strSql.Append(" where id=@id");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;


			int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		 
		/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string selectList )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from im_group_member ");
	        		            	strSql.Append(" where ID in ("+selectList + ")  ");
						int rows=DbHelperSQLite.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
			 
		}
		 
		
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImGroupMember GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, group_id, user_id, alias_name, head_image, remark, quit, created_time  ");			
			strSql.Append("  from im_group_member ");
			strSql.Append(" where id=@id");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			
			ImGroupMember model=new ImGroupMember();
			DataSet ds=DbHelperSQLite.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["group_id"].ToString()!="")
				{
					model.group_id=long.Parse(ds.Tables[0].Rows[0]["group_id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["user_id"].ToString()!="")
				{
					model.user_id=long.Parse(ds.Tables[0].Rows[0]["user_id"].ToString());
				}
																																				model.alias_name= ds.Tables[0].Rows[0]["alias_name"].ToString();
																																model.head_image= ds.Tables[0].Rows[0]["head_image"].ToString();
																																model.remark= ds.Tables[0].Rows[0]["remark"].ToString();
																												if(ds.Tables[0].Rows[0]["quit"].ToString()!="")
				{
					model.quit=int.Parse(ds.Tables[0].Rows[0]["quit"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["created_time"].ToString()!="")
				{
					model.created_time=DateTime.Parse(ds.Tables[0].Rows[0]["created_time"].ToString());
				}
																														
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM im_group_member ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQLite.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM im_group_member ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQLite.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("select * from V__group_member_View where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.AppendLine(strWhere);
            }
            if (orderby.Trim() != "")
            {
                strSql.AppendLine(" order by ");
                strSql.AppendLine(orderby);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("select * from V__group_member_View where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.AppendLine(strWhere);
            }
            
            IDataParameter[] parameters = new SQLiteParameter[5];   
            parameters[0]=new SQLiteParameter("@SQL", DbType.String);   
            parameters[1]=new SQLiteParameter("@Order", DbType.String); 
            parameters[2]=new SQLiteParameter("@PageIndex", DbType.Int32);  
            parameters[3]=new SQLiteParameter("@PageSize", DbType.Int32);   
            parameters[4]=new SQLiteParameter("@TotalRecorder", DbType.Int32);  
            parameters[4].Direction=ParameterDirection.Output;
  
            parameters[0].Value=strSql.ToString();   
            parameters[1].Value=orderby;   
            parameters[2].Value=pageIndex;   
            parameters[3].Value=pageSize;   
            DataSet ds = DbHelperSQL.RunProcedure("P_SplitPage1", parameters,"tbCurrentPage");
            recordCount = int.Parse(parameters[4].Value == System.DBNull.Value ? "0" : parameters[4].Value.ToString());
            return ds;
        }
        
	}
}

