﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using YiWangYi.DBUtility;
using Team.Zyzy.Im.Business.Model;

namespace Team.Zyzy.Im.Business.DAL
{
	 	//im_private_message
		public partial class ImPrivateMessageDal
	{
		public bool Exists(long id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from im_private_message");
			strSql.Append(" where ");
			                                       strSql.Append(" id = @id  ");
                            			SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			return DbHelperSQLite.Exists(strSql.ToString(),parameters);
		}

		public bool UpdateMessageStatus(long send_id ,long recv_id,int status)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update im_private_message set ");
			strSql.Append(" status = @status   ");
			strSql.Append(" where send_id = @send_id and recv_id = @recv_id ");

			SQLiteParameter[] parameters = {
						new SQLiteParameter("@send_id", DbType.Int64) ,
						new SQLiteParameter("@recv_id", DbType.Int64) ,
						new SQLiteParameter("@status", DbType.Byte) 

			};

			parameters[0].Value = send_id;
			parameters[1].Value = recv_id;
			parameters[2].Value = status;
			int rows = DbHelperSQLite.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public bool UpdateRecallMessage(long id, int status)
		{
			StringBuilder strSql = new StringBuilder();
			strSql.Append("update im_private_message set ");
			strSql.Append(" status = @status   ");
			strSql.Append(" where id = @id ");

			SQLiteParameter[] parameters = {
						new SQLiteParameter("@id", DbType.Int64) ,
						new SQLiteParameter("@status", DbType.Byte)

			};

			parameters[0].Value = id;
			parameters[1].Value = status;
			int rows = DbHelperSQLite.ExecuteSql(strSql.ToString(), parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}



		/// <summary>
		/// 增加一条数据
		/// </summary>
		public long Add(ImPrivateMessage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into im_private_message(");			
            strSql.Append("id,send_id,recv_id,content,type,status,send_time");
			strSql.Append(") values (");
            strSql.Append("@id,@send_id,@recv_id,@content,@type,@status,@send_time");            
            strSql.Append(") ");            
            strSql.Append(";");		
			SQLiteParameter[] parameters = {
						new SQLiteParameter("@id", DbType.Int64) ,
						new SQLiteParameter("@send_id", DbType.Int64) ,            
                        new SQLiteParameter("@recv_id", DbType.Int64) ,            
                        new SQLiteParameter("@content", DbType.String) ,            
                        new SQLiteParameter("@type", DbType.Byte) ,            
                        new SQLiteParameter("@status", DbType.Byte) ,            
                        new SQLiteParameter("@send_time", DbType.DateTime)             
              
            };
			parameters[0].Value = model.id;
			parameters[1].Value = model.send_id;                        
            parameters[2].Value = model.recv_id;                        
            parameters[3].Value = model.content;                        
            parameters[4].Value = model.type;                        
            parameters[5].Value = model.status;                        
            parameters[6].Value = model.send_time;                        
			   
			object obj = DbHelperSQLite.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{				                    
            	return Convert.ToInt64(obj);                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(ImPrivateMessage model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update im_private_message set ");
			                                                
            strSql.Append(" send_id = @send_id , ");                                    
            strSql.Append(" recv_id = @recv_id , ");                                    
            strSql.Append(" content = @content , ");                                    
            strSql.Append(" type = @type , ");                                    
            strSql.Append(" status = @status , ");                                    
            strSql.Append(" send_time = @send_time  ");            			
			strSql.Append(" where id=@id ");
						
SQLiteParameter[] parameters = {
			            new SQLiteParameter("@id", DbType.Int64) ,            
                        new SQLiteParameter("@send_id", DbType.Int64) ,            
                        new SQLiteParameter("@recv_id", DbType.Int64) ,            
                        new SQLiteParameter("@content", DbType.String) ,            
                        new SQLiteParameter("@type", DbType.Byte) ,            
                        new SQLiteParameter("@status", DbType.Byte) ,            
                        new SQLiteParameter("@send_time", DbType.DateTime)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.send_id;                        
            parameters[2].Value = model.recv_id;                        
            parameters[3].Value = model.content;                        
            parameters[4].Value = model.type;                        
            parameters[5].Value = model.status;                        
            parameters[6].Value = model.send_time;                        
            int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		///设置删除标志   
		/// </summary>
		public bool SetDeleteTag(int id ,bool isDelete)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update im_private_message set IsDelete="+(isDelete ? 1 : 0));
			strSql.Append(" where id=@id ");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(long id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from im_private_message ");
			strSql.Append(" where id=@id");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;


			int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		 
		/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string selectList )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from im_private_message ");
	        		            	strSql.Append(" where ID in ("+selectList + ")  ");
						int rows=DbHelperSQLite.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
			 
		}
		 
		
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImPrivateMessage GetModel(long id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, send_id, recv_id, content, type, status, send_time  ");			
			strSql.Append("  from im_private_message ");
			strSql.Append(" where id=@id");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			
			ImPrivateMessage model=new ImPrivateMessage();
			DataSet ds=DbHelperSQLite.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["send_id"].ToString()!="")
				{
					model.send_id=long.Parse(ds.Tables[0].Rows[0]["send_id"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["recv_id"].ToString()!="")
				{
					model.recv_id=long.Parse(ds.Tables[0].Rows[0]["recv_id"].ToString());
				}
																																				model.content= ds.Tables[0].Rows[0]["content"].ToString();
																												if(ds.Tables[0].Rows[0]["type"].ToString()!="")
				{
					model.type=int.Parse(ds.Tables[0].Rows[0]["type"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["status"].ToString()!="")
				{
					model.status=int.Parse(ds.Tables[0].Rows[0]["status"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["send_time"].ToString()!="")
				{
					model.send_time=DateTime.Parse(ds.Tables[0].Rows[0]["send_time"].ToString());
				}
																														
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM im_private_message ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQLite.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM im_private_message ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQLite.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("select * from V__private_message_View where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.AppendLine(strWhere);
            }
            if (orderby.Trim() != "")
            {
                strSql.AppendLine(" order by ");
                strSql.AppendLine(orderby);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("select * from V__private_message_View where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.AppendLine(strWhere);
            }
            
            IDataParameter[] parameters = new SQLiteParameter[5];   
            parameters[0]=new SQLiteParameter("@SQL", DbType.String);   
            parameters[1]=new SQLiteParameter("@Order", DbType.String); 
            parameters[2]=new SQLiteParameter("@PageIndex", DbType.Int32);  
            parameters[3]=new SQLiteParameter("@PageSize", DbType.Int32);   
            parameters[4]=new SQLiteParameter("@TotalRecorder", DbType.Int32);  
            parameters[4].Direction=ParameterDirection.Output;
  
            parameters[0].Value=strSql.ToString();   
            parameters[1].Value=orderby;   
            parameters[2].Value=pageIndex;   
            parameters[3].Value=pageSize;   
            DataSet ds = DbHelperSQL.RunProcedure("P_SplitPage1", parameters,"tbCurrentPage");
            recordCount = int.Parse(parameters[4].Value == System.DBNull.Value ? "0" : parameters[4].Value.ToString());
            return ds;
        }
	}
}

