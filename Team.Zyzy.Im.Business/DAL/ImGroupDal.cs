﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using YiWangYi.DBUtility;
using Team.Zyzy.Im.Business.Model;

namespace Team.Zyzy.Im.Business.DAL
{
	 	//im_group
		public partial class ImGroupDal
	{
		public bool Exists(long id)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) from im_group");
			strSql.Append(" where ");
			                                       strSql.Append(" id = @id  ");
                            			SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			return DbHelperSQLite.Exists(strSql.ToString(),parameters);
		}
		
				
		
		/// <summary>
		/// 增加一条数据
		/// </summary>
		public long Add(ImGroup model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("insert into im_group(");			
            strSql.Append("id,name,owner_id,head_image,head_image_thumb,notice,remark,deleted,created_time");
			strSql.Append(") values (");
            strSql.Append("@id,@name,@owner_id,@head_image,@head_image_thumb,@notice,@remark,@deleted,@created_time");            
            strSql.Append(") ");            
            strSql.Append(";");		
			SQLiteParameter[] parameters = {
				        new SQLiteParameter("@id", DbType.Int64) ,
						new SQLiteParameter("@name", DbType.String) ,            
                        new SQLiteParameter("@owner_id", DbType.Int64) ,            
                        new SQLiteParameter("@head_image", DbType.String) ,            
                        new SQLiteParameter("@head_image_thumb", DbType.String) ,            
                        new SQLiteParameter("@notice", DbType.String) ,            
                        new SQLiteParameter("@remark", DbType.String) ,            
                        new SQLiteParameter("@deleted", DbType.Byte) ,            
                        new SQLiteParameter("@created_time", DbType.DateTime)             
              
            };
			parameters[0].Value = model.id;
			parameters[1].Value = model.name;                        
            parameters[2].Value = model.owner_id;                        
            parameters[3].Value = model.head_image;                        
            parameters[4].Value = model.head_image_thumb;                        
            parameters[5].Value = model.notice;                        
            parameters[6].Value = model.remark;                        
            parameters[7].Value = model.deleted;                        
            parameters[8].Value = model.created_time;                        
			   
			object obj = DbHelperSQLite.GetSingle(strSql.ToString(),parameters);			
			if (obj == null)
			{
				return 0;
			}
			else
			{
				                    
            	return Convert.ToInt64(obj);
                                                                  
			}			   
            			
		}
		
		
		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(ImGroup model)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update im_group set ");
			                                                
            strSql.Append(" name = @name , ");                                    
            strSql.Append(" owner_id = @owner_id , ");                                    
            strSql.Append(" head_image = @head_image , ");                                    
            strSql.Append(" head_image_thumb = @head_image_thumb , ");                                    
            strSql.Append(" notice = @notice , ");                                    
            strSql.Append(" remark = @remark , ");                                    
            strSql.Append(" deleted = @deleted , ");                                    
            strSql.Append(" created_time = @created_time  ");            			
			strSql.Append(" where id=@id ");
						
SQLiteParameter[] parameters = {
			            new SQLiteParameter("@id", DbType.Int64) ,            
                        new SQLiteParameter("@name", DbType.String) ,            
                        new SQLiteParameter("@owner_id", DbType.Int64) ,            
                        new SQLiteParameter("@head_image", DbType.String) ,            
                        new SQLiteParameter("@head_image_thumb", DbType.String) ,            
                        new SQLiteParameter("@notice", DbType.String) ,            
                        new SQLiteParameter("@remark", DbType.String) ,            
                        new SQLiteParameter("@deleted", DbType.Byte) ,            
                        new SQLiteParameter("@created_time", DbType.DateTime)             
              
            };
						            
            parameters[0].Value = model.id;                        
            parameters[1].Value = model.name;                        
            parameters[2].Value = model.owner_id;                        
            parameters[3].Value = model.head_image;                        
            parameters[4].Value = model.head_image_thumb;                        
            parameters[5].Value = model.notice;                        
            parameters[6].Value = model.remark;                        
            parameters[7].Value = model.deleted;                        
            parameters[8].Value = model.created_time;                        
            int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		///设置删除标志   
		/// </summary>
		public bool SetDeleteTag(int id ,bool isDelete)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("update im_group set IsDelete="+(isDelete ? 1 : 0));
			strSql.Append(" where id=@id ");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(long id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from im_group ");
			strSql.Append(" where id=@id");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;


			int rows=DbHelperSQLite.ExecuteSql(strSql.ToString(),parameters);
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		 
		/// <summary>
		/// 批量删除一批数据
		/// </summary>
		public bool DeleteList(string selectList )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from im_group ");
	        		            	strSql.Append(" where ID in ("+selectList + ")  ");
						int rows=DbHelperSQLite.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
			 
		}
		 
		
		
		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImGroup GetModel(int id)
		{
			
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id, name, owner_id, head_image, head_image_thumb, notice, remark, deleted, created_time  ");			
			strSql.Append("  from im_group ");
			strSql.Append(" where id=@id");
						SQLiteParameter[] parameters = {
					new SQLiteParameter("@id", DbType.Int64,8)
			};
			parameters[0].Value = id;

			
			ImGroup model=new ImGroup();
			DataSet ds=DbHelperSQLite.Query(strSql.ToString(),parameters);
			
			if(ds.Tables[0].Rows.Count>0)
			{
												if(ds.Tables[0].Rows[0]["id"].ToString()!="")
				{
					model.id=int.Parse(ds.Tables[0].Rows[0]["id"].ToString());
				}
																																				model.name= ds.Tables[0].Rows[0]["name"].ToString();
																												if(ds.Tables[0].Rows[0]["owner_id"].ToString()!="")
				{
					model.owner_id=long.Parse(ds.Tables[0].Rows[0]["owner_id"].ToString());
				}
																																				model.head_image= ds.Tables[0].Rows[0]["head_image"].ToString();
																																model.head_image_thumb= ds.Tables[0].Rows[0]["head_image_thumb"].ToString();
																																model.notice= ds.Tables[0].Rows[0]["notice"].ToString();
																																model.remark= ds.Tables[0].Rows[0]["remark"].ToString();
																												if(ds.Tables[0].Rows[0]["deleted"].ToString()!="")
				{
					model.deleted=int.Parse(ds.Tables[0].Rows[0]["deleted"].ToString());
				}
																																if(ds.Tables[0].Rows[0]["created_time"].ToString()!="")
				{
					model.created_time=DateTime.Parse(ds.Tables[0].Rows[0]["created_time"].ToString());
				}
																														
				return model;
			}
			else
			{
				return null;
			}
		}
		
		
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select * ");
			strSql.Append(" FROM im_group ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQLite.Query(strSql.ToString());
		}
		
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" * ");
			strSql.Append(" FROM im_group ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQLite.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("select * from V__group_View where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.AppendLine(strWhere);
            }
            if (orderby.Trim() != "")
            {
                strSql.AppendLine(" order by ");
                strSql.AppendLine(orderby);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("select * from V__group_View where 1=1 ");
            if (strWhere.Trim() != "")
            {
                strSql.AppendLine(strWhere);
            }
            
            IDataParameter[] parameters = new SQLiteParameter[5];   
            parameters[0]=new SQLiteParameter("@SQL", DbType.String);   
            parameters[1]=new SQLiteParameter("@Order", DbType.String); 
            parameters[2]=new SQLiteParameter("@PageIndex", DbType.Int32);  
            parameters[3]=new SQLiteParameter("@PageSize", DbType.Int32);   
            parameters[4]=new SQLiteParameter("@TotalRecorder", DbType.Int32);  
            parameters[4].Direction=ParameterDirection.Output;
  
            parameters[0].Value=strSql.ToString();   
            parameters[1].Value=orderby;   
            parameters[2].Value=pageIndex;   
            parameters[3].Value=pageSize;   
            DataSet ds = DbHelperSQL.RunProcedure("P_SplitPage1", parameters,"tbCurrentPage");
            recordCount = int.Parse(parameters[4].Value == System.DBNull.Value ? "0" : parameters[4].Value.ToString());
            return ds;
        }
	}
}

