﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class GroupInviteVO
    {

        public long groupId;

        public List<long> friendIds;
    }
}
