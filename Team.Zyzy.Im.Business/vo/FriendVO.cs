﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class FriendVO
    {

        public long id;

        public String nickName;

        public String headImage;

        public String signature;

        public String remarkName;

        public long classifyId;

        public String onlineRemark;
    }

}
