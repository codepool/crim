﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class ExchangeCandidateVO
    {
        public String candidate;

        public String usernameFragment;

        public String sdpMid;

        public int sdpMLineIndex;

        public void setUsernameFragmentBySdp(string sdp)
        {
            this.candidate = sdp;
            if (sdp != null)
            {
                string[] cds = sdp.Split(' ');
                if (cds.Length > 10)
                {
                    this.usernameFragment = cds[11];
                }
            }
        }
    }
}
