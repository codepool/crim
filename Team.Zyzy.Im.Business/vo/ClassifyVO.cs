﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Team.Zyzy.Im.Business.vo
{
    public class ClassifyVO:IComparable<ClassifyVO>
    {
        public long id;
        public String classifyName;
        public int type;
        public int sortKey;

        public int CompareTo(ClassifyVO other)
        {
            if (this.type == other.type)
            {
                if (this.sortKey == other.sortKey)
                {
                    return this.classifyName.CompareTo(other.classifyName);
                }
                return this.sortKey.CompareTo(other.sortKey);
            }
            return this.type.CompareTo(other.type);
        }
    }
}
