﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class IceCandidateVO
    {
        public String sdp;

        public String type;

        public String sdpMid;

        public int sdpMLineIndex;
    }
}
