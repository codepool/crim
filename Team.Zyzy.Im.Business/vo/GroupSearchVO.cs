﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class GroupSearchVO
    {

        public long id;

        public String name;

        public long ownerId;
        public String ownerName;
        public bool ownerOnline;

        public String headImage;

        public String headImageThumb;

        public String remark;

    }
}