﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class UserVO
    {

        public long id;
        public String userName;
        public String nickName;
        public int sex;
        public int type;
        public String signature;
        public String headImage;
        public String headImageThumb;
        public Boolean? online;

        public String onlineRemark;
        public String phone;
        public int ssoType;
        public String ssoName;
        public String ssoKey;

    }
}
