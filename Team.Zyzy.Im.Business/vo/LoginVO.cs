﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class LoginVO
    {

        public String accessToken;
        public int accessTokenExpiresIn;
        public String refreshToken;
        public int refreshTokenExpiresIn;
    }

}
