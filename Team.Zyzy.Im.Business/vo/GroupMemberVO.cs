﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class GroupMemberVO
    {

        public long userId;

        public String aliasName;

        public String headImage;

        public Boolean quit;

        public Boolean online;

        public String remark;

    }
}
