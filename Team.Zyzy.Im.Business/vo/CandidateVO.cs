﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class CandidateVO
    {
        /// <summary>
        /// 
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string candidate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string component { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string foundation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int port { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long priority { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string protocol { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string relatedAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string relatedPort { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int sdpMLineIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string sdpMid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string tcpType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string usernameFragment { get; set; }

        public void initBySdp(string sdp)
        {
            if (sdp != null)
            {
                string[] cds = sdp.Split(' ');
                if (cds.Length > 10)
                {
                    this.candidate = sdp;
                    this.foundation = cds[0].Replace("candidate:", "");
                    this.component = (cds[1]=="1") ? "rtp" : "rtcp";
                    this.protocol = cds[2];
                    this.priority = long.Parse(cds[3]);
                    this.address = cds[4];
                    this.port = int.Parse(cds[5]);
                    this.type = cds[7];
                    this.sdpMid = cds[9];
                    this.usernameFragment = cds[11];
                }
            }
        }
    }
}
