﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class HistoryMessagePageVO
    {

        public long friendId { get; set; }
        public long groupId { get; set; }
        public long page { get; set; }
        public long size { get; set; }
    }

}
