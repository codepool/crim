﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.vo
{
    public class GroupVO
    {

        public long id;

        public long? classifyId;

        public String name;

        public long ownerId;

        public String headImage;

        public String headImageThumb;

        public String notice;

        public String aliasName;

        public String remark;

    }

}
