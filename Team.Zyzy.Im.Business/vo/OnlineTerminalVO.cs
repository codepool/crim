﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YiWangYi.MsgQQ.WinUI.General.QQListBar.QQListBar;

namespace Team.Zyzy.Im.Business.vo
{
    public class OnlineTerminalVO
    {
        public long  userId;
        public List<int> terminals;
        public TerminalType GetTerminalType()
        {
            TerminalType terminalType = TerminalType.None;
            if (terminals.Contains(0))
            {
                terminalType = terminalType | TerminalType.Web;
            }
            if(terminals.Contains(1))
            {
                terminalType = terminalType | TerminalType.App;
            }
            if(terminals.Contains(2))
            {
                terminalType = terminalType | TerminalType.Pc;
            }
            return terminalType;
        }
    }
}
