﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Team.Zyzy.Im.Business.Model{

	/// <summary>
    /// im_friend
    /// </summary>
	[Serializable]
	public class ImFriend
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private long _id;
        public long id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// user_id
        /// </summary>		
		private long _user_id;
        public long user_id
        {
            get{ return _user_id; }
            set{ _user_id = value; }
        }        
		/// <summary>
		/// friend_id
        /// </summary>		
		private long _friend_id;
        public long friend_id
        {
            get{ return _friend_id; }
            set{ _friend_id = value; }
        }        
		/// <summary>
		/// friend_nick_name
        /// </summary>		
		private string _friend_nick_name;
        public string friend_nick_name
        {
            get{ return _friend_nick_name; }
            set{ _friend_nick_name = value; }
        }        
		/// <summary>
		/// friend_head_image
        /// </summary>		
		private string _friend_head_image;
        public string friend_head_image
        {
            get{ return _friend_head_image; }
            set{ _friend_head_image = value; }
        }        
		/// <summary>
		/// created_time
        /// </summary>		
		private DateTime _created_time=new DateTime(1986, 1, 9);
        public DateTime created_time
        {
            get{ return _created_time; }
            set{ _created_time = value; }
        }        
				
				
	}
}

