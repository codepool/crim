﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Team.Zyzy.Im.Business.Model{

	/// <summary>
    /// im_group_member
    /// </summary>
	[Serializable]
	public class ImGroupMember
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private long _id;
        public long id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// group_id
        /// </summary>		
		private long _group_id;
        public long group_id
        {
            get{ return _group_id; }
            set{ _group_id = value; }
        }        
		/// <summary>
		/// user_id
        /// </summary>		
		private long _user_id;
        public long user_id
        {
            get{ return _user_id; }
            set{ _user_id = value; }
        }        
		/// <summary>
		/// alias_name
        /// </summary>		
		private string _alias_name;
        public string alias_name
        {
            get{ return _alias_name; }
            set{ _alias_name = value; }
        }        
		/// <summary>
		/// head_image
        /// </summary>		
		private string _head_image;
        public string head_image
        {
            get{ return _head_image; }
            set{ _head_image = value; }
        }        
		/// <summary>
		/// remark
        /// </summary>		
		private string _remark;
        public string remark
        {
            get{ return _remark; }
            set{ _remark = value; }
        }        
		/// <summary>
		/// quit
        /// </summary>		
		private int _quit;
        public int quit
        {
            get{ return _quit; }
            set{ _quit = value; }
        }        
		/// <summary>
		/// created_time
        /// </summary>		
		private DateTime _created_time=new DateTime(1986, 1, 9);
        public DateTime created_time
        {
            get{ return _created_time; }
            set{ _created_time = value; }
        }        
				
				
	}
}

