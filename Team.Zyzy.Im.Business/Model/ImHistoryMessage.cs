﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using Team.Zyzy.Im.Business.enums;

namespace Team.Zyzy.Im.Business.Model
{

    /// <summary>
    /// im_history_message
    /// </summary>
    [Serializable]
    public class ImHistoryMessage
    {

        /// <summary>
        /// id
        /// </summary>		
        private int _id;
        public int id
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// source_type
        /// </summary>		
        private int _source_type;
        public int source_type
        {
            get { return _source_type; }
            set { _source_type = value; }
        }
        /// <summary>
        /// source_id
        /// </summary>		
        private long _source_id;
        public long source_id
        {
            get { return _source_id; }
            set { _source_id = value; }
        }
        /// <summary>
        /// send_id
        /// </summary>		
        private long _opposite_id;
        public long opposite_id
        {
            get { return _opposite_id; }
            set { _opposite_id = value; }
        }

        private long _send_id;
        public long send_id
        {
            get { return _send_id; }
            set { _send_id = value; }
        }

        private string _send_name;
        public string send_name
        {
            get { return _send_name; }
            set { _send_name = value; }
        }
        private string _name;
        public string opposite_name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// content
        /// </summary>		
        private string _content;
        public string content
        {
            get { return _content; }
            set { _content = value; }
        }
        /// <summary>
        /// type
        /// </summary>		
        private int _type;
        public int type
        {
            get { return _type; }
            set { _type = value; }
        }
        /// <summary>
        /// status
        /// </summary>		
        private int _status;
        public int status
        {
            get { return _status; }
            set { _status = value; }
        }
        /// <summary>
        /// send_time
        /// </summary>		
        private DateTime _send_time = new DateTime(1986, 1, 9);
        public DateTime send_time
        {
            get { return _send_time; }
            set { _send_time = value; }
        }

        public string GetContentText(long userId)
        {
            string sendName = (this.send_id == userId) ? "我：" : "对方：";
            if (source_type == 1)
            {
                sendName = this.send_name + "：";
            }
            return getContentText(sendName);
        }

        public string GetContentText()
        {
            string sendName = this.send_name + "：";
            return getContentText(sendName);
        }

        private string getContentText(string sendName)
        {
            switch ((MessageStatus)this.status)
            {
                case MessageStatus.Recall:
                    return sendName + "已经撤回";
                case MessageStatus.UnSend:
                    return sendName + "发送失败";
            }
            switch (this.type)
            {
                case (int)MessageType.TEXT:
                    return sendName + this.content;
                case (int)MessageType.IMAGE:
                    return sendName + "图片";
                case (int)MessageType.FILE:
                    return sendName + "文件";
                case (int)MessageType.AUDIO:
                    return sendName + "语音";
                case (int)MessageType.VIDEO:
                    return sendName + "视频";
            }
            return sendName + "其他";
        }
    }
}