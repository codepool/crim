﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Team.Zyzy.Im.Business.Model{

	/// <summary>
    /// im_group_message
    /// </summary>
	[Serializable]
	public class ImGroupMessage
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private long _id;
        public long id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// group_id
        /// </summary>		
		private long _group_id;
        public long group_id
        {
            get{ return _group_id; }
            set{ _group_id = value; }
        }        
		/// <summary>
		/// send_id
        /// </summary>		
		private long _send_id;
        public long send_id
        {
            get{ return _send_id; }
            set{ _send_id = value; }
        }        
		/// <summary>
		/// send_nick_name
        /// </summary>		
		private string _send_nick_name;
        public string send_nick_name
        {
            get{ return _send_nick_name; }
            set{ _send_nick_name = value; }
        }        
		/// <summary>
		/// content
        /// </summary>		
		private string _content;
        public string content
        {
            get{ return _content; }
            set{ _content = value; }
        }        
		/// <summary>
		/// at_user_ids
        /// </summary>		
		private string _at_user_ids;
        public string at_user_ids
        {
            get{ return _at_user_ids; }
            set{ _at_user_ids = value; }
        }        
		/// <summary>
		/// type
        /// </summary>		
		private int _type;
        public int type
        {
            get{ return _type; }
            set{ _type = value; }
        }        
		/// <summary>
		/// status
        /// </summary>		
		private int _status;
        public int status
        {
            get{ return _status; }
            set{ _status = value; }
        }        
		/// <summary>
		/// send_time
        /// </summary>		
		private DateTime _send_time=new DateTime(1986, 1, 9);
        public DateTime send_time
        {
            get{ return _send_time; }
            set{ _send_time = value; }
        }        
				
				
	}
}

