﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Team.Zyzy.Im.Business.Model{

	/// <summary>
    /// im_group
    /// </summary>
	[Serializable]
	public class ImGroup
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private long _id;
        public long id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// name
        /// </summary>		
		private string _name;
        public string name
        {
            get{ return _name; }
            set{ _name = value; }
        }        
		/// <summary>
		/// owner_id
        /// </summary>		
		private long _owner_id;
        public long owner_id
        {
            get{ return _owner_id; }
            set{ _owner_id = value; }
        }        
		/// <summary>
		/// head_image
        /// </summary>		
		private string _head_image;
        public string head_image
        {
            get{ return _head_image; }
            set{ _head_image = value; }
        }        
		/// <summary>
		/// head_image_thumb
        /// </summary>		
		private string _head_image_thumb;
        public string head_image_thumb
        {
            get{ return _head_image_thumb; }
            set{ _head_image_thumb = value; }
        }        
		/// <summary>
		/// notice
        /// </summary>		
		private string _notice;
        public string notice
        {
            get{ return _notice; }
            set{ _notice = value; }
        }        
		/// <summary>
		/// remark
        /// </summary>		
		private string _remark;
        public string remark
        {
            get{ return _remark; }
            set{ _remark = value; }
        }        
		/// <summary>
		/// deleted
        /// </summary>		
		private int _deleted;
        public int deleted
        {
            get{ return _deleted; }
            set{ _deleted = value; }
        }        
		/// <summary>
		/// created_time
        /// </summary>		
		private DateTime _created_time=new DateTime(1986, 1, 9);
        public DateTime created_time
        {
            get{ return _created_time; }
            set{ _created_time = value; }
        }        
				
				
	}
}

