﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
namespace Team.Zyzy.Im.Business.Model{

	/// <summary>
    /// im_user
    /// </summary>
	[Serializable]
	public class ImUser
	{
   		     
      	/// <summary>
		/// id
        /// </summary>		
		private long _id;
        public long id
        {
            get{ return _id; }
            set{ _id = value; }
        }        
		/// <summary>
		/// user_name
        /// </summary>		
		private string _user_name;
        public string user_name
        {
            get{ return _user_name; }
            set{ _user_name = value; }
        }        
		/// <summary>
		/// nick_name
        /// </summary>		
		private string _nick_name;
        public string nick_name
        {
            get{ return _nick_name; }
            set{ _nick_name = value; }
        }        
		/// <summary>
		/// head_image
        /// </summary>		
		private string _head_image;
        public string head_image
        {
            get{ return _head_image; }
            set{ _head_image = value; }
        }        
		/// <summary>
		/// head_image_thumb
        /// </summary>		
		private string _head_image_thumb;
        public string head_image_thumb
        {
            get{ return _head_image_thumb; }
            set{ _head_image_thumb = value; }
        }        
		/// <summary>
		/// password
        /// </summary>		
		private string _password;
        public string password
        {
            get{ return _password; }
            set{ _password = value; }
        }        
		/// <summary>
		/// sex
        /// </summary>		
		private int _sex;
        public int sex
        {
            get{ return _sex; }
            set{ _sex = value; }
        }        
		/// <summary>
		/// type
        /// </summary>		
		private int _type;
        public int type
        {
            get{ return _type; }
            set{ _type = value; }
        }        
		/// <summary>
		/// signature
        /// </summary>		
		private string _signature;
        public string signature
        {
            get{ return _signature; }
            set{ _signature = value; }
        }        
		/// <summary>
		/// last_login_time
        /// </summary>		
		private DateTime _last_login_time=new DateTime(1986, 1, 9);
        public DateTime last_login_time
        {
            get{ return _last_login_time; }
            set{ _last_login_time = value; }
        }        
		/// <summary>
		/// created_time
        /// </summary>		
		private DateTime _created_time=new DateTime(1986, 1, 9);
        public DateTime created_time
        {
            get{ return _created_time; }
            set{ _created_time = value; }
        }        
				
				
	}
}

