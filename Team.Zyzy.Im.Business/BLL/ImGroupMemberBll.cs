﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.DAL;

namespace Team.Zyzy.Im.Business.BLL
{
	//ImGroupMember
	public partial class ImGroupMemberBll
	{
   		     
		private readonly ImGroupMemberDal dal=new ImGroupMemberDal();
		private static readonly ImGroupMemberBll _instance = new ImGroupMemberBll();
		private ImGroupMemberBll() { }
		static ImGroupMemberBll() { }
		public static ImGroupMemberBll Instance
        {
            get
            {
                return _instance;
            }
        }
        
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long id)
		{
			return dal.Exists(id);
		}

			/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImGroupMember GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImGroupMember> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImGroupMember> DataTableToList(DataTable dt)
		{
			List<ImGroupMember> modelList = new List<ImGroupMember>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				ImGroupMember model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new ImGroupMember();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=long.Parse(dt.Rows[n]["id"].ToString());
				}
																																if(dt.Rows[n]["group_id"].ToString()!="")
				{
					model.group_id=long.Parse(dt.Rows[n]["group_id"].ToString());
				}
																																if(dt.Rows[n]["user_id"].ToString()!="")
				{
					model.user_id=long.Parse(dt.Rows[n]["user_id"].ToString());
				}
																																				model.alias_name= dt.Rows[n]["alias_name"].ToString();
																																model.head_image= dt.Rows[n]["head_image"].ToString();
																																model.remark= dt.Rows[n]["remark"].ToString();
																												if(dt.Rows[n]["quit"].ToString()!="")
				{
					model.quit=int.Parse(dt.Rows[n]["quit"].ToString());
				}
																																if(dt.Rows[n]["created_time"].ToString()!="")
				{
					model.created_time=DateTime.Parse(dt.Rows[n]["created_time"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}
		
	    /// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            return dal.GetAllPageList(strWhere,orderby);
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            return dal.GetPageList(strWhere,orderby,pageIndex,pageSize,out recordCount);
        }
        

#endregion
   
	}
}