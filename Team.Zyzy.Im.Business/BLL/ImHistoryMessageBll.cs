﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.DAL;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;

namespace Team.Zyzy.Im.Business.BLL
{
	//ImHistoryMessage
	public partial class ImHistoryMessageBll
	{

		private readonly ImHistoryMessageDal dal = new ImHistoryMessageDal();
		private static readonly ImHistoryMessageBll _instance = new ImHistoryMessageBll();
		private ImHistoryMessageBll() { }
		static ImHistoryMessageBll() { }
		public static ImHistoryMessageBll Instance
		{
			get
			{
				return _instance;
			}
		}

		public bool UpdatePrivateMessageStatusRead(long send_id)
		{
			return dal.UpdateMessageStatus(send_id, (int)MessageSourceType.Friend,(int)MessageStatus.Read);
		}

		public bool UpdatePrivateMessageStatusRecall(long send_id)
		{
			return dal.UpdateMessageStatus(send_id, (int)MessageSourceType.Friend, (int)MessageStatus.Recall);
		}
		public bool UpdateGroupMessageStatusRead(long send_id)
		{
			return dal.UpdateMessageStatus(send_id, (int)MessageSourceType.Group, (int)MessageStatus.Read);
		}

		public bool UpdateGroupMessageStatusRecall(long send_id)
		{
			return dal.UpdateMessageStatus(send_id, (int)MessageSourceType.Group, (int)MessageStatus.Recall);
		}

		public List<ImHistoryMessage> GetImHistoryList()
		{
			string sqlwhere = string.Format(" 1=1 order by send_time desc ");
			List<ImHistoryMessage> imHistoryMessages = GetModelList(sqlwhere);
			return imHistoryMessages;
		}

		public ImHistoryMessage GetImHistory(ImHistoryMessage imHistory)
		{
			string sqlwhere = string.Format(" source_type={0} and opposite_id={1} ", imHistory.source_type, imHistory.opposite_id);
			List<ImHistoryMessage> imHistoryMessages = GetModelList(sqlwhere);
			if (imHistoryMessages == null || imHistoryMessages.Count == 0)
			{
				return null;
			}
			else
			{
				return imHistoryMessages[0];

			}
		}

		public int AddOrUpdate(long userId, ImPrivateMessage input)
		{
			long friendId = (input.send_id == userId ? input.recv_id : input.send_id);
			ImHistoryMessage imHistory = new ImHistoryMessage();
			imHistory.source_type = (int)MessageSourceType.Friend;
			imHistory.source_id = input.id;
			imHistory.opposite_id = friendId;
			imHistory.send_id = input.send_id;
			imHistory.send_name= (input.send_id == userId ? "我" : "对方");
			imHistory.content = input.content;
			imHistory.type = input.type;
			imHistory.status = input.status;
			imHistory.send_time = DateTime.Now;
			ImFriend imFriend = ImFriendBll.Instance.GetImFriend(userId, friendId);
			if (imFriend != null)
			{
				imHistory.opposite_name = imFriend.friend_nick_name;
			}
			ImHistoryMessage oldMessage = GetImHistory(imHistory);
			if (oldMessage == null)
			{
				return dal.Add(imHistory);
			}
			else
			{
				imHistory.id = oldMessage.id;
				if (oldMessage.source_id < imHistory.source_id)
				{
					return dal.Update(imHistory) ? imHistory.id : 0;
				}
				return 0;
			}
		}

		public int AddOrUpdate(ImGroupMessage input)
		{
			ImHistoryMessage imHistory = new ImHistoryMessage();
			imHistory.source_type = (int)MessageSourceType.Group;
			imHistory.source_id = input.id;
			imHistory.opposite_id = input.group_id;
			imHistory.send_id = input.send_id;
			imHistory.send_name = input.send_nick_name;
			imHistory.content = input.content;
			imHistory.type = input.type;
			imHistory.status = input.status;
			imHistory.send_time = DateTime.Now;
			ImGroup imgroup = ImGroupBll.Instance.GetImGroup(input.group_id);
			if (imgroup != null)
			{
				imHistory.opposite_name = imgroup.name;
			}
			ImHistoryMessage oldMessage = GetImHistory(imHistory);
			if (oldMessage == null)
			{
				return dal.Add(imHistory);
			}
			else
			{
				imHistory.id = oldMessage.id;
				if ( oldMessage.source_id <= imHistory.source_id)
				{
					return dal.Update(imHistory) ? imHistory.id : 0;
				}
				return 0;
			}
		}

		public long GetGroupMaxId()
        {
			return dal.GetMaxId((int)MessageSourceType.Group);
        }
		public long GetPrivateMaxId()
		{
			return dal.GetMaxId((int)MessageSourceType.Friend);
		}

		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			return dal.Exists(id);
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImHistoryMessage GetModel(long id)
		{

			return dal.GetModel(id);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top, string strWhere, string filedOrder)
		{
			return dal.GetList(Top, strWhere, filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImHistoryMessage> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImHistoryMessage> DataTableToList(DataTable dt)
		{
			List<ImHistoryMessage> modelList = new List<ImHistoryMessage>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				ImHistoryMessage model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new ImHistoryMessage();
					if (dt.Rows[n]["id"].ToString() != "")
					{
						model.id = int.Parse(dt.Rows[n]["id"].ToString());
					}
					if (dt.Rows[n]["source_type"].ToString() != "")
					{
						model.source_type = int.Parse(dt.Rows[n]["source_type"].ToString());
					}
					model.send_name = dt.Rows[n]["send_name"].ToString();
					if (dt.Rows[n]["source_id"].ToString() != "")
					{
						model.source_id = long.Parse(dt.Rows[n]["source_id"].ToString());
					}
					if (dt.Rows[n]["opposite_id"].ToString() != "")
					{
						model.opposite_id = long.Parse(dt.Rows[n]["opposite_id"].ToString());
					}
					model.opposite_name = dt.Rows[n]["opposite_name"].ToString();
					model.content = dt.Rows[n]["content"].ToString();
					if (dt.Rows[n]["type"].ToString() != "")
					{
						model.type = int.Parse(dt.Rows[n]["type"].ToString());
					}
					if (dt.Rows[n]["status"].ToString() != "")
					{
						model.status = int.Parse(dt.Rows[n]["status"].ToString());
					}
					if (dt.Rows[n]["send_time"].ToString() != "")
					{
						model.send_time = DateTime.Parse(dt.Rows[n]["send_time"].ToString());
					}
					if (dt.Rows[n]["send_id"].ToString() != "")
					{
						model.send_id = long.Parse(dt.Rows[n]["send_id"].ToString());
					}

					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere, string orderby)
		{
			return dal.GetAllPageList(strWhere, orderby);
		}

		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere, string orderby, int pageIndex, int pageSize, out int recordCount)
		{
			return dal.GetPageList(strWhere, orderby, pageIndex, pageSize, out recordCount);
		}


		#endregion
	}
}