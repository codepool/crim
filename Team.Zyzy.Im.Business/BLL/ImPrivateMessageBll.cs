﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.DAL;
using Team.Zyzy.Im.Business.entity;
using Team.Zyzy.Im.Business.enums;

namespace Team.Zyzy.Im.Business.BLL
{
	//ImPrivateMessage
	public partial class ImPrivateMessageBll
	{
   		     
		private readonly ImPrivateMessageDal dal=new ImPrivateMessageDal();
		private static readonly ImPrivateMessageBll _instance = new ImPrivateMessageBll();
		private ImPrivateMessageBll() { }
		static ImPrivateMessageBll() { }
		public static ImPrivateMessageBll Instance
        {
            get
            {
                return _instance;
            }
        }

		public bool UpdateMessageStatusRead(long send_id, long recv_id)
		{
			return dal.UpdateMessageStatus(send_id, recv_id, (int)MessageStatus.Read);
		}

		public bool UpdateMessageStatusRecall(long msgId)
		{
			return dal.UpdateRecallMessage(msgId, (int)MessageStatus.Recall);
		}

		public long Add(long userId,PrivateMessage input)
		{
			ImPrivateMessage temp = new ImPrivateMessage();
			temp.id = input.id;
			temp.send_id = input.sendId;
			temp.recv_id = input.recvId;
			temp.content = input.content;
			temp.type = input.type;
			temp.status = input.status;
			temp.send_time = input.getSendTime();
			long id= ImHistoryMessageBll.Instance.AddOrUpdate(userId, temp);
			if (id > 0)
			{
				dal.Add(temp);
			}
			return id;
		}

		public List<PrivateMessage> GetRecentTopList(long friendId,int rowCount)
        {
			List<PrivateMessage> messages = new List<PrivateMessage>();
			string sqlWhere = string.Format(" (send_id={0} or recv_id={0}) order by id desc limit {1} ", friendId, rowCount);
			List<ImPrivateMessage> imPrivates = GetModelList(sqlWhere);
			if(imPrivates==null || imPrivates.Count == 0)
            {
				return messages;
			}
			foreach(ImPrivateMessage item in imPrivates)
            {
				PrivateMessage temp = new PrivateMessage();
				temp.id = item.id;
				temp.sendId = item.send_id;
				temp.recvId = item.recv_id;
				temp.content = item.content;
				temp.type = item.type;
				temp.status = item.status;
				temp.setSendTime(item.send_time);
				messages.Add(temp);
			}
			return messages;
		}



			#region  Method
			/// <summary>
			/// 是否存在该记录
			/// </summary>
			public bool Exists(long id)
		{
			return dal.Exists(id);
		}

			/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImPrivateMessage GetModel(long id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImPrivateMessage> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImPrivateMessage> DataTableToList(DataTable dt)
		{
			List<ImPrivateMessage> modelList = new List<ImPrivateMessage>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				ImPrivateMessage model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new ImPrivateMessage();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=long.Parse(dt.Rows[n]["id"].ToString());
				}
																																if(dt.Rows[n]["send_id"].ToString()!="")
				{
					model.send_id=long.Parse(dt.Rows[n]["send_id"].ToString());
				}
																																if(dt.Rows[n]["recv_id"].ToString()!="")
				{
					model.recv_id=long.Parse(dt.Rows[n]["recv_id"].ToString());
				}
																																				model.content= dt.Rows[n]["content"].ToString();
																												if(dt.Rows[n]["type"].ToString()!="")
				{
					model.type=int.Parse(dt.Rows[n]["type"].ToString());
				}
																																if(dt.Rows[n]["status"].ToString()!="")
				{
					model.status=int.Parse(dt.Rows[n]["status"].ToString());
				}
																																if(dt.Rows[n]["send_time"].ToString()!="")
				{
					model.send_time=DateTime.Parse(dt.Rows[n]["send_time"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}
		
	    /// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            return dal.GetAllPageList(strWhere,orderby);
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            return dal.GetPageList(strWhere,orderby,pageIndex,pageSize,out recordCount);
        }
	
#endregion
   
	}
}