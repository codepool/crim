﻿using System; 
using System.Text;
using System.Collections.Generic; 
using System.Data;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.DAL;
using Team.Zyzy.Im.Business.vo;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.BLL{
	/// <summary>
	/// im_friend
	/// </summary>

	public partial class ImFriendBll
	{
   		     
		private readonly ImFriendDal dal =new ImFriendDal();
		private static readonly ImFriendBll _instance = new ImFriendBll();
		private ImFriendBll() { }
		static ImFriendBll() { }
		public static ImFriendBll Instance
        {
            get
            {
                return _instance;
            }
        }

		public void SyncFriends(long userId, List<FriendVO> friendVOs)
		{
			foreach (FriendVO item in friendVOs)
			{
				ImFriend oldFriend = GetImFriend(userId, item.id);
				if (oldFriend == null)
				{
					ImFriend friend = new ImFriend();
					friend.id = item.id;
					friend.user_id = userId;
					friend.friend_id = item.id;
					friend.friend_head_image = item.headImage;
					friend.friend_nick_name = item.nickName;
					dal.Add(friend);
				}
                else
                {
					if(oldFriend.friend_nick_name!=item.nickName || oldFriend.friend_head_image != item.headImage)
                    {
						oldFriend.user_id = userId;
						oldFriend.friend_id = item.id;
						oldFriend.friend_head_image = item.headImage;
						oldFriend.friend_nick_name = item.nickName;
						dal.Update(oldFriend);
                    }
                }
			}
		}

		public ImFriend GetImFriend(long userId, long friendId)
		{
			string sqlwhere = string.Format(" user_id={0} and friend_id={1} ", userId,friendId);
			List<ImFriend> friend = GetModelList(sqlwhere);
			if (friend == null || friend.Count == 0)
			{
				return null;
			}
			else
			{
				return friend[0];
			}
		}


		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long id)
		{
			return dal.Exists(id);
		}

			/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImFriend GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImFriend> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImFriend> DataTableToList(DataTable dt)
		{
			List<ImFriend> modelList = new List<ImFriend>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				ImFriend model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new ImFriend();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=long.Parse(dt.Rows[n]["id"].ToString());
				}
																																if(dt.Rows[n]["user_id"].ToString()!="")
				{
					model.user_id=long.Parse(dt.Rows[n]["user_id"].ToString());
				}
																																if(dt.Rows[n]["friend_id"].ToString()!="")
				{
					model.friend_id=long.Parse(dt.Rows[n]["friend_id"].ToString());
				}
																																				model.friend_nick_name= dt.Rows[n]["friend_nick_name"].ToString();
																																model.friend_head_image= dt.Rows[n]["friend_head_image"].ToString();
																												if(dt.Rows[n]["created_time"].ToString()!="")
				{
					model.created_time=DateTime.Parse(dt.Rows[n]["created_time"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}
		
	    /// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            return dal.GetAllPageList(strWhere,orderby);
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            return dal.GetPageList(strWhere,orderby,pageIndex,pageSize,out recordCount);
        }
        

#endregion
   
	}
}