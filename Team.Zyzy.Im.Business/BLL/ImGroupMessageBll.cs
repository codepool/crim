﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.DAL;
using Team.Zyzy.Im.Business.entity;

namespace Team.Zyzy.Im.Business.BLL
{
	//ImGroupMessage
	public partial class ImGroupMessageBll
	{
   		     
		private readonly ImGroupMessageDal dal=new ImGroupMessageDal();
		private static readonly ImGroupMessageBll _instance = new ImGroupMessageBll();
		private ImGroupMessageBll() { }
		static ImGroupMessageBll() { }
		public static ImGroupMessageBll Instance
        {
            get
            {
                return _instance;
            }
        }

		public long Add(GroupMessage input)
		{
			ImGroupMessage temp = new ImGroupMessage();
			temp.id = input.id;
			temp.group_id = input.groupId;
			temp.send_id = input.sendId;
			temp.send_nick_name = input.sendNickName;
			temp.content = input.content;
			temp.at_user_ids = input.getAtUserIds();
			temp.type = input.type;
			temp.status = input.status;
			temp.send_time = input.getSendTime();
			long id= ImHistoryMessageBll.Instance.AddOrUpdate(temp);
			if (id > 0)
			{
				dal.Add(temp);
			}
			return id;
		}

		public List<GroupMessage> GetRecentTopList(long groupId, int rowCount)
		{
			List<GroupMessage> messages = new List<GroupMessage>();
			string sqlWhere = string.Format(" (send_id={0} or group_id={0}) order by id desc limit {1} ", groupId, rowCount);
			List<ImGroupMessage> imPrivates = GetModelList(sqlWhere);
			if (imPrivates == null || imPrivates.Count == 0)
			{
				return messages;
			}
			foreach (ImGroupMessage item in imPrivates)
			{
				GroupMessage temp = new GroupMessage();
				temp.id = item.id;
				temp.sendId = item.send_id;
				temp.sendNickName = item.send_nick_name;
				temp.setAtUserIds(item.at_user_ids);
				temp.groupId = item.group_id;
				temp.content = item.content;
				temp.type = item.type;
				temp.status = item.status;
				temp.setSendTime(item.send_time);
				messages.Add(temp);
			}
			return messages;
		}

		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long id)
		{
			return dal.Exists(id);
		}

			/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImGroupMessage GetModel(long id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImGroupMessage> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImGroupMessage> DataTableToList(DataTable dt)
		{
			List<ImGroupMessage> modelList = new List<ImGroupMessage>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				ImGroupMessage model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new ImGroupMessage();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=long.Parse(dt.Rows[n]["id"].ToString());
				}
																																if(dt.Rows[n]["group_id"].ToString()!="")
				{
					model.group_id=long.Parse(dt.Rows[n]["group_id"].ToString());
				}
																																if(dt.Rows[n]["send_id"].ToString()!="")
				{
					model.send_id=long.Parse(dt.Rows[n]["send_id"].ToString());
				}
																																				model.send_nick_name= dt.Rows[n]["send_nick_name"].ToString();
																																model.content= dt.Rows[n]["content"].ToString();
																																model.at_user_ids= dt.Rows[n]["at_user_ids"].ToString();
																												if(dt.Rows[n]["type"].ToString()!="")
				{
					model.type=int.Parse(dt.Rows[n]["type"].ToString());
				}
																																if(dt.Rows[n]["status"].ToString()!="")
				{
					model.status=int.Parse(dt.Rows[n]["status"].ToString());
				}
																																if(dt.Rows[n]["send_time"].ToString()!="")
				{
					model.send_time=DateTime.Parse(dt.Rows[n]["send_time"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}
		
	    /// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            return dal.GetAllPageList(strWhere,orderby);
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            return dal.GetPageList(strWhere,orderby,pageIndex,pageSize,out recordCount);
        }
        

#endregion
   
	}
}