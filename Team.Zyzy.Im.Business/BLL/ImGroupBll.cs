﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.DAL;
using Team.Zyzy.Im.Business.vo;

namespace Team.Zyzy.Im.Business.BLL
{
	//ImGroup
	public partial class ImGroupBll
	{
   		     
		private readonly ImGroupDal dal=new ImGroupDal();
		private static readonly ImGroupBll _instance = new ImGroupBll();
		private ImGroupBll() { }
		static ImGroupBll() { }
		public static ImGroupBll Instance
        {
            get
            {
                return _instance;
            }
        }

		public void SyncGroups(List<GroupVO> groupVOs)
		{
			foreach (GroupVO item in groupVOs)
			{
				ImGroup oldGroup= GetImGroup(item.id);
				ImGroup group = new ImGroup();
				group.id = item.id;
				group.name = item.name;
				group.owner_id = item.ownerId;
				group.head_image = item.headImage;
				group.head_image_thumb = item.headImageThumb;
				group.notice = item.notice;
				group.remark = item.remark;
				group.created_time = DateTime.Now;
				if (oldGroup == null)
				{
					dal.Add(group);
				}
				else
				{
					if (oldGroup.name != item.name || oldGroup.head_image_thumb != item.headImageThumb || oldGroup.owner_id != item.ownerId)
					{
						group.id = oldGroup.id;
						dal.Update(group);
					}
				}
			}

		}

		public ImGroup GetImGroup(long id)
		{
			string sqlwhere = string.Format(" id={0} ", id);
			List<ImGroup> group = GetModelList(sqlwhere);
			if (group == null || group.Count == 0)
			{
				return null;
			}
			else
			{
				return group[0];
			}
		}

		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long id)
		{
			return dal.Exists(id);
		}

			/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImGroup GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImGroup> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImGroup> DataTableToList(DataTable dt)
		{
			List<ImGroup> modelList = new List<ImGroup>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				ImGroup model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new ImGroup();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=long.Parse(dt.Rows[n]["id"].ToString());
				}
																																				model.name= dt.Rows[n]["name"].ToString();
																												if(dt.Rows[n]["owner_id"].ToString()!="")
				{
					model.owner_id=long.Parse(dt.Rows[n]["owner_id"].ToString());
				}
																																				model.head_image= dt.Rows[n]["head_image"].ToString();
																																model.head_image_thumb= dt.Rows[n]["head_image_thumb"].ToString();
																																model.notice= dt.Rows[n]["notice"].ToString();
																																model.remark= dt.Rows[n]["remark"].ToString();
																												if(dt.Rows[n]["deleted"].ToString()!="")
				{
					model.deleted=int.Parse(dt.Rows[n]["deleted"].ToString());
				}
																																if(dt.Rows[n]["created_time"].ToString()!="")
				{
					model.created_time=DateTime.Parse(dt.Rows[n]["created_time"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}
		
	    /// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            return dal.GetAllPageList(strWhere,orderby);
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            return dal.GetPageList(strWhere,orderby,pageIndex,pageSize,out recordCount);
        }
	
#endregion
   
	}
}