﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Team.Zyzy.Im.Business.Model;
using Team.Zyzy.Im.Business.DAL;

namespace Team.Zyzy.Im.Business.BLL
{
	//ImUser
	public partial class ImUserBll
	{
   		     
		private readonly ImUserDal dal=new ImUserDal();
		private static readonly ImUserBll _instance = new ImUserBll();
		private ImUserBll() { }
		static ImUserBll() { }
		public static ImUserBll Instance
        {
            get
            {
                return _instance;
            }
        }
        
		#region  Method
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(long id)
		{
			return dal.Exists(id);
		}

			/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public ImUser GetModel(int id)
		{
			
			return dal.GetModel(id);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImUser> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<ImUser> DataTableToList(DataTable dt)
		{
			List<ImUser> modelList = new List<ImUser>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				ImUser model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = new ImUser();					
													if(dt.Rows[n]["id"].ToString()!="")
				{
					model.id=long.Parse(dt.Rows[n]["id"].ToString());
				}
																																				model.user_name= dt.Rows[n]["user_name"].ToString();
																																model.nick_name= dt.Rows[n]["nick_name"].ToString();
																																model.head_image= dt.Rows[n]["head_image"].ToString();
																																model.head_image_thumb= dt.Rows[n]["head_image_thumb"].ToString();
																																model.password= dt.Rows[n]["password"].ToString();
																												if(dt.Rows[n]["sex"].ToString()!="")
				{
					model.sex=int.Parse(dt.Rows[n]["sex"].ToString());
				}
																																if(dt.Rows[n]["type"].ToString()!="")
				{
					model.type=int.Parse(dt.Rows[n]["type"].ToString());
				}
																																				model.signature= dt.Rows[n]["signature"].ToString();
																												if(dt.Rows[n]["last_login_time"].ToString()!="")
				{
					model.last_login_time=DateTime.Parse(dt.Rows[n]["last_login_time"].ToString());
				}
																																if(dt.Rows[n]["created_time"].ToString()!="")
				{
					model.created_time=DateTime.Parse(dt.Rows[n]["created_time"].ToString());
				}
																										
				
					modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}
		
	    /// <summary>
		/// 获得导出全部数据   
		/// </summary>
		public DataSet GetAllPageList(string strWhere,string orderby)
        {
            return dal.GetAllPageList(strWhere,orderby);
        }
		
		/// <summary>
		/// 获得翻页数据   
		/// </summary>
		public DataSet GetPageList(string strWhere,string orderby, int pageIndex, int pageSize, out int recordCount)
        {
            return dal.GetPageList(strWhere,orderby,pageIndex,pageSize,out recordCount);
        }
        

#endregion
   
	}
}