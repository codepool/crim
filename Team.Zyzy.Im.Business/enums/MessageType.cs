﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Team.Zyzy.Im.Business.enums
{
    public enum MessageType
    {
        [Description("文字")]
        TEXT = 0,
        [Description("图片")]
        IMAGE = 1,
        [Description("文件")]
        FILE = 2,
        [Description("音频")]
        AUDIO = 3,
        [Description("视频")]
        VIDEO = 4,
        [Description("撤回")]
        RECALL = 10,
        [Description("已读")]
        READED = 11,

        [Description("呼叫")]
        RTC_CALL = 101,
        [Description("接受")]
        RTC_ACCEPT = 102,
        [Description("拒绝")]
        RTC_REJECT = 103,
        [Description("取消呼叫")]
        RTC_CANCEL = 104,
        [Description("呼叫失败")]
        RTC_FAILED = 105,
        [Description("挂断")]
        RTC_HANDUP = 106,
        [Description("同步candidate")]
        RTC_CANDIDATE = 107,
    }
}
