﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Team.Zyzy.Im.Business.enums
{
    public enum MessageSourceType
    {
        [Description("好友")]
        Friend = 0,
        [Description("群组")]
        Group = 1,
        [Description("系统")]
        System = 2,
    }
}
