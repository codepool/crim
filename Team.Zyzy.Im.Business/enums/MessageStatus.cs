﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Team.Zyzy.Im.Business.enums
{
    public enum MessageStatus
    {
        [Description("发送中")]
        Sending = -1,
        [Description("发送失败")]
        UnSend = 0,
        [Description("未读")]
        Send = 1,
        [Description("撤回")]
        Recall = 2,
        [Description("已读")]
        Read = 3,
    }
}
