﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Team.Zyzy.Im.Business.dto
{
    public class ModifyPwdDTO
    {
        public String oldPassword;
        public String newPassword;
    }
}
